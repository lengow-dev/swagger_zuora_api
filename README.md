# Swagger Zuora API

## Download swagger codegen

Download the appropriate version of swagger codegen 2.2.2 from this Maven repository
https://oss.sonatype.org/content/repositories/releases/io/swagger/swagger-codegen-cli/

## Usage

```bash
$ java -jar swagger-codegen-2.2.2.jar generate -l php -i swagger.yaml -o .
```