<?php
/**
 * POSTAccountType
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swaagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Zuora API Reference
 *
 * # Introduction Welcome to the reference for the Zuora REST API!  <a href=\"http://en.wikipedia.org/wiki/REST_API\" target=\"_blank\">REST</a> is a web-service protocol that lends itself to rapid development by using everyday HTTP and JSON technology.  The Zuora REST API provides a broad set of operations and resources that:    * Enable Web Storefront integration from your website.   * Support self-service subscriber sign-ups and account management.   * Process revenue schedules through custom revenue rule models.   * Enable manipulation of most objects in the Zuora Object Model.      ## Endpoints      The Zuora REST API is provided via the following endpoints.   | Tenant              | Base URL for REST Endpoints |   |-------------------------|-------------------------|   |Production | https://rest.zuora.com/v1   |   |API Sandbox    | https://rest.apisandbox.zuora.com/v1|      The production endpoint provides access to your live user data. The API Sandbox tenant is a good place to test your code without affecting real-world data. To use it, you must be provisioned with an API Sandbox tenant - your Zuora representative can help you if needed.      ## Access to the API      If you have a Zuora tenant, you already have access to the API.      If you don't have a Zuora tenant, go to <a href=\" https://www.zuora.com/resource/zuora-test-drive\" target=\"_blank\">https://www.zuora.com/resource/zuora-test-drive</a> and sign up for a Production Test Drive tenant. The tenant comes with seed data, such as a sample product catalog.  We recommend that you <a href=\"https://knowledgecenter.zuora.com/CF_Users_and_Administrators/A_Administrator_Settings/Manage_Users/Create_an_API_User\" target=\"_blank\">create an API user</a> specifically for making API calls. Don't log in to the Zuora UI with this account. Logging in to the UI enables a security feature that periodically expires the account's password, which may eventually cause authentication failures with the API. Note that a user role does not have write access to Zuora REST services unless it has the API Write Access permission as described in those instructions.  # Authentication  There are three ways to authenticate:    * Use username and password. Include authentication with each request in the header:         * `apiAccessKeyId`      * `apiSecretAccessKey`     * `entityId` or `entityName` (Only for [Zuora Multi-entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity \"Multi-entity\"). See \"Entity Id and Entity Name\" below for more information.)   * Use an authorization cookie. The cookie authorizes the user to make calls to the REST API for the duration specified in  **Administration > Security Policies > Session timeout**. The cookie expiration time is reset with this duration after every call to the REST API. To obtain a cookie, call the REST  `connections` resource with the following API user information:         *   ID         *   password     *   entity Id or entity name (Only for [Zuora Multi-entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity \"Multi-entity\"). See \"Entity Id and Entity Name\" below for more information.)         * For CORS-enabled APIs only: Include a 'single-use' token in the request header, which re-authenticates the user with each request. See below for more details.  ## Entity Id and Entity Name  The `entityId` and `entityName` parameters are only used for [Zuora Multi-entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity \"Zuora Multi-entity\").   The  `entityId` and `entityName` parameters specify the Id and the [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name \"Introduction to Entity and Entity Hierarchy\") that you want to access, respectively. Note that you must have permission to access the entity.   You can specify either the `entityId` or `entityName` parameter in the authentication to access and view an entity.    * If both `entityId` and `entityName` are specified in the authentication, an error occurs.    * If neither `entityId` nor `entityName` is specified in the authentication, you will log in to the entity in which your user account is created.      To get the entity Id and entity name, you can use the GET Entities REST call. For more information, see [API User Authentication](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/A_Overview_of_Multi-entity#API_User_Authentication \"API User Authentication\").      ## Token Authentication for CORS-Enabled APIs      The CORS mechanism enables REST API calls to Zuora to be made directly from your customer's browser, with all credit card and security information transmitted directly to Zuora. This minimizes your PCI compliance burden, allows you to implement advanced validation on your payment forms, and  makes your payment forms look just like any other part of your website.    For security reasons, instead of using cookies, an API request via CORS uses **tokens** for authentication.  The token method of authentication is only designed for use with requests that must originate from your customer's browser; **it should  not be considered a replacement to the existing cookie authentication** mechanism.  See [Zuora CORS REST](https://knowledgecenter.zuora.com/DC_Developers/REST_API/A_REST_basics/G_CORS_REST \"Zuora CORS REST\") for details on how CORS works and how you can begin to implement customer calls to the Zuora REST APIs. See  [HMAC Signatures](https://www.zuora.com/developer/API-Reference/#operation/POSTHMACSignature \"HMAC Signatures\") for details on the HMAC method that returns the authentication token.  # Requests and Responses  ## Request IDs  As a general rule, when asked to supply a \"key\" for an account or subscription (accountKey, account-key, subscriptionKey, subscription-key), you can provide either the actual ID or  the number of the entity.  ## HTTP Request Body  Most of the parameters and data accompanying your requests will be contained in the body of the HTTP request.   The Zuora REST API accepts JSON in the HTTP request body. No other data format (e.g., XML) is supported.  ## Testing a Request  Use a third party client, such as [curl](https://curl.haxx.se \"curl\"), [Postman](https://www.getpostman.com \"Postman\"), or [Advanced REST Client](https://advancedrestclient.com \"Advanced REST Client\"), to test the Zuora REST API.  You can test the Zuora REST API from the Zuora API Sandbox or Production tenants. If connecting to Production, bear in mind that you are working with your live production data, not sample data or test data.  ## Testing with Credit Cards  Sooner or later it will probably be necessary to test some transactions that involve credit cards. For suggestions on how to handle this, see [Going Live With Your Payment Gateway](https://knowledgecenter.zuora.com/CB_Billing/M_Payment_Gateways/C_Managing_Payment_Gateways/B_Going_Live_Payment_Gateways#Testing_with_Credit_Cards \"C_Zuora_User_Guides/A_Billing_and_Payments/M_Payment_Gateways/C_Managing_Payment_Gateways/B_Going_Live_Payment_Gateways#Testing_with_Credit_Cards\" ).  ## Error Handling  Responses and error codes are detailed in [Responses and errors](https://knowledgecenter.zuora.com/DC_Developers/REST_API/A_REST_basics/3_Responses_and_errors \"Responses and errors\").  # Pagination  When retrieving information (using GET methods), the optional `pageSize` query parameter sets the maximum number of rows to return in a response. The maximum is `40`; larger values are treated as `40`. If this value is empty or invalid, `pageSize` typically defaults to `10`.  The default value for the maximum number of rows retrieved can be overridden at the method level.  If more rows are available, the response will include a `nextPage` element, which contains a URL for requesting the next page.  If this value is not provided, no more rows are available. No \"previous page\" element is explicitly provided; to support backward paging, use the previous call.  ## Array Size  For data items that are not paginated, the REST API supports arrays of up to 300 rows.  Thus, for instance, repeated pagination can retrieve thousands of customer accounts, but within any account an array of no more than 300 rate plans is returned.  # API Versions  The Zuora REST API are version controlled. Versioning ensures that Zuora REST API changes are backward compatible. Zuora uses a major and minor version nomenclature to manage changes. By specifying a version in a REST request, you can get expected responses regardless of future changes to the API.  ## Major Version  The major version number of the REST API appears in the REST URL. Currently, Zuora only supports the **v1** major version. For example, `POST https://rest.zuora.com/v1/subscriptions`.  ## Minor Version  Zuora uses minor versions for the REST API to control small changes. For example, a field in a REST method is deprecated and a new field is used to replace it.   Some fields in the REST methods are supported as of minor versions. If a field is not noted with a minor version, this field is available for all minor versions. If a field is noted with a minor version, this field is in version control. You must specify the supported minor version in the request header to process without an error.   If a field is in version control, it is either with a minimum minor version or a maximum minor version, or both of them. You can only use this field with the minor version between the minimum and the maximum minor versions. For example, the `invoiceCollect` field in the POST Subscription method is in version control and its maximum minor version is 189.0. You can only use this field with the minor version 189.0 or earlier.  If you specify a version number in the request header that is not supported, Zuora will use the minimum minor version of the REST API. In our REST API documentation, if a field or feature requires a minor version number, we note that in the field description.  You only need to specify the version number when you use the fields require a minor version. To specify the minor version, set the `zuora-version` parameter to the minor version number in the request header for the request call. For example, the `collect` field is in 196.0 minor version. If you want to use this field for the POST Subscription method, set the  `zuora-version` parameter to `196.0` in the request header. The `zuora-version` parameter is case sensitive.  For all the REST API fields, by default, if the minor version is not specified in the request header, Zuora will use the minimum minor version of the REST API to avoid breaking your integration.   ### Minor Version History  The supported minor versions are not serial. This section documents the changes made to each Zuora REST API minor version.  The following table lists the supported versions and the fields that have a Zuora REST API minor version.  | Fields         | Minor Version      | REST Methods    | Description | |:--------|:--------|:--------|:--------| | invoiceCollect | 189.0 and earlier  | [Create Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_Subscription \"Create Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\"); [Renew Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_RenewSubscription \"Renew Subscription\"); [Cancel Subscription](https://www.zuora.com/developer/api-reference/#operation/POSTSubscriptionCancellation \"Cancel Subscription\"); [Suspend Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionSuspend \"Suspend Subscription\"); [Resume Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionResume \"Resume Subscription\"); [Create Account](https://www.zuora.com/developer/API-Reference/#operation/POST_Account \"Create Account\")|Generates an invoice and collects a payment for a subscription. | | collect        | 196.0 and later    | [Create Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_Subscription \"Create Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\"); [Renew Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_RenewSubscription \"Renew Subscription\"); [Cancel Subscription](https://www.zuora.com/developer/api-reference/#operation/POSTSubscriptionCancellation \"Cancel Subscription\"); [Suspend Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionSuspend \"Suspend Subscription\"); [Resume Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionResume \"Resume Subscription\"); [Create Account](https://www.zuora.com/developer/API-Reference/#operation/POST_Account \"Create Account\")|Collects an automatic payment for a subscription. | | invoice | 196.0 and later| [Create Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_Subscription \"Create Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\"); [Renew Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_RenewSubscription \"Renew Subscription\"); [Cancel Subscription](https://www.zuora.com/developer/api-reference/#operation/POSTSubscriptionCancellation \"Cancel Subscription\"); [Suspend Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionSuspend \"Suspend Subscription\"); [Resume Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionResume \"Resume Subscription\"); [Create Account](https://www.zuora.com/developer/API-Reference/#operation/POST_Account \"Create Account\")|Generates an invoice for a subscription. | | invoiceTargetDate | 196.0 and earlier  | [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\") |Date through which charges are calculated on the invoice, as `yyyy-mm-dd`. | | targetDate | 207.0 and later | [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\") |Date through which charges are calculated on the invoice, as `yyyy-mm-dd`. | | includeExisting DraftInvoiceItems | 196.0 and earlier| [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") | Specifies whether to include draft invoice items in subscription previews. Specify it to be `true` (default) to include draft invoice items in the preview result. Specify it to be `false` to excludes draft invoice items in the preview result. | | includeExisting DraftDocItems | 207.0 and later  | [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") | Specifies whether to include draft invoice items in subscription previews. Specify it to be `true` (default) to include draft invoice items in the preview result. Specify it to be `false` to excludes draft invoice items in the preview result. | | previewType | 196.0 and earlier| [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") | The type of preview you will receive. The possible values are `InvoiceItem`(default), `ChargeMetrics`, and `InvoiceItemChargeMetrics`. | | previewType | 207.0 and later  | [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") | The type of preview you will receive. The possible values are `LegalDoc`(default), `ChargeMetrics`, and `LegalDocChargeMetrics`. |  #### Version 207.0 and Later  The response structure of the [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\") and [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") methods are changed. The following invoice related response fields are moved to the invoice container:    * amount   * amountWithoutTax   * taxAmount   * invoiceItems   * targetDate   * chargeMetrics  # Zuora Object Model  The following diagram presents a high-level view of the key Zuora objects. Click the image to open it in a new tab to resize it.  <a href=\"https://www.zuora.com/wp-content/uploads/2017/01/ZuoraERD.jpeg\" target=\"_blank\"><img src=\"https://www.zuora.com/wp-content/uploads/2017/01/ZuoraERD.jpeg\" alt=\"Zuora Object Model Diagram\"></a>
 *
 * OpenAPI spec version: 2017-04-14
 * Contact: docs@zuora.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;

/**
 * POSTAccountType Class Doc Comment
 *
 * @category    Class
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class POSTAccountType implements ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'POSTAccountType';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = [
        'account_number' => 'string',
        'additional_email_addresses' => 'string[]',
        'apply_credit_balance' => 'bool',
        'auto_pay' => 'bool',
        'batch' => 'string',
        'bill_cycle_day' => 'int',
        'bill_to_contact' => '\Swagger\Client\Model\POSTAccountTypeBillToContact',
        'collect' => 'string',
        'communication_profile_id' => 'string',
        'credit_card' => '\Swagger\Client\Model\POSTAccountTypeCreditCard',
        'crm_id' => 'string',
        'currency' => 'string',
        'custom_field__c' => 'string',
        'hpm_credit_card_payment_method_id' => 'string',
        'invoice' => 'string',
        'invoice_collect' => 'bool',
        'invoice_delivery_prefs_email' => 'bool',
        'invoice_delivery_prefs_print' => 'bool',
        'invoice_target_date' => '\DateTime',
        'invoice_template_id' => 'string',
        'i_dclient__c' => 'string',
        'sire_nfacturation__c' => 'string',
        'codesage__c' => 'string',
        'engagement__c' => '\DateTime',
        'legacyid__c' => 'string',
        'typeversion__c' => 'string',
        'analytique__c' => 'string',
        'compteclient__c' => 'string',
        'name' => 'string',
        'notes' => 'string',
        'payment_gateway' => 'string',
        'payment_term' => 'string',
        'sold_to_contact' => '\Swagger\Client\Model\POSTAccountTypeSoldToContact',
        'subscription' => '\Swagger\Client\Model\POSTAccountTypeSubscription',
        'tagging' => 'string',
        'tax_info' => '\Swagger\Client\Model\POSTAccountTypeTaxInfo'
    ];

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = [
        'account_number' => 'accountNumber',
        'additional_email_addresses' => 'additionalEmailAddresses',
        'apply_credit_balance' => 'applyCreditBalance',
        'auto_pay' => 'autoPay',
        'batch' => 'batch',
        'bill_cycle_day' => 'billCycleDay',
        'bill_to_contact' => 'billToContact',
        'collect' => 'collect',
        'communication_profile_id' => 'communicationProfileId',
        'credit_card' => 'creditCard',
        'crm_id' => 'crmId',
        'currency' => 'currency',
        'custom_field__c' => 'customField__c',
        'hpm_credit_card_payment_method_id' => 'hpmCreditCardPaymentMethodId',
        'invoice' => 'invoice',
        'invoice_collect' => 'invoiceCollect',
        'invoice_delivery_prefs_email' => 'invoiceDeliveryPrefsEmail',
        'invoice_delivery_prefs_print' => 'invoiceDeliveryPrefsPrint',
        'invoice_target_date' => 'invoiceTargetDate',
        'invoice_template_id' => 'invoiceTemplateId',
        'i_dclient__c' => 'IDclient__c',
        'sire_nfacturation__c' => 'SIRENfacturation__c',
        'codesage__c' => 'Codesage__c',
        'engagement__c' => 'Engagement__c',
        'legacyid__c' => 'Legacyid__c',
        'typeversion__c' => 'Typeversion__c',
        'analytique__c' => 'Analytique__c',
        'compteclient__c' => 'Compteclient__c',
        'name' => 'name',
        'notes' => 'notes',
        'payment_gateway' => 'paymentGateway',
        'payment_term' => 'paymentTerm',
        'sold_to_contact' => 'soldToContact',
        'subscription' => 'subscription',
        'tagging' => 'tagging',
        'tax_info' => 'taxInfo'
    ];


    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = [
        'account_number' => 'setAccountNumber',
        'additional_email_addresses' => 'setAdditionalEmailAddresses',
        'apply_credit_balance' => 'setApplyCreditBalance',
        'auto_pay' => 'setAutoPay',
        'batch' => 'setBatch',
        'bill_cycle_day' => 'setBillCycleDay',
        'bill_to_contact' => 'setBillToContact',
        'collect' => 'setCollect',
        'communication_profile_id' => 'setCommunicationProfileId',
        'credit_card' => 'setCreditCard',
        'crm_id' => 'setCrmId',
        'currency' => 'setCurrency',
        'custom_field__c' => 'setCustomFieldC',
        'hpm_credit_card_payment_method_id' => 'setHpmCreditCardPaymentMethodId',
        'invoice' => 'setInvoice',
        'invoice_collect' => 'setInvoiceCollect',
        'invoice_delivery_prefs_email' => 'setInvoiceDeliveryPrefsEmail',
        'invoice_delivery_prefs_print' => 'setInvoiceDeliveryPrefsPrint',
        'invoice_target_date' => 'setInvoiceTargetDate',
        'invoice_template_id' => 'setInvoiceTemplateId',
        'i_dclient__c' => 'setIDclientC',
        'sire_nfacturation__c' => 'setSireNfacturationC',
        'codesage__c' => 'setCodesageC',
        'engagement__c' => 'setEngagementC',
        'legacyid__c' => 'setLegacyidC',
        'typeversion__c' => 'setTypeversionC',
        'analytique__c' => 'setAnalytiqueC',
        'compteclient__c' => 'setCompteclientC',
        'name' => 'setName',
        'notes' => 'setNotes',
        'payment_gateway' => 'setPaymentGateway',
        'payment_term' => 'setPaymentTerm',
        'sold_to_contact' => 'setSoldToContact',
        'subscription' => 'setSubscription',
        'tagging' => 'setTagging',
        'tax_info' => 'setTaxInfo'
    ];


    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = [
        'account_number' => 'getAccountNumber',
        'additional_email_addresses' => 'getAdditionalEmailAddresses',
        'apply_credit_balance' => 'getApplyCreditBalance',
        'auto_pay' => 'getAutoPay',
        'batch' => 'getBatch',
        'bill_cycle_day' => 'getBillCycleDay',
        'bill_to_contact' => 'getBillToContact',
        'collect' => 'getCollect',
        'communication_profile_id' => 'getCommunicationProfileId',
        'credit_card' => 'getCreditCard',
        'crm_id' => 'getCrmId',
        'currency' => 'getCurrency',
        'custom_field__c' => 'getCustomFieldC',
        'hpm_credit_card_payment_method_id' => 'getHpmCreditCardPaymentMethodId',
        'invoice' => 'getInvoice',
        'invoice_collect' => 'getInvoiceCollect',
        'invoice_delivery_prefs_email' => 'getInvoiceDeliveryPrefsEmail',
        'invoice_delivery_prefs_print' => 'getInvoiceDeliveryPrefsPrint',
        'invoice_target_date' => 'getInvoiceTargetDate',
        'invoice_template_id' => 'getInvoiceTemplateId',
        'i_dclient__c' => 'getIDclientC',
        'sire_nfacturation__c' => 'getSireNfacturationC',
        'codesage__c' => 'getCodesageC',
        'engagement__c' => 'getEngagementC',
        'legacyid__c' => 'getLegacyidC',
        'typeversion__c' => 'getTypeversionC',
        'analytique__c' => 'getAnalytiqueC',
        'compteclient__c' => 'getCompteclientC',
        'name' => 'getName',
        'notes' => 'getNotes',
        'payment_gateway' => 'getPaymentGateway',
        'payment_term' => 'getPaymentTerm',
        'sold_to_contact' => 'getSoldToContact',
        'subscription' => 'getSubscription',
        'tagging' => 'getTagging',
        'tax_info' => 'getTaxInfo'
    ];

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    public static function setters()
    {
        return self::$setters;
    }

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['account_number'] = isset($data['account_number']) ? $data['account_number'] : null;
        $this->container['additional_email_addresses'] = isset($data['additional_email_addresses']) ? $data['additional_email_addresses'] : null;
        $this->container['apply_credit_balance'] = isset($data['apply_credit_balance']) ? $data['apply_credit_balance'] : null;
        $this->container['auto_pay'] = isset($data['auto_pay']) ? $data['auto_pay'] : null;
        $this->container['batch'] = isset($data['batch']) ? $data['batch'] : null;
        $this->container['bill_cycle_day'] = isset($data['bill_cycle_day']) ? $data['bill_cycle_day'] : null;
        $this->container['bill_to_contact'] = isset($data['bill_to_contact']) ? $data['bill_to_contact'] : null;
        $this->container['collect'] = isset($data['collect']) ? $data['collect'] : null;
        $this->container['communication_profile_id'] = isset($data['communication_profile_id']) ? $data['communication_profile_id'] : null;
        $this->container['credit_card'] = isset($data['credit_card']) ? $data['credit_card'] : null;
        $this->container['crm_id'] = isset($data['crm_id']) ? $data['crm_id'] : null;
        $this->container['currency'] = isset($data['currency']) ? $data['currency'] : null;
        $this->container['custom_field__c'] = isset($data['custom_field__c']) ? $data['custom_field__c'] : null;
        $this->container['hpm_credit_card_payment_method_id'] = isset($data['hpm_credit_card_payment_method_id']) ? $data['hpm_credit_card_payment_method_id'] : null;
        $this->container['invoice'] = isset($data['invoice']) ? $data['invoice'] : null;
        $this->container['invoice_collect'] = isset($data['invoice_collect']) ? $data['invoice_collect'] : null;
        $this->container['invoice_delivery_prefs_email'] = isset($data['invoice_delivery_prefs_email']) ? $data['invoice_delivery_prefs_email'] : null;
        $this->container['invoice_delivery_prefs_print'] = isset($data['invoice_delivery_prefs_print']) ? $data['invoice_delivery_prefs_print'] : null;
        $this->container['invoice_target_date'] = isset($data['invoice_target_date']) ? $data['invoice_target_date'] : null;
        $this->container['invoice_template_id'] = isset($data['invoice_template_id']) ? $data['invoice_template_id'] : null;
        $this->container['i_dclient__c'] = isset($data['i_dclient__c']) ? $data['i_dclient__c'] : null;
        $this->container['sire_nfacturation__c'] = isset($data['sire_nfacturation__c']) ? $data['sire_nfacturation__c'] : null;
        $this->container['codesage__c'] = isset($data['codesage__c']) ? $data['codesage__c'] : null;
        $this->container['engagement__c'] = isset($data['engagement__c']) ? $data['engagement__c'] : null;
        $this->container['legacyid__c'] = isset($data['legacyid__c']) ? $data['legacyid__c'] : null;
        $this->container['typeversion__c'] = isset($data['typeversion__c']) ? $data['typeversion__c'] : null;
        $this->container['analytique__c'] = isset($data['analytique__c']) ? $data['analytique__c'] : null;
        $this->container['compteclient__c'] = isset($data['compteclient__c']) ? $data['compteclient__c'] : null;
        $this->container['name'] = isset($data['name']) ? $data['name'] : null;
        $this->container['notes'] = isset($data['notes']) ? $data['notes'] : null;
        $this->container['payment_gateway'] = isset($data['payment_gateway']) ? $data['payment_gateway'] : null;
        $this->container['payment_term'] = isset($data['payment_term']) ? $data['payment_term'] : null;
        $this->container['sold_to_contact'] = isset($data['sold_to_contact']) ? $data['sold_to_contact'] : null;
        $this->container['subscription'] = isset($data['subscription']) ? $data['subscription'] : null;
        $this->container['tagging'] = isset($data['tagging']) ? $data['tagging'] : null;
        $this->container['tax_info'] = isset($data['tax_info']) ? $data['tax_info'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];

        if ($this->container['currency'] === null) {
            $invalid_properties[] = "'currency' can't be null";
        }
        if ($this->container['name'] === null) {
            $invalid_properties[] = "'name' can't be null";
        }
        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        if ($this->container['currency'] === null) {
            return false;
        }
        if ($this->container['name'] === null) {
            return false;
        }
        return true;
    }


    /**
     * Gets account_number
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->container['account_number'];
    }

    /**
     * Sets account_number
     * @param string $account_number A unique account number, up to 50 characters that do not begin with the default account number prefix.  If no account number is specified, one is generated.
     * @return $this
     */
    public function setAccountNumber($account_number)
    {
        $this->container['account_number'] = $account_number;

        return $this;
    }

    /**
     * Gets additional_email_addresses
     * @return string[]
     */
    public function getAdditionalEmailAddresses()
    {
        return $this->container['additional_email_addresses'];
    }

    /**
     * Sets additional_email_addresses
     * @param string[] $additional_email_addresses A list of additional email addresses to receive emailed invoices. Use a comma to separate each email address.  **Note:** Invoices are emailed to the email addresses specified in this field only when the `invoiceDeliveryPrefsEmail` field is `true`.
     * @return $this
     */
    public function setAdditionalEmailAddresses($additional_email_addresses)
    {
        $this->container['additional_email_addresses'] = $additional_email_addresses;

        return $this;
    }

    /**
     * Gets apply_credit_balance
     * @return bool
     */
    public function getApplyCreditBalance()
    {
        return $this->container['apply_credit_balance'];
    }

    /**
     * Sets apply_credit_balance
     * @param bool $apply_credit_balance Applies a credit balance to an invoice.   If the value is `true`, the credit balance is applied to the invoice. If the value is `false`, no action is taken.  Prerequisite: `invoice` must be `true`.   **Note:** If you are using the field `invoiceCollect` rather than the field `invoice`, the `invoiceCollect` value must be `true`.  To view the credit balance adjustment, retrieve the details of the invoice using the Get Invoices method.
     * @return $this
     */
    public function setApplyCreditBalance($apply_credit_balance)
    {
        $this->container['apply_credit_balance'] = $apply_credit_balance;

        return $this;
    }

    /**
     * Gets auto_pay
     * @return bool
     */
    public function getAutoPay()
    {
        return $this->container['auto_pay'];
    }

    /**
     * Sets auto_pay
     * @param bool $auto_pay Specifies whether future payments are to be automatically billed when they are due. Possible values are: `true`, `false`.
     * @return $this
     */
    public function setAutoPay($auto_pay)
    {
        $this->container['auto_pay'] = $auto_pay;

        return $this;
    }

    /**
     * Gets batch
     * @return string
     */
    public function getBatch()
    {
        return $this->container['batch'];
    }

    /**
     * Sets batch
     * @param string $batch The alias name given to a batch. A string of 50 characters or less.
     * @return $this
     */
    public function setBatch($batch)
    {
        $this->container['batch'] = $batch;

        return $this;
    }

    /**
     * Gets bill_cycle_day
     * @return int
     */
    public function getBillCycleDay()
    {
        return $this->container['bill_cycle_day'];
    }

    /**
     * Sets bill_cycle_day
     * @param int $bill_cycle_day The account's bill cycle day (BCD), when bill runs generate invoices for the account.  Specify any day of the month (1-31, where 31 = end-of-month), or 0 for auto-set.  Required if no subscription will be created.   Optional if a subscription is created and defaults to the day-of-the-month of the subscription's `contractEffectiveDate`.
     * @return $this
     */
    public function setBillCycleDay($bill_cycle_day)
    {
        $this->container['bill_cycle_day'] = $bill_cycle_day;

        return $this;
    }

    /**
     * Gets bill_to_contact
     * @return \Swagger\Client\Model\POSTAccountTypeBillToContact
     */
    public function getBillToContact()
    {
        return $this->container['bill_to_contact'];
    }

    /**
     * Sets bill_to_contact
     * @param \Swagger\Client\Model\POSTAccountTypeBillToContact $bill_to_contact
     * @return $this
     */
    public function setBillToContact($bill_to_contact)
    {
        $this->container['bill_to_contact'] = $bill_to_contact;

        return $this;
    }

    /**
     * Gets collect
     * @return string
     */
    public function getCollect()
    {
        return $this->container['collect'];
    }

    /**
     * Sets collect
     * @param string $collect Collects an automatic payment for a subscription. The collection generated in this operation is only for this subscription, not for the entire customer account.  If the value is `true`, the automatic payment is collected. If the value is `false`, no action is taken.  The default value is `true`.  Prerequisite: invoice must be `true`.   **Note:** This field is in Zuora REST API version control. Supported minor versions are 196.0 or later. To use this field in the method, you must set the `zuora-version` parameter to the minor version number in the request header.
     * @return $this
     */
    public function setCollect($collect)
    {
        $this->container['collect'] = $collect;

        return $this;
    }

    /**
     * Gets communication_profile_id
     * @return string
     */
    public function getCommunicationProfileId()
    {
        return $this->container['communication_profile_id'];
    }

    /**
     * Sets communication_profile_id
     * @param string $communication_profile_id The ID of a communication profile.
     * @return $this
     */
    public function setCommunicationProfileId($communication_profile_id)
    {
        $this->container['communication_profile_id'] = $communication_profile_id;

        return $this;
    }

    /**
     * Gets credit_card
     * @return \Swagger\Client\Model\POSTAccountTypeCreditCard
     */
    public function getCreditCard()
    {
        return $this->container['credit_card'];
    }

    /**
     * Sets credit_card
     * @param \Swagger\Client\Model\POSTAccountTypeCreditCard $credit_card
     * @return $this
     */
    public function setCreditCard($credit_card)
    {
        $this->container['credit_card'] = $credit_card;

        return $this;
    }

    /**
     * Gets crm_id
     * @return string
     */
    public function getCrmId()
    {
        return $this->container['crm_id'];
    }

    /**
     * Sets crm_id
     * @param string $crm_id CRM account ID for the account, up to 100 characters.
     * @return $this
     */
    public function setCrmId($crm_id)
    {
        $this->container['crm_id'] = $crm_id;

        return $this;
    }

    /**
     * Gets currency
     * @return string
     */
    public function getCurrency()
    {
        return $this->container['currency'];
    }

    /**
     * Sets currency
     * @param string $currency A currency as defined in Billing Settings in the Zuora UI.
     * @return $this
     */
    public function setCurrency($currency)
    {
        $this->container['currency'] = $currency;

        return $this;
    }

    /**
     * Gets custom_field__c
     * @return string
     */
    public function getCustomFieldC()
    {
        return $this->container['custom_field__c'];
    }

    /**
     * Sets custom_field__c
     * @param string $custom_field__c Any custom fields defined for this object. The custom field name is case-sensitive.
     * @return $this
     */
    public function setCustomFieldC($custom_field__c)
    {
        $this->container['custom_field__c'] = $custom_field__c;

        return $this;
    }

    /**
     * Gets hpm_credit_card_payment_method_id
     * @return string
     */
    public function getHpmCreditCardPaymentMethodId()
    {
        return $this->container['hpm_credit_card_payment_method_id'];
    }

    /**
     * Sets hpm_credit_card_payment_method_id
     * @param string $hpm_credit_card_payment_method_id The ID of the HPM credit card payment method associated with this account. You must provide either this field or the `creditCard` structure, but not both.  **Note:** Non-credit card payment methods are not supported.
     * @return $this
     */
    public function setHpmCreditCardPaymentMethodId($hpm_credit_card_payment_method_id)
    {
        $this->container['hpm_credit_card_payment_method_id'] = $hpm_credit_card_payment_method_id;

        return $this;
    }

    /**
     * Gets invoice
     * @return string
     */
    public function getInvoice()
    {
        return $this->container['invoice'];
    }

    /**
     * Sets invoice
     * @param string $invoice Creates an invoice for a subscription. The invoice generated in this operation is only for this subscription, not for the entire customer account.  If the value is `true`, an invoice is created. If the value is `false`, no action is taken.  The default value is `true`.   **Note:** This field is in Zuora REST API version control. Supported minor versions are 196.0 or later. To use this field in the method, you must set the `zuora-version` parameter to the minor version number in the request header.
     * @return $this
     */
    public function setInvoice($invoice)
    {
        $this->container['invoice'] = $invoice;

        return $this;
    }

    /**
     * Gets invoice_collect
     * @return bool
     */
    public function getInvoiceCollect()
    {
        return $this->container['invoice_collect'];
    }

    /**
     * Sets invoice_collect
     * @param bool $invoice_collect **Note:** This field has been replaced by the `invoice` field and the `collect` field. `invoiceCollect` is available only for backward compatibility.  If `true` (default), and a subscription is created, an invoice is generated at account creation time and payment is immediately collected using the account's default payment method.  This field is in Zuora REST API version control. Supported minor versions are 186.0, 187.0, 188.0, 189.0, and 196.0.
     * @return $this
     */
    public function setInvoiceCollect($invoice_collect)
    {
        $this->container['invoice_collect'] = $invoice_collect;

        return $this;
    }

    /**
     * Gets invoice_delivery_prefs_email
     * @return bool
     */
    public function getInvoiceDeliveryPrefsEmail()
    {
        return $this->container['invoice_delivery_prefs_email'];
    }

    /**
     * Sets invoice_delivery_prefs_email
     * @param bool $invoice_delivery_prefs_email Whether the customer wants to receive invoices through email.   The default value is `false`.
     * @return $this
     */
    public function setInvoiceDeliveryPrefsEmail($invoice_delivery_prefs_email)
    {
        $this->container['invoice_delivery_prefs_email'] = $invoice_delivery_prefs_email;

        return $this;
    }

    /**
     * Gets invoice_delivery_prefs_print
     * @return bool
     */
    public function getInvoiceDeliveryPrefsPrint()
    {
        return $this->container['invoice_delivery_prefs_print'];
    }

    /**
     * Sets invoice_delivery_prefs_print
     * @param bool $invoice_delivery_prefs_print Whether the customer wants to receive printed invoices, such as through postal mail.  The default value is `false`.
     * @return $this
     */
    public function setInvoiceDeliveryPrefsPrint($invoice_delivery_prefs_print)
    {
        $this->container['invoice_delivery_prefs_print'] = $invoice_delivery_prefs_print;

        return $this;
    }

    /**
     * Gets invoice_target_date
     * @return \DateTime
     */
    public function getInvoiceTargetDate()
    {
        return $this->container['invoice_target_date'];
    }

    /**
     * Sets invoice_target_date
     * @param \DateTime $invoice_target_date If `invoiceCollect` is `true`, the target date for the invoice (that is, the date through which charges should be calculated). In `yyyy-mm-dd` format; defaults to the current date.
     * @return $this
     */
    public function setInvoiceTargetDate($invoice_target_date)
    {
        $this->container['invoice_target_date'] = $invoice_target_date;

        return $this;
    }

    /**
     * Gets invoice_template_id
     * @return string
     */
    public function getInvoiceTemplateId()
    {
        return $this->container['invoice_template_id'];
    }

    /**
     * Sets invoice_template_id
     * @param string $invoice_template_id Invoice template ID, configured in Billing Settings in the Zuora UI.
     * @return $this
     */
    public function setInvoiceTemplateId($invoice_template_id)
    {
        $this->container['invoice_template_id'] = $invoice_template_id;

        return $this;
    }

    /**
     * Gets i_dclient__c
     * @return string
     */
    public function getIDclientC()
    {
        return $this->container['i_dclient__c'];
    }

    /**
     * Sets i_dclient__c
     * @param string $i_dclient__c ID of the v2 account.
     * @return $this
     */
    public function setIDclientC($i_dclient__c)
    {
        $this->container['i_dclient__c'] = $i_dclient__c;

        return $this;
    }

    /**
     * Gets sire_nfacturation__c
     * @return string
     */
    public function getSireNfacturationC()
    {
        return $this->container['sire_nfacturation__c'];
    }

    /**
     * Sets sire_nfacturation__c
     * @param string $sire_nfacturation__c Registration number.
     * @return $this
     */
    public function setSireNfacturationC($sire_nfacturation__c)
    {
        $this->container['sire_nfacturation__c'] = $sire_nfacturation__c;

        return $this;
    }

    /**
     * Gets codesage__c
     * @return string
     */
    public function getCodesageC()
    {
        return $this->container['codesage__c'];
    }

    /**
     * Sets codesage__c
     * @param string $codesage__c SAGE code, use by the accounting team.
     * @return $this
     */
    public function setCodesageC($codesage__c)
    {
        $this->container['codesage__c'] = $codesage__c;

        return $this;
    }

    /**
     * Gets engagement__c
     * @return \DateTime
     */
    public function getEngagementC()
    {
        return $this->container['engagement__c'];
    }

    /**
     * Sets engagement__c
     * @param \DateTime $engagement__c Commitment date.
     * @return $this
     */
    public function setEngagementC($engagement__c)
    {
        $this->container['engagement__c'] = $engagement__c;

        return $this;
    }

    /**
     * Gets legacyid__c
     * @return string
     */
    public function getLegacyidC()
    {
        return $this->container['legacyid__c'];
    }

    /**
     * Sets legacyid__c
     * @param string $legacyid__c Legacy identifier.
     * @return $this
     */
    public function setLegacyidC($legacyid__c)
    {
        $this->container['legacyid__c'] = $legacyid__c;

        return $this;
    }

    /**
     * Gets typeversion__c
     * @return string
     */
    public function getTypeversionC()
    {
        return $this->container['typeversion__c'];
    }

    /**
     * Sets typeversion__c
     * @param string $typeversion__c Accoun type, V2 or V3.
     * @return $this
     */
    public function setTypeversionC($typeversion__c)
    {
        $this->container['typeversion__c'] = $typeversion__c;

        return $this;
    }

    /**
     * Gets analytique__c
     * @return string
     */
    public function getAnalytiqueC()
    {
        return $this->container['analytique__c'];
    }

    /**
     * Sets analytique__c
     * @param string $analytique__c Analytics code.
     * @return $this
     */
    public function setAnalytiqueC($analytique__c)
    {
        $this->container['analytique__c'] = $analytique__c;

        return $this;
    }

    /**
     * Gets compteclient__c
     * @return string
     */
    public function getCompteclientC()
    {
        return $this->container['compteclient__c'];
    }

    /**
     * Sets compteclient__c
     * @param string $compteclient__c Accounting code.
     * @return $this
     */
    public function setCompteclientC($compteclient__c)
    {
        $this->container['compteclient__c'] = $compteclient__c;

        return $this;
    }

    /**
     * Gets name
     * @return string
     */
    public function getName()
    {
        return $this->container['name'];
    }

    /**
     * Sets name
     * @param string $name Account name, up to 255 characters.
     * @return $this
     */
    public function setName($name)
    {
        $this->container['name'] = $name;

        return $this;
    }

    /**
     * Gets notes
     * @return string
     */
    public function getNotes()
    {
        return $this->container['notes'];
    }

    /**
     * Sets notes
     * @param string $notes A string of up to 65,535 characters.
     * @return $this
     */
    public function setNotes($notes)
    {
        $this->container['notes'] = $notes;

        return $this;
    }

    /**
     * Gets payment_gateway
     * @return string
     */
    public function getPaymentGateway()
    {
        return $this->container['payment_gateway'];
    }

    /**
     * Sets payment_gateway
     * @param string $payment_gateway The name of the payment gateway instance. If null or left unassigned, the Account will use the Default Gateway.
     * @return $this
     */
    public function setPaymentGateway($payment_gateway)
    {
        $this->container['payment_gateway'] = $payment_gateway;

        return $this;
    }

    /**
     * Gets payment_term
     * @return string
     */
    public function getPaymentTerm()
    {
        return $this->container['payment_term'];
    }

    /**
     * Sets payment_term
     * @param string $payment_term Payment terms for this account. Possible values are: `Due Upon Receipt`, `Net 30`, `Net 60`, `Net 90`.
     * @return $this
     */
    public function setPaymentTerm($payment_term)
    {
        $this->container['payment_term'] = $payment_term;

        return $this;
    }

    /**
     * Gets sold_to_contact
     * @return \Swagger\Client\Model\POSTAccountTypeSoldToContact
     */
    public function getSoldToContact()
    {
        return $this->container['sold_to_contact'];
    }

    /**
     * Sets sold_to_contact
     * @param \Swagger\Client\Model\POSTAccountTypeSoldToContact $sold_to_contact
     * @return $this
     */
    public function setSoldToContact($sold_to_contact)
    {
        $this->container['sold_to_contact'] = $sold_to_contact;

        return $this;
    }

    /**
     * Gets subscription
     * @return \Swagger\Client\Model\POSTAccountTypeSubscription
     */
    public function getSubscription()
    {
        return $this->container['subscription'];
    }

    /**
     * Sets subscription
     * @param \Swagger\Client\Model\POSTAccountTypeSubscription $subscription
     * @return $this
     */
    public function setSubscription($subscription)
    {
        $this->container['subscription'] = $subscription;

        return $this;
    }

    /**
     * Gets tagging
     * @return string
     */
    public function getTagging()
    {
        return $this->container['tagging'];
    }

    /**
     * Sets tagging
     * @param string $tagging 
     * @return $this
     */
    public function setTagging($tagging)
    {
        $this->container['tagging'] = $tagging;

        return $this;
    }

    /**
     * Gets tax_info
     * @return \Swagger\Client\Model\POSTAccountTypeTaxInfo
     */
    public function getTaxInfo()
    {
        return $this->container['tax_info'];
    }

    /**
     * Sets tax_info
     * @param \Swagger\Client\Model\POSTAccountTypeTaxInfo $tax_info
     * @return $this
     */
    public function setTaxInfo($tax_info)
    {
        $this->container['tax_info'] = $tax_info;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this));
    }
}


