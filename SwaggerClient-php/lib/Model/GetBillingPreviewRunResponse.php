<?php
/**
 * GetBillingPreviewRunResponse
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swaagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Zuora API Reference
 *
 * # Introduction Welcome to the reference for the Zuora REST API!  <a href=\"http://en.wikipedia.org/wiki/REST_API\" target=\"_blank\">REST</a> is a web-service protocol that lends itself to rapid development by using everyday HTTP and JSON technology.  The Zuora REST API provides a broad set of operations and resources that:    * Enable Web Storefront integration from your website.   * Support self-service subscriber sign-ups and account management.   * Process revenue schedules through custom revenue rule models.   * Enable manipulation of most objects in the Zuora Object Model.      ## Endpoints      The Zuora REST API is provided via the following endpoints.   | Tenant              | Base URL for REST Endpoints |   |-------------------------|-------------------------|   |Production | https://rest.zuora.com/v1   |   |API Sandbox    | https://rest.apisandbox.zuora.com/v1|      The production endpoint provides access to your live user data. The API Sandbox tenant is a good place to test your code without affecting real-world data. To use it, you must be provisioned with an API Sandbox tenant - your Zuora representative can help you if needed.      ## Access to the API      If you have a Zuora tenant, you already have access to the API.      If you don't have a Zuora tenant, go to <a href=\" https://www.zuora.com/resource/zuora-test-drive\" target=\"_blank\">https://www.zuora.com/resource/zuora-test-drive</a> and sign up for a Production Test Drive tenant. The tenant comes with seed data, such as a sample product catalog.  We recommend that you <a href=\"https://knowledgecenter.zuora.com/CF_Users_and_Administrators/A_Administrator_Settings/Manage_Users/Create_an_API_User\" target=\"_blank\">create an API user</a> specifically for making API calls. Don't log in to the Zuora UI with this account. Logging in to the UI enables a security feature that periodically expires the account's password, which may eventually cause authentication failures with the API. Note that a user role does not have write access to Zuora REST services unless it has the API Write Access permission as described in those instructions.  # Authentication  There are three ways to authenticate:    * Use username and password. Include authentication with each request in the header:         * `apiAccessKeyId`      * `apiSecretAccessKey`     * `entityId` or `entityName` (Only for [Zuora Multi-entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity \"Multi-entity\"). See \"Entity Id and Entity Name\" below for more information.)   * Use an authorization cookie. The cookie authorizes the user to make calls to the REST API for the duration specified in  **Administration > Security Policies > Session timeout**. The cookie expiration time is reset with this duration after every call to the REST API. To obtain a cookie, call the REST  `connections` resource with the following API user information:         *   ID         *   password     *   entity Id or entity name (Only for [Zuora Multi-entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity \"Multi-entity\"). See \"Entity Id and Entity Name\" below for more information.)         * For CORS-enabled APIs only: Include a 'single-use' token in the request header, which re-authenticates the user with each request. See below for more details.  ## Entity Id and Entity Name  The `entityId` and `entityName` parameters are only used for [Zuora Multi-entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity \"Zuora Multi-entity\").   The  `entityId` and `entityName` parameters specify the Id and the [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name \"Introduction to Entity and Entity Hierarchy\") that you want to access, respectively. Note that you must have permission to access the entity.   You can specify either the `entityId` or `entityName` parameter in the authentication to access and view an entity.    * If both `entityId` and `entityName` are specified in the authentication, an error occurs.    * If neither `entityId` nor `entityName` is specified in the authentication, you will log in to the entity in which your user account is created.      To get the entity Id and entity name, you can use the GET Entities REST call. For more information, see [API User Authentication](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/A_Overview_of_Multi-entity#API_User_Authentication \"API User Authentication\").      ## Token Authentication for CORS-Enabled APIs      The CORS mechanism enables REST API calls to Zuora to be made directly from your customer's browser, with all credit card and security information transmitted directly to Zuora. This minimizes your PCI compliance burden, allows you to implement advanced validation on your payment forms, and  makes your payment forms look just like any other part of your website.    For security reasons, instead of using cookies, an API request via CORS uses **tokens** for authentication.  The token method of authentication is only designed for use with requests that must originate from your customer's browser; **it should  not be considered a replacement to the existing cookie authentication** mechanism.  See [Zuora CORS REST](https://knowledgecenter.zuora.com/DC_Developers/REST_API/A_REST_basics/G_CORS_REST \"Zuora CORS REST\") for details on how CORS works and how you can begin to implement customer calls to the Zuora REST APIs. See  [HMAC Signatures](https://www.zuora.com/developer/API-Reference/#operation/POSTHMACSignature \"HMAC Signatures\") for details on the HMAC method that returns the authentication token.  # Requests and Responses  ## Request IDs  As a general rule, when asked to supply a \"key\" for an account or subscription (accountKey, account-key, subscriptionKey, subscription-key), you can provide either the actual ID or  the number of the entity.  ## HTTP Request Body  Most of the parameters and data accompanying your requests will be contained in the body of the HTTP request.   The Zuora REST API accepts JSON in the HTTP request body. No other data format (e.g., XML) is supported.  ## Testing a Request  Use a third party client, such as [curl](https://curl.haxx.se \"curl\"), [Postman](https://www.getpostman.com \"Postman\"), or [Advanced REST Client](https://advancedrestclient.com \"Advanced REST Client\"), to test the Zuora REST API.  You can test the Zuora REST API from the Zuora API Sandbox or Production tenants. If connecting to Production, bear in mind that you are working with your live production data, not sample data or test data.  ## Testing with Credit Cards  Sooner or later it will probably be necessary to test some transactions that involve credit cards. For suggestions on how to handle this, see [Going Live With Your Payment Gateway](https://knowledgecenter.zuora.com/CB_Billing/M_Payment_Gateways/C_Managing_Payment_Gateways/B_Going_Live_Payment_Gateways#Testing_with_Credit_Cards \"C_Zuora_User_Guides/A_Billing_and_Payments/M_Payment_Gateways/C_Managing_Payment_Gateways/B_Going_Live_Payment_Gateways#Testing_with_Credit_Cards\" ).  ## Error Handling  Responses and error codes are detailed in [Responses and errors](https://knowledgecenter.zuora.com/DC_Developers/REST_API/A_REST_basics/3_Responses_and_errors \"Responses and errors\").  # Pagination  When retrieving information (using GET methods), the optional `pageSize` query parameter sets the maximum number of rows to return in a response. The maximum is `40`; larger values are treated as `40`. If this value is empty or invalid, `pageSize` typically defaults to `10`.  The default value for the maximum number of rows retrieved can be overridden at the method level.  If more rows are available, the response will include a `nextPage` element, which contains a URL for requesting the next page.  If this value is not provided, no more rows are available. No \"previous page\" element is explicitly provided; to support backward paging, use the previous call.  ## Array Size  For data items that are not paginated, the REST API supports arrays of up to 300 rows.  Thus, for instance, repeated pagination can retrieve thousands of customer accounts, but within any account an array of no more than 300 rate plans is returned.  # API Versions  The Zuora REST API are version controlled. Versioning ensures that Zuora REST API changes are backward compatible. Zuora uses a major and minor version nomenclature to manage changes. By specifying a version in a REST request, you can get expected responses regardless of future changes to the API.  ## Major Version  The major version number of the REST API appears in the REST URL. Currently, Zuora only supports the **v1** major version. For example, `POST https://rest.zuora.com/v1/subscriptions`.  ## Minor Version  Zuora uses minor versions for the REST API to control small changes. For example, a field in a REST method is deprecated and a new field is used to replace it.   Some fields in the REST methods are supported as of minor versions. If a field is not noted with a minor version, this field is available for all minor versions. If a field is noted with a minor version, this field is in version control. You must specify the supported minor version in the request header to process without an error.   If a field is in version control, it is either with a minimum minor version or a maximum minor version, or both of them. You can only use this field with the minor version between the minimum and the maximum minor versions. For example, the `invoiceCollect` field in the POST Subscription method is in version control and its maximum minor version is 189.0. You can only use this field with the minor version 189.0 or earlier.  If you specify a version number in the request header that is not supported, Zuora will use the minimum minor version of the REST API. In our REST API documentation, if a field or feature requires a minor version number, we note that in the field description.  You only need to specify the version number when you use the fields require a minor version. To specify the minor version, set the `zuora-version` parameter to the minor version number in the request header for the request call. For example, the `collect` field is in 196.0 minor version. If you want to use this field for the POST Subscription method, set the  `zuora-version` parameter to `196.0` in the request header. The `zuora-version` parameter is case sensitive.  For all the REST API fields, by default, if the minor version is not specified in the request header, Zuora will use the minimum minor version of the REST API to avoid breaking your integration.   ### Minor Version History  The supported minor versions are not serial. This section documents the changes made to each Zuora REST API minor version.  The following table lists the supported versions and the fields that have a Zuora REST API minor version.  | Fields         | Minor Version      | REST Methods    | Description | |:--------|:--------|:--------|:--------| | invoiceCollect | 189.0 and earlier  | [Create Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_Subscription \"Create Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\"); [Renew Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_RenewSubscription \"Renew Subscription\"); [Cancel Subscription](https://www.zuora.com/developer/api-reference/#operation/POSTSubscriptionCancellation \"Cancel Subscription\"); [Suspend Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionSuspend \"Suspend Subscription\"); [Resume Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionResume \"Resume Subscription\"); [Create Account](https://www.zuora.com/developer/API-Reference/#operation/POST_Account \"Create Account\")|Generates an invoice and collects a payment for a subscription. | | collect        | 196.0 and later    | [Create Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_Subscription \"Create Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\"); [Renew Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_RenewSubscription \"Renew Subscription\"); [Cancel Subscription](https://www.zuora.com/developer/api-reference/#operation/POSTSubscriptionCancellation \"Cancel Subscription\"); [Suspend Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionSuspend \"Suspend Subscription\"); [Resume Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionResume \"Resume Subscription\"); [Create Account](https://www.zuora.com/developer/API-Reference/#operation/POST_Account \"Create Account\")|Collects an automatic payment for a subscription. | | invoice | 196.0 and later| [Create Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_Subscription \"Create Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\"); [Renew Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_RenewSubscription \"Renew Subscription\"); [Cancel Subscription](https://www.zuora.com/developer/api-reference/#operation/POSTSubscriptionCancellation \"Cancel Subscription\"); [Suspend Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionSuspend \"Suspend Subscription\"); [Resume Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionResume \"Resume Subscription\"); [Create Account](https://www.zuora.com/developer/API-Reference/#operation/POST_Account \"Create Account\")|Generates an invoice for a subscription. | | invoiceTargetDate | 196.0 and earlier  | [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\") |Date through which charges are calculated on the invoice, as `yyyy-mm-dd`. | | targetDate | 207.0 and later | [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\") |Date through which charges are calculated on the invoice, as `yyyy-mm-dd`. | | includeExisting DraftInvoiceItems | 196.0 and earlier| [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") | Specifies whether to include draft invoice items in subscription previews. Specify it to be `true` (default) to include draft invoice items in the preview result. Specify it to be `false` to excludes draft invoice items in the preview result. | | includeExisting DraftDocItems | 207.0 and later  | [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") | Specifies whether to include draft invoice items in subscription previews. Specify it to be `true` (default) to include draft invoice items in the preview result. Specify it to be `false` to excludes draft invoice items in the preview result. | | previewType | 196.0 and earlier| [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") | The type of preview you will receive. The possible values are `InvoiceItem`(default), `ChargeMetrics`, and `InvoiceItemChargeMetrics`. | | previewType | 207.0 and later  | [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") | The type of preview you will receive. The possible values are `LegalDoc`(default), `ChargeMetrics`, and `LegalDocChargeMetrics`. |  #### Version 207.0 and Later  The response structure of the [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\") and [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") methods are changed. The following invoice related response fields are moved to the invoice container:    * amount   * amountWithoutTax   * taxAmount   * invoiceItems   * targetDate   * chargeMetrics  # Zuora Object Model  The following diagram presents a high-level view of the key Zuora objects. Click the image to open it in a new tab to resize it.  <a href=\"https://www.zuora.com/wp-content/uploads/2017/01/ZuoraERD.jpeg\" target=\"_blank\"><img src=\"https://www.zuora.com/wp-content/uploads/2017/01/ZuoraERD.jpeg\" alt=\"Zuora Object Model Diagram\"></a>
 *
 * OpenAPI spec version: 2017-04-14
 * Contact: docs@zuora.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;

/**
 * GetBillingPreviewRunResponse Class Doc Comment
 *
 * @category    Class
 * @description get billingPreviewRun response
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class GetBillingPreviewRunResponse implements ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'GetBillingPreviewRunResponse';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = [
        'assume_renewal' => 'string',
        'batch' => 'string',
        'batches' => 'string',
        'charge_type_to_exclude' => 'string',
        'created_by_id' => 'string',
        'created_date' => 'string',
        'end_date' => 'string',
        'error_message' => 'string',
        'including_draft_items' => 'bool',
        'including_evergreen_subscription' => 'bool',
        'result_file_url' => 'string',
        'run_number' => 'string',
        'start_date' => 'string',
        'status' => 'string',
        'storage_option' => 'string',
        'succeeded_accounts' => 'int',
        'success' => 'bool',
        'target_date' => '\DateTime',
        'total_accounts' => 'int',
        'updated_by_id' => 'string',
        'updated_date' => '\DateTime'
    ];

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = [
        'assume_renewal' => 'assumeRenewal',
        'batch' => 'batch',
        'batches' => 'batches',
        'charge_type_to_exclude' => 'chargeTypeToExclude',
        'created_by_id' => 'createdById',
        'created_date' => 'createdDate',
        'end_date' => 'endDate',
        'error_message' => 'errorMessage',
        'including_draft_items' => 'includingDraftItems',
        'including_evergreen_subscription' => 'includingEvergreenSubscription',
        'result_file_url' => 'resultFileUrl',
        'run_number' => 'runNumber',
        'start_date' => 'startDate',
        'status' => 'status',
        'storage_option' => 'storageOption',
        'succeeded_accounts' => 'succeededAccounts',
        'success' => 'success',
        'target_date' => 'targetDate',
        'total_accounts' => 'totalAccounts',
        'updated_by_id' => 'updatedById',
        'updated_date' => 'updatedDate'
    ];


    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = [
        'assume_renewal' => 'setAssumeRenewal',
        'batch' => 'setBatch',
        'batches' => 'setBatches',
        'charge_type_to_exclude' => 'setChargeTypeToExclude',
        'created_by_id' => 'setCreatedById',
        'created_date' => 'setCreatedDate',
        'end_date' => 'setEndDate',
        'error_message' => 'setErrorMessage',
        'including_draft_items' => 'setIncludingDraftItems',
        'including_evergreen_subscription' => 'setIncludingEvergreenSubscription',
        'result_file_url' => 'setResultFileUrl',
        'run_number' => 'setRunNumber',
        'start_date' => 'setStartDate',
        'status' => 'setStatus',
        'storage_option' => 'setStorageOption',
        'succeeded_accounts' => 'setSucceededAccounts',
        'success' => 'setSuccess',
        'target_date' => 'setTargetDate',
        'total_accounts' => 'setTotalAccounts',
        'updated_by_id' => 'setUpdatedById',
        'updated_date' => 'setUpdatedDate'
    ];


    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = [
        'assume_renewal' => 'getAssumeRenewal',
        'batch' => 'getBatch',
        'batches' => 'getBatches',
        'charge_type_to_exclude' => 'getChargeTypeToExclude',
        'created_by_id' => 'getCreatedById',
        'created_date' => 'getCreatedDate',
        'end_date' => 'getEndDate',
        'error_message' => 'getErrorMessage',
        'including_draft_items' => 'getIncludingDraftItems',
        'including_evergreen_subscription' => 'getIncludingEvergreenSubscription',
        'result_file_url' => 'getResultFileUrl',
        'run_number' => 'getRunNumber',
        'start_date' => 'getStartDate',
        'status' => 'getStatus',
        'storage_option' => 'getStorageOption',
        'succeeded_accounts' => 'getSucceededAccounts',
        'success' => 'getSuccess',
        'target_date' => 'getTargetDate',
        'total_accounts' => 'getTotalAccounts',
        'updated_by_id' => 'getUpdatedById',
        'updated_date' => 'getUpdatedDate'
    ];

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    public static function setters()
    {
        return self::$setters;
    }

    public static function getters()
    {
        return self::$getters;
    }

    const STORAGE_OPTION_CSV = 'Csv';
    const STORAGE_OPTION_DATABASE = 'Database';
    

    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public function getStorageOptionAllowableValues()
    {
        return [
            self::STORAGE_OPTION_CSV,
            self::STORAGE_OPTION_DATABASE,
        ];
    }
    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['assume_renewal'] = isset($data['assume_renewal']) ? $data['assume_renewal'] : null;
        $this->container['batch'] = isset($data['batch']) ? $data['batch'] : null;
        $this->container['batches'] = isset($data['batches']) ? $data['batches'] : null;
        $this->container['charge_type_to_exclude'] = isset($data['charge_type_to_exclude']) ? $data['charge_type_to_exclude'] : null;
        $this->container['created_by_id'] = isset($data['created_by_id']) ? $data['created_by_id'] : null;
        $this->container['created_date'] = isset($data['created_date']) ? $data['created_date'] : null;
        $this->container['end_date'] = isset($data['end_date']) ? $data['end_date'] : null;
        $this->container['error_message'] = isset($data['error_message']) ? $data['error_message'] : null;
        $this->container['including_draft_items'] = isset($data['including_draft_items']) ? $data['including_draft_items'] : null;
        $this->container['including_evergreen_subscription'] = isset($data['including_evergreen_subscription']) ? $data['including_evergreen_subscription'] : null;
        $this->container['result_file_url'] = isset($data['result_file_url']) ? $data['result_file_url'] : null;
        $this->container['run_number'] = isset($data['run_number']) ? $data['run_number'] : null;
        $this->container['start_date'] = isset($data['start_date']) ? $data['start_date'] : null;
        $this->container['status'] = isset($data['status']) ? $data['status'] : null;
        $this->container['storage_option'] = isset($data['storage_option']) ? $data['storage_option'] : null;
        $this->container['succeeded_accounts'] = isset($data['succeeded_accounts']) ? $data['succeeded_accounts'] : null;
        $this->container['success'] = isset($data['success']) ? $data['success'] : null;
        $this->container['target_date'] = isset($data['target_date']) ? $data['target_date'] : null;
        $this->container['total_accounts'] = isset($data['total_accounts']) ? $data['total_accounts'] : null;
        $this->container['updated_by_id'] = isset($data['updated_by_id']) ? $data['updated_by_id'] : null;
        $this->container['updated_date'] = isset($data['updated_date']) ? $data['updated_date'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];

        $allowed_values = ["Csv", "Database"];
        if (!in_array($this->container['storage_option'], $allowed_values)) {
            $invalid_properties[] = "invalid value for 'storage_option', must be one of 'Csv', 'Database'.";
        }

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        $allowed_values = ["Csv", "Database"];
        if (!in_array($this->container['storage_option'], $allowed_values)) {
            return false;
        }
        return true;
    }


    /**
     * Gets assume_renewal
     * @return string
     */
    public function getAssumeRenewal()
    {
        return $this->container['assume_renewal'];
    }

    /**
     * Sets assume_renewal
     * @param string $assume_renewal 
     * @return $this
     */
    public function setAssumeRenewal($assume_renewal)
    {
        $this->container['assume_renewal'] = $assume_renewal;

        return $this;
    }

    /**
     * Gets batch
     * @return string
     */
    public function getBatch()
    {
        return $this->container['batch'];
    }

    /**
     * Sets batch
     * @param string $batch The customer batch included in the billing preview run.         **Note**: This field is not available if you set the `zuora-version` request header to `314.0` or later.
     * @return $this
     */
    public function setBatch($batch)
    {
        $this->container['batch'] = $batch;

        return $this;
    }

    /**
     * Gets batches
     * @return string
     */
    public function getBatches()
    {
        return $this->container['batches'];
    }

    /**
     * Sets batches
     * @param string $batches The customer batches included in the billing preview run.   **Note**: This field is only available if you set the `zuora-version` request header to `314.0` or later.
     * @return $this
     */
    public function setBatches($batches)
    {
        $this->container['batches'] = $batches;

        return $this;
    }

    /**
     * Gets charge_type_to_exclude
     * @return string
     */
    public function getChargeTypeToExclude()
    {
        return $this->container['charge_type_to_exclude'];
    }

    /**
     * Sets charge_type_to_exclude
     * @param string $charge_type_to_exclude The charge types excluded from the forecast run.
     * @return $this
     */
    public function setChargeTypeToExclude($charge_type_to_exclude)
    {
        $this->container['charge_type_to_exclude'] = $charge_type_to_exclude;

        return $this;
    }

    /**
     * Gets created_by_id
     * @return string
     */
    public function getCreatedById()
    {
        return $this->container['created_by_id'];
    }

    /**
     * Sets created_by_id
     * @param string $created_by_id The ID of the user who created the billing preview run.
     * @return $this
     */
    public function setCreatedById($created_by_id)
    {
        $this->container['created_by_id'] = $created_by_id;

        return $this;
    }

    /**
     * Gets created_date
     * @return string
     */
    public function getCreatedDate()
    {
        return $this->container['created_date'];
    }

    /**
     * Sets created_date
     * @param string $created_date The date and time when the billing preview run was created.
     * @return $this
     */
    public function setCreatedDate($created_date)
    {
        $this->container['created_date'] = $created_date;

        return $this;
    }

    /**
     * Gets end_date
     * @return string
     */
    public function getEndDate()
    {
        return $this->container['end_date'];
    }

    /**
     * Sets end_date
     * @param string $end_date The date and time when the billing preview run completes.
     * @return $this
     */
    public function setEndDate($end_date)
    {
        $this->container['end_date'] = $end_date;

        return $this;
    }

    /**
     * Gets error_message
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->container['error_message'];
    }

    /**
     * Sets error_message
     * @param string $error_message The error message generated by a failed billing preview run.
     * @return $this
     */
    public function setErrorMessage($error_message)
    {
        $this->container['error_message'] = $error_message;

        return $this;
    }

    /**
     * Gets including_draft_items
     * @return bool
     */
    public function getIncludingDraftItems()
    {
        return $this->container['including_draft_items'];
    }

    /**
     * Sets including_draft_items
     * @param bool $including_draft_items Whether draft document items are included in the billing preview run. By default, draft document items are not included.
     * @return $this
     */
    public function setIncludingDraftItems($including_draft_items)
    {
        $this->container['including_draft_items'] = $including_draft_items;

        return $this;
    }

    /**
     * Gets including_evergreen_subscription
     * @return bool
     */
    public function getIncludingEvergreenSubscription()
    {
        return $this->container['including_evergreen_subscription'];
    }

    /**
     * Sets including_evergreen_subscription
     * @param bool $including_evergreen_subscription Indicates if evergreen subscriptions are included in the billing preview run.
     * @return $this
     */
    public function setIncludingEvergreenSubscription($including_evergreen_subscription)
    {
        $this->container['including_evergreen_subscription'] = $including_evergreen_subscription;

        return $this;
    }

    /**
     * Gets result_file_url
     * @return string
     */
    public function getResultFileUrl()
    {
        return $this->container['result_file_url'];
    }

    /**
     * Sets result_file_url
     * @param string $result_file_url The URL of the zipped CSV result file generated by the billing preview run. This file contains the preview invoice item data and credit memo item data for the specified customers.  If the value of `storageOption` field is `Database`, the returned `resultFileUrl` field is null.  **Note:** The credit memo item data is only available if you have Invoice Settlement feature enabled. The Invoice Settlement feature is generally available as of Zuora Billing Release 296 (March 2021). This feature includes Unapplied Payments, Credit and Debit Memo, and Invoice Item Settlement. If you want to enable Invoice Settlement, see [Invoice Settlement Enablement and Checklist Guide](https://knowledgecenter.zuora.com/Billing/Billing_and_Payments/Invoice_Settlement/Invoice_Settlement_Migration_Checklist_and_Guide) for more information.
     * @return $this
     */
    public function setResultFileUrl($result_file_url)
    {
        $this->container['result_file_url'] = $result_file_url;

        return $this;
    }

    /**
     * Gets run_number
     * @return string
     */
    public function getRunNumber()
    {
        return $this->container['run_number'];
    }

    /**
     * Sets run_number
     * @param string $run_number The run number of the billing preview run.
     * @return $this
     */
    public function setRunNumber($run_number)
    {
        $this->container['run_number'] = $run_number;

        return $this;
    }

    /**
     * Gets start_date
     * @return string
     */
    public function getStartDate()
    {
        return $this->container['start_date'];
    }

    /**
     * Sets start_date
     * @param string $start_date The date and time when the billing preview run starts.
     * @return $this
     */
    public function setStartDate($start_date)
    {
        $this->container['start_date'] = $start_date;

        return $this;
    }

    /**
     * Gets status
     * @return string
     */
    public function getStatus()
    {
        return $this->container['status'];
    }

    /**
     * Sets status
     * @param string $status The status of the >billing preview run.  **Possible values:**   * 0: Pending * 1: Processing * 2: Completed * 3: Error * 4: Canceled
     * @return $this
     */
    public function setStatus($status)
    {
        $this->container['status'] = $status;

        return $this;
    }

    /**
     * Gets storage_option
     * @return string
     */
    public function getStorageOption()
    {
        return $this->container['storage_option'];
    }

    /**
     * Sets storage_option
     * @param string $storage_option The saving options. The default value is `Csv`.
     * @return $this
     */
    public function setStorageOption($storage_option)
    {
        $allowed_values = array('Csv', 'Database');
        if (!is_null($storage_option) && (!in_array($storage_option, $allowed_values))) {
            throw new \InvalidArgumentException("Invalid value for 'storage_option', must be one of 'Csv', 'Database'");
        }
        $this->container['storage_option'] = $storage_option;

        return $this;
    }

    /**
     * Gets succeeded_accounts
     * @return int
     */
    public function getSucceededAccounts()
    {
        return $this->container['succeeded_accounts'];
    }

    /**
     * Sets succeeded_accounts
     * @param int $succeeded_accounts The number of accounts for which preview invoice item data and credit memo item data was successfully generated during the billing preview run.  **Note:** The credit memo item data is only available if you have Invoice Settlement feature enabled. The Invoice Settlement feature is generally available as of Zuora Billing Release 296 (March 2021). This feature includes Unapplied Payments, Credit and Debit Memo, and Invoice Item Settlement. If you want to enable Invoice Settlement, see [Invoice Settlement Enablement and Checklist Guide](https://knowledgecenter.zuora.com/Billing/Billing_and_Payments/Invoice_Settlement/Invoice_Settlement_Migration_Checklist_and_Guide) for more information.
     * @return $this
     */
    public function setSucceededAccounts($succeeded_accounts)
    {
        $this->container['succeeded_accounts'] = $succeeded_accounts;

        return $this;
    }

    /**
     * Gets success
     * @return bool
     */
    public function getSuccess()
    {
        return $this->container['success'];
    }

    /**
     * Sets success
     * @param bool $success Returns `true` if the request was processed successfully.
     * @return $this
     */
    public function setSuccess($success)
    {
        $this->container['success'] = $success;

        return $this;
    }

    /**
     * Gets target_date
     * @return \DateTime
     */
    public function getTargetDate()
    {
        return $this->container['target_date'];
    }

    /**
     * Sets target_date
     * @param \DateTime $target_date The target date for the billing preview run.
     * @return $this
     */
    public function setTargetDate($target_date)
    {
        $this->container['target_date'] = $target_date;

        return $this;
    }

    /**
     * Gets total_accounts
     * @return int
     */
    public function getTotalAccounts()
    {
        return $this->container['total_accounts'];
    }

    /**
     * Sets total_accounts
     * @param int $total_accounts The total number of accounts in the billing preview run.
     * @return $this
     */
    public function setTotalAccounts($total_accounts)
    {
        $this->container['total_accounts'] = $total_accounts;

        return $this;
    }

    /**
     * Gets updated_by_id
     * @return string
     */
    public function getUpdatedById()
    {
        return $this->container['updated_by_id'];
    }

    /**
     * Sets updated_by_id
     * @param string $updated_by_id The ID of the user who last updated the billing preview run.
     * @return $this
     */
    public function setUpdatedById($updated_by_id)
    {
        $this->container['updated_by_id'] = $updated_by_id;

        return $this;
    }

    /**
     * Gets updated_date
     * @return \DateTime
     */
    public function getUpdatedDate()
    {
        return $this->container['updated_date'];
    }

    /**
     * Sets updated_date
     * @param \DateTime $updated_date The date and time when the billing preview run was last updated.
     * @return $this
     */
    public function setUpdatedDate($updated_date)
    {
        $this->container['updated_date'] = $updated_date;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this));
    }
}


