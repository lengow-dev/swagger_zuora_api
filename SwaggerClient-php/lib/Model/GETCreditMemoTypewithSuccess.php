<?php
/**
 * GETCreditMemoTypewithSuccess
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swaagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Zuora API Reference
 *
 * # Introduction Welcome to the reference for the Zuora REST API!  <a href=\"http://en.wikipedia.org/wiki/REST_API\" target=\"_blank\">REST</a> is a web-service protocol that lends itself to rapid development by using everyday HTTP and JSON technology.  The Zuora REST API provides a broad set of operations and resources that:    * Enable Web Storefront integration from your website.   * Support self-service subscriber sign-ups and account management.   * Process revenue schedules through custom revenue rule models.   * Enable manipulation of most objects in the Zuora Object Model.      ## Endpoints      The Zuora REST API is provided via the following endpoints.   | Tenant              | Base URL for REST Endpoints |   |-------------------------|-------------------------|   |Production | https://rest.zuora.com/v1   |   |API Sandbox    | https://rest.apisandbox.zuora.com/v1|      The production endpoint provides access to your live user data. The API Sandbox tenant is a good place to test your code without affecting real-world data. To use it, you must be provisioned with an API Sandbox tenant - your Zuora representative can help you if needed.      ## Access to the API      If you have a Zuora tenant, you already have access to the API.      If you don't have a Zuora tenant, go to <a href=\" https://www.zuora.com/resource/zuora-test-drive\" target=\"_blank\">https://www.zuora.com/resource/zuora-test-drive</a> and sign up for a Production Test Drive tenant. The tenant comes with seed data, such as a sample product catalog.  We recommend that you <a href=\"https://knowledgecenter.zuora.com/CF_Users_and_Administrators/A_Administrator_Settings/Manage_Users/Create_an_API_User\" target=\"_blank\">create an API user</a> specifically for making API calls. Don't log in to the Zuora UI with this account. Logging in to the UI enables a security feature that periodically expires the account's password, which may eventually cause authentication failures with the API. Note that a user role does not have write access to Zuora REST services unless it has the API Write Access permission as described in those instructions.  # Authentication  There are three ways to authenticate:    * Use username and password. Include authentication with each request in the header:         * `apiAccessKeyId`      * `apiSecretAccessKey`     * `entityId` or `entityName` (Only for [Zuora Multi-entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity \"Multi-entity\"). See \"Entity Id and Entity Name\" below for more information.)   * Use an authorization cookie. The cookie authorizes the user to make calls to the REST API for the duration specified in  **Administration > Security Policies > Session timeout**. The cookie expiration time is reset with this duration after every call to the REST API. To obtain a cookie, call the REST  `connections` resource with the following API user information:         *   ID         *   password     *   entity Id or entity name (Only for [Zuora Multi-entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity \"Multi-entity\"). See \"Entity Id and Entity Name\" below for more information.)         * For CORS-enabled APIs only: Include a 'single-use' token in the request header, which re-authenticates the user with each request. See below for more details.  ## Entity Id and Entity Name  The `entityId` and `entityName` parameters are only used for [Zuora Multi-entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity \"Zuora Multi-entity\").   The  `entityId` and `entityName` parameters specify the Id and the [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name \"Introduction to Entity and Entity Hierarchy\") that you want to access, respectively. Note that you must have permission to access the entity.   You can specify either the `entityId` or `entityName` parameter in the authentication to access and view an entity.    * If both `entityId` and `entityName` are specified in the authentication, an error occurs.    * If neither `entityId` nor `entityName` is specified in the authentication, you will log in to the entity in which your user account is created.      To get the entity Id and entity name, you can use the GET Entities REST call. For more information, see [API User Authentication](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/A_Overview_of_Multi-entity#API_User_Authentication \"API User Authentication\").      ## Token Authentication for CORS-Enabled APIs      The CORS mechanism enables REST API calls to Zuora to be made directly from your customer's browser, with all credit card and security information transmitted directly to Zuora. This minimizes your PCI compliance burden, allows you to implement advanced validation on your payment forms, and  makes your payment forms look just like any other part of your website.    For security reasons, instead of using cookies, an API request via CORS uses **tokens** for authentication.  The token method of authentication is only designed for use with requests that must originate from your customer's browser; **it should  not be considered a replacement to the existing cookie authentication** mechanism.  See [Zuora CORS REST](https://knowledgecenter.zuora.com/DC_Developers/REST_API/A_REST_basics/G_CORS_REST \"Zuora CORS REST\") for details on how CORS works and how you can begin to implement customer calls to the Zuora REST APIs. See  [HMAC Signatures](https://www.zuora.com/developer/API-Reference/#operation/POSTHMACSignature \"HMAC Signatures\") for details on the HMAC method that returns the authentication token.  # Requests and Responses  ## Request IDs  As a general rule, when asked to supply a \"key\" for an account or subscription (accountKey, account-key, subscriptionKey, subscription-key), you can provide either the actual ID or  the number of the entity.  ## HTTP Request Body  Most of the parameters and data accompanying your requests will be contained in the body of the HTTP request.   The Zuora REST API accepts JSON in the HTTP request body. No other data format (e.g., XML) is supported.  ## Testing a Request  Use a third party client, such as [curl](https://curl.haxx.se \"curl\"), [Postman](https://www.getpostman.com \"Postman\"), or [Advanced REST Client](https://advancedrestclient.com \"Advanced REST Client\"), to test the Zuora REST API.  You can test the Zuora REST API from the Zuora API Sandbox or Production tenants. If connecting to Production, bear in mind that you are working with your live production data, not sample data or test data.  ## Testing with Credit Cards  Sooner or later it will probably be necessary to test some transactions that involve credit cards. For suggestions on how to handle this, see [Going Live With Your Payment Gateway](https://knowledgecenter.zuora.com/CB_Billing/M_Payment_Gateways/C_Managing_Payment_Gateways/B_Going_Live_Payment_Gateways#Testing_with_Credit_Cards \"C_Zuora_User_Guides/A_Billing_and_Payments/M_Payment_Gateways/C_Managing_Payment_Gateways/B_Going_Live_Payment_Gateways#Testing_with_Credit_Cards\" ).  ## Error Handling  Responses and error codes are detailed in [Responses and errors](https://knowledgecenter.zuora.com/DC_Developers/REST_API/A_REST_basics/3_Responses_and_errors \"Responses and errors\").  # Pagination  When retrieving information (using GET methods), the optional `pageSize` query parameter sets the maximum number of rows to return in a response. The maximum is `40`; larger values are treated as `40`. If this value is empty or invalid, `pageSize` typically defaults to `10`.  The default value for the maximum number of rows retrieved can be overridden at the method level.  If more rows are available, the response will include a `nextPage` element, which contains a URL for requesting the next page.  If this value is not provided, no more rows are available. No \"previous page\" element is explicitly provided; to support backward paging, use the previous call.  ## Array Size  For data items that are not paginated, the REST API supports arrays of up to 300 rows.  Thus, for instance, repeated pagination can retrieve thousands of customer accounts, but within any account an array of no more than 300 rate plans is returned.  # API Versions  The Zuora REST API are version controlled. Versioning ensures that Zuora REST API changes are backward compatible. Zuora uses a major and minor version nomenclature to manage changes. By specifying a version in a REST request, you can get expected responses regardless of future changes to the API.  ## Major Version  The major version number of the REST API appears in the REST URL. Currently, Zuora only supports the **v1** major version. For example, `POST https://rest.zuora.com/v1/subscriptions`.  ## Minor Version  Zuora uses minor versions for the REST API to control small changes. For example, a field in a REST method is deprecated and a new field is used to replace it.   Some fields in the REST methods are supported as of minor versions. If a field is not noted with a minor version, this field is available for all minor versions. If a field is noted with a minor version, this field is in version control. You must specify the supported minor version in the request header to process without an error.   If a field is in version control, it is either with a minimum minor version or a maximum minor version, or both of them. You can only use this field with the minor version between the minimum and the maximum minor versions. For example, the `invoiceCollect` field in the POST Subscription method is in version control and its maximum minor version is 189.0. You can only use this field with the minor version 189.0 or earlier.  If you specify a version number in the request header that is not supported, Zuora will use the minimum minor version of the REST API. In our REST API documentation, if a field or feature requires a minor version number, we note that in the field description.  You only need to specify the version number when you use the fields require a minor version. To specify the minor version, set the `zuora-version` parameter to the minor version number in the request header for the request call. For example, the `collect` field is in 196.0 minor version. If you want to use this field for the POST Subscription method, set the  `zuora-version` parameter to `196.0` in the request header. The `zuora-version` parameter is case sensitive.  For all the REST API fields, by default, if the minor version is not specified in the request header, Zuora will use the minimum minor version of the REST API to avoid breaking your integration.   ### Minor Version History  The supported minor versions are not serial. This section documents the changes made to each Zuora REST API minor version.  The following table lists the supported versions and the fields that have a Zuora REST API minor version.  | Fields         | Minor Version      | REST Methods    | Description | |:--------|:--------|:--------|:--------| | invoiceCollect | 189.0 and earlier  | [Create Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_Subscription \"Create Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\"); [Renew Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_RenewSubscription \"Renew Subscription\"); [Cancel Subscription](https://www.zuora.com/developer/api-reference/#operation/POSTSubscriptionCancellation \"Cancel Subscription\"); [Suspend Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionSuspend \"Suspend Subscription\"); [Resume Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionResume \"Resume Subscription\"); [Create Account](https://www.zuora.com/developer/API-Reference/#operation/POST_Account \"Create Account\")|Generates an invoice and collects a payment for a subscription. | | collect        | 196.0 and later    | [Create Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_Subscription \"Create Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\"); [Renew Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_RenewSubscription \"Renew Subscription\"); [Cancel Subscription](https://www.zuora.com/developer/api-reference/#operation/POSTSubscriptionCancellation \"Cancel Subscription\"); [Suspend Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionSuspend \"Suspend Subscription\"); [Resume Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionResume \"Resume Subscription\"); [Create Account](https://www.zuora.com/developer/API-Reference/#operation/POST_Account \"Create Account\")|Collects an automatic payment for a subscription. | | invoice | 196.0 and later| [Create Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_Subscription \"Create Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\"); [Renew Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_RenewSubscription \"Renew Subscription\"); [Cancel Subscription](https://www.zuora.com/developer/api-reference/#operation/POSTSubscriptionCancellation \"Cancel Subscription\"); [Suspend Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionSuspend \"Suspend Subscription\"); [Resume Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionResume \"Resume Subscription\"); [Create Account](https://www.zuora.com/developer/API-Reference/#operation/POST_Account \"Create Account\")|Generates an invoice for a subscription. | | invoiceTargetDate | 196.0 and earlier  | [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\") |Date through which charges are calculated on the invoice, as `yyyy-mm-dd`. | | targetDate | 207.0 and later | [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\") |Date through which charges are calculated on the invoice, as `yyyy-mm-dd`. | | includeExisting DraftInvoiceItems | 196.0 and earlier| [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") | Specifies whether to include draft invoice items in subscription previews. Specify it to be `true` (default) to include draft invoice items in the preview result. Specify it to be `false` to excludes draft invoice items in the preview result. | | includeExisting DraftDocItems | 207.0 and later  | [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") | Specifies whether to include draft invoice items in subscription previews. Specify it to be `true` (default) to include draft invoice items in the preview result. Specify it to be `false` to excludes draft invoice items in the preview result. | | previewType | 196.0 and earlier| [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") | The type of preview you will receive. The possible values are `InvoiceItem`(default), `ChargeMetrics`, and `InvoiceItemChargeMetrics`. | | previewType | 207.0 and later  | [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") | The type of preview you will receive. The possible values are `LegalDoc`(default), `ChargeMetrics`, and `LegalDocChargeMetrics`. |  #### Version 207.0 and Later  The response structure of the [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\") and [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") methods are changed. The following invoice related response fields are moved to the invoice container:    * amount   * amountWithoutTax   * taxAmount   * invoiceItems   * targetDate   * chargeMetrics  # Zuora Object Model  The following diagram presents a high-level view of the key Zuora objects. Click the image to open it in a new tab to resize it.  <a href=\"https://www.zuora.com/wp-content/uploads/2017/01/ZuoraERD.jpeg\" target=\"_blank\"><img src=\"https://www.zuora.com/wp-content/uploads/2017/01/ZuoraERD.jpeg\" alt=\"Zuora Object Model Diagram\"></a>
 *
 * OpenAPI spec version: 2017-04-14
 * Contact: docs@zuora.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;

/**
 * GETCreditMemoTypewithSuccess Class Doc Comment
 *
 * @category    Class
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class GETCreditMemoTypewithSuccess implements ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'GETCreditMemoTypewithSuccess';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = [
        'integration_id__ns' => 'string',
        'integration_status__ns' => 'string',
        'origin__ns' => 'string',
        'sync_date__ns' => 'string',
        'transaction__ns' => 'string',
        'account_id' => 'string',
        'account_number' => 'string',
        'amount' => 'double',
        'applied_amount' => 'double',
        'auto_apply_upon_posting' => 'bool',
        'bill_to_contact_id' => 'string',
        'cancelled_by_id' => 'string',
        'cancelled_on' => '\DateTime',
        'comment' => 'string',
        'created_by_id' => 'string',
        'created_date' => '\DateTime',
        'credit_memo_date' => '\DateTime',
        'currency' => 'string',
        'exclude_from_auto_apply_rules' => 'bool',
        'id' => 'string',
        'latest_pdf_file_id' => 'string',
        'number' => 'string',
        'posted_by_id' => 'string',
        'posted_on' => '\DateTime',
        'reason_code' => 'string',
        'referred_invoice_id' => 'string',
        'refund_amount' => 'double',
        'reversed' => 'bool',
        'sequence_set_id' => 'string',
        'source' => 'string',
        'source_id' => 'string',
        'source_type' => 'string',
        'status' => 'string',
        'target_date' => '\DateTime',
        'tax_amount' => 'double',
        'tax_message' => 'string',
        'tax_status' => 'string',
        'total_tax_exempt_amount' => 'double',
        'transferred_to_accounting' => 'string',
        'unapplied_amount' => 'double',
        'updated_by_id' => 'string',
        'updated_date' => '\DateTime'
    ];

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = [
        'integration_id__ns' => 'IntegrationId__NS',
        'integration_status__ns' => 'IntegrationStatus__NS',
        'origin__ns' => 'Origin__NS',
        'sync_date__ns' => 'SyncDate__NS',
        'transaction__ns' => 'Transaction__NS',
        'account_id' => 'accountId',
        'account_number' => 'accountNumber',
        'amount' => 'amount',
        'applied_amount' => 'appliedAmount',
        'auto_apply_upon_posting' => 'autoApplyUponPosting',
        'bill_to_contact_id' => 'billToContactId',
        'cancelled_by_id' => 'cancelledById',
        'cancelled_on' => 'cancelledOn',
        'comment' => 'comment',
        'created_by_id' => 'createdById',
        'created_date' => 'createdDate',
        'credit_memo_date' => 'creditMemoDate',
        'currency' => 'currency',
        'exclude_from_auto_apply_rules' => 'excludeFromAutoApplyRules',
        'id' => 'id',
        'latest_pdf_file_id' => 'latestPDFFileId',
        'number' => 'number',
        'posted_by_id' => 'postedById',
        'posted_on' => 'postedOn',
        'reason_code' => 'reasonCode',
        'referred_invoice_id' => 'referredInvoiceId',
        'refund_amount' => 'refundAmount',
        'reversed' => 'reversed',
        'sequence_set_id' => 'sequenceSetId',
        'source' => 'source',
        'source_id' => 'sourceId',
        'source_type' => 'sourceType',
        'status' => 'status',
        'target_date' => 'targetDate',
        'tax_amount' => 'taxAmount',
        'tax_message' => 'taxMessage',
        'tax_status' => 'taxStatus',
        'total_tax_exempt_amount' => 'totalTaxExemptAmount',
        'transferred_to_accounting' => 'transferredToAccounting',
        'unapplied_amount' => 'unappliedAmount',
        'updated_by_id' => 'updatedById',
        'updated_date' => 'updatedDate'
    ];


    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = [
        'integration_id__ns' => 'setIntegrationIdNs',
        'integration_status__ns' => 'setIntegrationStatusNs',
        'origin__ns' => 'setOriginNs',
        'sync_date__ns' => 'setSyncDateNs',
        'transaction__ns' => 'setTransactionNs',
        'account_id' => 'setAccountId',
        'account_number' => 'setAccountNumber',
        'amount' => 'setAmount',
        'applied_amount' => 'setAppliedAmount',
        'auto_apply_upon_posting' => 'setAutoApplyUponPosting',
        'bill_to_contact_id' => 'setBillToContactId',
        'cancelled_by_id' => 'setCancelledById',
        'cancelled_on' => 'setCancelledOn',
        'comment' => 'setComment',
        'created_by_id' => 'setCreatedById',
        'created_date' => 'setCreatedDate',
        'credit_memo_date' => 'setCreditMemoDate',
        'currency' => 'setCurrency',
        'exclude_from_auto_apply_rules' => 'setExcludeFromAutoApplyRules',
        'id' => 'setId',
        'latest_pdf_file_id' => 'setLatestPdfFileId',
        'number' => 'setNumber',
        'posted_by_id' => 'setPostedById',
        'posted_on' => 'setPostedOn',
        'reason_code' => 'setReasonCode',
        'referred_invoice_id' => 'setReferredInvoiceId',
        'refund_amount' => 'setRefundAmount',
        'reversed' => 'setReversed',
        'sequence_set_id' => 'setSequenceSetId',
        'source' => 'setSource',
        'source_id' => 'setSourceId',
        'source_type' => 'setSourceType',
        'status' => 'setStatus',
        'target_date' => 'setTargetDate',
        'tax_amount' => 'setTaxAmount',
        'tax_message' => 'setTaxMessage',
        'tax_status' => 'setTaxStatus',
        'total_tax_exempt_amount' => 'setTotalTaxExemptAmount',
        'transferred_to_accounting' => 'setTransferredToAccounting',
        'unapplied_amount' => 'setUnappliedAmount',
        'updated_by_id' => 'setUpdatedById',
        'updated_date' => 'setUpdatedDate'
    ];


    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = [
        'integration_id__ns' => 'getIntegrationIdNs',
        'integration_status__ns' => 'getIntegrationStatusNs',
        'origin__ns' => 'getOriginNs',
        'sync_date__ns' => 'getSyncDateNs',
        'transaction__ns' => 'getTransactionNs',
        'account_id' => 'getAccountId',
        'account_number' => 'getAccountNumber',
        'amount' => 'getAmount',
        'applied_amount' => 'getAppliedAmount',
        'auto_apply_upon_posting' => 'getAutoApplyUponPosting',
        'bill_to_contact_id' => 'getBillToContactId',
        'cancelled_by_id' => 'getCancelledById',
        'cancelled_on' => 'getCancelledOn',
        'comment' => 'getComment',
        'created_by_id' => 'getCreatedById',
        'created_date' => 'getCreatedDate',
        'credit_memo_date' => 'getCreditMemoDate',
        'currency' => 'getCurrency',
        'exclude_from_auto_apply_rules' => 'getExcludeFromAutoApplyRules',
        'id' => 'getId',
        'latest_pdf_file_id' => 'getLatestPdfFileId',
        'number' => 'getNumber',
        'posted_by_id' => 'getPostedById',
        'posted_on' => 'getPostedOn',
        'reason_code' => 'getReasonCode',
        'referred_invoice_id' => 'getReferredInvoiceId',
        'refund_amount' => 'getRefundAmount',
        'reversed' => 'getReversed',
        'sequence_set_id' => 'getSequenceSetId',
        'source' => 'getSource',
        'source_id' => 'getSourceId',
        'source_type' => 'getSourceType',
        'status' => 'getStatus',
        'target_date' => 'getTargetDate',
        'tax_amount' => 'getTaxAmount',
        'tax_message' => 'getTaxMessage',
        'tax_status' => 'getTaxStatus',
        'total_tax_exempt_amount' => 'getTotalTaxExemptAmount',
        'transferred_to_accounting' => 'getTransferredToAccounting',
        'unapplied_amount' => 'getUnappliedAmount',
        'updated_by_id' => 'getUpdatedById',
        'updated_date' => 'getUpdatedDate'
    ];

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    public static function setters()
    {
        return self::$setters;
    }

    public static function getters()
    {
        return self::$getters;
    }

    const SOURCE_TYPE_SUBSCRIPTION = 'Subscription';
    const SOURCE_TYPE_STANDALONE = 'Standalone';
    const SOURCE_TYPE_INVOICE = 'Invoice';
    const SOURCE_TYPE_ORDER = 'Order';
    const SOURCE_TYPE_CREDIT_MEMO = 'CreditMemo';
    const SOURCE_TYPE_CONSOLIDATION = 'Consolidation';
    const STATUS_DRAFT = 'Draft';
    const STATUS_POSTED = 'Posted';
    const STATUS_CANCELED = 'Canceled';
    const STATUS_ERROR = 'Error';
    const STATUS_PENDING_FOR_TAX = 'PendingForTax';
    const STATUS_GENERATING = 'Generating';
    const STATUS_CANCEL_IN_PROGRESS = 'CancelInProgress';
    const TAX_STATUS_COMPLETE = 'Complete';
    const TAX_STATUS_ERROR = 'Error';
    const TRANSFERRED_TO_ACCOUNTING_PROCESSING = 'Processing';
    const TRANSFERRED_TO_ACCOUNTING_YES = 'Yes';
    const TRANSFERRED_TO_ACCOUNTING_NO = 'No';
    const TRANSFERRED_TO_ACCOUNTING_ERROR = 'Error';
    const TRANSFERRED_TO_ACCOUNTING_IGNORE = 'Ignore';
    

    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public function getSourceTypeAllowableValues()
    {
        return [
            self::SOURCE_TYPE_SUBSCRIPTION,
            self::SOURCE_TYPE_STANDALONE,
            self::SOURCE_TYPE_INVOICE,
            self::SOURCE_TYPE_ORDER,
            self::SOURCE_TYPE_CREDIT_MEMO,
            self::SOURCE_TYPE_CONSOLIDATION,
        ];
    }
    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public function getStatusAllowableValues()
    {
        return [
            self::STATUS_DRAFT,
            self::STATUS_POSTED,
            self::STATUS_CANCELED,
            self::STATUS_ERROR,
            self::STATUS_PENDING_FOR_TAX,
            self::STATUS_GENERATING,
            self::STATUS_CANCEL_IN_PROGRESS,
        ];
    }
    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public function getTaxStatusAllowableValues()
    {
        return [
            self::TAX_STATUS_COMPLETE,
            self::TAX_STATUS_ERROR,
        ];
    }
    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public function getTransferredToAccountingAllowableValues()
    {
        return [
            self::TRANSFERRED_TO_ACCOUNTING_PROCESSING,
            self::TRANSFERRED_TO_ACCOUNTING_YES,
            self::TRANSFERRED_TO_ACCOUNTING_NO,
            self::TRANSFERRED_TO_ACCOUNTING_ERROR,
            self::TRANSFERRED_TO_ACCOUNTING_IGNORE,
        ];
    }
    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['integration_id__ns'] = isset($data['integration_id__ns']) ? $data['integration_id__ns'] : null;
        $this->container['integration_status__ns'] = isset($data['integration_status__ns']) ? $data['integration_status__ns'] : null;
        $this->container['origin__ns'] = isset($data['origin__ns']) ? $data['origin__ns'] : null;
        $this->container['sync_date__ns'] = isset($data['sync_date__ns']) ? $data['sync_date__ns'] : null;
        $this->container['transaction__ns'] = isset($data['transaction__ns']) ? $data['transaction__ns'] : null;
        $this->container['account_id'] = isset($data['account_id']) ? $data['account_id'] : null;
        $this->container['account_number'] = isset($data['account_number']) ? $data['account_number'] : null;
        $this->container['amount'] = isset($data['amount']) ? $data['amount'] : null;
        $this->container['applied_amount'] = isset($data['applied_amount']) ? $data['applied_amount'] : null;
        $this->container['auto_apply_upon_posting'] = isset($data['auto_apply_upon_posting']) ? $data['auto_apply_upon_posting'] : null;
        $this->container['bill_to_contact_id'] = isset($data['bill_to_contact_id']) ? $data['bill_to_contact_id'] : null;
        $this->container['cancelled_by_id'] = isset($data['cancelled_by_id']) ? $data['cancelled_by_id'] : null;
        $this->container['cancelled_on'] = isset($data['cancelled_on']) ? $data['cancelled_on'] : null;
        $this->container['comment'] = isset($data['comment']) ? $data['comment'] : null;
        $this->container['created_by_id'] = isset($data['created_by_id']) ? $data['created_by_id'] : null;
        $this->container['created_date'] = isset($data['created_date']) ? $data['created_date'] : null;
        $this->container['credit_memo_date'] = isset($data['credit_memo_date']) ? $data['credit_memo_date'] : null;
        $this->container['currency'] = isset($data['currency']) ? $data['currency'] : null;
        $this->container['exclude_from_auto_apply_rules'] = isset($data['exclude_from_auto_apply_rules']) ? $data['exclude_from_auto_apply_rules'] : null;
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
        $this->container['latest_pdf_file_id'] = isset($data['latest_pdf_file_id']) ? $data['latest_pdf_file_id'] : null;
        $this->container['number'] = isset($data['number']) ? $data['number'] : null;
        $this->container['posted_by_id'] = isset($data['posted_by_id']) ? $data['posted_by_id'] : null;
        $this->container['posted_on'] = isset($data['posted_on']) ? $data['posted_on'] : null;
        $this->container['reason_code'] = isset($data['reason_code']) ? $data['reason_code'] : null;
        $this->container['referred_invoice_id'] = isset($data['referred_invoice_id']) ? $data['referred_invoice_id'] : null;
        $this->container['refund_amount'] = isset($data['refund_amount']) ? $data['refund_amount'] : null;
        $this->container['reversed'] = isset($data['reversed']) ? $data['reversed'] : null;
        $this->container['sequence_set_id'] = isset($data['sequence_set_id']) ? $data['sequence_set_id'] : null;
        $this->container['source'] = isset($data['source']) ? $data['source'] : null;
        $this->container['source_id'] = isset($data['source_id']) ? $data['source_id'] : null;
        $this->container['source_type'] = isset($data['source_type']) ? $data['source_type'] : null;
        $this->container['status'] = isset($data['status']) ? $data['status'] : null;
        $this->container['target_date'] = isset($data['target_date']) ? $data['target_date'] : null;
        $this->container['tax_amount'] = isset($data['tax_amount']) ? $data['tax_amount'] : null;
        $this->container['tax_message'] = isset($data['tax_message']) ? $data['tax_message'] : null;
        $this->container['tax_status'] = isset($data['tax_status']) ? $data['tax_status'] : null;
        $this->container['total_tax_exempt_amount'] = isset($data['total_tax_exempt_amount']) ? $data['total_tax_exempt_amount'] : null;
        $this->container['transferred_to_accounting'] = isset($data['transferred_to_accounting']) ? $data['transferred_to_accounting'] : null;
        $this->container['unapplied_amount'] = isset($data['unapplied_amount']) ? $data['unapplied_amount'] : null;
        $this->container['updated_by_id'] = isset($data['updated_by_id']) ? $data['updated_by_id'] : null;
        $this->container['updated_date'] = isset($data['updated_date']) ? $data['updated_date'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];

        if (!is_null($this->container['integration_id__ns']) && (strlen($this->container['integration_id__ns']) > 255)) {
            $invalid_properties[] = "invalid value for 'integration_id__ns', the character length must be smaller than or equal to 255.";
        }

        if (!is_null($this->container['integration_status__ns']) && (strlen($this->container['integration_status__ns']) > 255)) {
            $invalid_properties[] = "invalid value for 'integration_status__ns', the character length must be smaller than or equal to 255.";
        }

        if (!is_null($this->container['origin__ns']) && (strlen($this->container['origin__ns']) > 255)) {
            $invalid_properties[] = "invalid value for 'origin__ns', the character length must be smaller than or equal to 255.";
        }

        if (!is_null($this->container['sync_date__ns']) && (strlen($this->container['sync_date__ns']) > 255)) {
            $invalid_properties[] = "invalid value for 'sync_date__ns', the character length must be smaller than or equal to 255.";
        }

        if (!is_null($this->container['transaction__ns']) && (strlen($this->container['transaction__ns']) > 255)) {
            $invalid_properties[] = "invalid value for 'transaction__ns', the character length must be smaller than or equal to 255.";
        }

        $allowed_values = ["Subscription", "Standalone", "Invoice", "Order", "CreditMemo", "Consolidation"];
        if (!in_array($this->container['source_type'], $allowed_values)) {
            $invalid_properties[] = "invalid value for 'source_type', must be one of 'Subscription', 'Standalone', 'Invoice', 'Order', 'CreditMemo', 'Consolidation'.";
        }

        $allowed_values = ["Draft", "Posted", "Canceled", "Error", "PendingForTax", "Generating", "CancelInProgress"];
        if (!in_array($this->container['status'], $allowed_values)) {
            $invalid_properties[] = "invalid value for 'status', must be one of 'Draft', 'Posted', 'Canceled', 'Error', 'PendingForTax', 'Generating', 'CancelInProgress'.";
        }

        $allowed_values = ["Complete", "Error"];
        if (!in_array($this->container['tax_status'], $allowed_values)) {
            $invalid_properties[] = "invalid value for 'tax_status', must be one of 'Complete', 'Error'.";
        }

        $allowed_values = ["Processing", "Yes", "No", "Error", "Ignore"];
        if (!in_array($this->container['transferred_to_accounting'], $allowed_values)) {
            $invalid_properties[] = "invalid value for 'transferred_to_accounting', must be one of 'Processing', 'Yes', 'No', 'Error', 'Ignore'.";
        }

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        if (strlen($this->container['integration_id__ns']) > 255) {
            return false;
        }
        if (strlen($this->container['integration_status__ns']) > 255) {
            return false;
        }
        if (strlen($this->container['origin__ns']) > 255) {
            return false;
        }
        if (strlen($this->container['sync_date__ns']) > 255) {
            return false;
        }
        if (strlen($this->container['transaction__ns']) > 255) {
            return false;
        }
        $allowed_values = ["Subscription", "Standalone", "Invoice", "Order", "CreditMemo", "Consolidation"];
        if (!in_array($this->container['source_type'], $allowed_values)) {
            return false;
        }
        $allowed_values = ["Draft", "Posted", "Canceled", "Error", "PendingForTax", "Generating", "CancelInProgress"];
        if (!in_array($this->container['status'], $allowed_values)) {
            return false;
        }
        $allowed_values = ["Complete", "Error"];
        if (!in_array($this->container['tax_status'], $allowed_values)) {
            return false;
        }
        $allowed_values = ["Processing", "Yes", "No", "Error", "Ignore"];
        if (!in_array($this->container['transferred_to_accounting'], $allowed_values)) {
            return false;
        }
        return true;
    }


    /**
     * Gets integration_id__ns
     * @return string
     */
    public function getIntegrationIdNs()
    {
        return $this->container['integration_id__ns'];
    }

    /**
     * Sets integration_id__ns
     * @param string $integration_id__ns ID of the corresponding object in NetSuite. Only available if you have installed the [Zuora Connector for NetSuite](https://www.zuora.com/connect/app/?appId=265).
     * @return $this
     */
    public function setIntegrationIdNs($integration_id__ns)
    {
        if (!is_null($integration_id__ns) && (strlen($integration_id__ns) > 255)) {
            throw new \InvalidArgumentException('invalid length for $integration_id__ns when calling GETCreditMemoTypewithSuccess., must be smaller than or equal to 255.');
        }

        $this->container['integration_id__ns'] = $integration_id__ns;

        return $this;
    }

    /**
     * Gets integration_status__ns
     * @return string
     */
    public function getIntegrationStatusNs()
    {
        return $this->container['integration_status__ns'];
    }

    /**
     * Sets integration_status__ns
     * @param string $integration_status__ns Status of the credit memo's synchronization with NetSuite. Only available if you have installed the [Zuora Connector for NetSuite](https://www.zuora.com/connect/app/?appId=265).
     * @return $this
     */
    public function setIntegrationStatusNs($integration_status__ns)
    {
        if (!is_null($integration_status__ns) && (strlen($integration_status__ns) > 255)) {
            throw new \InvalidArgumentException('invalid length for $integration_status__ns when calling GETCreditMemoTypewithSuccess., must be smaller than or equal to 255.');
        }

        $this->container['integration_status__ns'] = $integration_status__ns;

        return $this;
    }

    /**
     * Gets origin__ns
     * @return string
     */
    public function getOriginNs()
    {
        return $this->container['origin__ns'];
    }

    /**
     * Sets origin__ns
     * @param string $origin__ns Origin of the corresponding object in NetSuite. Only available if you have installed the [Zuora Connector for NetSuite](https://www.zuora.com/connect/app/?appId=265).
     * @return $this
     */
    public function setOriginNs($origin__ns)
    {
        if (!is_null($origin__ns) && (strlen($origin__ns) > 255)) {
            throw new \InvalidArgumentException('invalid length for $origin__ns when calling GETCreditMemoTypewithSuccess., must be smaller than or equal to 255.');
        }

        $this->container['origin__ns'] = $origin__ns;

        return $this;
    }

    /**
     * Gets sync_date__ns
     * @return string
     */
    public function getSyncDateNs()
    {
        return $this->container['sync_date__ns'];
    }

    /**
     * Sets sync_date__ns
     * @param string $sync_date__ns Date when the credit memo was synchronized with NetSuite. Only available if you have installed the [Zuora Connector for NetSuite](https://www.zuora.com/connect/app/?appId=265).
     * @return $this
     */
    public function setSyncDateNs($sync_date__ns)
    {
        if (!is_null($sync_date__ns) && (strlen($sync_date__ns) > 255)) {
            throw new \InvalidArgumentException('invalid length for $sync_date__ns when calling GETCreditMemoTypewithSuccess., must be smaller than or equal to 255.');
        }

        $this->container['sync_date__ns'] = $sync_date__ns;

        return $this;
    }

    /**
     * Gets transaction__ns
     * @return string
     */
    public function getTransactionNs()
    {
        return $this->container['transaction__ns'];
    }

    /**
     * Sets transaction__ns
     * @param string $transaction__ns Related transaction in NetSuite. Only available if you have installed the [Zuora Connector for NetSuite](https://www.zuora.com/connect/app/?appId=265).
     * @return $this
     */
    public function setTransactionNs($transaction__ns)
    {
        if (!is_null($transaction__ns) && (strlen($transaction__ns) > 255)) {
            throw new \InvalidArgumentException('invalid length for $transaction__ns when calling GETCreditMemoTypewithSuccess., must be smaller than or equal to 255.');
        }

        $this->container['transaction__ns'] = $transaction__ns;

        return $this;
    }

    /**
     * Gets account_id
     * @return string
     */
    public function getAccountId()
    {
        return $this->container['account_id'];
    }

    /**
     * Sets account_id
     * @param string $account_id The ID of the customer account associated with the credit memo.
     * @return $this
     */
    public function setAccountId($account_id)
    {
        $this->container['account_id'] = $account_id;

        return $this;
    }

    /**
     * Gets account_number
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->container['account_number'];
    }

    /**
     * Sets account_number
     * @param string $account_number The number of the account associated with the credit memo.
     * @return $this
     */
    public function setAccountNumber($account_number)
    {
        $this->container['account_number'] = $account_number;

        return $this;
    }

    /**
     * Gets amount
     * @return double
     */
    public function getAmount()
    {
        return $this->container['amount'];
    }

    /**
     * Sets amount
     * @param double $amount The total amount of the credit memo.
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->container['amount'] = $amount;

        return $this;
    }

    /**
     * Gets applied_amount
     * @return double
     */
    public function getAppliedAmount()
    {
        return $this->container['applied_amount'];
    }

    /**
     * Sets applied_amount
     * @param double $applied_amount The applied amount of the credit memo.
     * @return $this
     */
    public function setAppliedAmount($applied_amount)
    {
        $this->container['applied_amount'] = $applied_amount;

        return $this;
    }

    /**
     * Gets auto_apply_upon_posting
     * @return bool
     */
    public function getAutoApplyUponPosting()
    {
        return $this->container['auto_apply_upon_posting'];
    }

    /**
     * Sets auto_apply_upon_posting
     * @param bool $auto_apply_upon_posting Whether the credit memo automatically applies to the invoice upon posting.
     * @return $this
     */
    public function setAutoApplyUponPosting($auto_apply_upon_posting)
    {
        $this->container['auto_apply_upon_posting'] = $auto_apply_upon_posting;

        return $this;
    }

    /**
     * Gets bill_to_contact_id
     * @return string
     */
    public function getBillToContactId()
    {
        return $this->container['bill_to_contact_id'];
    }

    /**
     * Sets bill_to_contact_id
     * @param string $bill_to_contact_id The ID of the bill-to contact associated with the credit memo. The value of this field is `null` if you have the [Flexible Billing Attributes](https://knowledgecenter.zuora.com/Billing/Subscriptions/Flexible_Billing_Attributes) feature disabled.
     * @return $this
     */
    public function setBillToContactId($bill_to_contact_id)
    {
        $this->container['bill_to_contact_id'] = $bill_to_contact_id;

        return $this;
    }

    /**
     * Gets cancelled_by_id
     * @return string
     */
    public function getCancelledById()
    {
        return $this->container['cancelled_by_id'];
    }

    /**
     * Sets cancelled_by_id
     * @param string $cancelled_by_id The ID of the Zuora user who cancelled the credit memo.
     * @return $this
     */
    public function setCancelledById($cancelled_by_id)
    {
        $this->container['cancelled_by_id'] = $cancelled_by_id;

        return $this;
    }

    /**
     * Gets cancelled_on
     * @return \DateTime
     */
    public function getCancelledOn()
    {
        return $this->container['cancelled_on'];
    }

    /**
     * Sets cancelled_on
     * @param \DateTime $cancelled_on The date and time when the credit memo was cancelled, in `yyyy-mm-dd hh:mm:ss` format.
     * @return $this
     */
    public function setCancelledOn($cancelled_on)
    {
        $this->container['cancelled_on'] = $cancelled_on;

        return $this;
    }

    /**
     * Gets comment
     * @return string
     */
    public function getComment()
    {
        return $this->container['comment'];
    }

    /**
     * Sets comment
     * @param string $comment Comments about the credit memo.
     * @return $this
     */
    public function setComment($comment)
    {
        $this->container['comment'] = $comment;

        return $this;
    }

    /**
     * Gets created_by_id
     * @return string
     */
    public function getCreatedById()
    {
        return $this->container['created_by_id'];
    }

    /**
     * Sets created_by_id
     * @param string $created_by_id The ID of the Zuora user who created the credit memo.
     * @return $this
     */
    public function setCreatedById($created_by_id)
    {
        $this->container['created_by_id'] = $created_by_id;

        return $this;
    }

    /**
     * Gets created_date
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->container['created_date'];
    }

    /**
     * Sets created_date
     * @param \DateTime $created_date The date and time when the credit memo was created, in `yyyy-mm-dd hh:mm:ss` format. For example, 2017-03-01 15:31:10.
     * @return $this
     */
    public function setCreatedDate($created_date)
    {
        $this->container['created_date'] = $created_date;

        return $this;
    }

    /**
     * Gets credit_memo_date
     * @return \DateTime
     */
    public function getCreditMemoDate()
    {
        return $this->container['credit_memo_date'];
    }

    /**
     * Sets credit_memo_date
     * @param \DateTime $credit_memo_date The date when the credit memo takes effect, in `yyyy-mm-dd` format. For example, 2017-05-20.
     * @return $this
     */
    public function setCreditMemoDate($credit_memo_date)
    {
        $this->container['credit_memo_date'] = $credit_memo_date;

        return $this;
    }

    /**
     * Gets currency
     * @return string
     */
    public function getCurrency()
    {
        return $this->container['currency'];
    }

    /**
     * Sets currency
     * @param string $currency A currency defined in the web-based UI administrative settings.
     * @return $this
     */
    public function setCurrency($currency)
    {
        $this->container['currency'] = $currency;

        return $this;
    }

    /**
     * Gets exclude_from_auto_apply_rules
     * @return bool
     */
    public function getExcludeFromAutoApplyRules()
    {
        return $this->container['exclude_from_auto_apply_rules'];
    }

    /**
     * Sets exclude_from_auto_apply_rules
     * @param bool $exclude_from_auto_apply_rules Whether the credit memo is excluded from the rule of automatically applying unapplied credit memos to invoices and debit memos during payment runs.
     * @return $this
     */
    public function setExcludeFromAutoApplyRules($exclude_from_auto_apply_rules)
    {
        $this->container['exclude_from_auto_apply_rules'] = $exclude_from_auto_apply_rules;

        return $this;
    }

    /**
     * Gets id
     * @return string
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     * @param string $id The unique ID of the credit memo.
     * @return $this
     */
    public function setId($id)
    {
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets latest_pdf_file_id
     * @return string
     */
    public function getLatestPdfFileId()
    {
        return $this->container['latest_pdf_file_id'];
    }

    /**
     * Sets latest_pdf_file_id
     * @param string $latest_pdf_file_id The ID of the latest PDF file generated for the credit memo.
     * @return $this
     */
    public function setLatestPdfFileId($latest_pdf_file_id)
    {
        $this->container['latest_pdf_file_id'] = $latest_pdf_file_id;

        return $this;
    }

    /**
     * Gets number
     * @return string
     */
    public function getNumber()
    {
        return $this->container['number'];
    }

    /**
     * Sets number
     * @param string $number The unique identification number of the credit memo.
     * @return $this
     */
    public function setNumber($number)
    {
        $this->container['number'] = $number;

        return $this;
    }

    /**
     * Gets posted_by_id
     * @return string
     */
    public function getPostedById()
    {
        return $this->container['posted_by_id'];
    }

    /**
     * Sets posted_by_id
     * @param string $posted_by_id The ID of the Zuora user who posted the credit memo.
     * @return $this
     */
    public function setPostedById($posted_by_id)
    {
        $this->container['posted_by_id'] = $posted_by_id;

        return $this;
    }

    /**
     * Gets posted_on
     * @return \DateTime
     */
    public function getPostedOn()
    {
        return $this->container['posted_on'];
    }

    /**
     * Sets posted_on
     * @param \DateTime $posted_on The date and time when the credit memo was posted, in `yyyy-mm-dd hh:mm:ss` format.
     * @return $this
     */
    public function setPostedOn($posted_on)
    {
        $this->container['posted_on'] = $posted_on;

        return $this;
    }

    /**
     * Gets reason_code
     * @return string
     */
    public function getReasonCode()
    {
        return $this->container['reason_code'];
    }

    /**
     * Sets reason_code
     * @param string $reason_code A code identifying the reason for the transaction. The value must be an existing reason code or empty.
     * @return $this
     */
    public function setReasonCode($reason_code)
    {
        $this->container['reason_code'] = $reason_code;

        return $this;
    }

    /**
     * Gets referred_invoice_id
     * @return string
     */
    public function getReferredInvoiceId()
    {
        return $this->container['referred_invoice_id'];
    }

    /**
     * Sets referred_invoice_id
     * @param string $referred_invoice_id The ID of a referred invoice.
     * @return $this
     */
    public function setReferredInvoiceId($referred_invoice_id)
    {
        $this->container['referred_invoice_id'] = $referred_invoice_id;

        return $this;
    }

    /**
     * Gets refund_amount
     * @return double
     */
    public function getRefundAmount()
    {
        return $this->container['refund_amount'];
    }

    /**
     * Sets refund_amount
     * @param double $refund_amount The amount of the refund on the credit memo.
     * @return $this
     */
    public function setRefundAmount($refund_amount)
    {
        $this->container['refund_amount'] = $refund_amount;

        return $this;
    }

    /**
     * Gets reversed
     * @return bool
     */
    public function getReversed()
    {
        return $this->container['reversed'];
    }

    /**
     * Sets reversed
     * @param bool $reversed Whether the credit memo is reversed.
     * @return $this
     */
    public function setReversed($reversed)
    {
        $this->container['reversed'] = $reversed;

        return $this;
    }

    /**
     * Gets sequence_set_id
     * @return string
     */
    public function getSequenceSetId()
    {
        return $this->container['sequence_set_id'];
    }

    /**
     * Sets sequence_set_id
     * @param string $sequence_set_id The ID of the sequence set associated with the credit memo. The value of this field is `null` if you have the [Flexible Billing Attributes](https://knowledgecenter.zuora.com/Billing/Subscriptions/Flexible_Billing_Attributes) feature disabled.
     * @return $this
     */
    public function setSequenceSetId($sequence_set_id)
    {
        $this->container['sequence_set_id'] = $sequence_set_id;

        return $this;
    }

    /**
     * Gets source
     * @return string
     */
    public function getSource()
    {
        return $this->container['source'];
    }

    /**
     * Sets source
     * @param string $source The source of the credit memo.  Possible values: - `BillRun`: The credit memo is generated by a bill run. - `API`: The credit memo is created by calling the [Invoice and collect](https://www.zuora.com/developer/api-references/api/operation/POST_TransactionInvoicePayment) operation, or by calling the Orders, Order Line Items, or Fulfillments API operations. - `ApiSubscribe`: The credit memo is created by calling the [Create subscription](https://www.zuora.com/developer/api-references/api/operation/POST_Subscription) and [Create account](https://www.zuora.com/developer/api-references/api/operation/POST_Account) operation. - `ApiAmend`: The credit memo is created by calling the [Update subscription](https://www.zuora.com/developer/api-references/api/operation/PUT_Subscription) operation. - `AdhocFromPrpc`: The credit memo is created from a product rate plan charge through the Zuora UI or by calling the [Create a credit memo from a charge](https://www.zuora.com/developer/api-references/api/operation/POST_CreditMemoFromPrpc) operation. - `AdhocFromInvoice`: The credit memo is created from an invoice or created by reversing an invoice. You can create a credit memo from an invoice through the Zuora UI or by calling the [Create credit memo from invoice](https://www.zuora.com/developer/api-references/api/operation/POST_CreditMemoFromInvoice) operation. You can create a credit memo by reversing an invoice through the Zuora UI or by calling the [Reverse invoice](https://www.zuora.com/developer/api-references/api/operation/PUT_ReverseInvoice) operation.
     * @return $this
     */
    public function setSource($source)
    {
        $this->container['source'] = $source;

        return $this;
    }

    /**
     * Gets source_id
     * @return string
     */
    public function getSourceId()
    {
        return $this->container['source_id'];
    }

    /**
     * Sets source_id
     * @param string $source_id The ID of the credit memo source.   If a credit memo is generated from a bill run, the value is the number of the corresponding bill run. Otherwise, the value is `null`.
     * @return $this
     */
    public function setSourceId($source_id)
    {
        $this->container['source_id'] = $source_id;

        return $this;
    }

    /**
     * Gets source_type
     * @return string
     */
    public function getSourceType()
    {
        return $this->container['source_type'];
    }

    /**
     * Sets source_type
     * @param string $source_type The type of the credit memo source.
     * @return $this
     */
    public function setSourceType($source_type)
    {
        $allowed_values = array('Subscription', 'Standalone', 'Invoice', 'Order', 'CreditMemo', 'Consolidation');
        if (!is_null($source_type) && (!in_array($source_type, $allowed_values))) {
            throw new \InvalidArgumentException("Invalid value for 'source_type', must be one of 'Subscription', 'Standalone', 'Invoice', 'Order', 'CreditMemo', 'Consolidation'");
        }
        $this->container['source_type'] = $source_type;

        return $this;
    }

    /**
     * Gets status
     * @return string
     */
    public function getStatus()
    {
        return $this->container['status'];
    }

    /**
     * Sets status
     * @param string $status The status of the credit memo.
     * @return $this
     */
    public function setStatus($status)
    {
        $allowed_values = array('Draft', 'Posted', 'Canceled', 'Error', 'PendingForTax', 'Generating', 'CancelInProgress');
        if (!is_null($status) && (!in_array($status, $allowed_values))) {
            throw new \InvalidArgumentException("Invalid value for 'status', must be one of 'Draft', 'Posted', 'Canceled', 'Error', 'PendingForTax', 'Generating', 'CancelInProgress'");
        }
        $this->container['status'] = $status;

        return $this;
    }

    /**
     * Gets target_date
     * @return \DateTime
     */
    public function getTargetDate()
    {
        return $this->container['target_date'];
    }

    /**
     * Sets target_date
     * @param \DateTime $target_date The target date for the credit memo, in `yyyy-mm-dd` format. For example, 2017-07-20.
     * @return $this
     */
    public function setTargetDate($target_date)
    {
        $this->container['target_date'] = $target_date;

        return $this;
    }

    /**
     * Gets tax_amount
     * @return double
     */
    public function getTaxAmount()
    {
        return $this->container['tax_amount'];
    }

    /**
     * Sets tax_amount
     * @param double $tax_amount The amount of taxation.
     * @return $this
     */
    public function setTaxAmount($tax_amount)
    {
        $this->container['tax_amount'] = $tax_amount;

        return $this;
    }

    /**
     * Gets tax_message
     * @return string
     */
    public function getTaxMessage()
    {
        return $this->container['tax_message'];
    }

    /**
     * Sets tax_message
     * @param string $tax_message The message about the status of tax calculation related to the credit memo. If tax calculation fails in one credit memo, this field displays the reason for the failure.
     * @return $this
     */
    public function setTaxMessage($tax_message)
    {
        $this->container['tax_message'] = $tax_message;

        return $this;
    }

    /**
     * Gets tax_status
     * @return string
     */
    public function getTaxStatus()
    {
        return $this->container['tax_status'];
    }

    /**
     * Sets tax_status
     * @param string $tax_status The status of tax calculation related to the credit memo. **Note**: This field is only applicable to tax calculation by third-party tax engines.
     * @return $this
     */
    public function setTaxStatus($tax_status)
    {
        $allowed_values = array('Complete', 'Error');
        if (!is_null($tax_status) && (!in_array($tax_status, $allowed_values))) {
            throw new \InvalidArgumentException("Invalid value for 'tax_status', must be one of 'Complete', 'Error'");
        }
        $this->container['tax_status'] = $tax_status;

        return $this;
    }

    /**
     * Gets total_tax_exempt_amount
     * @return double
     */
    public function getTotalTaxExemptAmount()
    {
        return $this->container['total_tax_exempt_amount'];
    }

    /**
     * Sets total_tax_exempt_amount
     * @param double $total_tax_exempt_amount The calculated tax amount excluded due to the exemption.
     * @return $this
     */
    public function setTotalTaxExemptAmount($total_tax_exempt_amount)
    {
        $this->container['total_tax_exempt_amount'] = $total_tax_exempt_amount;

        return $this;
    }

    /**
     * Gets transferred_to_accounting
     * @return string
     */
    public function getTransferredToAccounting()
    {
        return $this->container['transferred_to_accounting'];
    }

    /**
     * Sets transferred_to_accounting
     * @param string $transferred_to_accounting Whether the credit memo was transferred to an external accounting system. Use this field for integration with accounting systems, such as NetSuite.
     * @return $this
     */
    public function setTransferredToAccounting($transferred_to_accounting)
    {
        $allowed_values = array('Processing', 'Yes', 'No', 'Error', 'Ignore');
        if (!is_null($transferred_to_accounting) && (!in_array($transferred_to_accounting, $allowed_values))) {
            throw new \InvalidArgumentException("Invalid value for 'transferred_to_accounting', must be one of 'Processing', 'Yes', 'No', 'Error', 'Ignore'");
        }
        $this->container['transferred_to_accounting'] = $transferred_to_accounting;

        return $this;
    }

    /**
     * Gets unapplied_amount
     * @return double
     */
    public function getUnappliedAmount()
    {
        return $this->container['unapplied_amount'];
    }

    /**
     * Sets unapplied_amount
     * @param double $unapplied_amount The unapplied amount of the credit memo.
     * @return $this
     */
    public function setUnappliedAmount($unapplied_amount)
    {
        $this->container['unapplied_amount'] = $unapplied_amount;

        return $this;
    }

    /**
     * Gets updated_by_id
     * @return string
     */
    public function getUpdatedById()
    {
        return $this->container['updated_by_id'];
    }

    /**
     * Sets updated_by_id
     * @param string $updated_by_id The ID of the Zuora user who last updated the credit memo.
     * @return $this
     */
    public function setUpdatedById($updated_by_id)
    {
        $this->container['updated_by_id'] = $updated_by_id;

        return $this;
    }

    /**
     * Gets updated_date
     * @return \DateTime
     */
    public function getUpdatedDate()
    {
        return $this->container['updated_date'];
    }

    /**
     * Sets updated_date
     * @param \DateTime $updated_date The date and time when the credit memo was last updated, in `yyyy-mm-dd hh:mm:ss` format. For example, 2017-03-02 15:36:10.
     * @return $this
     */
    public function setUpdatedDate($updated_date)
    {
        $this->container['updated_date'] = $updated_date;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this));
    }
}


