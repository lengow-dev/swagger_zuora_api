<?php
/**
 * SubmitDataQueryRequest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swaagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Zuora API Reference
 *
 * # Introduction Welcome to the reference for the Zuora REST API!  <a href=\"http://en.wikipedia.org/wiki/REST_API\" target=\"_blank\">REST</a> is a web-service protocol that lends itself to rapid development by using everyday HTTP and JSON technology.  The Zuora REST API provides a broad set of operations and resources that:    * Enable Web Storefront integration from your website.   * Support self-service subscriber sign-ups and account management.   * Process revenue schedules through custom revenue rule models.   * Enable manipulation of most objects in the Zuora Object Model.      ## Endpoints      The Zuora REST API is provided via the following endpoints.   | Tenant              | Base URL for REST Endpoints |   |-------------------------|-------------------------|   |Production | https://rest.zuora.com/v1   |   |API Sandbox    | https://rest.apisandbox.zuora.com/v1|      The production endpoint provides access to your live user data. The API Sandbox tenant is a good place to test your code without affecting real-world data. To use it, you must be provisioned with an API Sandbox tenant - your Zuora representative can help you if needed.      ## Access to the API      If you have a Zuora tenant, you already have access to the API.      If you don't have a Zuora tenant, go to <a href=\" https://www.zuora.com/resource/zuora-test-drive\" target=\"_blank\">https://www.zuora.com/resource/zuora-test-drive</a> and sign up for a Production Test Drive tenant. The tenant comes with seed data, such as a sample product catalog.  We recommend that you <a href=\"https://knowledgecenter.zuora.com/CF_Users_and_Administrators/A_Administrator_Settings/Manage_Users/Create_an_API_User\" target=\"_blank\">create an API user</a> specifically for making API calls. Don't log in to the Zuora UI with this account. Logging in to the UI enables a security feature that periodically expires the account's password, which may eventually cause authentication failures with the API. Note that a user role does not have write access to Zuora REST services unless it has the API Write Access permission as described in those instructions.  # Authentication  There are three ways to authenticate:    * Use username and password. Include authentication with each request in the header:         * `apiAccessKeyId`      * `apiSecretAccessKey`     * `entityId` or `entityName` (Only for [Zuora Multi-entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity \"Multi-entity\"). See \"Entity Id and Entity Name\" below for more information.)   * Use an authorization cookie. The cookie authorizes the user to make calls to the REST API for the duration specified in  **Administration > Security Policies > Session timeout**. The cookie expiration time is reset with this duration after every call to the REST API. To obtain a cookie, call the REST  `connections` resource with the following API user information:         *   ID         *   password     *   entity Id or entity name (Only for [Zuora Multi-entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity \"Multi-entity\"). See \"Entity Id and Entity Name\" below for more information.)         * For CORS-enabled APIs only: Include a 'single-use' token in the request header, which re-authenticates the user with each request. See below for more details.  ## Entity Id and Entity Name  The `entityId` and `entityName` parameters are only used for [Zuora Multi-entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity \"Zuora Multi-entity\").   The  `entityId` and `entityName` parameters specify the Id and the [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name \"Introduction to Entity and Entity Hierarchy\") that you want to access, respectively. Note that you must have permission to access the entity.   You can specify either the `entityId` or `entityName` parameter in the authentication to access and view an entity.    * If both `entityId` and `entityName` are specified in the authentication, an error occurs.    * If neither `entityId` nor `entityName` is specified in the authentication, you will log in to the entity in which your user account is created.      To get the entity Id and entity name, you can use the GET Entities REST call. For more information, see [API User Authentication](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/A_Overview_of_Multi-entity#API_User_Authentication \"API User Authentication\").      ## Token Authentication for CORS-Enabled APIs      The CORS mechanism enables REST API calls to Zuora to be made directly from your customer's browser, with all credit card and security information transmitted directly to Zuora. This minimizes your PCI compliance burden, allows you to implement advanced validation on your payment forms, and  makes your payment forms look just like any other part of your website.    For security reasons, instead of using cookies, an API request via CORS uses **tokens** for authentication.  The token method of authentication is only designed for use with requests that must originate from your customer's browser; **it should  not be considered a replacement to the existing cookie authentication** mechanism.  See [Zuora CORS REST](https://knowledgecenter.zuora.com/DC_Developers/REST_API/A_REST_basics/G_CORS_REST \"Zuora CORS REST\") for details on how CORS works and how you can begin to implement customer calls to the Zuora REST APIs. See  [HMAC Signatures](https://www.zuora.com/developer/API-Reference/#operation/POSTHMACSignature \"HMAC Signatures\") for details on the HMAC method that returns the authentication token.  # Requests and Responses  ## Request IDs  As a general rule, when asked to supply a \"key\" for an account or subscription (accountKey, account-key, subscriptionKey, subscription-key), you can provide either the actual ID or  the number of the entity.  ## HTTP Request Body  Most of the parameters and data accompanying your requests will be contained in the body of the HTTP request.   The Zuora REST API accepts JSON in the HTTP request body. No other data format (e.g., XML) is supported.  ## Testing a Request  Use a third party client, such as [curl](https://curl.haxx.se \"curl\"), [Postman](https://www.getpostman.com \"Postman\"), or [Advanced REST Client](https://advancedrestclient.com \"Advanced REST Client\"), to test the Zuora REST API.  You can test the Zuora REST API from the Zuora API Sandbox or Production tenants. If connecting to Production, bear in mind that you are working with your live production data, not sample data or test data.  ## Testing with Credit Cards  Sooner or later it will probably be necessary to test some transactions that involve credit cards. For suggestions on how to handle this, see [Going Live With Your Payment Gateway](https://knowledgecenter.zuora.com/CB_Billing/M_Payment_Gateways/C_Managing_Payment_Gateways/B_Going_Live_Payment_Gateways#Testing_with_Credit_Cards \"C_Zuora_User_Guides/A_Billing_and_Payments/M_Payment_Gateways/C_Managing_Payment_Gateways/B_Going_Live_Payment_Gateways#Testing_with_Credit_Cards\" ).  ## Error Handling  Responses and error codes are detailed in [Responses and errors](https://knowledgecenter.zuora.com/DC_Developers/REST_API/A_REST_basics/3_Responses_and_errors \"Responses and errors\").  # Pagination  When retrieving information (using GET methods), the optional `pageSize` query parameter sets the maximum number of rows to return in a response. The maximum is `40`; larger values are treated as `40`. If this value is empty or invalid, `pageSize` typically defaults to `10`.  The default value for the maximum number of rows retrieved can be overridden at the method level.  If more rows are available, the response will include a `nextPage` element, which contains a URL for requesting the next page.  If this value is not provided, no more rows are available. No \"previous page\" element is explicitly provided; to support backward paging, use the previous call.  ## Array Size  For data items that are not paginated, the REST API supports arrays of up to 300 rows.  Thus, for instance, repeated pagination can retrieve thousands of customer accounts, but within any account an array of no more than 300 rate plans is returned.  # API Versions  The Zuora REST API are version controlled. Versioning ensures that Zuora REST API changes are backward compatible. Zuora uses a major and minor version nomenclature to manage changes. By specifying a version in a REST request, you can get expected responses regardless of future changes to the API.  ## Major Version  The major version number of the REST API appears in the REST URL. Currently, Zuora only supports the **v1** major version. For example, `POST https://rest.zuora.com/v1/subscriptions`.  ## Minor Version  Zuora uses minor versions for the REST API to control small changes. For example, a field in a REST method is deprecated and a new field is used to replace it.   Some fields in the REST methods are supported as of minor versions. If a field is not noted with a minor version, this field is available for all minor versions. If a field is noted with a minor version, this field is in version control. You must specify the supported minor version in the request header to process without an error.   If a field is in version control, it is either with a minimum minor version or a maximum minor version, or both of them. You can only use this field with the minor version between the minimum and the maximum minor versions. For example, the `invoiceCollect` field in the POST Subscription method is in version control and its maximum minor version is 189.0. You can only use this field with the minor version 189.0 or earlier.  If you specify a version number in the request header that is not supported, Zuora will use the minimum minor version of the REST API. In our REST API documentation, if a field or feature requires a minor version number, we note that in the field description.  You only need to specify the version number when you use the fields require a minor version. To specify the minor version, set the `zuora-version` parameter to the minor version number in the request header for the request call. For example, the `collect` field is in 196.0 minor version. If you want to use this field for the POST Subscription method, set the  `zuora-version` parameter to `196.0` in the request header. The `zuora-version` parameter is case sensitive.  For all the REST API fields, by default, if the minor version is not specified in the request header, Zuora will use the minimum minor version of the REST API to avoid breaking your integration.   ### Minor Version History  The supported minor versions are not serial. This section documents the changes made to each Zuora REST API minor version.  The following table lists the supported versions and the fields that have a Zuora REST API minor version.  | Fields         | Minor Version      | REST Methods    | Description | |:--------|:--------|:--------|:--------| | invoiceCollect | 189.0 and earlier  | [Create Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_Subscription \"Create Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\"); [Renew Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_RenewSubscription \"Renew Subscription\"); [Cancel Subscription](https://www.zuora.com/developer/api-reference/#operation/POSTSubscriptionCancellation \"Cancel Subscription\"); [Suspend Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionSuspend \"Suspend Subscription\"); [Resume Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionResume \"Resume Subscription\"); [Create Account](https://www.zuora.com/developer/API-Reference/#operation/POST_Account \"Create Account\")|Generates an invoice and collects a payment for a subscription. | | collect        | 196.0 and later    | [Create Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_Subscription \"Create Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\"); [Renew Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_RenewSubscription \"Renew Subscription\"); [Cancel Subscription](https://www.zuora.com/developer/api-reference/#operation/POSTSubscriptionCancellation \"Cancel Subscription\"); [Suspend Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionSuspend \"Suspend Subscription\"); [Resume Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionResume \"Resume Subscription\"); [Create Account](https://www.zuora.com/developer/API-Reference/#operation/POST_Account \"Create Account\")|Collects an automatic payment for a subscription. | | invoice | 196.0 and later| [Create Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_Subscription \"Create Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\"); [Renew Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_RenewSubscription \"Renew Subscription\"); [Cancel Subscription](https://www.zuora.com/developer/api-reference/#operation/POSTSubscriptionCancellation \"Cancel Subscription\"); [Suspend Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionSuspend \"Suspend Subscription\"); [Resume Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionResume \"Resume Subscription\"); [Create Account](https://www.zuora.com/developer/API-Reference/#operation/POST_Account \"Create Account\")|Generates an invoice for a subscription. | | invoiceTargetDate | 196.0 and earlier  | [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\") |Date through which charges are calculated on the invoice, as `yyyy-mm-dd`. | | targetDate | 207.0 and later | [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\") |Date through which charges are calculated on the invoice, as `yyyy-mm-dd`. | | includeExisting DraftInvoiceItems | 196.0 and earlier| [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") | Specifies whether to include draft invoice items in subscription previews. Specify it to be `true` (default) to include draft invoice items in the preview result. Specify it to be `false` to excludes draft invoice items in the preview result. | | includeExisting DraftDocItems | 207.0 and later  | [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") | Specifies whether to include draft invoice items in subscription previews. Specify it to be `true` (default) to include draft invoice items in the preview result. Specify it to be `false` to excludes draft invoice items in the preview result. | | previewType | 196.0 and earlier| [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") | The type of preview you will receive. The possible values are `InvoiceItem`(default), `ChargeMetrics`, and `InvoiceItemChargeMetrics`. | | previewType | 207.0 and later  | [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") | The type of preview you will receive. The possible values are `LegalDoc`(default), `ChargeMetrics`, and `LegalDocChargeMetrics`. |  #### Version 207.0 and Later  The response structure of the [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\") and [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") methods are changed. The following invoice related response fields are moved to the invoice container:    * amount   * amountWithoutTax   * taxAmount   * invoiceItems   * targetDate   * chargeMetrics  # Zuora Object Model  The following diagram presents a high-level view of the key Zuora objects. Click the image to open it in a new tab to resize it.  <a href=\"https://www.zuora.com/wp-content/uploads/2017/01/ZuoraERD.jpeg\" target=\"_blank\"><img src=\"https://www.zuora.com/wp-content/uploads/2017/01/ZuoraERD.jpeg\" alt=\"Zuora Object Model Diagram\"></a>
 *
 * OpenAPI spec version: 2017-04-14
 * Contact: docs@zuora.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;

/**
 * SubmitDataQueryRequest Class Doc Comment
 *
 * @category    Class
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class SubmitDataQueryRequest implements ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'SubmitDataQueryRequest';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = [
        'column_separator' => 'string',
        'compression' => 'string',
        'encryption_key' => 'string',
        'output' => '\Swagger\Client\Model\SubmitDataQueryRequestOutput',
        'output_format' => 'string',
        'query' => 'string',
        'read_deleted' => 'bool',
        'source_data' => 'string',
        'use_index_join' => 'bool'
    ];

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = [
        'column_separator' => 'columnSeparator',
        'compression' => 'compression',
        'encryption_key' => 'encryptionKey',
        'output' => 'output',
        'output_format' => 'outputFormat',
        'query' => 'query',
        'read_deleted' => 'readDeleted',
        'source_data' => 'sourceData',
        'use_index_join' => 'useIndexJoin'
    ];


    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = [
        'column_separator' => 'setColumnSeparator',
        'compression' => 'setCompression',
        'encryption_key' => 'setEncryptionKey',
        'output' => 'setOutput',
        'output_format' => 'setOutputFormat',
        'query' => 'setQuery',
        'read_deleted' => 'setReadDeleted',
        'source_data' => 'setSourceData',
        'use_index_join' => 'setUseIndexJoin'
    ];


    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = [
        'column_separator' => 'getColumnSeparator',
        'compression' => 'getCompression',
        'encryption_key' => 'getEncryptionKey',
        'output' => 'getOutput',
        'output_format' => 'getOutputFormat',
        'query' => 'getQuery',
        'read_deleted' => 'getReadDeleted',
        'source_data' => 'getSourceData',
        'use_index_join' => 'getUseIndexJoin'
    ];

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    public static function setters()
    {
        return self::$setters;
    }

    public static function getters()
    {
        return self::$getters;
    }

    const COMPRESSION_NONE = 'NONE';
    const COMPRESSION_GZIP = 'GZIP';
    const COMPRESSION_ZIP = 'ZIP';
    const OUTPUT_FORMAT_JSON = 'JSON';
    const OUTPUT_FORMAT_CSV = 'CSV';
    const OUTPUT_FORMAT_TSV = 'TSV';
    const OUTPUT_FORMAT_DSV = 'DSV';
    const SOURCE_DATA_LIVE = 'LIVE';
    

    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public function getCompressionAllowableValues()
    {
        return [
            self::COMPRESSION_NONE,
            self::COMPRESSION_GZIP,
            self::COMPRESSION_ZIP,
        ];
    }
    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public function getOutputFormatAllowableValues()
    {
        return [
            self::OUTPUT_FORMAT_JSON,
            self::OUTPUT_FORMAT_CSV,
            self::OUTPUT_FORMAT_TSV,
            self::OUTPUT_FORMAT_DSV,
        ];
    }
    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public function getSourceDataAllowableValues()
    {
        return [
            self::SOURCE_DATA_LIVE,
        ];
    }
    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['column_separator'] = isset($data['column_separator']) ? $data['column_separator'] : null;
        $this->container['compression'] = isset($data['compression']) ? $data['compression'] : null;
        $this->container['encryption_key'] = isset($data['encryption_key']) ? $data['encryption_key'] : null;
        $this->container['output'] = isset($data['output']) ? $data['output'] : null;
        $this->container['output_format'] = isset($data['output_format']) ? $data['output_format'] : null;
        $this->container['query'] = isset($data['query']) ? $data['query'] : null;
        $this->container['read_deleted'] = isset($data['read_deleted']) ? $data['read_deleted'] : false;
        $this->container['source_data'] = isset($data['source_data']) ? $data['source_data'] : null;
        $this->container['use_index_join'] = isset($data['use_index_join']) ? $data['use_index_join'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];

        if ($this->container['compression'] === null) {
            $invalid_properties[] = "'compression' can't be null";
        }
        $allowed_values = ["NONE", "GZIP", "ZIP"];
        if (!in_array($this->container['compression'], $allowed_values)) {
            $invalid_properties[] = "invalid value for 'compression', must be one of 'NONE', 'GZIP', 'ZIP'.";
        }

        if ($this->container['output_format'] === null) {
            $invalid_properties[] = "'output_format' can't be null";
        }
        $allowed_values = ["JSON", "CSV", "TSV", "DSV"];
        if (!in_array($this->container['output_format'], $allowed_values)) {
            $invalid_properties[] = "invalid value for 'output_format', must be one of 'JSON', 'CSV', 'TSV', 'DSV'.";
        }

        if ($this->container['query'] === null) {
            $invalid_properties[] = "'query' can't be null";
        }
        $allowed_values = ["LIVE"];
        if (!in_array($this->container['source_data'], $allowed_values)) {
            $invalid_properties[] = "invalid value for 'source_data', must be one of 'LIVE'.";
        }

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        if ($this->container['compression'] === null) {
            return false;
        }
        $allowed_values = ["NONE", "GZIP", "ZIP"];
        if (!in_array($this->container['compression'], $allowed_values)) {
            return false;
        }
        if ($this->container['output_format'] === null) {
            return false;
        }
        $allowed_values = ["JSON", "CSV", "TSV", "DSV"];
        if (!in_array($this->container['output_format'], $allowed_values)) {
            return false;
        }
        if ($this->container['query'] === null) {
            return false;
        }
        $allowed_values = ["LIVE"];
        if (!in_array($this->container['source_data'], $allowed_values)) {
            return false;
        }
        return true;
    }


    /**
     * Gets column_separator
     * @return string
     */
    public function getColumnSeparator()
    {
        return $this->container['column_separator'];
    }

    /**
     * Sets column_separator
     * @param string $column_separator The column separator. Only applicable if the `outputFormat` is `DSV`.
     * @return $this
     */
    public function setColumnSeparator($column_separator)
    {
        $this->container['column_separator'] = $column_separator;

        return $this;
    }

    /**
     * Gets compression
     * @return string
     */
    public function getCompression()
    {
        return $this->container['compression'];
    }

    /**
     * Sets compression
     * @param string $compression Specifies whether Zuora compresses the query results.
     * @return $this
     */
    public function setCompression($compression)
    {
        $allowed_values = array('NONE', 'GZIP', 'ZIP');
        if ((!in_array($compression, $allowed_values))) {
            throw new \InvalidArgumentException("Invalid value for 'compression', must be one of 'NONE', 'GZIP', 'ZIP'");
        }
        $this->container['compression'] = $compression;

        return $this;
    }

    /**
     * Gets encryption_key
     * @return string
     */
    public function getEncryptionKey()
    {
        return $this->container['encryption_key'];
    }

    /**
     * Sets encryption_key
     * @param string $encryption_key Base-64 encoded public key of an RSA key-pair.   Note that Data Query only supports 1024-bit RSA keys.  If you set this field, Zuora encrypts the query results using the provided public key. You must use the corresponding private key to decrypt the query results.
     * @return $this
     */
    public function setEncryptionKey($encryption_key)
    {
        $this->container['encryption_key'] = $encryption_key;

        return $this;
    }

    /**
     * Gets output
     * @return \Swagger\Client\Model\SubmitDataQueryRequestOutput
     */
    public function getOutput()
    {
        return $this->container['output'];
    }

    /**
     * Sets output
     * @param \Swagger\Client\Model\SubmitDataQueryRequestOutput $output
     * @return $this
     */
    public function setOutput($output)
    {
        $this->container['output'] = $output;

        return $this;
    }

    /**
     * Gets output_format
     * @return string
     */
    public function getOutputFormat()
    {
        return $this->container['output_format'];
    }

    /**
     * Sets output_format
     * @param string $output_format Specifies the format of the query results.  * `JSON` - Each row in the query results will be a JSON object. The format of the query result file is [JSON Lines](http://jsonlines.org/). * `CSV` - Each row in the query results will be a comma-separated list of values. * `TSV` - Each row in the query results will be a tab-separated list of values. * `DSV` - Pass any character as your custom delimiter into the `columnSeparator` field.
     * @return $this
     */
    public function setOutputFormat($output_format)
    {
        $allowed_values = array('JSON', 'CSV', 'TSV', 'DSV');
        if ((!in_array($output_format, $allowed_values))) {
            throw new \InvalidArgumentException("Invalid value for 'output_format', must be one of 'JSON', 'CSV', 'TSV', 'DSV'");
        }
        $this->container['output_format'] = $output_format;

        return $this;
    }

    /**
     * Gets query
     * @return string
     */
    public function getQuery()
    {
        return $this->container['query'];
    }

    /**
     * Sets query
     * @param string $query The query to perform. See [SQL Queries in Data Query](https://knowledgecenter.zuora.com/DC_Developers/BA_Data_Query/BA_SQL_Queries_in_Data_Query) for more information.
     * @return $this
     */
    public function setQuery($query)
    {
        $this->container['query'] = $query;

        return $this;
    }

    /**
     * Gets read_deleted
     * @return bool
     */
    public function getReadDeleted()
    {
        return $this->container['read_deleted'];
    }

    /**
     * Sets read_deleted
     * @param bool $read_deleted Indicates whether the query will retrieve only the deleted record. If `readDeleted` is set to `false` or it is not included in the request body, the query will retrieve only the non-deleted records. If it is set to `true`, only the deleted records will be retrieved.  If you select the `deleted` column in the `query` field, both non-deleted and deleted records will be retrieved regardless of the value in the `readDeleted` field.  Note that Data Query is subject to Zuora Data Retention Policy. The retention period of deleted data is 30 days. You can only retrieve deleted data for 30 days through Data Query.
     * @return $this
     */
    public function setReadDeleted($read_deleted)
    {
        $this->container['read_deleted'] = $read_deleted;

        return $this;
    }

    /**
     * Gets source_data
     * @return string
     */
    public function getSourceData()
    {
        return $this->container['source_data'];
    }

    /**
     * Sets source_data
     * @param string $source_data Specify the source that data queries run against. Currently, the only valid value is `LIVE`, representing the live transactional databases at Zuora (Data Query Live).
     * @return $this
     */
    public function setSourceData($source_data)
    {
        $allowed_values = array('LIVE');
        if (!is_null($source_data) && (!in_array($source_data, $allowed_values))) {
            throw new \InvalidArgumentException("Invalid value for 'source_data', must be one of 'LIVE'");
        }
        $this->container['source_data'] = $source_data;

        return $this;
    }

    /**
     * Gets use_index_join
     * @return bool
     */
    public function getUseIndexJoin()
    {
        return $this->container['use_index_join'];
    }

    /**
     * Sets use_index_join
     * @param bool $use_index_join Indicates whether to use Index Join. Index join is useful when you have a specific reference value in your WHERE clause to index another large table by. See [Use Index Join](https://knowledgecenter.zuora.com/DC_Developers/BA_Data_Query/Best_practices_of_Data_Query#Use_Index_Join) for more information.
     * @return $this
     */
    public function setUseIndexJoin($use_index_join)
    {
        $this->container['use_index_join'] = $use_index_join;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this));
    }
}


