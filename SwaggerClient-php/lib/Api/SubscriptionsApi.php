<?php
/**
 * SubscriptionsApi
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Zuora API Reference
 *
 * # Introduction Welcome to the reference for the Zuora REST API!  <a href=\"http://en.wikipedia.org/wiki/REST_API\" target=\"_blank\">REST</a> is a web-service protocol that lends itself to rapid development by using everyday HTTP and JSON technology.  The Zuora REST API provides a broad set of operations and resources that:    * Enable Web Storefront integration from your website.   * Support self-service subscriber sign-ups and account management.   * Process revenue schedules through custom revenue rule models.   * Enable manipulation of most objects in the Zuora Object Model.      ## Endpoints      The Zuora REST API is provided via the following endpoints.   | Tenant              | Base URL for REST Endpoints |   |-------------------------|-------------------------|   |Production | https://rest.zuora.com/v1   |   |API Sandbox    | https://rest.apisandbox.zuora.com/v1|      The production endpoint provides access to your live user data. The API Sandbox tenant is a good place to test your code without affecting real-world data. To use it, you must be provisioned with an API Sandbox tenant - your Zuora representative can help you if needed.      ## Access to the API      If you have a Zuora tenant, you already have access to the API.      If you don't have a Zuora tenant, go to <a href=\" https://www.zuora.com/resource/zuora-test-drive\" target=\"_blank\">https://www.zuora.com/resource/zuora-test-drive</a> and sign up for a Production Test Drive tenant. The tenant comes with seed data, such as a sample product catalog.  We recommend that you <a href=\"https://knowledgecenter.zuora.com/CF_Users_and_Administrators/A_Administrator_Settings/Manage_Users/Create_an_API_User\" target=\"_blank\">create an API user</a> specifically for making API calls. Don't log in to the Zuora UI with this account. Logging in to the UI enables a security feature that periodically expires the account's password, which may eventually cause authentication failures with the API. Note that a user role does not have write access to Zuora REST services unless it has the API Write Access permission as described in those instructions.  # Authentication  There are three ways to authenticate:    * Use username and password. Include authentication with each request in the header:         * `apiAccessKeyId`      * `apiSecretAccessKey`     * `entityId` or `entityName` (Only for [Zuora Multi-entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity \"Multi-entity\"). See \"Entity Id and Entity Name\" below for more information.)   * Use an authorization cookie. The cookie authorizes the user to make calls to the REST API for the duration specified in  **Administration > Security Policies > Session timeout**. The cookie expiration time is reset with this duration after every call to the REST API. To obtain a cookie, call the REST  `connections` resource with the following API user information:         *   ID         *   password     *   entity Id or entity name (Only for [Zuora Multi-entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity \"Multi-entity\"). See \"Entity Id and Entity Name\" below for more information.)         * For CORS-enabled APIs only: Include a 'single-use' token in the request header, which re-authenticates the user with each request. See below for more details.  ## Entity Id and Entity Name  The `entityId` and `entityName` parameters are only used for [Zuora Multi-entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity \"Zuora Multi-entity\").   The  `entityId` and `entityName` parameters specify the Id and the [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name \"Introduction to Entity and Entity Hierarchy\") that you want to access, respectively. Note that you must have permission to access the entity.   You can specify either the `entityId` or `entityName` parameter in the authentication to access and view an entity.    * If both `entityId` and `entityName` are specified in the authentication, an error occurs.    * If neither `entityId` nor `entityName` is specified in the authentication, you will log in to the entity in which your user account is created.      To get the entity Id and entity name, you can use the GET Entities REST call. For more information, see [API User Authentication](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/A_Overview_of_Multi-entity#API_User_Authentication \"API User Authentication\").      ## Token Authentication for CORS-Enabled APIs      The CORS mechanism enables REST API calls to Zuora to be made directly from your customer's browser, with all credit card and security information transmitted directly to Zuora. This minimizes your PCI compliance burden, allows you to implement advanced validation on your payment forms, and  makes your payment forms look just like any other part of your website.    For security reasons, instead of using cookies, an API request via CORS uses **tokens** for authentication.  The token method of authentication is only designed for use with requests that must originate from your customer's browser; **it should  not be considered a replacement to the existing cookie authentication** mechanism.  See [Zuora CORS REST](https://knowledgecenter.zuora.com/DC_Developers/REST_API/A_REST_basics/G_CORS_REST \"Zuora CORS REST\") for details on how CORS works and how you can begin to implement customer calls to the Zuora REST APIs. See  [HMAC Signatures](https://www.zuora.com/developer/API-Reference/#operation/POSTHMACSignature \"HMAC Signatures\") for details on the HMAC method that returns the authentication token.  # Requests and Responses  ## Request IDs  As a general rule, when asked to supply a \"key\" for an account or subscription (accountKey, account-key, subscriptionKey, subscription-key), you can provide either the actual ID or  the number of the entity.  ## HTTP Request Body  Most of the parameters and data accompanying your requests will be contained in the body of the HTTP request.   The Zuora REST API accepts JSON in the HTTP request body. No other data format (e.g., XML) is supported.  ## Testing a Request  Use a third party client, such as [curl](https://curl.haxx.se \"curl\"), [Postman](https://www.getpostman.com \"Postman\"), or [Advanced REST Client](https://advancedrestclient.com \"Advanced REST Client\"), to test the Zuora REST API.  You can test the Zuora REST API from the Zuora API Sandbox or Production tenants. If connecting to Production, bear in mind that you are working with your live production data, not sample data or test data.  ## Testing with Credit Cards  Sooner or later it will probably be necessary to test some transactions that involve credit cards. For suggestions on how to handle this, see [Going Live With Your Payment Gateway](https://knowledgecenter.zuora.com/CB_Billing/M_Payment_Gateways/C_Managing_Payment_Gateways/B_Going_Live_Payment_Gateways#Testing_with_Credit_Cards \"C_Zuora_User_Guides/A_Billing_and_Payments/M_Payment_Gateways/C_Managing_Payment_Gateways/B_Going_Live_Payment_Gateways#Testing_with_Credit_Cards\" ).  ## Error Handling  Responses and error codes are detailed in [Responses and errors](https://knowledgecenter.zuora.com/DC_Developers/REST_API/A_REST_basics/3_Responses_and_errors \"Responses and errors\").  # Pagination  When retrieving information (using GET methods), the optional `pageSize` query parameter sets the maximum number of rows to return in a response. The maximum is `40`; larger values are treated as `40`. If this value is empty or invalid, `pageSize` typically defaults to `10`.  The default value for the maximum number of rows retrieved can be overridden at the method level.  If more rows are available, the response will include a `nextPage` element, which contains a URL for requesting the next page.  If this value is not provided, no more rows are available. No \"previous page\" element is explicitly provided; to support backward paging, use the previous call.  ## Array Size  For data items that are not paginated, the REST API supports arrays of up to 300 rows.  Thus, for instance, repeated pagination can retrieve thousands of customer accounts, but within any account an array of no more than 300 rate plans is returned.  # API Versions  The Zuora REST API are version controlled. Versioning ensures that Zuora REST API changes are backward compatible. Zuora uses a major and minor version nomenclature to manage changes. By specifying a version in a REST request, you can get expected responses regardless of future changes to the API.  ## Major Version  The major version number of the REST API appears in the REST URL. Currently, Zuora only supports the **v1** major version. For example, `POST https://rest.zuora.com/v1/subscriptions`.  ## Minor Version  Zuora uses minor versions for the REST API to control small changes. For example, a field in a REST method is deprecated and a new field is used to replace it.   Some fields in the REST methods are supported as of minor versions. If a field is not noted with a minor version, this field is available for all minor versions. If a field is noted with a minor version, this field is in version control. You must specify the supported minor version in the request header to process without an error.   If a field is in version control, it is either with a minimum minor version or a maximum minor version, or both of them. You can only use this field with the minor version between the minimum and the maximum minor versions. For example, the `invoiceCollect` field in the POST Subscription method is in version control and its maximum minor version is 189.0. You can only use this field with the minor version 189.0 or earlier.  If you specify a version number in the request header that is not supported, Zuora will use the minimum minor version of the REST API. In our REST API documentation, if a field or feature requires a minor version number, we note that in the field description.  You only need to specify the version number when you use the fields require a minor version. To specify the minor version, set the `zuora-version` parameter to the minor version number in the request header for the request call. For example, the `collect` field is in 196.0 minor version. If you want to use this field for the POST Subscription method, set the  `zuora-version` parameter to `196.0` in the request header. The `zuora-version` parameter is case sensitive.  For all the REST API fields, by default, if the minor version is not specified in the request header, Zuora will use the minimum minor version of the REST API to avoid breaking your integration.   ### Minor Version History  The supported minor versions are not serial. This section documents the changes made to each Zuora REST API minor version.  The following table lists the supported versions and the fields that have a Zuora REST API minor version.  | Fields         | Minor Version      | REST Methods    | Description | |:--------|:--------|:--------|:--------| | invoiceCollect | 189.0 and earlier  | [Create Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_Subscription \"Create Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\"); [Renew Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_RenewSubscription \"Renew Subscription\"); [Cancel Subscription](https://www.zuora.com/developer/api-reference/#operation/POSTSubscriptionCancellation \"Cancel Subscription\"); [Suspend Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionSuspend \"Suspend Subscription\"); [Resume Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionResume \"Resume Subscription\"); [Create Account](https://www.zuora.com/developer/API-Reference/#operation/POST_Account \"Create Account\")|Generates an invoice and collects a payment for a subscription. | | collect        | 196.0 and later    | [Create Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_Subscription \"Create Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\"); [Renew Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_RenewSubscription \"Renew Subscription\"); [Cancel Subscription](https://www.zuora.com/developer/api-reference/#operation/POSTSubscriptionCancellation \"Cancel Subscription\"); [Suspend Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionSuspend \"Suspend Subscription\"); [Resume Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionResume \"Resume Subscription\"); [Create Account](https://www.zuora.com/developer/API-Reference/#operation/POST_Account \"Create Account\")|Collects an automatic payment for a subscription. | | invoice | 196.0 and later| [Create Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_Subscription \"Create Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\"); [Renew Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_RenewSubscription \"Renew Subscription\"); [Cancel Subscription](https://www.zuora.com/developer/api-reference/#operation/POSTSubscriptionCancellation \"Cancel Subscription\"); [Suspend Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionSuspend \"Suspend Subscription\"); [Resume Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionResume \"Resume Subscription\"); [Create Account](https://www.zuora.com/developer/API-Reference/#operation/POST_Account \"Create Account\")|Generates an invoice for a subscription. | | invoiceTargetDate | 196.0 and earlier  | [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\") |Date through which charges are calculated on the invoice, as `yyyy-mm-dd`. | | targetDate | 207.0 and later | [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\") |Date through which charges are calculated on the invoice, as `yyyy-mm-dd`. | | includeExisting DraftInvoiceItems | 196.0 and earlier| [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") | Specifies whether to include draft invoice items in subscription previews. Specify it to be `true` (default) to include draft invoice items in the preview result. Specify it to be `false` to excludes draft invoice items in the preview result. | | includeExisting DraftDocItems | 207.0 and later  | [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") | Specifies whether to include draft invoice items in subscription previews. Specify it to be `true` (default) to include draft invoice items in the preview result. Specify it to be `false` to excludes draft invoice items in the preview result. | | previewType | 196.0 and earlier| [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") | The type of preview you will receive. The possible values are `InvoiceItem`(default), `ChargeMetrics`, and `InvoiceItemChargeMetrics`. | | previewType | 207.0 and later  | [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") | The type of preview you will receive. The possible values are `LegalDoc`(default), `ChargeMetrics`, and `LegalDocChargeMetrics`. |  #### Version 207.0 and Later  The response structure of the [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\") and [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") methods are changed. The following invoice related response fields are moved to the invoice container:    * amount   * amountWithoutTax   * taxAmount   * invoiceItems   * targetDate   * chargeMetrics  # Zuora Object Model  The following diagram presents a high-level view of the key Zuora objects. Click the image to open it in a new tab to resize it.  <a href=\"https://www.zuora.com/wp-content/uploads/2017/01/ZuoraERD.jpeg\" target=\"_blank\"><img src=\"https://www.zuora.com/wp-content/uploads/2017/01/ZuoraERD.jpeg\" alt=\"Zuora Object Model Diagram\"></a>
 *
 * OpenAPI spec version: 2017-04-14
 * Contact: docs@zuora.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Api;

use \Swagger\Client\ApiClient;
use \Swagger\Client\ApiException;
use \Swagger\Client\Configuration;
use \Swagger\Client\ObjectSerializer;

/**
 * SubscriptionsApi Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class SubscriptionsApi
{
    /**
     * API Client
     *
     * @var \Swagger\Client\ApiClient instance of the ApiClient
     */
    protected $apiClient;

    /**
     * Constructor
     *
     * @param \Swagger\Client\ApiClient|null $apiClient The api client to use
     */
    public function __construct(\Swagger\Client\ApiClient $apiClient = null)
    {
        if ($apiClient === null) {
            $apiClient = new ApiClient();
        }

        $this->apiClient = $apiClient;
    }

    /**
     * Get API client
     *
     * @return \Swagger\Client\ApiClient get the API client
     */
    public function getApiClient()
    {
        return $this->apiClient;
    }

    /**
     * Set the API client
     *
     * @param \Swagger\Client\ApiClient $apiClient set the API client
     *
     * @return SubscriptionsApi
     */
    public function setApiClient(\Swagger\Client\ApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
        return $this;
    }

    /**
     * Operation gETSubscriptionsByAccount
     *
     * Get subscriptions by account
     *
     * @param string $account_key Possible values are: * an account number * an account ID (required)
     * @param string $entity_id The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $entity_name The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $charge_detail The segmented rate plan charges.  When an amendment results in a change to a charge, Zuora creates a segmented rate plan charge. Use this field to track segment charges.  Possible values are:  * __last-segment__: (Default) The last rate plan charge on the subscription. The last rate plan charge is the last one in the order of time on the subscription rather than the most recent changed charge on the subscription.  * __current-segment__: The segmented charge that is active on today’s date (effectiveStartDate &lt;&#x3D; today’s date &lt; effectiveEndDate).    * __all-segments__: All the segmented charges. The &#x60;chargeSegments&#x60; field is returned in the response. The &#x60;chargeSegments&#x60; field contains an array of the charge information for all the charge segments.   * __specific-segment&amp;as-of-date&#x3D;date__: The segmented charge that is active on a date you specified (effectiveStartDate &lt;&#x3D; specific date &lt; effectiveEndDate). The format of the date is yyyy-mm-dd. (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\GETSubscriptionWrapper
     */
    public function gETSubscriptionsByAccount($account_key, $entity_id = null, $entity_name = null, $charge_detail = null)
    {
        list($response) = $this->gETSubscriptionsByAccountWithHttpInfo($account_key, $entity_id, $entity_name, $charge_detail);
        return $response;
    }

    /**
     * Operation gETSubscriptionsByAccountWithHttpInfo
     *
     * Get subscriptions by account
     *
     * @param string $account_key Possible values are: * an account number * an account ID (required)
     * @param string $entity_id The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $entity_name The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $charge_detail The segmented rate plan charges.  When an amendment results in a change to a charge, Zuora creates a segmented rate plan charge. Use this field to track segment charges.  Possible values are:  * __last-segment__: (Default) The last rate plan charge on the subscription. The last rate plan charge is the last one in the order of time on the subscription rather than the most recent changed charge on the subscription.  * __current-segment__: The segmented charge that is active on today’s date (effectiveStartDate &lt;&#x3D; today’s date &lt; effectiveEndDate).    * __all-segments__: All the segmented charges. The &#x60;chargeSegments&#x60; field is returned in the response. The &#x60;chargeSegments&#x60; field contains an array of the charge information for all the charge segments.   * __specific-segment&amp;as-of-date&#x3D;date__: The segmented charge that is active on a date you specified (effectiveStartDate &lt;&#x3D; specific date &lt; effectiveEndDate). The format of the date is yyyy-mm-dd. (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return array of \Swagger\Client\Model\GETSubscriptionWrapper, HTTP status code, HTTP response headers (array of strings)
     */
    public function gETSubscriptionsByAccountWithHttpInfo($account_key, $entity_id = null, $entity_name = null, $charge_detail = null)
    {
        // verify the required parameter 'account_key' is set
        if ($account_key === null) {
            throw new \InvalidArgumentException('Missing the required parameter $account_key when calling gETSubscriptionsByAccount');
        }
        // parse inputs
        $resourcePath = "/v1/subscriptions/accounts/{account-key}";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json; charset=utf-8']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json; charset=utf-8']);

        // query params
        if ($charge_detail !== null) {
            $queryParams['charge-detail'] = $this->apiClient->getSerializer()->toQueryValue($charge_detail);
        }
        // header params
        if ($entity_id !== null) {
            $headerParams['entityId'] = $this->apiClient->getSerializer()->toHeaderValue($entity_id);
        }
        // header params
        if ($entity_name !== null) {
            $headerParams['entityName'] = $this->apiClient->getSerializer()->toHeaderValue($entity_name);
        }
        // path params
        if ($account_key !== null) {
            $resourcePath = str_replace(
                "{" . "account-key" . "}",
                $this->apiClient->getSerializer()->toPathValue($account_key),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Swagger\Client\Model\GETSubscriptionWrapper',
                '/v1/subscriptions/accounts/{account-key}'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, '\Swagger\Client\Model\GETSubscriptionWrapper', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Swagger\Client\Model\GETSubscriptionWrapper', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation gETSubscriptionsByKey
     *
     * Get subscriptions by key
     *
     * @param string $subscription_key Possible values are:   * a subscription number   * a subscription ID (required)
     * @param string $entity_id The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $entity_name The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $charge_detail The segmented rate plan charges. When an amendment results in a change to a charge, Zuora creates a segmented rate plan charge. Use this field to track segment charges.  Possible values are:   * __last-segment__: (Default) The last rate plan charge on the subscription. The last rate plan charge is the last one in the order of time on the subscription rather than the most recent changed charge on the subscription.  * __current-segment__: The segmented charge that is active on today’s date (effectiveStartDate &lt;&#x3D; today’s date &lt; effectiveEndDate).    * __all-segments__: All the segmented charges. The &#x60;chargeSegments&#x60; field is returned in the response. The &#x60;chargeSegments&#x60; field contains an array of the charge information for all the charge segments.   * __specific-segment&amp;as-of-date&#x3D;date__: The segmented charge that is active on a date you specified (effectiveStartDate &lt;&#x3D; specific date &lt; effectiveEndDate). The format of the date is yyyy-mm-dd. (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\GETSubscriptionTypeWithSuccess
     */
    public function gETSubscriptionsByKey($subscription_key, $entity_id = null, $entity_name = null, $charge_detail = null)
    {
        list($response) = $this->gETSubscriptionsByKeyWithHttpInfo($subscription_key, $entity_id, $entity_name, $charge_detail);
        return $response;
    }

    /**
     * Operation gETSubscriptionsByKeyWithHttpInfo
     *
     * Get subscriptions by key
     *
     * @param string $subscription_key Possible values are:   * a subscription number   * a subscription ID (required)
     * @param string $entity_id The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $entity_name The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $charge_detail The segmented rate plan charges. When an amendment results in a change to a charge, Zuora creates a segmented rate plan charge. Use this field to track segment charges.  Possible values are:   * __last-segment__: (Default) The last rate plan charge on the subscription. The last rate plan charge is the last one in the order of time on the subscription rather than the most recent changed charge on the subscription.  * __current-segment__: The segmented charge that is active on today’s date (effectiveStartDate &lt;&#x3D; today’s date &lt; effectiveEndDate).    * __all-segments__: All the segmented charges. The &#x60;chargeSegments&#x60; field is returned in the response. The &#x60;chargeSegments&#x60; field contains an array of the charge information for all the charge segments.   * __specific-segment&amp;as-of-date&#x3D;date__: The segmented charge that is active on a date you specified (effectiveStartDate &lt;&#x3D; specific date &lt; effectiveEndDate). The format of the date is yyyy-mm-dd. (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return array of \Swagger\Client\Model\GETSubscriptionTypeWithSuccess, HTTP status code, HTTP response headers (array of strings)
     */
    public function gETSubscriptionsByKeyWithHttpInfo($subscription_key, $entity_id = null, $entity_name = null, $charge_detail = null)
    {
        // verify the required parameter 'subscription_key' is set
        if ($subscription_key === null) {
            throw new \InvalidArgumentException('Missing the required parameter $subscription_key when calling gETSubscriptionsByKey');
        }
        // parse inputs
        $resourcePath = "/v1/subscriptions/{subscription-key}";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json; charset=utf-8']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json; charset=utf-8']);

        // query params
        if ($charge_detail !== null) {
            $queryParams['charge-detail'] = $this->apiClient->getSerializer()->toQueryValue($charge_detail);
        }
        // header params
        if ($entity_id !== null) {
            $headerParams['entityId'] = $this->apiClient->getSerializer()->toHeaderValue($entity_id);
        }
        // header params
        if ($entity_name !== null) {
            $headerParams['entityName'] = $this->apiClient->getSerializer()->toHeaderValue($entity_name);
        }
        // path params
        if ($subscription_key !== null) {
            $resourcePath = str_replace(
                "{" . "subscription-key" . "}",
                $this->apiClient->getSerializer()->toPathValue($subscription_key),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Swagger\Client\Model\GETSubscriptionTypeWithSuccess',
                '/v1/subscriptions/{subscription-key}'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, '\Swagger\Client\Model\GETSubscriptionTypeWithSuccess', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Swagger\Client\Model\GETSubscriptionTypeWithSuccess', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation gETSubscriptionsByKeyAndVersion
     *
     * Get subscriptions by key and version
     *
     * @param string $subscription_key Subscription number. For example, A-S00000135. (required)
     * @param string $version Subscription version. For example, 1. (required)
     * @param string $entity_id The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $entity_name The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $charge_detail The segmented rate plan charges. When an amendment results in a change to a charge, Zuora creates a segmented rate plan charge. Use this field to track segment charges.  Possible values are:   * __last-segment__: (Default) The last rate plan charge on the subscription. The last rate plan charge is the last one in the order of time on the subscription rather than the most recent changed charge on the subscription.  * __current-segment__: The segmented charge that is active on today’s date (effectiveStartDate &lt;&#x3D; today’s date &lt; effectiveEndDate).    * __all-segments__: All the segmented charges. The &#x60;chargeSegments&#x60; field is returned in the response. The &#x60;chargeSegments&#x60; field contains an array of the charge information for all the charge segments.   * __specific-segment&amp;as-of-date&#x3D;date__: The segmented charge that is active on a date you specified (effectiveStartDate &lt;&#x3D; specific date &lt; effectiveEndDate). The format of the date is yyyy-mm-dd. (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\GETSubscriptionTypeWithSuccess
     */
    public function gETSubscriptionsByKeyAndVersion($subscription_key, $version, $entity_id = null, $entity_name = null, $charge_detail = null)
    {
        list($response) = $this->gETSubscriptionsByKeyAndVersionWithHttpInfo($subscription_key, $version, $entity_id, $entity_name, $charge_detail);
        return $response;
    }

    /**
     * Operation gETSubscriptionsByKeyAndVersionWithHttpInfo
     *
     * Get subscriptions by key and version
     *
     * @param string $subscription_key Subscription number. For example, A-S00000135. (required)
     * @param string $version Subscription version. For example, 1. (required)
     * @param string $entity_id The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $entity_name The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $charge_detail The segmented rate plan charges. When an amendment results in a change to a charge, Zuora creates a segmented rate plan charge. Use this field to track segment charges.  Possible values are:   * __last-segment__: (Default) The last rate plan charge on the subscription. The last rate plan charge is the last one in the order of time on the subscription rather than the most recent changed charge on the subscription.  * __current-segment__: The segmented charge that is active on today’s date (effectiveStartDate &lt;&#x3D; today’s date &lt; effectiveEndDate).    * __all-segments__: All the segmented charges. The &#x60;chargeSegments&#x60; field is returned in the response. The &#x60;chargeSegments&#x60; field contains an array of the charge information for all the charge segments.   * __specific-segment&amp;as-of-date&#x3D;date__: The segmented charge that is active on a date you specified (effectiveStartDate &lt;&#x3D; specific date &lt; effectiveEndDate). The format of the date is yyyy-mm-dd. (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return array of \Swagger\Client\Model\GETSubscriptionTypeWithSuccess, HTTP status code, HTTP response headers (array of strings)
     */
    public function gETSubscriptionsByKeyAndVersionWithHttpInfo($subscription_key, $version, $entity_id = null, $entity_name = null, $charge_detail = null)
    {
        // verify the required parameter 'subscription_key' is set
        if ($subscription_key === null) {
            throw new \InvalidArgumentException('Missing the required parameter $subscription_key when calling gETSubscriptionsByKeyAndVersion');
        }
        // verify the required parameter 'version' is set
        if ($version === null) {
            throw new \InvalidArgumentException('Missing the required parameter $version when calling gETSubscriptionsByKeyAndVersion');
        }
        // parse inputs
        $resourcePath = "/v1/subscriptions/{subscription-key}/versions/{version}";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json; charset=utf-8']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json; charset=utf-8']);

        // query params
        if ($charge_detail !== null) {
            $queryParams['charge-detail'] = $this->apiClient->getSerializer()->toQueryValue($charge_detail);
        }
        // header params
        if ($entity_id !== null) {
            $headerParams['entityId'] = $this->apiClient->getSerializer()->toHeaderValue($entity_id);
        }
        // header params
        if ($entity_name !== null) {
            $headerParams['entityName'] = $this->apiClient->getSerializer()->toHeaderValue($entity_name);
        }
        // path params
        if ($subscription_key !== null) {
            $resourcePath = str_replace(
                "{" . "subscription-key" . "}",
                $this->apiClient->getSerializer()->toPathValue($subscription_key),
                $resourcePath
            );
        }
        // path params
        if ($version !== null) {
            $resourcePath = str_replace(
                "{" . "version" . "}",
                $this->apiClient->getSerializer()->toPathValue($version),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Swagger\Client\Model\GETSubscriptionTypeWithSuccess',
                '/v1/subscriptions/{subscription-key}/versions/{version}'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, '\Swagger\Client\Model\GETSubscriptionTypeWithSuccess', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Swagger\Client\Model\GETSubscriptionTypeWithSuccess', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation objectDELETESubscription
     *
     * CRUD: Delete Subscription
     *
     * @param string $id Object id (required)
     * @param string $entity_id The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $entity_name The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\ProxyDeleteResponse
     */
    public function objectDELETESubscription($id, $entity_id = null, $entity_name = null)
    {
        list($response) = $this->objectDELETESubscriptionWithHttpInfo($id, $entity_id, $entity_name);
        return $response;
    }

    /**
     * Operation objectDELETESubscriptionWithHttpInfo
     *
     * CRUD: Delete Subscription
     *
     * @param string $id Object id (required)
     * @param string $entity_id The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $entity_name The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return array of \Swagger\Client\Model\ProxyDeleteResponse, HTTP status code, HTTP response headers (array of strings)
     */
    public function objectDELETESubscriptionWithHttpInfo($id, $entity_id = null, $entity_name = null)
    {
        // verify the required parameter 'id' is set
        if ($id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $id when calling objectDELETESubscription');
        }
        // parse inputs
        $resourcePath = "/v1/object/subscription/{id}";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json; charset=utf-8']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json; charset=utf-8']);

        // header params
        if ($entity_id !== null) {
            $headerParams['entityId'] = $this->apiClient->getSerializer()->toHeaderValue($entity_id);
        }
        // header params
        if ($entity_name !== null) {
            $headerParams['entityName'] = $this->apiClient->getSerializer()->toHeaderValue($entity_name);
        }
        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                "{" . "id" . "}",
                $this->apiClient->getSerializer()->toPathValue($id),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'DELETE',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Swagger\Client\Model\ProxyDeleteResponse',
                '/v1/object/subscription/{id}'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, '\Swagger\Client\Model\ProxyDeleteResponse', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Swagger\Client\Model\ProxyDeleteResponse', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation objectGETSubscription
     *
     * CRUD: Retrieve Subscription
     *
     * @param string $id Object id (required)
     * @param string $entity_id The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $entity_name The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $fields Object fields to return (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\ProxyGetSubscription
     */
    public function objectGETSubscription($id, $entity_id = null, $entity_name = null, $fields = null)
    {
        list($response) = $this->objectGETSubscriptionWithHttpInfo($id, $entity_id, $entity_name, $fields);
        return $response;
    }

    /**
     * Operation objectGETSubscriptionWithHttpInfo
     *
     * CRUD: Retrieve Subscription
     *
     * @param string $id Object id (required)
     * @param string $entity_id The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $entity_name The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $fields Object fields to return (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return array of \Swagger\Client\Model\ProxyGetSubscription, HTTP status code, HTTP response headers (array of strings)
     */
    public function objectGETSubscriptionWithHttpInfo($id, $entity_id = null, $entity_name = null, $fields = null)
    {
        // verify the required parameter 'id' is set
        if ($id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $id when calling objectGETSubscription');
        }
        // parse inputs
        $resourcePath = "/v1/object/subscription/{id}";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json; charset=utf-8']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json; charset=utf-8']);

        // query params
        if ($fields !== null) {
            $queryParams['fields'] = $this->apiClient->getSerializer()->toQueryValue($fields);
        }
        // header params
        if ($entity_id !== null) {
            $headerParams['entityId'] = $this->apiClient->getSerializer()->toHeaderValue($entity_id);
        }
        // header params
        if ($entity_name !== null) {
            $headerParams['entityName'] = $this->apiClient->getSerializer()->toHeaderValue($entity_name);
        }
        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                "{" . "id" . "}",
                $this->apiClient->getSerializer()->toPathValue($id),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Swagger\Client\Model\ProxyGetSubscription',
                '/v1/object/subscription/{id}'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, '\Swagger\Client\Model\ProxyGetSubscription', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Swagger\Client\Model\ProxyGetSubscription', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
                case 404:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Swagger\Client\Model\ProxyNoDataResponse', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation objectPUTSubscription
     *
     * CRUD: Update Subscription
     *
     * @param string $id Object id (required)
     * @param \Swagger\Client\Model\ProxyModifySubscription $modify_request  (required)
     * @param string $entity_id The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $entity_name The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\ProxyCreateOrModifyResponse
     */
    public function objectPUTSubscription($id, $modify_request, $entity_id = null, $entity_name = null)
    {
        list($response) = $this->objectPUTSubscriptionWithHttpInfo($id, $modify_request, $entity_id, $entity_name);
        return $response;
    }

    /**
     * Operation objectPUTSubscriptionWithHttpInfo
     *
     * CRUD: Update Subscription
     *
     * @param string $id Object id (required)
     * @param \Swagger\Client\Model\ProxyModifySubscription $modify_request  (required)
     * @param string $entity_id The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $entity_name The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return array of \Swagger\Client\Model\ProxyCreateOrModifyResponse, HTTP status code, HTTP response headers (array of strings)
     */
    public function objectPUTSubscriptionWithHttpInfo($id, $modify_request, $entity_id = null, $entity_name = null)
    {
        // verify the required parameter 'id' is set
        if ($id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $id when calling objectPUTSubscription');
        }
        // verify the required parameter 'modify_request' is set
        if ($modify_request === null) {
            throw new \InvalidArgumentException('Missing the required parameter $modify_request when calling objectPUTSubscription');
        }
        // parse inputs
        $resourcePath = "/v1/object/subscription/{id}";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json; charset=utf-8']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json; charset=utf-8']);

        // header params
        if ($entity_id !== null) {
            $headerParams['entityId'] = $this->apiClient->getSerializer()->toHeaderValue($entity_id);
        }
        // header params
        if ($entity_name !== null) {
            $headerParams['entityName'] = $this->apiClient->getSerializer()->toHeaderValue($entity_name);
        }
        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                "{" . "id" . "}",
                $this->apiClient->getSerializer()->toPathValue($id),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        // body params
        $_tempBody = null;
        if (isset($modify_request)) {
            $_tempBody = $modify_request;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'PUT',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Swagger\Client\Model\ProxyCreateOrModifyResponse',
                '/v1/object/subscription/{id}'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, '\Swagger\Client\Model\ProxyCreateOrModifyResponse', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Swagger\Client\Model\ProxyCreateOrModifyResponse', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation pOSTPreviewSubscription
     *
     * Preview subscription
     *
     * @param \Swagger\Client\Model\POSTSubscriptionPreviewType $request  (required)
     * @param string $entity_id The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $entity_name The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $zuora_version The minor version of the Zuora REST API.   You only need to set this parameter if you use the following fields: * targetDate * includeExistingDraftDocItems * previewType  See [Zuora REST API Versions](https://www.zuora.com/developer/api-reference/#section/API-Versions) for more information. (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\POSTSubscriptionPreviewResponseType
     */
    public function pOSTPreviewSubscription($request, $entity_id = null, $entity_name = null, $zuora_version = null)
    {
        list($response) = $this->pOSTPreviewSubscriptionWithHttpInfo($request, $entity_id, $entity_name, $zuora_version);
        return $response;
    }

    /**
     * Operation pOSTPreviewSubscriptionWithHttpInfo
     *
     * Preview subscription
     *
     * @param \Swagger\Client\Model\POSTSubscriptionPreviewType $request  (required)
     * @param string $entity_id The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $entity_name The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $zuora_version The minor version of the Zuora REST API.   You only need to set this parameter if you use the following fields: * targetDate * includeExistingDraftDocItems * previewType  See [Zuora REST API Versions](https://www.zuora.com/developer/api-reference/#section/API-Versions) for more information. (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return array of \Swagger\Client\Model\POSTSubscriptionPreviewResponseType, HTTP status code, HTTP response headers (array of strings)
     */
    public function pOSTPreviewSubscriptionWithHttpInfo($request, $entity_id = null, $entity_name = null, $zuora_version = null)
    {
        // verify the required parameter 'request' is set
        if ($request === null) {
            throw new \InvalidArgumentException('Missing the required parameter $request when calling pOSTPreviewSubscription');
        }
        // parse inputs
        $resourcePath = "/v1/subscriptions/preview";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json; charset=utf-8']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json; charset=utf-8']);

        // header params
        if ($entity_id !== null) {
            $headerParams['entityId'] = $this->apiClient->getSerializer()->toHeaderValue($entity_id);
        }
        // header params
        if ($entity_name !== null) {
            $headerParams['entityName'] = $this->apiClient->getSerializer()->toHeaderValue($entity_name);
        }
        // header params
        if ($zuora_version !== null) {
            $headerParams['zuora-version'] = $this->apiClient->getSerializer()->toHeaderValue($zuora_version);
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        // body params
        $_tempBody = null;
        if (isset($request)) {
            $_tempBody = $request;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'POST',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Swagger\Client\Model\POSTSubscriptionPreviewResponseType',
                '/v1/subscriptions/preview'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, '\Swagger\Client\Model\POSTSubscriptionPreviewResponseType', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Swagger\Client\Model\POSTSubscriptionPreviewResponseType', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation pOSTSubscription
     *
     * Create subscription
     *
     * @param \Swagger\Client\Model\POSTSubscriptionType $request  (required)
     * @param string $entity_id The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $entity_name The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $zuora_version The minor version of the Zuora REST API. You only need to set this parameter if you use the __collect__ or __invoice__ field. (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\POSTSubscriptionResponseType
     */
    public function pOSTSubscription($request, $entity_id = null, $entity_name = null, $zuora_version = null)
    {
        list($response) = $this->pOSTSubscriptionWithHttpInfo($request, $entity_id, $entity_name, $zuora_version);
        return $response;
    }

    /**
     * Operation pOSTSubscriptionWithHttpInfo
     *
     * Create subscription
     *
     * @param \Swagger\Client\Model\POSTSubscriptionType $request  (required)
     * @param string $entity_id The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $entity_name The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $zuora_version The minor version of the Zuora REST API. You only need to set this parameter if you use the __collect__ or __invoice__ field. (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return array of \Swagger\Client\Model\POSTSubscriptionResponseType, HTTP status code, HTTP response headers (array of strings)
     */
    public function pOSTSubscriptionWithHttpInfo($request, $entity_id = null, $entity_name = null, $zuora_version = null)
    {
        // verify the required parameter 'request' is set
        if ($request === null) {
            throw new \InvalidArgumentException('Missing the required parameter $request when calling pOSTSubscription');
        }
        // parse inputs
        $resourcePath = "/v1/subscriptions";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json; charset=utf-8']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json; charset=utf-8']);

        // header params
        if ($entity_id !== null) {
            $headerParams['entityId'] = $this->apiClient->getSerializer()->toHeaderValue($entity_id);
        }
        // header params
        if ($entity_name !== null) {
            $headerParams['entityName'] = $this->apiClient->getSerializer()->toHeaderValue($entity_name);
        }
        // header params
        if ($zuora_version !== null) {
            $headerParams['zuora-version'] = $this->apiClient->getSerializer()->toHeaderValue($zuora_version);
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        // body params
        $_tempBody = null;
        if (isset($request)) {
            $_tempBody = $request;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'POST',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Swagger\Client\Model\POSTSubscriptionResponseType',
                '/v1/subscriptions'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, '\Swagger\Client\Model\POSTSubscriptionResponseType', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Swagger\Client\Model\POSTSubscriptionResponseType', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation pUTCancelSubscription
     *
     * Cancel subscription
     *
     * @param string $subscription_key Subscription number or ID. Subscription status must be &#x60;Active&#x60;. (required)
     * @param \Swagger\Client\Model\POSTSubscriptionCancellationType $request  (required)
     * @param string $entity_id The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $entity_name The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $zuora_version The minor version of the Zuora REST API. You only need to set this parameter if you use the __collect__ or __invoice__ field. (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\POSTSubscriptionCancellationResponseType
     */
    public function pUTCancelSubscription($subscription_key, $request, $entity_id = null, $entity_name = null, $zuora_version = null)
    {
        list($response) = $this->pUTCancelSubscriptionWithHttpInfo($subscription_key, $request, $entity_id, $entity_name, $zuora_version);
        return $response;
    }

    /**
     * Operation pUTCancelSubscriptionWithHttpInfo
     *
     * Cancel subscription
     *
     * @param string $subscription_key Subscription number or ID. Subscription status must be &#x60;Active&#x60;. (required)
     * @param \Swagger\Client\Model\POSTSubscriptionCancellationType $request  (required)
     * @param string $entity_id The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $entity_name The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $zuora_version The minor version of the Zuora REST API. You only need to set this parameter if you use the __collect__ or __invoice__ field. (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return array of \Swagger\Client\Model\POSTSubscriptionCancellationResponseType, HTTP status code, HTTP response headers (array of strings)
     */
    public function pUTCancelSubscriptionWithHttpInfo($subscription_key, $request, $entity_id = null, $entity_name = null, $zuora_version = null)
    {
        // verify the required parameter 'subscription_key' is set
        if ($subscription_key === null) {
            throw new \InvalidArgumentException('Missing the required parameter $subscription_key when calling pUTCancelSubscription');
        }
        // verify the required parameter 'request' is set
        if ($request === null) {
            throw new \InvalidArgumentException('Missing the required parameter $request when calling pUTCancelSubscription');
        }
        // parse inputs
        $resourcePath = "/v1/subscriptions/{subscription-key}/cancel";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json; charset=utf-8']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json; charset=utf-8']);

        // header params
        if ($entity_id !== null) {
            $headerParams['entityId'] = $this->apiClient->getSerializer()->toHeaderValue($entity_id);
        }
        // header params
        if ($entity_name !== null) {
            $headerParams['entityName'] = $this->apiClient->getSerializer()->toHeaderValue($entity_name);
        }
        // header params
        if ($zuora_version !== null) {
            $headerParams['zuora-version'] = $this->apiClient->getSerializer()->toHeaderValue($zuora_version);
        }
        // path params
        if ($subscription_key !== null) {
            $resourcePath = str_replace(
                "{" . "subscription-key" . "}",
                $this->apiClient->getSerializer()->toPathValue($subscription_key),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        // body params
        $_tempBody = null;
        if (isset($request)) {
            $_tempBody = $request;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'PUT',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Swagger\Client\Model\POSTSubscriptionCancellationResponseType',
                '/v1/subscriptions/{subscription-key}/cancel'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, '\Swagger\Client\Model\POSTSubscriptionCancellationResponseType', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Swagger\Client\Model\POSTSubscriptionCancellationResponseType', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation pUTRenewSubscription
     *
     * Renew subscription
     *
     * @param string $subscription_key Subscription number or ID (required)
     * @param \Swagger\Client\Model\PUTRenewSubscriptionType $request  (required)
     * @param string $entity_id The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $entity_name The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $zuora_version The minor version of the Zuora REST API. You only need to set this parameter if you use the __collect__ or __invoice__ field. (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\PUTRenewSubscriptionResponseType
     */
    public function pUTRenewSubscription($subscription_key, $request, $entity_id = null, $entity_name = null, $zuora_version = null)
    {
        list($response) = $this->pUTRenewSubscriptionWithHttpInfo($subscription_key, $request, $entity_id, $entity_name, $zuora_version);
        return $response;
    }

    /**
     * Operation pUTRenewSubscriptionWithHttpInfo
     *
     * Renew subscription
     *
     * @param string $subscription_key Subscription number or ID (required)
     * @param \Swagger\Client\Model\PUTRenewSubscriptionType $request  (required)
     * @param string $entity_id The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $entity_name The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $zuora_version The minor version of the Zuora REST API. You only need to set this parameter if you use the __collect__ or __invoice__ field. (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return array of \Swagger\Client\Model\PUTRenewSubscriptionResponseType, HTTP status code, HTTP response headers (array of strings)
     */
    public function pUTRenewSubscriptionWithHttpInfo($subscription_key, $request, $entity_id = null, $entity_name = null, $zuora_version = null)
    {
        // verify the required parameter 'subscription_key' is set
        if ($subscription_key === null) {
            throw new \InvalidArgumentException('Missing the required parameter $subscription_key when calling pUTRenewSubscription');
        }
        // verify the required parameter 'request' is set
        if ($request === null) {
            throw new \InvalidArgumentException('Missing the required parameter $request when calling pUTRenewSubscription');
        }
        // parse inputs
        $resourcePath = "/v1/subscriptions/{subscription-key}/renew";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json; charset=utf-8']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json; charset=utf-8']);

        // header params
        if ($entity_id !== null) {
            $headerParams['entityId'] = $this->apiClient->getSerializer()->toHeaderValue($entity_id);
        }
        // header params
        if ($entity_name !== null) {
            $headerParams['entityName'] = $this->apiClient->getSerializer()->toHeaderValue($entity_name);
        }
        // header params
        if ($zuora_version !== null) {
            $headerParams['zuora-version'] = $this->apiClient->getSerializer()->toHeaderValue($zuora_version);
        }
        // path params
        if ($subscription_key !== null) {
            $resourcePath = str_replace(
                "{" . "subscription-key" . "}",
                $this->apiClient->getSerializer()->toPathValue($subscription_key),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        // body params
        $_tempBody = null;
        if (isset($request)) {
            $_tempBody = $request;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'PUT',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Swagger\Client\Model\PUTRenewSubscriptionResponseType',
                '/v1/subscriptions/{subscription-key}/renew'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, '\Swagger\Client\Model\PUTRenewSubscriptionResponseType', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Swagger\Client\Model\PUTRenewSubscriptionResponseType', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation pUTResumeSubscription
     *
     * Resume subscription
     *
     * @param string $subscription_key Subscription number or ID. Subscription status must be Active. (required)
     * @param \Swagger\Client\Model\PUTSubscriptionResumeType $request  (required)
     * @param string $entity_id The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $entity_name The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $zuora_version The minor version of the Zuora REST API. You only need to set this parameter if you use the __collect__ or __invoice__ field. (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\PUTSubscriptionResumeResponseType
     */
    public function pUTResumeSubscription($subscription_key, $request, $entity_id = null, $entity_name = null, $zuora_version = null)
    {
        list($response) = $this->pUTResumeSubscriptionWithHttpInfo($subscription_key, $request, $entity_id, $entity_name, $zuora_version);
        return $response;
    }

    /**
     * Operation pUTResumeSubscriptionWithHttpInfo
     *
     * Resume subscription
     *
     * @param string $subscription_key Subscription number or ID. Subscription status must be Active. (required)
     * @param \Swagger\Client\Model\PUTSubscriptionResumeType $request  (required)
     * @param string $entity_id The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $entity_name The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $zuora_version The minor version of the Zuora REST API. You only need to set this parameter if you use the __collect__ or __invoice__ field. (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return array of \Swagger\Client\Model\PUTSubscriptionResumeResponseType, HTTP status code, HTTP response headers (array of strings)
     */
    public function pUTResumeSubscriptionWithHttpInfo($subscription_key, $request, $entity_id = null, $entity_name = null, $zuora_version = null)
    {
        // verify the required parameter 'subscription_key' is set
        if ($subscription_key === null) {
            throw new \InvalidArgumentException('Missing the required parameter $subscription_key when calling pUTResumeSubscription');
        }
        // verify the required parameter 'request' is set
        if ($request === null) {
            throw new \InvalidArgumentException('Missing the required parameter $request when calling pUTResumeSubscription');
        }
        // parse inputs
        $resourcePath = "/v1/subscriptions/{subscription-key}/resume";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json; charset=utf-8']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json; charset=utf-8']);

        // header params
        if ($entity_id !== null) {
            $headerParams['entityId'] = $this->apiClient->getSerializer()->toHeaderValue($entity_id);
        }
        // header params
        if ($entity_name !== null) {
            $headerParams['entityName'] = $this->apiClient->getSerializer()->toHeaderValue($entity_name);
        }
        // header params
        if ($zuora_version !== null) {
            $headerParams['zuora-version'] = $this->apiClient->getSerializer()->toHeaderValue($zuora_version);
        }
        // path params
        if ($subscription_key !== null) {
            $resourcePath = str_replace(
                "{" . "subscription-key" . "}",
                $this->apiClient->getSerializer()->toPathValue($subscription_key),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        // body params
        $_tempBody = null;
        if (isset($request)) {
            $_tempBody = $request;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'PUT',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Swagger\Client\Model\PUTSubscriptionResumeResponseType',
                '/v1/subscriptions/{subscription-key}/resume'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, '\Swagger\Client\Model\PUTSubscriptionResumeResponseType', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Swagger\Client\Model\PUTSubscriptionResumeResponseType', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation pUTSubscription
     *
     * Update subscription
     *
     * @param string $subscription_key Subscription number or ID. (required)
     * @param \Swagger\Client\Model\PUTSubscriptionType $request  (required)
     * @param string $entity_id The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $entity_name The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $zuora_version The minor version of the Zuora REST API.   You only need to set this parameter if you use the following fields: * collect * invoice * includeExistingDraftDocItems * previewType  See [Zuora REST API Versions](https://www.zuora.com/developer/api-reference/#section/API-Versions) for more information. (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\PUTSubscriptionResponseType
     */
    public function pUTSubscription($subscription_key, $request, $entity_id = null, $entity_name = null, $zuora_version = null)
    {
        list($response) = $this->pUTSubscriptionWithHttpInfo($subscription_key, $request, $entity_id, $entity_name, $zuora_version);
        return $response;
    }

    /**
     * Operation pUTSubscriptionWithHttpInfo
     *
     * Update subscription
     *
     * @param string $subscription_key Subscription number or ID. (required)
     * @param \Swagger\Client\Model\PUTSubscriptionType $request  (required)
     * @param string $entity_id The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $entity_name The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $zuora_version The minor version of the Zuora REST API.   You only need to set this parameter if you use the following fields: * collect * invoice * includeExistingDraftDocItems * previewType  See [Zuora REST API Versions](https://www.zuora.com/developer/api-reference/#section/API-Versions) for more information. (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return array of \Swagger\Client\Model\PUTSubscriptionResponseType, HTTP status code, HTTP response headers (array of strings)
     */
    public function pUTSubscriptionWithHttpInfo($subscription_key, $request, $entity_id = null, $entity_name = null, $zuora_version = null)
    {
        // verify the required parameter 'subscription_key' is set
        if ($subscription_key === null) {
            throw new \InvalidArgumentException('Missing the required parameter $subscription_key when calling pUTSubscription');
        }
        // verify the required parameter 'request' is set
        if ($request === null) {
            throw new \InvalidArgumentException('Missing the required parameter $request when calling pUTSubscription');
        }
        // parse inputs
        $resourcePath = "/v1/subscriptions/{subscription-key}";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json; charset=utf-8']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json; charset=utf-8']);

        // header params
        if ($entity_id !== null) {
            $headerParams['entityId'] = $this->apiClient->getSerializer()->toHeaderValue($entity_id);
        }
        // header params
        if ($entity_name !== null) {
            $headerParams['entityName'] = $this->apiClient->getSerializer()->toHeaderValue($entity_name);
        }
        // header params
        if ($zuora_version !== null) {
            $headerParams['zuora-version'] = $this->apiClient->getSerializer()->toHeaderValue($zuora_version);
        }
        // path params
        if ($subscription_key !== null) {
            $resourcePath = str_replace(
                "{" . "subscription-key" . "}",
                $this->apiClient->getSerializer()->toPathValue($subscription_key),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        // body params
        $_tempBody = null;
        if (isset($request)) {
            $_tempBody = $request;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'PUT',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Swagger\Client\Model\PUTSubscriptionResponseType',
                '/v1/subscriptions/{subscription-key}'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, '\Swagger\Client\Model\PUTSubscriptionResponseType', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Swagger\Client\Model\PUTSubscriptionResponseType', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation pUTSuspendSubscription
     *
     * Suspend subscription
     *
     * @param string $subscription_key Subscription number or ID. Subscription status must be Active. (required)
     * @param \Swagger\Client\Model\PUTSubscriptionSuspendType $request  (required)
     * @param string $entity_id The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $entity_name The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $zuora_version The minor version of the Zuora REST API. You only need to set this parameter if you use the __collect__ or __invoice__ field. (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\PUTSubscriptionSuspendResponseType
     */
    public function pUTSuspendSubscription($subscription_key, $request, $entity_id = null, $entity_name = null, $zuora_version = null)
    {
        list($response) = $this->pUTSuspendSubscriptionWithHttpInfo($subscription_key, $request, $entity_id, $entity_name, $zuora_version);
        return $response;
    }

    /**
     * Operation pUTSuspendSubscriptionWithHttpInfo
     *
     * Suspend subscription
     *
     * @param string $subscription_key Subscription number or ID. Subscription status must be Active. (required)
     * @param \Swagger\Client\Model\PUTSubscriptionSuspendType $request  (required)
     * @param string $entity_id The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $entity_name The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). (optional)
     * @param string $zuora_version The minor version of the Zuora REST API. You only need to set this parameter if you use the __collect__ or __invoice__ field. (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return array of \Swagger\Client\Model\PUTSubscriptionSuspendResponseType, HTTP status code, HTTP response headers (array of strings)
     */
    public function pUTSuspendSubscriptionWithHttpInfo($subscription_key, $request, $entity_id = null, $entity_name = null, $zuora_version = null)
    {
        // verify the required parameter 'subscription_key' is set
        if ($subscription_key === null) {
            throw new \InvalidArgumentException('Missing the required parameter $subscription_key when calling pUTSuspendSubscription');
        }
        // verify the required parameter 'request' is set
        if ($request === null) {
            throw new \InvalidArgumentException('Missing the required parameter $request when calling pUTSuspendSubscription');
        }
        // parse inputs
        $resourcePath = "/v1/subscriptions/{subscription-key}/suspend";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json; charset=utf-8']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json; charset=utf-8']);

        // header params
        if ($entity_id !== null) {
            $headerParams['entityId'] = $this->apiClient->getSerializer()->toHeaderValue($entity_id);
        }
        // header params
        if ($entity_name !== null) {
            $headerParams['entityName'] = $this->apiClient->getSerializer()->toHeaderValue($entity_name);
        }
        // header params
        if ($zuora_version !== null) {
            $headerParams['zuora-version'] = $this->apiClient->getSerializer()->toHeaderValue($zuora_version);
        }
        // path params
        if ($subscription_key !== null) {
            $resourcePath = str_replace(
                "{" . "subscription-key" . "}",
                $this->apiClient->getSerializer()->toPathValue($subscription_key),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        // body params
        $_tempBody = null;
        if (isset($request)) {
            $_tempBody = $request;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'PUT',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Swagger\Client\Model\PUTSubscriptionSuspendResponseType',
                '/v1/subscriptions/{subscription-key}/suspend'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, '\Swagger\Client\Model\PUTSubscriptionSuspendResponseType', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Swagger\Client\Model\PUTSubscriptionSuspendResponseType', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }
}
