<?php
/**
 * CreditMemosApi
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Zuora API Reference
 *
 * # Introduction Welcome to the reference for the Zuora REST API!  <a href=\"http://en.wikipedia.org/wiki/REST_API\" target=\"_blank\">REST</a> is a web-service protocol that lends itself to rapid development by using everyday HTTP and JSON technology.  The Zuora REST API provides a broad set of operations and resources that:    * Enable Web Storefront integration from your website.   * Support self-service subscriber sign-ups and account management.   * Process revenue schedules through custom revenue rule models.   * Enable manipulation of most objects in the Zuora Object Model.      ## Endpoints      The Zuora REST API is provided via the following endpoints.   | Tenant              | Base URL for REST Endpoints |   |-------------------------|-------------------------|   |Production | https://rest.zuora.com/v1   |   |API Sandbox    | https://rest.apisandbox.zuora.com/v1|      The production endpoint provides access to your live user data. The API Sandbox tenant is a good place to test your code without affecting real-world data. To use it, you must be provisioned with an API Sandbox tenant - your Zuora representative can help you if needed.      ## Access to the API      If you have a Zuora tenant, you already have access to the API.      If you don't have a Zuora tenant, go to <a href=\" https://www.zuora.com/resource/zuora-test-drive\" target=\"_blank\">https://www.zuora.com/resource/zuora-test-drive</a> and sign up for a Production Test Drive tenant. The tenant comes with seed data, such as a sample product catalog.  We recommend that you <a href=\"https://knowledgecenter.zuora.com/CF_Users_and_Administrators/A_Administrator_Settings/Manage_Users/Create_an_API_User\" target=\"_blank\">create an API user</a> specifically for making API calls. Don't log in to the Zuora UI with this account. Logging in to the UI enables a security feature that periodically expires the account's password, which may eventually cause authentication failures with the API. Note that a user role does not have write access to Zuora REST services unless it has the API Write Access permission as described in those instructions.  # Authentication  There are three ways to authenticate:    * Use username and password. Include authentication with each request in the header:         * `apiAccessKeyId`      * `apiSecretAccessKey`     * `entityId` or `entityName` (Only for [Zuora Multi-entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity \"Multi-entity\"). See \"Entity Id and Entity Name\" below for more information.)   * Use an authorization cookie. The cookie authorizes the user to make calls to the REST API for the duration specified in  **Administration > Security Policies > Session timeout**. The cookie expiration time is reset with this duration after every call to the REST API. To obtain a cookie, call the REST  `connections` resource with the following API user information:         *   ID         *   password     *   entity Id or entity name (Only for [Zuora Multi-entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity \"Multi-entity\"). See \"Entity Id and Entity Name\" below for more information.)         * For CORS-enabled APIs only: Include a 'single-use' token in the request header, which re-authenticates the user with each request. See below for more details.  ## Entity Id and Entity Name  The `entityId` and `entityName` parameters are only used for [Zuora Multi-entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity \"Zuora Multi-entity\").   The  `entityId` and `entityName` parameters specify the Id and the [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name \"Introduction to Entity and Entity Hierarchy\") that you want to access, respectively. Note that you must have permission to access the entity.   You can specify either the `entityId` or `entityName` parameter in the authentication to access and view an entity.    * If both `entityId` and `entityName` are specified in the authentication, an error occurs.    * If neither `entityId` nor `entityName` is specified in the authentication, you will log in to the entity in which your user account is created.      To get the entity Id and entity name, you can use the GET Entities REST call. For more information, see [API User Authentication](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/A_Overview_of_Multi-entity#API_User_Authentication \"API User Authentication\").      ## Token Authentication for CORS-Enabled APIs      The CORS mechanism enables REST API calls to Zuora to be made directly from your customer's browser, with all credit card and security information transmitted directly to Zuora. This minimizes your PCI compliance burden, allows you to implement advanced validation on your payment forms, and  makes your payment forms look just like any other part of your website.    For security reasons, instead of using cookies, an API request via CORS uses **tokens** for authentication.  The token method of authentication is only designed for use with requests that must originate from your customer's browser; **it should  not be considered a replacement to the existing cookie authentication** mechanism.  See [Zuora CORS REST](https://knowledgecenter.zuora.com/DC_Developers/REST_API/A_REST_basics/G_CORS_REST \"Zuora CORS REST\") for details on how CORS works and how you can begin to implement customer calls to the Zuora REST APIs. See  [HMAC Signatures](https://www.zuora.com/developer/API-Reference/#operation/POSTHMACSignature \"HMAC Signatures\") for details on the HMAC method that returns the authentication token.  # Requests and Responses  ## Request IDs  As a general rule, when asked to supply a \"key\" for an account or subscription (accountKey, account-key, subscriptionKey, subscription-key), you can provide either the actual ID or  the number of the entity.  ## HTTP Request Body  Most of the parameters and data accompanying your requests will be contained in the body of the HTTP request.   The Zuora REST API accepts JSON in the HTTP request body. No other data format (e.g., XML) is supported.  ## Testing a Request  Use a third party client, such as [curl](https://curl.haxx.se \"curl\"), [Postman](https://www.getpostman.com \"Postman\"), or [Advanced REST Client](https://advancedrestclient.com \"Advanced REST Client\"), to test the Zuora REST API.  You can test the Zuora REST API from the Zuora API Sandbox or Production tenants. If connecting to Production, bear in mind that you are working with your live production data, not sample data or test data.  ## Testing with Credit Cards  Sooner or later it will probably be necessary to test some transactions that involve credit cards. For suggestions on how to handle this, see [Going Live With Your Payment Gateway](https://knowledgecenter.zuora.com/CB_Billing/M_Payment_Gateways/C_Managing_Payment_Gateways/B_Going_Live_Payment_Gateways#Testing_with_Credit_Cards \"C_Zuora_User_Guides/A_Billing_and_Payments/M_Payment_Gateways/C_Managing_Payment_Gateways/B_Going_Live_Payment_Gateways#Testing_with_Credit_Cards\" ).  ## Error Handling  Responses and error codes are detailed in [Responses and errors](https://knowledgecenter.zuora.com/DC_Developers/REST_API/A_REST_basics/3_Responses_and_errors \"Responses and errors\").  # Pagination  When retrieving information (using GET methods), the optional `pageSize` query parameter sets the maximum number of rows to return in a response. The maximum is `40`; larger values are treated as `40`. If this value is empty or invalid, `pageSize` typically defaults to `10`.  The default value for the maximum number of rows retrieved can be overridden at the method level.  If more rows are available, the response will include a `nextPage` element, which contains a URL for requesting the next page.  If this value is not provided, no more rows are available. No \"previous page\" element is explicitly provided; to support backward paging, use the previous call.  ## Array Size  For data items that are not paginated, the REST API supports arrays of up to 300 rows.  Thus, for instance, repeated pagination can retrieve thousands of customer accounts, but within any account an array of no more than 300 rate plans is returned.  # API Versions  The Zuora REST API are version controlled. Versioning ensures that Zuora REST API changes are backward compatible. Zuora uses a major and minor version nomenclature to manage changes. By specifying a version in a REST request, you can get expected responses regardless of future changes to the API.  ## Major Version  The major version number of the REST API appears in the REST URL. Currently, Zuora only supports the **v1** major version. For example, `POST https://rest.zuora.com/v1/subscriptions`.  ## Minor Version  Zuora uses minor versions for the REST API to control small changes. For example, a field in a REST method is deprecated and a new field is used to replace it.   Some fields in the REST methods are supported as of minor versions. If a field is not noted with a minor version, this field is available for all minor versions. If a field is noted with a minor version, this field is in version control. You must specify the supported minor version in the request header to process without an error.   If a field is in version control, it is either with a minimum minor version or a maximum minor version, or both of them. You can only use this field with the minor version between the minimum and the maximum minor versions. For example, the `invoiceCollect` field in the POST Subscription method is in version control and its maximum minor version is 189.0. You can only use this field with the minor version 189.0 or earlier.  If you specify a version number in the request header that is not supported, Zuora will use the minimum minor version of the REST API. In our REST API documentation, if a field or feature requires a minor version number, we note that in the field description.  You only need to specify the version number when you use the fields require a minor version. To specify the minor version, set the `zuora-version` parameter to the minor version number in the request header for the request call. For example, the `collect` field is in 196.0 minor version. If you want to use this field for the POST Subscription method, set the  `zuora-version` parameter to `196.0` in the request header. The `zuora-version` parameter is case sensitive.  For all the REST API fields, by default, if the minor version is not specified in the request header, Zuora will use the minimum minor version of the REST API to avoid breaking your integration.   ### Minor Version History  The supported minor versions are not serial. This section documents the changes made to each Zuora REST API minor version.  The following table lists the supported versions and the fields that have a Zuora REST API minor version.  | Fields         | Minor Version      | REST Methods    | Description | |:--------|:--------|:--------|:--------| | invoiceCollect | 189.0 and earlier  | [Create Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_Subscription \"Create Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\"); [Renew Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_RenewSubscription \"Renew Subscription\"); [Cancel Subscription](https://www.zuora.com/developer/api-reference/#operation/POSTSubscriptionCancellation \"Cancel Subscription\"); [Suspend Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionSuspend \"Suspend Subscription\"); [Resume Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionResume \"Resume Subscription\"); [Create Account](https://www.zuora.com/developer/API-Reference/#operation/POST_Account \"Create Account\")|Generates an invoice and collects a payment for a subscription. | | collect        | 196.0 and later    | [Create Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_Subscription \"Create Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\"); [Renew Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_RenewSubscription \"Renew Subscription\"); [Cancel Subscription](https://www.zuora.com/developer/api-reference/#operation/POSTSubscriptionCancellation \"Cancel Subscription\"); [Suspend Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionSuspend \"Suspend Subscription\"); [Resume Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionResume \"Resume Subscription\"); [Create Account](https://www.zuora.com/developer/API-Reference/#operation/POST_Account \"Create Account\")|Collects an automatic payment for a subscription. | | invoice | 196.0 and later| [Create Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_Subscription \"Create Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\"); [Renew Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_RenewSubscription \"Renew Subscription\"); [Cancel Subscription](https://www.zuora.com/developer/api-reference/#operation/POSTSubscriptionCancellation \"Cancel Subscription\"); [Suspend Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionSuspend \"Suspend Subscription\"); [Resume Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionResume \"Resume Subscription\"); [Create Account](https://www.zuora.com/developer/API-Reference/#operation/POST_Account \"Create Account\")|Generates an invoice for a subscription. | | invoiceTargetDate | 196.0 and earlier  | [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\") |Date through which charges are calculated on the invoice, as `yyyy-mm-dd`. | | targetDate | 207.0 and later | [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\") |Date through which charges are calculated on the invoice, as `yyyy-mm-dd`. | | includeExisting DraftInvoiceItems | 196.0 and earlier| [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") | Specifies whether to include draft invoice items in subscription previews. Specify it to be `true` (default) to include draft invoice items in the preview result. Specify it to be `false` to excludes draft invoice items in the preview result. | | includeExisting DraftDocItems | 207.0 and later  | [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") | Specifies whether to include draft invoice items in subscription previews. Specify it to be `true` (default) to include draft invoice items in the preview result. Specify it to be `false` to excludes draft invoice items in the preview result. | | previewType | 196.0 and earlier| [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") | The type of preview you will receive. The possible values are `InvoiceItem`(default), `ChargeMetrics`, and `InvoiceItemChargeMetrics`. | | previewType | 207.0 and later  | [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") | The type of preview you will receive. The possible values are `LegalDoc`(default), `ChargeMetrics`, and `LegalDocChargeMetrics`. |  #### Version 207.0 and Later  The response structure of the [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\") and [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") methods are changed. The following invoice related response fields are moved to the invoice container:    * amount   * amountWithoutTax   * taxAmount   * invoiceItems   * targetDate   * chargeMetrics  # Zuora Object Model  The following diagram presents a high-level view of the key Zuora objects. Click the image to open it in a new tab to resize it.  <a href=\"https://www.zuora.com/wp-content/uploads/2017/01/ZuoraERD.jpeg\" target=\"_blank\"><img src=\"https://www.zuora.com/wp-content/uploads/2017/01/ZuoraERD.jpeg\" alt=\"Zuora Object Model Diagram\"></a>
 *
 * OpenAPI spec version: 2017-04-14
 * Contact: docs@zuora.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Api;

use \Swagger\Client\ApiClient;
use \Swagger\Client\ApiException;
use \Swagger\Client\Configuration;
use \Swagger\Client\ObjectSerializer;

/**
 * CreditMemosApi Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class CreditMemosApi
{
    /**
     * API Client
     *
     * @var \Swagger\Client\ApiClient instance of the ApiClient
     */
    protected $apiClient;

    /**
     * Constructor
     *
     * @param \Swagger\Client\ApiClient|null $apiClient The api client to use
     */
    public function __construct(\Swagger\Client\ApiClient $apiClient = null)
    {
        if ($apiClient === null) {
            $apiClient = new ApiClient();
        }

        $this->apiClient = $apiClient;
    }

    /**
     * Get API client
     *
     * @return \Swagger\Client\ApiClient get the API client
     */
    public function getApiClient()
    {
        return $this->apiClient;
    }

    /**
     * Set the API client
     *
     * @param \Swagger\Client\ApiClient $apiClient set the API client
     *
     * @return CreditMemosApi
     */
    public function setApiClient(\Swagger\Client\ApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
        return $this;
    }

    /**
     * Operation gETCreditMemos
     *
     * List credit memos
     *
     * @param string $accept_encoding Include the &#x60;Accept-Encoding: gzip&#x60; header to compress responses as a gzipped file. It can significantly reduce the bandwidth required for a response.   If specified, Zuora automatically compresses responses that contain over 1000 bytes of data, and the response contains a &#x60;Content-Encoding&#x60; header with the compression algorithm so that your client can decompress it. (optional)
     * @param string $content_encoding Include the &#x60;Content-Encoding: gzip&#x60; header to compress a request. With this header specified, you should upload a gzipped file for the request payload instead of sending the JSON payload. (optional)
     * @param string $authorization The value is in the &#x60;Bearer {token}&#x60; format where {token} is a valid OAuth token generated by calling [Create an OAuth token](https://www.zuora.com/developer/api-references/api/operation/createToken). (optional)
     * @param string $zuora_track_id A custom identifier for tracing the API call. If you set a value for this header, Zuora returns the same value in the response headers. This header enables you to associate your system process identifiers with Zuora API calls, to assist with troubleshooting in the event of an issue.  The value of this field must use the US-ASCII character set and must not include any of the following characters: colon (&#x60;:&#x60;), semicolon (&#x60;;&#x60;), double quote (&#x60;\&quot;&#x60;), and quote (&#x60;&#39;&#x60;). (optional)
     * @param string $zuora_entity_ids An entity ID. If you have [Zuora Multi-entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity) enabled and the OAuth token is valid for more than one entity, you must use this header to specify which entity to perform the operation in. If the OAuth token is only valid for a single entity, or you do not have Zuora Multi-entity enabled, you do not need to set this header. (optional)
     * @param int $page The index number of the page that you want to retrieve. This parameter is dependent on &#x60;pageSize&#x60;. You must set &#x60;pageSize&#x60; before specifying &#x60;page&#x60;. For example, if you set &#x60;pageSize&#x60; to &#x60;20&#x60; and &#x60;page&#x60; to &#x60;2&#x60;, the 21st to 40th records are returned in the response. (optional, default to 1)
     * @param int $page_size The number of records returned per page in the response. (optional, default to 20)
     * @param string $account_id This parameter filters the response based on the &#x60;accountId&#x60; field. (optional)
     * @param string $account_number This parameter filters the response based on the &#x60;accountNumber&#x60; field. (optional)
     * @param double $amount This parameter filters the response based on the &#x60;amount&#x60; field. (optional)
     * @param double $applied_amount This parameter filters the response based on the &#x60;appliedAmount&#x60; field. (optional)
     * @param bool $auto_apply_upon_posting This parameter filters the response based on the &#x60;autoApplyUponPosting&#x60; field. (optional)
     * @param string $created_by_id This parameter filters the response based on the &#x60;createdById&#x60; field. (optional)
     * @param \DateTime $created_date This parameter filters the response based on the &#x60;createdDate&#x60; field. (optional)
     * @param \DateTime $credit_memo_date This parameter filters the response based on the &#x60;creditMemoDate&#x60; field. (optional)
     * @param string $currency This parameter filters the response based on the &#x60;currency&#x60; field. (optional)
     * @param bool $exclude_from_auto_apply_rules This parameter filters the response based on the &#x60;excludeFromAutoApplyRules&#x60; field. (optional)
     * @param string $number This parameter filters the response based on the &#x60;number&#x60; field. (optional)
     * @param string $referred_invoice_id This parameter filters the response based on the &#x60;referredInvoiceId&#x60; field. (optional)
     * @param double $refund_amount This parameter filters the response based on the &#x60;refundAmount&#x60; field. (optional)
     * @param string $status This parameter filters the response based on the &#x60;status&#x60; field. (optional)
     * @param \DateTime $target_date This parameter filters the response based on the &#x60;targetDate&#x60; field. (optional)
     * @param double $tax_amount This parameter filters the response based on the &#x60;taxAmount&#x60; field. (optional)
     * @param double $total_tax_exempt_amount This parameter filters the response based on the &#x60;totalTaxExemptAmount&#x60; field. (optional)
     * @param string $transferred_to_accounting This parameter filters the response based on the &#x60;transferredToAccounting&#x60; field. (optional)
     * @param double $unapplied_amount This parameter filters the response based on the &#x60;unappliedAmount&#x60; field. (optional)
     * @param string $updated_by_id This parameter filters the response based on the &#x60;updatedById&#x60; field. (optional)
     * @param \DateTime $updated_date This parameter filters the response based on the &#x60;updatedDate&#x60; field. (optional)
     * @param string $sort This parameter restricts the order of the data returned in the response. You can use this parameter to supply a dimension you want to sort on.  A sortable field uses the following form:   *operator* *field_name*  You can use at most two sortable fields in one URL path. Use a comma to separate sortable fields. For example:  *operator* *field_name*, *operator* *field_name*    *operator* is used to mark the order of sequencing. The operator is optional. If you only specify the sortable field without any operator, the response data is sorted in descending order by this field.    - The &#x60;-&#x60; operator indicates an ascending order.   - The &#x60;+&#x60; operator indicates a descending order.  By default, the response data is displayed in descending order by credit memo number.  *field_name* indicates the name of a sortable field. The supported sortable fields of this operation are as below:    - accountId   - amount   - appliedAmount   - createdById   - createdDate   - creditMemoDate   - number   - referredInvoiceId   - refundAmount   - status   - targetDate   - taxAmount   - totalTaxExemptAmount   - transferredToAccounting   - unappliedAmount   - updatedDate     Examples:  - /v1/creditmemos?sort&#x3D;+number  - /v1/creditmemos?status&#x3D;Processed&amp;sort&#x3D;-number,+amount (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\GETCreditMemoCollectionType
     */
    public function gETCreditMemos($accept_encoding = null, $content_encoding = null, $authorization = null, $zuora_track_id = null, $zuora_entity_ids = null, $page = null, $page_size = null, $account_id = null, $account_number = null, $amount = null, $applied_amount = null, $auto_apply_upon_posting = null, $created_by_id = null, $created_date = null, $credit_memo_date = null, $currency = null, $exclude_from_auto_apply_rules = null, $number = null, $referred_invoice_id = null, $refund_amount = null, $status = null, $target_date = null, $tax_amount = null, $total_tax_exempt_amount = null, $transferred_to_accounting = null, $unapplied_amount = null, $updated_by_id = null, $updated_date = null, $sort = null)
    {
        list($response) = $this->gETCreditMemosWithHttpInfo($accept_encoding, $content_encoding, $authorization, $zuora_track_id, $zuora_entity_ids, $page, $page_size, $account_id, $account_number, $amount, $applied_amount, $auto_apply_upon_posting, $created_by_id, $created_date, $credit_memo_date, $currency, $exclude_from_auto_apply_rules, $number, $referred_invoice_id, $refund_amount, $status, $target_date, $tax_amount, $total_tax_exempt_amount, $transferred_to_accounting, $unapplied_amount, $updated_by_id, $updated_date, $sort);
        return $response;
    }

    /**
     * Operation gETCreditMemosWithHttpInfo
     *
     * List credit memos
     *
     * @param string $accept_encoding Include the &#x60;Accept-Encoding: gzip&#x60; header to compress responses as a gzipped file. It can significantly reduce the bandwidth required for a response.   If specified, Zuora automatically compresses responses that contain over 1000 bytes of data, and the response contains a &#x60;Content-Encoding&#x60; header with the compression algorithm so that your client can decompress it. (optional)
     * @param string $content_encoding Include the &#x60;Content-Encoding: gzip&#x60; header to compress a request. With this header specified, you should upload a gzipped file for the request payload instead of sending the JSON payload. (optional)
     * @param string $authorization The value is in the &#x60;Bearer {token}&#x60; format where {token} is a valid OAuth token generated by calling [Create an OAuth token](https://www.zuora.com/developer/api-references/api/operation/createToken). (optional)
     * @param string $zuora_track_id A custom identifier for tracing the API call. If you set a value for this header, Zuora returns the same value in the response headers. This header enables you to associate your system process identifiers with Zuora API calls, to assist with troubleshooting in the event of an issue.  The value of this field must use the US-ASCII character set and must not include any of the following characters: colon (&#x60;:&#x60;), semicolon (&#x60;;&#x60;), double quote (&#x60;\&quot;&#x60;), and quote (&#x60;&#39;&#x60;). (optional)
     * @param string $zuora_entity_ids An entity ID. If you have [Zuora Multi-entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity) enabled and the OAuth token is valid for more than one entity, you must use this header to specify which entity to perform the operation in. If the OAuth token is only valid for a single entity, or you do not have Zuora Multi-entity enabled, you do not need to set this header. (optional)
     * @param int $page The index number of the page that you want to retrieve. This parameter is dependent on &#x60;pageSize&#x60;. You must set &#x60;pageSize&#x60; before specifying &#x60;page&#x60;. For example, if you set &#x60;pageSize&#x60; to &#x60;20&#x60; and &#x60;page&#x60; to &#x60;2&#x60;, the 21st to 40th records are returned in the response. (optional, default to 1)
     * @param int $page_size The number of records returned per page in the response. (optional, default to 20)
     * @param string $account_id This parameter filters the response based on the &#x60;accountId&#x60; field. (optional)
     * @param string $account_number This parameter filters the response based on the &#x60;accountNumber&#x60; field. (optional)
     * @param double $amount This parameter filters the response based on the &#x60;amount&#x60; field. (optional)
     * @param double $applied_amount This parameter filters the response based on the &#x60;appliedAmount&#x60; field. (optional)
     * @param bool $auto_apply_upon_posting This parameter filters the response based on the &#x60;autoApplyUponPosting&#x60; field. (optional)
     * @param string $created_by_id This parameter filters the response based on the &#x60;createdById&#x60; field. (optional)
     * @param \DateTime $created_date This parameter filters the response based on the &#x60;createdDate&#x60; field. (optional)
     * @param \DateTime $credit_memo_date This parameter filters the response based on the &#x60;creditMemoDate&#x60; field. (optional)
     * @param string $currency This parameter filters the response based on the &#x60;currency&#x60; field. (optional)
     * @param bool $exclude_from_auto_apply_rules This parameter filters the response based on the &#x60;excludeFromAutoApplyRules&#x60; field. (optional)
     * @param string $number This parameter filters the response based on the &#x60;number&#x60; field. (optional)
     * @param string $referred_invoice_id This parameter filters the response based on the &#x60;referredInvoiceId&#x60; field. (optional)
     * @param double $refund_amount This parameter filters the response based on the &#x60;refundAmount&#x60; field. (optional)
     * @param string $status This parameter filters the response based on the &#x60;status&#x60; field. (optional)
     * @param \DateTime $target_date This parameter filters the response based on the &#x60;targetDate&#x60; field. (optional)
     * @param double $tax_amount This parameter filters the response based on the &#x60;taxAmount&#x60; field. (optional)
     * @param double $total_tax_exempt_amount This parameter filters the response based on the &#x60;totalTaxExemptAmount&#x60; field. (optional)
     * @param string $transferred_to_accounting This parameter filters the response based on the &#x60;transferredToAccounting&#x60; field. (optional)
     * @param double $unapplied_amount This parameter filters the response based on the &#x60;unappliedAmount&#x60; field. (optional)
     * @param string $updated_by_id This parameter filters the response based on the &#x60;updatedById&#x60; field. (optional)
     * @param \DateTime $updated_date This parameter filters the response based on the &#x60;updatedDate&#x60; field. (optional)
     * @param string $sort This parameter restricts the order of the data returned in the response. You can use this parameter to supply a dimension you want to sort on.  A sortable field uses the following form:   *operator* *field_name*  You can use at most two sortable fields in one URL path. Use a comma to separate sortable fields. For example:  *operator* *field_name*, *operator* *field_name*    *operator* is used to mark the order of sequencing. The operator is optional. If you only specify the sortable field without any operator, the response data is sorted in descending order by this field.    - The &#x60;-&#x60; operator indicates an ascending order.   - The &#x60;+&#x60; operator indicates a descending order.  By default, the response data is displayed in descending order by credit memo number.  *field_name* indicates the name of a sortable field. The supported sortable fields of this operation are as below:    - accountId   - amount   - appliedAmount   - createdById   - createdDate   - creditMemoDate   - number   - referredInvoiceId   - refundAmount   - status   - targetDate   - taxAmount   - totalTaxExemptAmount   - transferredToAccounting   - unappliedAmount   - updatedDate     Examples:  - /v1/creditmemos?sort&#x3D;+number  - /v1/creditmemos?status&#x3D;Processed&amp;sort&#x3D;-number,+amount (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return array of \Swagger\Client\Model\GETCreditMemoCollectionType, HTTP status code, HTTP response headers (array of strings)
     */
    public function gETCreditMemosWithHttpInfo($accept_encoding = null, $content_encoding = null, $authorization = null, $zuora_track_id = null, $zuora_entity_ids = null, $page = null, $page_size = null, $account_id = null, $account_number = null, $amount = null, $applied_amount = null, $auto_apply_upon_posting = null, $created_by_id = null, $created_date = null, $credit_memo_date = null, $currency = null, $exclude_from_auto_apply_rules = null, $number = null, $referred_invoice_id = null, $refund_amount = null, $status = null, $target_date = null, $tax_amount = null, $total_tax_exempt_amount = null, $transferred_to_accounting = null, $unapplied_amount = null, $updated_by_id = null, $updated_date = null, $sort = null)
    {
        if (!is_null($zuora_track_id) && (strlen($zuora_track_id) > 64)) {
            throw new \InvalidArgumentException('invalid length for "$zuora_track_id" when calling CreditMemosApi.gETCreditMemos, must be smaller than or equal to 64.');
        }

        if (!is_null($page) && ($page < 1)) {
            throw new \InvalidArgumentException('invalid value for "$page" when calling CreditMemosApi.gETCreditMemos, must be bigger than or equal to 1.');
        }

        if (!is_null($page_size) && ($page_size > 40)) {
            throw new \InvalidArgumentException('invalid value for "$page_size" when calling CreditMemosApi.gETCreditMemos, must be smaller than or equal to 40.');
        }

        // parse inputs
        $resourcePath = "/v1/creditmemos";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json; charset=utf-8']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json; charset=utf-8']);

        // query params
        if ($page !== null) {
            $queryParams['page'] = $this->apiClient->getSerializer()->toQueryValue($page);
        }
        // query params
        if ($page_size !== null) {
            $queryParams['pageSize'] = $this->apiClient->getSerializer()->toQueryValue($page_size);
        }
        // query params
        if ($account_id !== null) {
            $queryParams['accountId'] = $this->apiClient->getSerializer()->toQueryValue($account_id);
        }
        // query params
        if ($account_number !== null) {
            $queryParams['accountNumber'] = $this->apiClient->getSerializer()->toQueryValue($account_number);
        }
        // query params
        if ($amount !== null) {
            $queryParams['amount'] = $this->apiClient->getSerializer()->toQueryValue($amount);
        }
        // query params
        if ($applied_amount !== null) {
            $queryParams['appliedAmount'] = $this->apiClient->getSerializer()->toQueryValue($applied_amount);
        }
        // query params
        if ($auto_apply_upon_posting !== null) {
            $queryParams['autoApplyUponPosting'] = $this->apiClient->getSerializer()->toQueryValue($auto_apply_upon_posting);
        }
        // query params
        if ($created_by_id !== null) {
            $queryParams['createdById'] = $this->apiClient->getSerializer()->toQueryValue($created_by_id);
        }
        // query params
        if ($created_date !== null) {
            $queryParams['createdDate'] = $this->apiClient->getSerializer()->toQueryValue($created_date);
        }
        // query params
        if ($credit_memo_date !== null) {
            $queryParams['creditMemoDate'] = $this->apiClient->getSerializer()->toQueryValue($credit_memo_date);
        }
        // query params
        if ($currency !== null) {
            $queryParams['currency'] = $this->apiClient->getSerializer()->toQueryValue($currency);
        }
        // query params
        if ($exclude_from_auto_apply_rules !== null) {
            $queryParams['excludeFromAutoApplyRules'] = $this->apiClient->getSerializer()->toQueryValue($exclude_from_auto_apply_rules);
        }
        // query params
        if ($number !== null) {
            $queryParams['number'] = $this->apiClient->getSerializer()->toQueryValue($number);
        }
        // query params
        if ($referred_invoice_id !== null) {
            $queryParams['referredInvoiceId'] = $this->apiClient->getSerializer()->toQueryValue($referred_invoice_id);
        }
        // query params
        if ($refund_amount !== null) {
            $queryParams['refundAmount'] = $this->apiClient->getSerializer()->toQueryValue($refund_amount);
        }
        // query params
        if ($status !== null) {
            $queryParams['status'] = $this->apiClient->getSerializer()->toQueryValue($status);
        }
        // query params
        if ($target_date !== null) {
            $queryParams['targetDate'] = $this->apiClient->getSerializer()->toQueryValue($target_date);
        }
        // query params
        if ($tax_amount !== null) {
            $queryParams['taxAmount'] = $this->apiClient->getSerializer()->toQueryValue($tax_amount);
        }
        // query params
        if ($total_tax_exempt_amount !== null) {
            $queryParams['totalTaxExemptAmount'] = $this->apiClient->getSerializer()->toQueryValue($total_tax_exempt_amount);
        }
        // query params
        if ($transferred_to_accounting !== null) {
            $queryParams['transferredToAccounting'] = $this->apiClient->getSerializer()->toQueryValue($transferred_to_accounting);
        }
        // query params
        if ($unapplied_amount !== null) {
            $queryParams['unappliedAmount'] = $this->apiClient->getSerializer()->toQueryValue($unapplied_amount);
        }
        // query params
        if ($updated_by_id !== null) {
            $queryParams['updatedById'] = $this->apiClient->getSerializer()->toQueryValue($updated_by_id);
        }
        // query params
        if ($updated_date !== null) {
            $queryParams['updatedDate'] = $this->apiClient->getSerializer()->toQueryValue($updated_date);
        }
        // query params
        if ($sort !== null) {
            $queryParams['sort'] = $this->apiClient->getSerializer()->toQueryValue($sort);
        }
        // header params
        if ($accept_encoding !== null) {
            $headerParams['Accept-Encoding'] = $this->apiClient->getSerializer()->toHeaderValue($accept_encoding);
        }
        // header params
        if ($content_encoding !== null) {
            $headerParams['Content-Encoding'] = $this->apiClient->getSerializer()->toHeaderValue($content_encoding);
        }
        // header params
        if ($authorization !== null) {
            $headerParams['Authorization'] = $this->apiClient->getSerializer()->toHeaderValue($authorization);
        }
        // header params
        if ($zuora_track_id !== null) {
            $headerParams['Zuora-Track-Id'] = $this->apiClient->getSerializer()->toHeaderValue($zuora_track_id);
        }
        // header params
        if ($zuora_entity_ids !== null) {
            $headerParams['Zuora-Entity-Ids'] = $this->apiClient->getSerializer()->toHeaderValue($zuora_entity_ids);
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Swagger\Client\Model\GETCreditMemoCollectionType',
                '/v1/creditmemos'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, '\Swagger\Client\Model\GETCreditMemoCollectionType', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Swagger\Client\Model\GETCreditMemoCollectionType', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation pOSTCreditMemoPDF
     *
     * Generate a credit memo PDF file
     *
     * @param string $credit_memo_key The unique ID or number of the credit memo that you want to create a PDF file for. For example, 8a8082e65b27f6c3015ba45ff82c7172 or CM00000001. (required)
     * @param string $idempotency_key Specify a unique idempotency key if you want to perform an idempotent POST or PATCH request. Do not use this header in other request types.   With this header specified, the Zuora server can identify subsequent retries of the same request using this value, which prevents the same operation from being performed multiple times by accident. (optional)
     * @param string $accept_encoding Include the &#x60;Accept-Encoding: gzip&#x60; header to compress responses as a gzipped file. It can significantly reduce the bandwidth required for a response.   If specified, Zuora automatically compresses responses that contain over 1000 bytes of data, and the response contains a &#x60;Content-Encoding&#x60; header with the compression algorithm so that your client can decompress it. (optional)
     * @param string $content_encoding Include the &#x60;Content-Encoding: gzip&#x60; header to compress a request. With this header specified, you should upload a gzipped file for the request payload instead of sending the JSON payload. (optional)
     * @param string $authorization The value is in the &#x60;Bearer {token}&#x60; format where {token} is a valid OAuth token generated by calling [Create an OAuth token](https://www.zuora.com/developer/api-references/api/operation/createToken). (optional)
     * @param string $zuora_track_id A custom identifier for tracing the API call. If you set a value for this header, Zuora returns the same value in the response headers. This header enables you to associate your system process identifiers with Zuora API calls, to assist with troubleshooting in the event of an issue.  The value of this field must use the US-ASCII character set and must not include any of the following characters: colon (&#x60;:&#x60;), semicolon (&#x60;;&#x60;), double quote (&#x60;\&quot;&#x60;), and quote (&#x60;&#39;&#x60;). (optional)
     * @param string $zuora_entity_ids An entity ID. If you have [Zuora Multi-entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity) enabled and the OAuth token is valid for more than one entity, you must use this header to specify which entity to perform the operation in. If the OAuth token is only valid for a single entity, or you do not have Zuora Multi-entity enabled, you do not need to set this header. (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\POSTMemoPdfResponse
     */
    public function pOSTCreditMemoPDF($credit_memo_key, $idempotency_key = null, $accept_encoding = null, $content_encoding = null, $authorization = null, $zuora_track_id = null, $zuora_entity_ids = null)
    {
        list($response) = $this->pOSTCreditMemoPDFWithHttpInfo($credit_memo_key, $idempotency_key, $accept_encoding, $content_encoding, $authorization, $zuora_track_id, $zuora_entity_ids);
        return $response;
    }

    /**
     * Operation pOSTCreditMemoPDFWithHttpInfo
     *
     * Generate a credit memo PDF file
     *
     * @param string $credit_memo_key The unique ID or number of the credit memo that you want to create a PDF file for. For example, 8a8082e65b27f6c3015ba45ff82c7172 or CM00000001. (required)
     * @param string $idempotency_key Specify a unique idempotency key if you want to perform an idempotent POST or PATCH request. Do not use this header in other request types.   With this header specified, the Zuora server can identify subsequent retries of the same request using this value, which prevents the same operation from being performed multiple times by accident. (optional)
     * @param string $accept_encoding Include the &#x60;Accept-Encoding: gzip&#x60; header to compress responses as a gzipped file. It can significantly reduce the bandwidth required for a response.   If specified, Zuora automatically compresses responses that contain over 1000 bytes of data, and the response contains a &#x60;Content-Encoding&#x60; header with the compression algorithm so that your client can decompress it. (optional)
     * @param string $content_encoding Include the &#x60;Content-Encoding: gzip&#x60; header to compress a request. With this header specified, you should upload a gzipped file for the request payload instead of sending the JSON payload. (optional)
     * @param string $authorization The value is in the &#x60;Bearer {token}&#x60; format where {token} is a valid OAuth token generated by calling [Create an OAuth token](https://www.zuora.com/developer/api-references/api/operation/createToken). (optional)
     * @param string $zuora_track_id A custom identifier for tracing the API call. If you set a value for this header, Zuora returns the same value in the response headers. This header enables you to associate your system process identifiers with Zuora API calls, to assist with troubleshooting in the event of an issue.  The value of this field must use the US-ASCII character set and must not include any of the following characters: colon (&#x60;:&#x60;), semicolon (&#x60;;&#x60;), double quote (&#x60;\&quot;&#x60;), and quote (&#x60;&#39;&#x60;). (optional)
     * @param string $zuora_entity_ids An entity ID. If you have [Zuora Multi-entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity) enabled and the OAuth token is valid for more than one entity, you must use this header to specify which entity to perform the operation in. If the OAuth token is only valid for a single entity, or you do not have Zuora Multi-entity enabled, you do not need to set this header. (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return array of \Swagger\Client\Model\POSTMemoPdfResponse, HTTP status code, HTTP response headers (array of strings)
     */
    public function pOSTCreditMemoPDFWithHttpInfo($credit_memo_key, $idempotency_key = null, $accept_encoding = null, $content_encoding = null, $authorization = null, $zuora_track_id = null, $zuora_entity_ids = null)
    {
        // verify the required parameter 'credit_memo_key' is set
        if ($credit_memo_key === null) {
            throw new \InvalidArgumentException('Missing the required parameter $credit_memo_key when calling pOSTCreditMemoPDF');
        }
        if (!is_null($idempotency_key) && (strlen($idempotency_key) > 255)) {
            throw new \InvalidArgumentException('invalid length for "$idempotency_key" when calling CreditMemosApi.pOSTCreditMemoPDF, must be smaller than or equal to 255.');
        }

        if (!is_null($zuora_track_id) && (strlen($zuora_track_id) > 64)) {
            throw new \InvalidArgumentException('invalid length for "$zuora_track_id" when calling CreditMemosApi.pOSTCreditMemoPDF, must be smaller than or equal to 64.');
        }

        // parse inputs
        $resourcePath = "/v1/creditmemos/{creditMemoKey}/pdfs";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json; charset=utf-8']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json; charset=utf-8']);

        // header params
        if ($idempotency_key !== null) {
            $headerParams['Idempotency-Key'] = $this->apiClient->getSerializer()->toHeaderValue($idempotency_key);
        }
        // header params
        if ($accept_encoding !== null) {
            $headerParams['Accept-Encoding'] = $this->apiClient->getSerializer()->toHeaderValue($accept_encoding);
        }
        // header params
        if ($content_encoding !== null) {
            $headerParams['Content-Encoding'] = $this->apiClient->getSerializer()->toHeaderValue($content_encoding);
        }
        // header params
        if ($authorization !== null) {
            $headerParams['Authorization'] = $this->apiClient->getSerializer()->toHeaderValue($authorization);
        }
        // header params
        if ($zuora_track_id !== null) {
            $headerParams['Zuora-Track-Id'] = $this->apiClient->getSerializer()->toHeaderValue($zuora_track_id);
        }
        // header params
        if ($zuora_entity_ids !== null) {
            $headerParams['Zuora-Entity-Ids'] = $this->apiClient->getSerializer()->toHeaderValue($zuora_entity_ids);
        }
        // path params
        if ($credit_memo_key !== null) {
            $resourcePath = str_replace(
                "{" . "creditMemoKey" . "}",
                $this->apiClient->getSerializer()->toPathValue($credit_memo_key),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'POST',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Swagger\Client\Model\POSTMemoPdfResponse',
                '/v1/creditmemos/{creditMemoKey}/pdfs'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, '\Swagger\Client\Model\POSTMemoPdfResponse', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Swagger\Client\Model\POSTMemoPdfResponse', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }
}
