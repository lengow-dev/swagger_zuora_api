# SwaggerClient-php
# Introduction Welcome to the reference for the Zuora REST API!  <a href=\"http://en.wikipedia.org/wiki/REST_API\" target=\"_blank\">REST</a> is a web-service protocol that lends itself to rapid development by using everyday HTTP and JSON technology.  The Zuora REST API provides a broad set of operations and resources that:    * Enable Web Storefront integration from your website.   * Support self-service subscriber sign-ups and account management.   * Process revenue schedules through custom revenue rule models.   * Enable manipulation of most objects in the Zuora Object Model.      ## Endpoints      The Zuora REST API is provided via the following endpoints.   | Tenant              | Base URL for REST Endpoints |   |-------------------------|-------------------------|   |Production | https://rest.zuora.com/v1   |   |API Sandbox    | https://rest.apisandbox.zuora.com/v1|      The production endpoint provides access to your live user data. The API Sandbox tenant is a good place to test your code without affecting real-world data. To use it, you must be provisioned with an API Sandbox tenant - your Zuora representative can help you if needed.      ## Access to the API      If you have a Zuora tenant, you already have access to the API.      If you don't have a Zuora tenant, go to <a href=\" https://www.zuora.com/resource/zuora-test-drive\" target=\"_blank\">https://www.zuora.com/resource/zuora-test-drive</a> and sign up for a Production Test Drive tenant. The tenant comes with seed data, such as a sample product catalog.  We recommend that you <a href=\"https://knowledgecenter.zuora.com/CF_Users_and_Administrators/A_Administrator_Settings/Manage_Users/Create_an_API_User\" target=\"_blank\">create an API user</a> specifically for making API calls. Don't log in to the Zuora UI with this account. Logging in to the UI enables a security feature that periodically expires the account's password, which may eventually cause authentication failures with the API. Note that a user role does not have write access to Zuora REST services unless it has the API Write Access permission as described in those instructions.  # Authentication  There are three ways to authenticate:    * Use username and password. Include authentication with each request in the header:         * `apiAccessKeyId`      * `apiSecretAccessKey`     * `entityId` or `entityName` (Only for [Zuora Multi-entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity \"Multi-entity\"). See \"Entity Id and Entity Name\" below for more information.)   * Use an authorization cookie. The cookie authorizes the user to make calls to the REST API for the duration specified in  **Administration > Security Policies > Session timeout**. The cookie expiration time is reset with this duration after every call to the REST API. To obtain a cookie, call the REST  `connections` resource with the following API user information:         *   ID         *   password     *   entity Id or entity name (Only for [Zuora Multi-entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity \"Multi-entity\"). See \"Entity Id and Entity Name\" below for more information.)         * For CORS-enabled APIs only: Include a 'single-use' token in the request header, which re-authenticates the user with each request. See below for more details.  ## Entity Id and Entity Name  The `entityId` and `entityName` parameters are only used for [Zuora Multi-entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity \"Zuora Multi-entity\").   The  `entityId` and `entityName` parameters specify the Id and the [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name \"Introduction to Entity and Entity Hierarchy\") that you want to access, respectively. Note that you must have permission to access the entity.   You can specify either the `entityId` or `entityName` parameter in the authentication to access and view an entity.    * If both `entityId` and `entityName` are specified in the authentication, an error occurs.    * If neither `entityId` nor `entityName` is specified in the authentication, you will log in to the entity in which your user account is created.      To get the entity Id and entity name, you can use the GET Entities REST call. For more information, see [API User Authentication](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/A_Overview_of_Multi-entity#API_User_Authentication \"API User Authentication\").      ## Token Authentication for CORS-Enabled APIs      The CORS mechanism enables REST API calls to Zuora to be made directly from your customer's browser, with all credit card and security information transmitted directly to Zuora. This minimizes your PCI compliance burden, allows you to implement advanced validation on your payment forms, and  makes your payment forms look just like any other part of your website.    For security reasons, instead of using cookies, an API request via CORS uses **tokens** for authentication.  The token method of authentication is only designed for use with requests that must originate from your customer's browser; **it should  not be considered a replacement to the existing cookie authentication** mechanism.  See [Zuora CORS REST](https://knowledgecenter.zuora.com/DC_Developers/REST_API/A_REST_basics/G_CORS_REST \"Zuora CORS REST\") for details on how CORS works and how you can begin to implement customer calls to the Zuora REST APIs. See  [HMAC Signatures](https://www.zuora.com/developer/API-Reference/#operation/POSTHMACSignature \"HMAC Signatures\") for details on the HMAC method that returns the authentication token.  # Requests and Responses  ## Request IDs  As a general rule, when asked to supply a \"key\" for an account or subscription (accountKey, account-key, subscriptionKey, subscription-key), you can provide either the actual ID or  the number of the entity.  ## HTTP Request Body  Most of the parameters and data accompanying your requests will be contained in the body of the HTTP request.   The Zuora REST API accepts JSON in the HTTP request body. No other data format (e.g., XML) is supported.  ## Testing a Request  Use a third party client, such as [curl](https://curl.haxx.se \"curl\"), [Postman](https://www.getpostman.com \"Postman\"), or [Advanced REST Client](https://advancedrestclient.com \"Advanced REST Client\"), to test the Zuora REST API.  You can test the Zuora REST API from the Zuora API Sandbox or Production tenants. If connecting to Production, bear in mind that you are working with your live production data, not sample data or test data.  ## Testing with Credit Cards  Sooner or later it will probably be necessary to test some transactions that involve credit cards. For suggestions on how to handle this, see [Going Live With Your Payment Gateway](https://knowledgecenter.zuora.com/CB_Billing/M_Payment_Gateways/C_Managing_Payment_Gateways/B_Going_Live_Payment_Gateways#Testing_with_Credit_Cards \"C_Zuora_User_Guides/A_Billing_and_Payments/M_Payment_Gateways/C_Managing_Payment_Gateways/B_Going_Live_Payment_Gateways#Testing_with_Credit_Cards\" ).  ## Error Handling  Responses and error codes are detailed in [Responses and errors](https://knowledgecenter.zuora.com/DC_Developers/REST_API/A_REST_basics/3_Responses_and_errors \"Responses and errors\").  # Pagination  When retrieving information (using GET methods), the optional `pageSize` query parameter sets the maximum number of rows to return in a response. The maximum is `40`; larger values are treated as `40`. If this value is empty or invalid, `pageSize` typically defaults to `10`.  The default value for the maximum number of rows retrieved can be overridden at the method level.  If more rows are available, the response will include a `nextPage` element, which contains a URL for requesting the next page.  If this value is not provided, no more rows are available. No \"previous page\" element is explicitly provided; to support backward paging, use the previous call.  ## Array Size  For data items that are not paginated, the REST API supports arrays of up to 300 rows.  Thus, for instance, repeated pagination can retrieve thousands of customer accounts, but within any account an array of no more than 300 rate plans is returned.  # API Versions  The Zuora REST API are version controlled. Versioning ensures that Zuora REST API changes are backward compatible. Zuora uses a major and minor version nomenclature to manage changes. By specifying a version in a REST request, you can get expected responses regardless of future changes to the API.  ## Major Version  The major version number of the REST API appears in the REST URL. Currently, Zuora only supports the **v1** major version. For example, `POST https://rest.zuora.com/v1/subscriptions`.  ## Minor Version  Zuora uses minor versions for the REST API to control small changes. For example, a field in a REST method is deprecated and a new field is used to replace it.   Some fields in the REST methods are supported as of minor versions. If a field is not noted with a minor version, this field is available for all minor versions. If a field is noted with a minor version, this field is in version control. You must specify the supported minor version in the request header to process without an error.   If a field is in version control, it is either with a minimum minor version or a maximum minor version, or both of them. You can only use this field with the minor version between the minimum and the maximum minor versions. For example, the `invoiceCollect` field in the POST Subscription method is in version control and its maximum minor version is 189.0. You can only use this field with the minor version 189.0 or earlier.  If you specify a version number in the request header that is not supported, Zuora will use the minimum minor version of the REST API. In our REST API documentation, if a field or feature requires a minor version number, we note that in the field description.  You only need to specify the version number when you use the fields require a minor version. To specify the minor version, set the `zuora-version` parameter to the minor version number in the request header for the request call. For example, the `collect` field is in 196.0 minor version. If you want to use this field for the POST Subscription method, set the  `zuora-version` parameter to `196.0` in the request header. The `zuora-version` parameter is case sensitive.  For all the REST API fields, by default, if the minor version is not specified in the request header, Zuora will use the minimum minor version of the REST API to avoid breaking your integration.   ### Minor Version History  The supported minor versions are not serial. This section documents the changes made to each Zuora REST API minor version.  The following table lists the supported versions and the fields that have a Zuora REST API minor version.  | Fields         | Minor Version      | REST Methods    | Description | |:--------|:--------|:--------|:--------| | invoiceCollect | 189.0 and earlier  | [Create Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_Subscription \"Create Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\"); [Renew Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_RenewSubscription \"Renew Subscription\"); [Cancel Subscription](https://www.zuora.com/developer/api-reference/#operation/POSTSubscriptionCancellation \"Cancel Subscription\"); [Suspend Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionSuspend \"Suspend Subscription\"); [Resume Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionResume \"Resume Subscription\"); [Create Account](https://www.zuora.com/developer/API-Reference/#operation/POST_Account \"Create Account\")|Generates an invoice and collects a payment for a subscription. | | collect        | 196.0 and later    | [Create Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_Subscription \"Create Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\"); [Renew Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_RenewSubscription \"Renew Subscription\"); [Cancel Subscription](https://www.zuora.com/developer/api-reference/#operation/POSTSubscriptionCancellation \"Cancel Subscription\"); [Suspend Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionSuspend \"Suspend Subscription\"); [Resume Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionResume \"Resume Subscription\"); [Create Account](https://www.zuora.com/developer/API-Reference/#operation/POST_Account \"Create Account\")|Collects an automatic payment for a subscription. | | invoice | 196.0 and later| [Create Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_Subscription \"Create Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\"); [Renew Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_RenewSubscription \"Renew Subscription\"); [Cancel Subscription](https://www.zuora.com/developer/api-reference/#operation/POSTSubscriptionCancellation \"Cancel Subscription\"); [Suspend Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionSuspend \"Suspend Subscription\"); [Resume Subscription](https://www.zuora.com/developer/api-reference/#operation/PUTSubscriptionResume \"Resume Subscription\"); [Create Account](https://www.zuora.com/developer/API-Reference/#operation/POST_Account \"Create Account\")|Generates an invoice for a subscription. | | invoiceTargetDate | 196.0 and earlier  | [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\") |Date through which charges are calculated on the invoice, as `yyyy-mm-dd`. | | targetDate | 207.0 and later | [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\") |Date through which charges are calculated on the invoice, as `yyyy-mm-dd`. | | includeExisting DraftInvoiceItems | 196.0 and earlier| [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") | Specifies whether to include draft invoice items in subscription previews. Specify it to be `true` (default) to include draft invoice items in the preview result. Specify it to be `false` to excludes draft invoice items in the preview result. | | includeExisting DraftDocItems | 207.0 and later  | [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") | Specifies whether to include draft invoice items in subscription previews. Specify it to be `true` (default) to include draft invoice items in the preview result. Specify it to be `false` to excludes draft invoice items in the preview result. | | previewType | 196.0 and earlier| [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") | The type of preview you will receive. The possible values are `InvoiceItem`(default), `ChargeMetrics`, and `InvoiceItemChargeMetrics`. | | previewType | 207.0 and later  | [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\"); [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") | The type of preview you will receive. The possible values are `LegalDoc`(default), `ChargeMetrics`, and `LegalDocChargeMetrics`. |  #### Version 207.0 and Later  The response structure of the [Preview Subscription](https://www.zuora.com/developer/api-reference/#operation/POST_SubscriptionPreview \"Preview Subscription\") and [Update Subscription](https://www.zuora.com/developer/api-reference/#operation/PUT_Subscription \"Update Subscription\") methods are changed. The following invoice related response fields are moved to the invoice container:    * amount   * amountWithoutTax   * taxAmount   * invoiceItems   * targetDate   * chargeMetrics  # Zuora Object Model  The following diagram presents a high-level view of the key Zuora objects. Click the image to open it in a new tab to resize it.  <a href=\"https://www.zuora.com/wp-content/uploads/2017/01/ZuoraERD.jpeg\" target=\"_blank\"><img src=\"https://www.zuora.com/wp-content/uploads/2017/01/ZuoraERD.jpeg\" alt=\"Zuora Object Model Diagram\"></a>

This PHP package is automatically generated by the [Swagger Codegen](https://github.com/swagger-api/swagger-codegen) project:

- API version: 2017-04-14
- Build package: io.swagger.codegen.languages.PhpClientCodegen

## Requirements

PHP 5.4.0 and later

## Installation & Usage
### Composer

To install the bindings via [Composer](http://getcomposer.org/), add the following to `composer.json`:

```
{
  "repositories": [
    {
      "type": "git",
      "url": "https://github.com/GIT_USER_ID/GIT_REPO_ID.git"
    }
  ],
  "require": {
    "GIT_USER_ID/GIT_REPO_ID": "*@dev"
  }
}
```

Then run `composer install`

### Manual Installation

Download the files and include `autoload.php`:

```php
    require_once('/path/to/SwaggerClient-php/autoload.php');
```

## Tests

To run the unit tests:

```
composer install
./vendor/bin/phpunit
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AccountingCodesApi();
$ac_id = "ac_id_example"; // string | ID of the accounting code you want to delete.
$entity_id = "entity_id_example"; // string | The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name).
$entity_name = "entity_name_example"; // string | The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name).

try {
    $result = $api_instance->dELETEAccountingCode($ac_id, $entity_id, $entity_name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingCodesApi->dELETEAccountingCode: ', $e->getMessage(), PHP_EOL;
}

?>
```

## Documentation for API Endpoints

All URIs are relative to *https://rest.zuora.com/*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AccountingCodesApi* | [**dELETEAccountingCode**](docs/Api/AccountingCodesApi.md#deleteaccountingcode) | **DELETE** /v1/accounting-codes/{ac-id} | Delete accounting code
*AccountingCodesApi* | [**gETAccountingCode**](docs/Api/AccountingCodesApi.md#getaccountingcode) | **GET** /v1/accounting-codes/{ac-id} | Query an accounting code
*AccountingCodesApi* | [**gETAllAccountingCodes**](docs/Api/AccountingCodesApi.md#getallaccountingcodes) | **GET** /v1/accounting-codes | Get all accounting codes
*AccountingCodesApi* | [**pOSTAccountingCode**](docs/Api/AccountingCodesApi.md#postaccountingcode) | **POST** /v1/accounting-codes | Create accounting code
*AccountingCodesApi* | [**pUTAccountingCode**](docs/Api/AccountingCodesApi.md#putaccountingcode) | **PUT** /v1/accounting-codes/{ac-id} | Update an accounting code
*AccountingCodesApi* | [**pUTActivateAccountingCode**](docs/Api/AccountingCodesApi.md#putactivateaccountingcode) | **PUT** /v1/accounting-codes/{ac-id}/activate | Activate accounting code
*AccountingCodesApi* | [**pUTDeactivateAccountingCode**](docs/Api/AccountingCodesApi.md#putdeactivateaccountingcode) | **PUT** /v1/accounting-codes/{ac-id}/deactivate | Deactivate accounting code
*AccountingPeriodsApi* | [**dELETEAccountingPeriod**](docs/Api/AccountingPeriodsApi.md#deleteaccountingperiod) | **DELETE** /v1/accounting-periods/{ap-id} | Delete accounting period
*AccountingPeriodsApi* | [**gETAccountingPeriod**](docs/Api/AccountingPeriodsApi.md#getaccountingperiod) | **GET** /v1/accounting-periods/{ap-id} | Get accounting period
*AccountingPeriodsApi* | [**gETAllAccountingPeriods**](docs/Api/AccountingPeriodsApi.md#getallaccountingperiods) | **GET** /v1/accounting-periods | Get all accounting periods
*AccountingPeriodsApi* | [**pOSTAccountingPeriod**](docs/Api/AccountingPeriodsApi.md#postaccountingperiod) | **POST** /v1/accounting-periods | Create accounting period
*AccountingPeriodsApi* | [**pUTCloseAccountingPeriod**](docs/Api/AccountingPeriodsApi.md#putcloseaccountingperiod) | **PUT** /v1/accounting-periods/{ap-id}/close | Close accounting period
*AccountingPeriodsApi* | [**pUTPendingCloseAccountingPeriod**](docs/Api/AccountingPeriodsApi.md#putpendingcloseaccountingperiod) | **PUT** /v1/accounting-periods/{ap-id}/pending-close | Set accounting period to pending close
*AccountingPeriodsApi* | [**pUTReopenAccountingPeriod**](docs/Api/AccountingPeriodsApi.md#putreopenaccountingperiod) | **PUT** /v1/accounting-periods/{ap-id}/reopen | Re-open accounting period
*AccountingPeriodsApi* | [**pUTRunTrialBalance**](docs/Api/AccountingPeriodsApi.md#putruntrialbalance) | **PUT** /v1/accounting-periods/{ap-id}/run-trial-balance | Run trial balance
*AccountingPeriodsApi* | [**pUTUpdateAccountingPeriod**](docs/Api/AccountingPeriodsApi.md#putupdateaccountingperiod) | **PUT** /v1/accounting-periods/{ap-id} | Update accounting period
*AccountsApi* | [**gETAccount**](docs/Api/AccountsApi.md#getaccount) | **GET** /v1/accounts/{account-key} | Get account
*AccountsApi* | [**gETAccountSummary**](docs/Api/AccountsApi.md#getaccountsummary) | **GET** /v1/accounts/{account-key}/summary | Get account summary
*AccountsApi* | [**objectDELETEAccount**](docs/Api/AccountsApi.md#objectdeleteaccount) | **DELETE** /v1/object/account/{id} | CRUD: Delete Account
*AccountsApi* | [**objectGETAccount**](docs/Api/AccountsApi.md#objectgetaccount) | **GET** /v1/object/account/{id} | CRUD: Retrieve Account
*AccountsApi* | [**objectPOSTAccount**](docs/Api/AccountsApi.md#objectpostaccount) | **POST** /v1/object/account | CRUD: Create Account
*AccountsApi* | [**objectPUTAccount**](docs/Api/AccountsApi.md#objectputaccount) | **PUT** /v1/object/account/{id} | CRUD: Update Account
*AccountsApi* | [**pOSTAccount**](docs/Api/AccountsApi.md#postaccount) | **POST** /v1/accounts | Create account
*AccountsApi* | [**pUTAccount**](docs/Api/AccountsApi.md#putaccount) | **PUT** /v1/accounts/{account-key} | Update account
*ActionsApi* | [**actionPOSTamend**](docs/Api/ActionsApi.md#actionpostamend) | **POST** /v1/action/amend | Amend
*ActionsApi* | [**actionPOSTcreate**](docs/Api/ActionsApi.md#actionpostcreate) | **POST** /v1/action/create | Create
*ActionsApi* | [**actionPOSTdelete**](docs/Api/ActionsApi.md#actionpostdelete) | **POST** /v1/action/delete | Delete
*ActionsApi* | [**actionPOSTexecute**](docs/Api/ActionsApi.md#actionpostexecute) | **POST** /v1/action/execute | Execute
*ActionsApi* | [**actionPOSTgenerate**](docs/Api/ActionsApi.md#actionpostgenerate) | **POST** /v1/action/generate | Generate
*ActionsApi* | [**actionPOSTquery**](docs/Api/ActionsApi.md#actionpostquery) | **POST** /v1/action/query | Query
*ActionsApi* | [**actionPOSTqueryMore**](docs/Api/ActionsApi.md#actionpostquerymore) | **POST** /v1/action/queryMore | QueryMore
*ActionsApi* | [**actionPOSTsubscribe**](docs/Api/ActionsApi.md#actionpostsubscribe) | **POST** /v1/action/subscribe | Subscribe
*ActionsApi* | [**actionPOSTupdate**](docs/Api/ActionsApi.md#actionpostupdate) | **POST** /v1/action/update | Update
*AmendmentsApi* | [**gETAmendmentsByKey**](docs/Api/AmendmentsApi.md#getamendmentsbykey) | **GET** /v1/amendments/{amendment-key} | Get amendments by key
*AmendmentsApi* | [**gETAmendmentsBySubscriptionID**](docs/Api/AmendmentsApi.md#getamendmentsbysubscriptionid) | **GET** /v1/amendments/subscriptions/{subscription-id} | Get amendments by subscription ID
*AmendmentsApi* | [**objectDELETEAmendment**](docs/Api/AmendmentsApi.md#objectdeleteamendment) | **DELETE** /v1/object/amendment/{id} | CRUD: Delete Amendment
*AmendmentsApi* | [**objectGETAmendment**](docs/Api/AmendmentsApi.md#objectgetamendment) | **GET** /v1/object/amendment/{id} | CRUD: Retrieve Amendment
*AmendmentsApi* | [**objectPOSTAmendment**](docs/Api/AmendmentsApi.md#objectpostamendment) | **POST** /v1/object/amendment | CRUD: Create Amendment
*AmendmentsApi* | [**objectPUTAmendment**](docs/Api/AmendmentsApi.md#objectputamendment) | **PUT** /v1/object/amendment/{id} | CRUD: Update Amendment
*AttachmentsApi* | [**dELETEAttachments**](docs/Api/AttachmentsApi.md#deleteattachments) | **DELETE** /v1/attachments/{attachment-id} | Delete attachments
*AttachmentsApi* | [**gETAttachments**](docs/Api/AttachmentsApi.md#getattachments) | **GET** /v1/attachments/{attachment-id} | View attachments
*AttachmentsApi* | [**gETAttachmentsList**](docs/Api/AttachmentsApi.md#getattachmentslist) | **GET** /v1/attachments/{object-type}/{object-key} | View attachments list
*AttachmentsApi* | [**pOSTAttachments**](docs/Api/AttachmentsApi.md#postattachments) | **POST** /v1/attachments | Add attachments
*AttachmentsApi* | [**pUTAttachments**](docs/Api/AttachmentsApi.md#putattachments) | **PUT** /v1/attachments/{attachment-id} | Edit attachments
*BillRunApi* | [**objectDELETEBillRun**](docs/Api/BillRunApi.md#objectdeletebillrun) | **DELETE** /v1/object/bill-run/{id} | CRUD: Delete Bill Run
*BillRunApi* | [**objectGETBillRun**](docs/Api/BillRunApi.md#objectgetbillrun) | **GET** /v1/object/bill-run/{id} | CRUD: Retrieve Bill Run
*BillRunApi* | [**objectPOSTBillRun**](docs/Api/BillRunApi.md#objectpostbillrun) | **POST** /v1/object/bill-run | CRUD: Create Bill Run
*BillRunApi* | [**objectPUTBillRun**](docs/Api/BillRunApi.md#objectputbillrun) | **PUT** /v1/object/bill-run/{id} | CRUD: Post or Cancel Bill Run
*BillingPreviewRunApi* | [**gETBillingPreviewRun**](docs/Api/BillingPreviewRunApi.md#getbillingpreviewrun) | **GET** /v1/billing-preview-runs/{billingPreviewRunId} | Retrieve a billing preview run
*BillingPreviewRunApi* | [**pOSTBillingPreviewRun**](docs/Api/BillingPreviewRunApi.md#postbillingpreviewrun) | **POST** /v1/billing-preview-runs | Create a billing preview run
*CatalogApi* | [**gETCatalog**](docs/Api/CatalogApi.md#getcatalog) | **GET** /v1/catalog/products | Get product catalog
*CatalogApi* | [**pUTCatalog**](docs/Api/CatalogApi.md#putcatalog) | **PUT** /v1/catalog/products/{product-id}/share | Multi-entity: Share a Product with an Entity
*ChargeRevenueSummariesApi* | [**gETCRSByCRSNumber**](docs/Api/ChargeRevenueSummariesApi.md#getcrsbycrsnumber) | **GET** /v1/charge-revenue-summaries/{crs-number} | Get charge summary details by CRS number
*ChargeRevenueSummariesApi* | [**gETCRSByChargeID**](docs/Api/ChargeRevenueSummariesApi.md#getcrsbychargeid) | **GET** /v1/charge-revenue-summaries/subscription-charges/{charge-key} | Get charge summary details by charge ID
*CommunicationProfilesApi* | [**objectDELETECommunicationProfile**](docs/Api/CommunicationProfilesApi.md#objectdeletecommunicationprofile) | **DELETE** /v1/object/communication-profile/{id} | CRUD: Delete CommunicationProfile
*CommunicationProfilesApi* | [**objectGETCommunicationProfile**](docs/Api/CommunicationProfilesApi.md#objectgetcommunicationprofile) | **GET** /v1/object/communication-profile/{id} | CRUD: Retrieve CommunicationProfile
*ConnectionsApi* | [**pOSTConnections**](docs/Api/ConnectionsApi.md#postconnections) | **POST** /v1/connections | Establish connection to Zuora REST API service
*ContactsApi* | [**objectDELETEContact**](docs/Api/ContactsApi.md#objectdeletecontact) | **DELETE** /v1/object/contact/{id} | CRUD: Delete Contact
*ContactsApi* | [**objectGETContact**](docs/Api/ContactsApi.md#objectgetcontact) | **GET** /v1/object/contact/{id} | CRUD: Retrieve Contact
*ContactsApi* | [**objectPOSTContact**](docs/Api/ContactsApi.md#objectpostcontact) | **POST** /v1/object/contact | CRUD: Create Contact
*ContactsApi* | [**objectPUTContact**](docs/Api/ContactsApi.md#objectputcontact) | **PUT** /v1/object/contact/{id} | CRUD: Update Contact
*CreditBalanceAdjustmentsApi* | [**objectDELETECreditBalanceAdjustment**](docs/Api/CreditBalanceAdjustmentsApi.md#objectdeletecreditbalanceadjustment) | **DELETE** /v1/object/credit-balance-adjustment/{id} | CRUD: Delete CreditBalanceAdjustment
*CreditBalanceAdjustmentsApi* | [**objectGETCreditBalanceAdjustment**](docs/Api/CreditBalanceAdjustmentsApi.md#objectgetcreditbalanceadjustment) | **GET** /v1/object/credit-balance-adjustment/{id} | CRUD: Retrieve CreditBalanceAdjustment
*CreditMemosApi* | [**gETCreditMemos**](docs/Api/CreditMemosApi.md#getcreditmemos) | **GET** /v1/creditmemos | List credit memos
*CreditMemosApi* | [**pOSTCreditMemoPDF**](docs/Api/CreditMemosApi.md#postcreditmemopdf) | **POST** /v1/creditmemos/{creditMemoKey}/pdfs | Generate a credit memo PDF file
*CustomExchangeRatesApi* | [**gETCustomExchangeRates**](docs/Api/CustomExchangeRatesApi.md#getcustomexchangerates) | **GET** /v1/custom-exchange-rates/{currency} | Get custom foreign currency exchange rates
*DataQueriesApi* | [**dELETEDataQueryJob**](docs/Api/DataQueriesApi.md#deletedataqueryjob) | **DELETE** /query/jobs/{job-id} | Cancel a data query job
*DataQueriesApi* | [**gETDataQueryJob**](docs/Api/DataQueriesApi.md#getdataqueryjob) | **GET** /query/jobs/{job-id} | Retrieve a data query job
*DataQueriesApi* | [**gETDataQueryJobs**](docs/Api/DataQueriesApi.md#getdataqueryjobs) | **GET** /query/jobs | List data query jobs
*DataQueriesApi* | [**pOSTDataQueryJob**](docs/Api/DataQueriesApi.md#postdataqueryjob) | **POST** /query/jobs | Submit a data query
*EntitiesApi* | [**dELETEEntities**](docs/Api/EntitiesApi.md#deleteentities) | **DELETE** /v1/entities/{id} | Multi-entity: Delete entity
*EntitiesApi* | [**gETEntities**](docs/Api/EntitiesApi.md#getentities) | **GET** /v1/entities | Multi-entity: Get entities
*EntitiesApi* | [**gETEntityById**](docs/Api/EntitiesApi.md#getentitybyid) | **GET** /v1/entities/{id} | Multi-entity: Get entity by Id
*EntitiesApi* | [**pOSTEntities**](docs/Api/EntitiesApi.md#postentities) | **POST** /v1/entities | Multi-entity: Create entity
*EntitiesApi* | [**pUTEntities**](docs/Api/EntitiesApi.md#putentities) | **PUT** /v1/entities/{id} | Multi-entity: Update entity
*EntitiesApi* | [**pUTProvisionEntity**](docs/Api/EntitiesApi.md#putprovisionentity) | **PUT** /v1/entities/{id}/provision | Multi-entity: Provision entity
*EntityConnectionsApi* | [**gETEntityConnections**](docs/Api/EntityConnectionsApi.md#getentityconnections) | **GET** /v1/entity-connections | Multi-entity: Get connections
*EntityConnectionsApi* | [**pOSTEntityConnections**](docs/Api/EntityConnectionsApi.md#postentityconnections) | **POST** /v1/entity-connections | Multi-entity: Initiate connection
*EntityConnectionsApi* | [**pUTEntityConnectionsAccept**](docs/Api/EntityConnectionsApi.md#putentityconnectionsaccept) | **PUT** /v1/entity-connections/{connection-id}/accept | Multi-entity: Accept connection
*EntityConnectionsApi* | [**pUTEntityConnectionsDeny**](docs/Api/EntityConnectionsApi.md#putentityconnectionsdeny) | **PUT** /v1/entity-connections/{connection-id}/deny | Multi-entity: Deny connection
*EntityConnectionsApi* | [**pUTEntityConnectionsDisconnect**](docs/Api/EntityConnectionsApi.md#putentityconnectionsdisconnect) | **PUT** /v1/entity-connections/{connection-id}/disconnect | Multi-entity: Disconnect connection
*ExportsApi* | [**objectDELETEExport**](docs/Api/ExportsApi.md#objectdeleteexport) | **DELETE** /v1/object/export/{id} | CRUD: Delete Export
*ExportsApi* | [**objectGETExport**](docs/Api/ExportsApi.md#objectgetexport) | **GET** /v1/object/export/{id} | CRUD: Retrieve Export
*ExportsApi* | [**objectPOSTExport**](docs/Api/ExportsApi.md#objectpostexport) | **POST** /v1/object/export | CRUD: Create Export
*FeaturesApi* | [**objectDELETEFeature**](docs/Api/FeaturesApi.md#objectdeletefeature) | **DELETE** /v1/object/feature/{id} | CRUD: Delete Feature
*FeaturesApi* | [**objectGETFeature**](docs/Api/FeaturesApi.md#objectgetfeature) | **GET** /v1/object/feature/{id} | CRUD: Retrieve Feature
*GetFilesApi* | [**gETFiles**](docs/Api/GetFilesApi.md#getfiles) | **GET** /v1/files/{file-id} | Get files
*HMACSignaturesApi* | [**pOSTHMACSignatures**](docs/Api/HMACSignaturesApi.md#posthmacsignatures) | **POST** /v1/hmac-signatures | Return HMAC signatures
*HostedPagesApi* | [**getHostedPages**](docs/Api/HostedPagesApi.md#gethostedpages) | **GET** /v1/hostedpages | Return hosted pages
*ImportsApi* | [**objectDELETEImport**](docs/Api/ImportsApi.md#objectdeleteimport) | **DELETE** /v1/object/import/{id} | CRUD: Delete Import
*ImportsApi* | [**objectGETImport**](docs/Api/ImportsApi.md#objectgetimport) | **GET** /v1/object/import/{id} | CRUD: Retrieve Import
*InvoiceAdjustmentsApi* | [**objectDELETEInvoiceAdjustment**](docs/Api/InvoiceAdjustmentsApi.md#objectdeleteinvoiceadjustment) | **DELETE** /v1/object/invoice-adjustment/{id} | CRUD: Delete InvoiceAdjustment
*InvoiceAdjustmentsApi* | [**objectGETInvoiceAdjustment**](docs/Api/InvoiceAdjustmentsApi.md#objectgetinvoiceadjustment) | **GET** /v1/object/invoice-adjustment/{id} | CRUD: Retrieve InvoiceAdjustment
*InvoiceAdjustmentsApi* | [**objectPOSTInvoiceAdjustment**](docs/Api/InvoiceAdjustmentsApi.md#objectpostinvoiceadjustment) | **POST** /v1/object/invoice-adjustment | CRUD: Create InvoiceAdjustment
*InvoiceAdjustmentsApi* | [**objectPUTInvoiceAdjustment**](docs/Api/InvoiceAdjustmentsApi.md#objectputinvoiceadjustment) | **PUT** /v1/object/invoice-adjustment/{id} | CRUD: Update InvoiceAdjustment
*InvoiceItemAdjustmentsApi* | [**objectDELETEInvoiceItemAdjustment**](docs/Api/InvoiceItemAdjustmentsApi.md#objectdeleteinvoiceitemadjustment) | **DELETE** /v1/object/invoice-item-adjustment/{id} | CRUD: Delete InvoiceItemAdjustment
*InvoiceItemAdjustmentsApi* | [**objectGETInvoiceItemAdjustment**](docs/Api/InvoiceItemAdjustmentsApi.md#objectgetinvoiceitemadjustment) | **GET** /v1/object/invoice-item-adjustment/{id} | CRUD: Retrieve InvoiceItemAdjustment
*InvoiceItemsApi* | [**objectDELETEInvoiceItem**](docs/Api/InvoiceItemsApi.md#objectdeleteinvoiceitem) | **DELETE** /v1/object/invoice-item/{id} | CRUD: Delete InvoiceItem
*InvoiceItemsApi* | [**objectGETInvoiceItem**](docs/Api/InvoiceItemsApi.md#objectgetinvoiceitem) | **GET** /v1/object/invoice-item/{id} | CRUD: Retrieve InvoiceItem
*InvoicePaymentsApi* | [**objectDELETEInvoicePayment**](docs/Api/InvoicePaymentsApi.md#objectdeleteinvoicepayment) | **DELETE** /v1/object/invoice-payment/{id} | CRUD: Delete InvoicePayment
*InvoicePaymentsApi* | [**objectGETInvoicePayment**](docs/Api/InvoicePaymentsApi.md#objectgetinvoicepayment) | **GET** /v1/object/invoice-payment/{id} | CRUD: Retrieve InvoicePayment
*InvoicePaymentsApi* | [**objectPOSTInvoicePayment**](docs/Api/InvoicePaymentsApi.md#objectpostinvoicepayment) | **POST** /v1/object/invoice-payment | CRUD: Create InvoicePayment
*InvoicePaymentsApi* | [**objectPUTInvoicePayment**](docs/Api/InvoicePaymentsApi.md#objectputinvoicepayment) | **PUT** /v1/object/invoice-payment/{id} | CRUD: Update InvoicePayment
*InvoiceSplitItemsApi* | [**objectDELETEInvoiceSplitItem**](docs/Api/InvoiceSplitItemsApi.md#objectdeleteinvoicesplititem) | **DELETE** /v1/object/invoice-split-item/{id} | CRUD: Delete InvoiceSplitItem
*InvoiceSplitItemsApi* | [**objectGETInvoiceSplitItem**](docs/Api/InvoiceSplitItemsApi.md#objectgetinvoicesplititem) | **GET** /v1/object/invoice-split-item/{id} | CRUD: Retrieve InvoiceSplitItem
*InvoiceSplitsApi* | [**objectDELETEInvoiceSplit**](docs/Api/InvoiceSplitsApi.md#objectdeleteinvoicesplit) | **DELETE** /v1/object/invoice-split/{id} | CRUD: Delete InvoiceSplit
*InvoiceSplitsApi* | [**objectGETInvoiceSplit**](docs/Api/InvoiceSplitsApi.md#objectgetinvoicesplit) | **GET** /v1/object/invoice-split/{id} | CRUD: Retrieve InvoiceSplit
*InvoicesApi* | [**objectDELETEInvoice**](docs/Api/InvoicesApi.md#objectdeleteinvoice) | **DELETE** /v1/object/invoice/{id} | CRUD: Delete Invoice
*InvoicesApi* | [**objectGETInvoice**](docs/Api/InvoicesApi.md#objectgetinvoice) | **GET** /v1/object/invoice/{id} | CRUD: Retrieve Invoice
*InvoicesApi* | [**objectPOSTInvoice**](docs/Api/InvoicesApi.md#objectpostinvoice) | **POST** /v1/object/invoice | CRUD: Create Invoice
*InvoicesApi* | [**objectPUTInvoice**](docs/Api/InvoicesApi.md#objectputinvoice) | **PUT** /v1/object/invoice/{id} | CRUD: Update Invoice
*JournalRunsApi* | [**dELETEJournalRun**](docs/Api/JournalRunsApi.md#deletejournalrun) | **DELETE** /v1/journal-runs/{jr-number} | Delete journal run
*JournalRunsApi* | [**gETJournalRun**](docs/Api/JournalRunsApi.md#getjournalrun) | **GET** /v1/journal-runs/{jr-number} | Get journal run
*JournalRunsApi* | [**pOSTJournalRun**](docs/Api/JournalRunsApi.md#postjournalrun) | **POST** /v1/journal-runs | Create journal run
*JournalRunsApi* | [**pUTJournalRun**](docs/Api/JournalRunsApi.md#putjournalrun) | **PUT** /v1/journal-runs/{jr-number}/cancel | Cancel journal run
*MassUpdaterApi* | [**gETMassUpdater**](docs/Api/MassUpdaterApi.md#getmassupdater) | **GET** /v1/bulk/{bulk-key} | Get mass action result
*MassUpdaterApi* | [**pOSTMassUpdater**](docs/Api/MassUpdaterApi.md#postmassupdater) | **POST** /v1/bulk | Perform mass action
*MassUpdaterApi* | [**pUTMassUpdater**](docs/Api/MassUpdaterApi.md#putmassupdater) | **PUT** /v1/bulk/{bulk-key}/stop | Stop mass action
*NotificationHistoryApi* | [**gETCalloutHistory**](docs/Api/NotificationHistoryApi.md#getcallouthistory) | **GET** /v1/notification-history/callout | Get callout notification histories
*NotificationHistoryApi* | [**gETEmailHistory**](docs/Api/NotificationHistoryApi.md#getemailhistory) | **GET** /v1/notification-history/email | Get email notification histories
*OAuthApi* | [**createToken**](docs/Api/OAuthApi.md#createtoken) | **POST** /oauth/token | Create an OAuth token
*OperationsApi* | [**pOSTBillingPreview**](docs/Api/OperationsApi.md#postbillingpreview) | **POST** /v1/operations/billing-preview | Create billing preview
*OperationsApi* | [**pOSTTransactionInvoicePayment**](docs/Api/OperationsApi.md#posttransactioninvoicepayment) | **POST** /v1/operations/invoice-collect | Invoice and collect
*PaymentMethodSnapshotsApi* | [**objectDELETEPaymentMethodSnapshot**](docs/Api/PaymentMethodSnapshotsApi.md#objectdeletepaymentmethodsnapshot) | **DELETE** /v1/object/payment-method-snapshot/{id} | CRUD: Delete PaymentMethodSnapshot
*PaymentMethodSnapshotsApi* | [**objectGETPaymentMethodSnapshot**](docs/Api/PaymentMethodSnapshotsApi.md#objectgetpaymentmethodsnapshot) | **GET** /v1/object/payment-method-snapshot/{id} | CRUD: Retrieve PaymentMethodSnapshot
*PaymentMethodTransactionLogsApi* | [**objectDELETEPaymentMethodTransactionLog**](docs/Api/PaymentMethodTransactionLogsApi.md#objectdeletepaymentmethodtransactionlog) | **DELETE** /v1/object/payment-method-transaction-log/{id} | CRUD: Delete PaymentMethodTransactionLog
*PaymentMethodTransactionLogsApi* | [**objectGETPaymentMethodTransactionLog**](docs/Api/PaymentMethodTransactionLogsApi.md#objectgetpaymentmethodtransactionlog) | **GET** /v1/object/payment-method-transaction-log/{id} | CRUD: Retrieve PaymentMethodTransactionLog
*PaymentMethodsApi* | [**dELETEPaymentMethods**](docs/Api/PaymentMethodsApi.md#deletepaymentmethods) | **DELETE** /v1/payment-methods/{payment-method-id} | Delete payment method
*PaymentMethodsApi* | [**gETPaymentMethods**](docs/Api/PaymentMethodsApi.md#getpaymentmethods) | **GET** /v1/payment-methods/credit-cards/accounts/{account-key} | Get payment methods
*PaymentMethodsApi* | [**objectDELETEPaymentMethod**](docs/Api/PaymentMethodsApi.md#objectdeletepaymentmethod) | **DELETE** /v1/object/payment-method/{id} | CRUD: Delete PaymentMethod
*PaymentMethodsApi* | [**objectGETPaymentMethod**](docs/Api/PaymentMethodsApi.md#objectgetpaymentmethod) | **GET** /v1/object/payment-method/{id} | CRUD: Retrieve PaymentMethod
*PaymentMethodsApi* | [**objectPOSTPaymentMethod**](docs/Api/PaymentMethodsApi.md#objectpostpaymentmethod) | **POST** /v1/object/payment-method | CRUD: Create PaymentMethod
*PaymentMethodsApi* | [**objectPUTPaymentMethod**](docs/Api/PaymentMethodsApi.md#objectputpaymentmethod) | **PUT** /v1/object/payment-method/{id} | CRUD: Update PaymentMethod
*PaymentMethodsApi* | [**pOSTPaymentMethods**](docs/Api/PaymentMethodsApi.md#postpaymentmethods) | **POST** /v1/payment-methods/credit-cards | Create payment method
*PaymentMethodsApi* | [**pOSTPaymentMethodsDecryption**](docs/Api/PaymentMethodsApi.md#postpaymentmethodsdecryption) | **POST** /v1/payment-methods/decryption | Create payment method decryption
*PaymentMethodsApi* | [**pUTPaymentMethods**](docs/Api/PaymentMethodsApi.md#putpaymentmethods) | **PUT** /v1/payment-methods/credit-cards/{payment-method-id} | Update payment method
*PaymentTransactionLogsApi* | [**objectDELETEPaymentTransactionLog**](docs/Api/PaymentTransactionLogsApi.md#objectdeletepaymenttransactionlog) | **DELETE** /v1/object/payment-transaction-log/{id} | CRUD: Delete PaymentTransactionLog
*PaymentTransactionLogsApi* | [**objectGETPaymentTransactionLog**](docs/Api/PaymentTransactionLogsApi.md#objectgetpaymenttransactionlog) | **GET** /v1/object/payment-transaction-log/{id} | CRUD: Retrieve PaymentTransactionLog
*PaymentsApi* | [**objectDELETEPayment**](docs/Api/PaymentsApi.md#objectdeletepayment) | **DELETE** /v1/object/payment/{id} | CRUD: Delete Payment
*PaymentsApi* | [**objectGETPayment**](docs/Api/PaymentsApi.md#objectgetpayment) | **GET** /v1/object/payment/{id} | CRUD: Retrieve Payment
*PaymentsApi* | [**objectPOSTPayment**](docs/Api/PaymentsApi.md#objectpostpayment) | **POST** /v1/object/payment | CRUD: Create Payment
*PaymentsApi* | [**objectPUTPayment**](docs/Api/PaymentsApi.md#objectputpayment) | **PUT** /v1/object/payment/{id} | CRUD: Update Payment
*ProductFeaturesApi* | [**objectDELETEProductFeature**](docs/Api/ProductFeaturesApi.md#objectdeleteproductfeature) | **DELETE** /v1/object/product-feature/{id} | CRUD: Delete ProductFeature
*ProductFeaturesApi* | [**objectGETProductFeature**](docs/Api/ProductFeaturesApi.md#objectgetproductfeature) | **GET** /v1/object/product-feature/{id} | CRUD: Retrieve ProductFeature
*ProductRatePlanChargeTiersApi* | [**objectDELETEProductRatePlanChargeTier**](docs/Api/ProductRatePlanChargeTiersApi.md#objectdeleteproductrateplanchargetier) | **DELETE** /v1/object/product-rate-plan-charge-tier/{id} | CRUD: Delete ProductRatePlanChargeTier
*ProductRatePlanChargeTiersApi* | [**objectGETProductRatePlanChargeTier**](docs/Api/ProductRatePlanChargeTiersApi.md#objectgetproductrateplanchargetier) | **GET** /v1/object/product-rate-plan-charge-tier/{id} | CRUD: Retrieve ProductRatePlanChargeTier
*ProductRatePlanChargesApi* | [**objectDELETEProductRatePlanCharge**](docs/Api/ProductRatePlanChargesApi.md#objectdeleteproductrateplancharge) | **DELETE** /v1/object/product-rate-plan-charge/{id} | CRUD: Delete ProductRatePlanCharge
*ProductRatePlanChargesApi* | [**objectGETProductRatePlanCharge**](docs/Api/ProductRatePlanChargesApi.md#objectgetproductrateplancharge) | **GET** /v1/object/product-rate-plan-charge/{id} | CRUD: Retrieve ProductRatePlanCharge
*ProductRatePlansApi* | [**objectDELETEProductRatePlan**](docs/Api/ProductRatePlansApi.md#objectdeleteproductrateplan) | **DELETE** /v1/object/product-rate-plan/{id} | CRUD: Delete ProductRatePlan
*ProductRatePlansApi* | [**objectGETProductRatePlan**](docs/Api/ProductRatePlansApi.md#objectgetproductrateplan) | **GET** /v1/object/product-rate-plan/{id} | CRUD: Retrieve ProductRatePlan
*ProductRatePlansApi* | [**objectPOSTProductRatePlan**](docs/Api/ProductRatePlansApi.md#objectpostproductrateplan) | **POST** /v1/object/product-rate-plan | CRUD: Create ProductRatePlan
*ProductRatePlansApi* | [**objectPUTProductRatePlan**](docs/Api/ProductRatePlansApi.md#objectputproductrateplan) | **PUT** /v1/object/product-rate-plan/{id} | CRUD: Update ProductRatePlan
*ProductsApi* | [**objectDELETEProduct**](docs/Api/ProductsApi.md#objectdeleteproduct) | **DELETE** /v1/object/product/{id} | CRUD: Delete Product
*ProductsApi* | [**objectGETProduct**](docs/Api/ProductsApi.md#objectgetproduct) | **GET** /v1/object/product/{id} | CRUD: Retrieve Product
*ProductsApi* | [**objectPOSTProduct**](docs/Api/ProductsApi.md#objectpostproduct) | **POST** /v1/object/product | CRUD: Create Product
*ProductsApi* | [**objectPUTProduct**](docs/Api/ProductsApi.md#objectputproduct) | **PUT** /v1/object/product/{id} | CRUD: Update Product
*QuotesDocumentApi* | [**pOSTQuotesDocument**](docs/Api/QuotesDocumentApi.md#postquotesdocument) | **POST** /v1/quotes/document | Generate quotes document
*RSASignaturesApi* | [**pOSTDecryptRSASignatures**](docs/Api/RSASignaturesApi.md#postdecryptrsasignatures) | **POST** /v1/rsa-signatures/decrypt | Decrypt RSA signature
*RSASignaturesApi* | [**pOSTRSASignatures**](docs/Api/RSASignaturesApi.md#postrsasignatures) | **POST** /v1/rsa-signatures | Generate RSA signature
*RatePlanChargeTiersApi* | [**objectDELETERatePlanChargeTier**](docs/Api/RatePlanChargeTiersApi.md#objectdeleterateplanchargetier) | **DELETE** /v1/object/rate-plan-charge-tier/{id} | CRUD: Delete RatePlanChargeTier
*RatePlanChargeTiersApi* | [**objectGETRatePlanChargeTier**](docs/Api/RatePlanChargeTiersApi.md#objectgetrateplanchargetier) | **GET** /v1/object/rate-plan-charge-tier/{id} | CRUD: Retrieve RatePlanChargeTier
*RatePlanChargesApi* | [**objectDELETERatePlanCharge**](docs/Api/RatePlanChargesApi.md#objectdeleterateplancharge) | **DELETE** /v1/object/rate-plan-charge/{id} | CRUD: Delete RatePlanCharge
*RatePlanChargesApi* | [**objectGETRatePlanCharge**](docs/Api/RatePlanChargesApi.md#objectgetrateplancharge) | **GET** /v1/object/rate-plan-charge/{id} | CRUD: Retrieve RatePlanCharge
*RatePlansApi* | [**objectDELETERatePlan**](docs/Api/RatePlansApi.md#objectdeleterateplan) | **DELETE** /v1/object/rate-plan/{id} | CRUD: Delete RatePlan
*RatePlansApi* | [**objectGETRatePlan**](docs/Api/RatePlansApi.md#objectgetrateplan) | **GET** /v1/object/rate-plan/{id} | CRUD: Retrieve RatePlan
*RefundInvoicePaymentsApi* | [**objectDELETERefundInvoicePayment**](docs/Api/RefundInvoicePaymentsApi.md#objectdeleterefundinvoicepayment) | **DELETE** /v1/object/refund-invoice-payment/{id} | CRUD: Delete RefundInvoicePayment
*RefundInvoicePaymentsApi* | [**objectGETRefundInvoicePayment**](docs/Api/RefundInvoicePaymentsApi.md#objectgetrefundinvoicepayment) | **GET** /v1/object/refund-invoice-payment/{id} | CRUD: Retrieve RefundInvoicePayment
*RefundTransactionLogsApi* | [**objectDELETERefundTransactionLog**](docs/Api/RefundTransactionLogsApi.md#objectdeleterefundtransactionlog) | **DELETE** /v1/object/refund-transaction-log/{id} | CRUD: Delete RefundTransactionLog
*RefundTransactionLogsApi* | [**objectGETRefundTransactionLog**](docs/Api/RefundTransactionLogsApi.md#objectgetrefundtransactionlog) | **GET** /v1/object/refund-transaction-log/{id} | CRUD: Retrieve RefundTransactionLog
*RefundsApi* | [**objectDELETERefund**](docs/Api/RefundsApi.md#objectdeleterefund) | **DELETE** /v1/object/refund/{id} | CRUD: Delete Refund
*RefundsApi* | [**objectGETRefund**](docs/Api/RefundsApi.md#objectgetrefund) | **GET** /v1/object/refund/{id} | CRUD: Retrieve Refund
*RefundsApi* | [**objectPOSTRefund**](docs/Api/RefundsApi.md#objectpostrefund) | **POST** /v1/object/refund | CRUD: Create Refund
*RefundsApi* | [**objectPUTRefund**](docs/Api/RefundsApi.md#objectputrefund) | **PUT** /v1/object/refund/{id} | CRUD: Update Refund
*RevenueEventsApi* | [**gETRevenueEventDetails**](docs/Api/RevenueEventsApi.md#getrevenueeventdetails) | **GET** /v1/revenue-events/{event-number} | Get revenue event details
*RevenueEventsApi* | [**gETRevenueEventForRevenueSchedule**](docs/Api/RevenueEventsApi.md#getrevenueeventforrevenueschedule) | **GET** /v1/revenue-events/revenue-schedules/{rs-number} | Get revenue events for a revenue schedule
*RevenueItemsApi* | [**gETRevenueItemsByChargeRevenueEventNumber**](docs/Api/RevenueItemsApi.md#getrevenueitemsbychargerevenueeventnumber) | **GET** /v1/revenue-items/revenue-events/{event-number} | Get revenue items by revenue event number
*RevenueItemsApi* | [**gETRevenueItemsByChargeRevenueSummaryNumber**](docs/Api/RevenueItemsApi.md#getrevenueitemsbychargerevenuesummarynumber) | **GET** /v1/revenue-items/charge-revenue-summaries/{crs-number} | Get revenue items by charge revenue summary number
*RevenueItemsApi* | [**gETRevenueItemsByRevenueSchedule**](docs/Api/RevenueItemsApi.md#getrevenueitemsbyrevenueschedule) | **GET** /v1/revenue-items/revenue-schedules/{rs-number} | Get revenue items by revenue schedule
*RevenueItemsApi* | [**pUTCustomFieldsonRevenueItemsByRevenueEvent**](docs/Api/RevenueItemsApi.md#putcustomfieldsonrevenueitemsbyrevenueevent) | **PUT** /v1/revenue-items/revenue-events/{event-number} | Update custom fields on revenue items by revenue event number
*RevenueItemsApi* | [**pUTCustomFieldsonRevenueItemsByRevenueSchedule**](docs/Api/RevenueItemsApi.md#putcustomfieldsonrevenueitemsbyrevenueschedule) | **PUT** /v1/revenue-items/revenue-schedules/{rs-number} | Update custom fields on revenue items by revenue schedule number
*RevenueRulesApi* | [**gETRevenueRecRules**](docs/Api/RevenueRulesApi.md#getrevenuerecrules) | **GET** /v1/revenue-recognition-rules/subscription-charges/{charge-key} | Get the rule associated with a charge
*RevenueSchedulesApi* | [**dELETERS**](docs/Api/RevenueSchedulesApi.md#deleters) | **DELETE** /v1/revenue-schedules/{rs-number} | Delete revenue schedule
*RevenueSchedulesApi* | [**gETRS**](docs/Api/RevenueSchedulesApi.md#getrs) | **GET** /v1/revenue-schedules/{rs-number} | Get revenue schedule details
*RevenueSchedulesApi* | [**gETRSbyInvoiceItem**](docs/Api/RevenueSchedulesApi.md#getrsbyinvoiceitem) | **GET** /v1/revenue-schedules/invoice-items/{invoice-item-id} | Get a revenue schedule by invoice item ID
*RevenueSchedulesApi* | [**gETRSbyInvoiceItemAdjustment**](docs/Api/RevenueSchedulesApi.md#getrsbyinvoiceitemadjustment) | **GET** /v1/revenue-schedules/invoice-item-adjustments/{invoice-item-adj-id}/ | Get a revenue schedule by invoice item adjustment
*RevenueSchedulesApi* | [**gETRSforSubscCharge**](docs/Api/RevenueSchedulesApi.md#getrsforsubsccharge) | **GET** /v1/revenue-schedules/subscription-charges/{charge-key} | Get revenue schedule by subscription charge
*RevenueSchedulesApi* | [**pOSTRSforInvoiceItemAdjustmentDistributeByDateRange**](docs/Api/RevenueSchedulesApi.md#postrsforinvoiceitemadjustmentdistributebydaterange) | **POST** /v1/revenue-schedules/invoice-item-adjustments/{invoice-item-adj-key}/distribute-revenue-with-date-range | Create a revenue schedule for an Invoice Item Adjustment (distribute by date range)
*RevenueSchedulesApi* | [**pOSTRSforInvoiceItemAdjustmentManualDistribution**](docs/Api/RevenueSchedulesApi.md#postrsforinvoiceitemadjustmentmanualdistribution) | **POST** /v1/revenue-schedules/invoice-item-adjustments/{invoice-item-adj-key} | Create a revenue schedule for an Invoice Item Adjustment (manual distribution)
*RevenueSchedulesApi* | [**pOSTRSforInvoiceItemDistributeByDateRange**](docs/Api/RevenueSchedulesApi.md#postrsforinvoiceitemdistributebydaterange) | **POST** /v1/revenue-schedules/invoice-items/{invoice-item-id}/distribute-revenue-with-date-range | Create a revenue schedule for an Invoice Item (distribute by date range)
*RevenueSchedulesApi* | [**pOSTRSforInvoiceItemManualDistribution**](docs/Api/RevenueSchedulesApi.md#postrsforinvoiceitemmanualdistribution) | **POST** /v1/revenue-schedules/invoice-items/{invoice-item-id} | Create a revenue schedule for an Invoice Item (manual distribution)
*RevenueSchedulesApi* | [**pOSTRSforSubsCharge**](docs/Api/RevenueSchedulesApi.md#postrsforsubscharge) | **POST** /v1/revenue-schedules/subscription-charges/{charge-key} | Create a revenue schedule on a subscription charge
*RevenueSchedulesApi* | [**pUTRSBasicInfo**](docs/Api/RevenueSchedulesApi.md#putrsbasicinfo) | **PUT** /v1/revenue-schedules/{rs-number}/basic-information | Update revenue schedule basic information
*RevenueSchedulesApi* | [**pUTRevenueAcrossAP**](docs/Api/RevenueSchedulesApi.md#putrevenueacrossap) | **PUT** /v1/revenue-schedules/{rs-number}/distribute-revenue-across-accounting-periods | Distribute revenue across accounting periods
*RevenueSchedulesApi* | [**pUTRevenueByRecognitionStartandEndDates**](docs/Api/RevenueSchedulesApi.md#putrevenuebyrecognitionstartandenddates) | **PUT** /v1/revenue-schedules/{rs-number}/distribute-revenue-with-date-range | Distribute revenue by recognition start and end dates
*RevenueSchedulesApi* | [**pUTRevenueSpecificDate**](docs/Api/RevenueSchedulesApi.md#putrevenuespecificdate) | **PUT** /v1/revenue-schedules/{rs-number}/distribute-revenue-on-specific-date | Distribute revenue on a specific date
*SettingsApi* | [**gETRevenueAutomationStartDate**](docs/Api/SettingsApi.md#getrevenueautomationstartdate) | **GET** /v1/settings/finance/revenue-automation-start-date | Get the revenue automation start date
*SubscriptionProductFeaturesApi* | [**objectDELETESubscriptionProductFeature**](docs/Api/SubscriptionProductFeaturesApi.md#objectdeletesubscriptionproductfeature) | **DELETE** /v1/object/subscription-product-feature/{id} | CRUD: Delete SubscriptionProductFeature
*SubscriptionProductFeaturesApi* | [**objectGETSubscriptionProductFeature**](docs/Api/SubscriptionProductFeaturesApi.md#objectgetsubscriptionproductfeature) | **GET** /v1/object/subscription-product-feature/{id} | CRUD: Retrieve SubscriptionProductFeature
*SubscriptionsApi* | [**gETSubscriptionsByAccount**](docs/Api/SubscriptionsApi.md#getsubscriptionsbyaccount) | **GET** /v1/subscriptions/accounts/{account-key} | Get subscriptions by account
*SubscriptionsApi* | [**gETSubscriptionsByKey**](docs/Api/SubscriptionsApi.md#getsubscriptionsbykey) | **GET** /v1/subscriptions/{subscription-key} | Get subscriptions by key
*SubscriptionsApi* | [**gETSubscriptionsByKeyAndVersion**](docs/Api/SubscriptionsApi.md#getsubscriptionsbykeyandversion) | **GET** /v1/subscriptions/{subscription-key}/versions/{version} | Get subscriptions by key and version
*SubscriptionsApi* | [**objectDELETESubscription**](docs/Api/SubscriptionsApi.md#objectdeletesubscription) | **DELETE** /v1/object/subscription/{id} | CRUD: Delete Subscription
*SubscriptionsApi* | [**objectGETSubscription**](docs/Api/SubscriptionsApi.md#objectgetsubscription) | **GET** /v1/object/subscription/{id} | CRUD: Retrieve Subscription
*SubscriptionsApi* | [**objectPUTSubscription**](docs/Api/SubscriptionsApi.md#objectputsubscription) | **PUT** /v1/object/subscription/{id} | CRUD: Update Subscription
*SubscriptionsApi* | [**pOSTPreviewSubscription**](docs/Api/SubscriptionsApi.md#postpreviewsubscription) | **POST** /v1/subscriptions/preview | Preview subscription
*SubscriptionsApi* | [**pOSTSubscription**](docs/Api/SubscriptionsApi.md#postsubscription) | **POST** /v1/subscriptions | Create subscription
*SubscriptionsApi* | [**pUTCancelSubscription**](docs/Api/SubscriptionsApi.md#putcancelsubscription) | **PUT** /v1/subscriptions/{subscription-key}/cancel | Cancel subscription
*SubscriptionsApi* | [**pUTRenewSubscription**](docs/Api/SubscriptionsApi.md#putrenewsubscription) | **PUT** /v1/subscriptions/{subscription-key}/renew | Renew subscription
*SubscriptionsApi* | [**pUTResumeSubscription**](docs/Api/SubscriptionsApi.md#putresumesubscription) | **PUT** /v1/subscriptions/{subscription-key}/resume | Resume subscription
*SubscriptionsApi* | [**pUTSubscription**](docs/Api/SubscriptionsApi.md#putsubscription) | **PUT** /v1/subscriptions/{subscription-key} | Update subscription
*SubscriptionsApi* | [**pUTSuspendSubscription**](docs/Api/SubscriptionsApi.md#putsuspendsubscription) | **PUT** /v1/subscriptions/{subscription-key}/suspend | Suspend subscription
*SummaryJournalEntriesApi* | [**dELETESummaryJournalEntry**](docs/Api/SummaryJournalEntriesApi.md#deletesummaryjournalentry) | **DELETE** /v1/journal-entries/{je-number} | Delete summary journal entry
*SummaryJournalEntriesApi* | [**gETAllSummaryJournalEntries**](docs/Api/SummaryJournalEntriesApi.md#getallsummaryjournalentries) | **GET** /v1/journal-entries/journal-runs/{jr-number} | Get all summary journal entries in a journal run
*SummaryJournalEntriesApi* | [**gETSummaryJournalEntry**](docs/Api/SummaryJournalEntriesApi.md#getsummaryjournalentry) | **GET** /v1/journal-entries/{je-number} | Get summary journal entry
*SummaryJournalEntriesApi* | [**pOSTSummaryJournalEntry**](docs/Api/SummaryJournalEntriesApi.md#postsummaryjournalentry) | **POST** /v1/journal-entries | Create summary journal entry
*SummaryJournalEntriesApi* | [**pUTBasicSummaryJournalEntry**](docs/Api/SummaryJournalEntriesApi.md#putbasicsummaryjournalentry) | **PUT** /v1/journal-entries/{je-number}/basic-information | Update basic information of a summary journal entry
*SummaryJournalEntriesApi* | [**pUTSummaryJournalEntry**](docs/Api/SummaryJournalEntriesApi.md#putsummaryjournalentry) | **PUT** /v1/journal-entries/{je-number}/cancel | Cancel summary journal entry
*TaxationItemsApi* | [**objectDELETETaxationItem**](docs/Api/TaxationItemsApi.md#objectdeletetaxationitem) | **DELETE** /v1/object/taxation-item/{id} | CRUD: Delete TaxationItem
*TaxationItemsApi* | [**objectGETTaxationItem**](docs/Api/TaxationItemsApi.md#objectgettaxationitem) | **GET** /v1/object/taxation-item/{id} | CRUD: Retrieve TaxationItem
*TaxationItemsApi* | [**objectPOSTTaxationItem**](docs/Api/TaxationItemsApi.md#objectposttaxationitem) | **POST** /v1/object/taxation-item | CRUD: Create TaxationItem
*TaxationItemsApi* | [**objectPUTTaxationItem**](docs/Api/TaxationItemsApi.md#objectputtaxationitem) | **PUT** /v1/object/taxation-item/{id} | CRUD: Update TaxationItem
*TransactionsApi* | [**gETTransactionInvoice**](docs/Api/TransactionsApi.md#gettransactioninvoice) | **GET** /v1/transactions/invoices/accounts/{account-key} | Get invoices
*TransactionsApi* | [**gETTransactionPayment**](docs/Api/TransactionsApi.md#gettransactionpayment) | **GET** /v1/transactions/payments/accounts/{account-key} | Get payments
*UnitOfMeasureApi* | [**objectDELETEUnitOfMeasure**](docs/Api/UnitOfMeasureApi.md#objectdeleteunitofmeasure) | **DELETE** /v1/object/unit-of-measure/{id} | CRUD: Delete UnitOfMeasure
*UnitOfMeasureApi* | [**objectGETUnitOfMeasure**](docs/Api/UnitOfMeasureApi.md#objectgetunitofmeasure) | **GET** /v1/object/unit-of-measure/{id} | CRUD: Retrieve UnitOfMeasure
*UnitOfMeasureApi* | [**objectPOSTUnitOfMeasure**](docs/Api/UnitOfMeasureApi.md#objectpostunitofmeasure) | **POST** /v1/object/unit-of-measure | CRUD: Create UnitOfMeasure
*UnitOfMeasureApi* | [**objectPUTUnitOfMeasure**](docs/Api/UnitOfMeasureApi.md#objectputunitofmeasure) | **PUT** /v1/object/unit-of-measure/{id} | CRUD: Update UnitOfMeasure
*UsageApi* | [**gETUsage**](docs/Api/UsageApi.md#getusage) | **GET** /v1/usage/accounts/{account-key} | Get usage
*UsageApi* | [**objectDELETEUsage**](docs/Api/UsageApi.md#objectdeleteusage) | **DELETE** /v1/object/usage/{id} | CRUD: Delete Usage
*UsageApi* | [**objectGETUsage**](docs/Api/UsageApi.md#objectgetusage) | **GET** /v1/object/usage/{id} | CRUD: Retrieve Usage
*UsageApi* | [**objectPOSTUsage**](docs/Api/UsageApi.md#objectpostusage) | **POST** /v1/object/usage | CRUD: Create Usage
*UsageApi* | [**objectPUTUsage**](docs/Api/UsageApi.md#objectputusage) | **PUT** /v1/object/usage/{id} | CRUD: Update Usage
*UsageApi* | [**pOSTUsage**](docs/Api/UsageApi.md#postusage) | **POST** /v1/usage | Post usage
*UsersApi* | [**gETEntitiesUserAccessible**](docs/Api/UsersApi.md#getentitiesuseraccessible) | **GET** /v1/users/{username}/accessible-entities | Multi-entity: Get entities that a user can access
*UsersApi* | [**pUTAcceptUserAccess**](docs/Api/UsersApi.md#putacceptuseraccess) | **PUT** /v1/users/{username}/accept-access | Multi-entity: Accept user access
*UsersApi* | [**pUTDenyUserAccess**](docs/Api/UsersApi.md#putdenyuseraccess) | **PUT** /v1/users/{username}/deny-access | Multi-entity: Deny user access
*UsersApi* | [**pUTSendUserAccessRequests**](docs/Api/UsersApi.md#putsenduseraccessrequests) | **PUT** /v1/users/{username}/request-access | Multi-entity: Send user access requests


## Documentation For Models

 - [AmendRequest](docs/Model/AmendRequest.md)
 - [AmendRequestAmendOptions](docs/Model/AmendRequestAmendOptions.md)
 - [AmendRequestPreviewOptions](docs/Model/AmendRequestPreviewOptions.md)
 - [AmendResult](docs/Model/AmendResult.md)
 - [Amendment](docs/Model/Amendment.md)
 - [AmendmentRatePlanData](docs/Model/AmendmentRatePlanData.md)
 - [BillingPreviewResult](docs/Model/BillingPreviewResult.md)
 - [ChargeMetricsData](docs/Model/ChargeMetricsData.md)
 - [CommonResponseType](docs/Model/CommonResponseType.md)
 - [CommonResponseTypeReasons](docs/Model/CommonResponseTypeReasons.md)
 - [CreateEntityResponseType](docs/Model/CreateEntityResponseType.md)
 - [CreateEntityType](docs/Model/CreateEntityType.md)
 - [CreditMemoObjectCustomFields](docs/Model/CreditMemoObjectCustomFields.md)
 - [CreditMemoObjectNSFields](docs/Model/CreditMemoObjectNSFields.md)
 - [DELETEntityResponseType](docs/Model/DELETEntityResponseType.md)
 - [DataQueryErrorResponse](docs/Model/DataQueryErrorResponse.md)
 - [DataQueryJob](docs/Model/DataQueryJob.md)
 - [DataQueryJobCancelled](docs/Model/DataQueryJobCancelled.md)
 - [DataQueryJobCommon](docs/Model/DataQueryJobCommon.md)
 - [DeleteDataQueryJobResponse](docs/Model/DeleteDataQueryJobResponse.md)
 - [DeleteResult](docs/Model/DeleteResult.md)
 - [ElectronicPaymentOptions](docs/Model/ElectronicPaymentOptions.md)
 - [Error](docs/Model/Error.md)
 - [EventRevenueItemType](docs/Model/EventRevenueItemType.md)
 - [ExecuteResult](docs/Model/ExecuteResult.md)
 - [ExternalPaymentOptions](docs/Model/ExternalPaymentOptions.md)
 - [FinanceInformation](docs/Model/FinanceInformation.md)
 - [GETAccountSummaryInvoiceType](docs/Model/GETAccountSummaryInvoiceType.md)
 - [GETAccountSummaryPaymentInvoiceType](docs/Model/GETAccountSummaryPaymentInvoiceType.md)
 - [GETAccountSummaryPaymentType](docs/Model/GETAccountSummaryPaymentType.md)
 - [GETAccountSummarySubscriptionRatePlanType](docs/Model/GETAccountSummarySubscriptionRatePlanType.md)
 - [GETAccountSummarySubscriptionType](docs/Model/GETAccountSummarySubscriptionType.md)
 - [GETAccountSummaryType](docs/Model/GETAccountSummaryType.md)
 - [GETAccountSummaryTypeBasicInfo](docs/Model/GETAccountSummaryTypeBasicInfo.md)
 - [GETAccountSummaryTypeBasicInfoDefaultPaymentMethod](docs/Model/GETAccountSummaryTypeBasicInfoDefaultPaymentMethod.md)
 - [GETAccountSummaryTypeBillToContact](docs/Model/GETAccountSummaryTypeBillToContact.md)
 - [GETAccountSummaryTypeSoldToContact](docs/Model/GETAccountSummaryTypeSoldToContact.md)
 - [GETAccountSummaryTypeTaxInfo](docs/Model/GETAccountSummaryTypeTaxInfo.md)
 - [GETAccountSummaryUsageType](docs/Model/GETAccountSummaryUsageType.md)
 - [GETAccountType](docs/Model/GETAccountType.md)
 - [GETAccountTypeBasicInfo](docs/Model/GETAccountTypeBasicInfo.md)
 - [GETAccountTypeBillToContact](docs/Model/GETAccountTypeBillToContact.md)
 - [GETAccountTypeBillingAndPayment](docs/Model/GETAccountTypeBillingAndPayment.md)
 - [GETAccountTypeMetrics](docs/Model/GETAccountTypeMetrics.md)
 - [GETAccountTypeSoldToContact](docs/Model/GETAccountTypeSoldToContact.md)
 - [GETAccountingCodeItemType](docs/Model/GETAccountingCodeItemType.md)
 - [GETAccountingCodeItemWithoutSuccessType](docs/Model/GETAccountingCodeItemWithoutSuccessType.md)
 - [GETAccountingCodesType](docs/Model/GETAccountingCodesType.md)
 - [GETAccountingPeriodFileIdsType](docs/Model/GETAccountingPeriodFileIdsType.md)
 - [GETAccountingPeriodType](docs/Model/GETAccountingPeriodType.md)
 - [GETAccountingPeriodWithoutSuccessType](docs/Model/GETAccountingPeriodWithoutSuccessType.md)
 - [GETAccountingPeriodsType](docs/Model/GETAccountingPeriodsType.md)
 - [GETAmendmentType](docs/Model/GETAmendmentType.md)
 - [GETAttachmentResponseType](docs/Model/GETAttachmentResponseType.md)
 - [GETAttachmentResponseWithoutSuccessType](docs/Model/GETAttachmentResponseWithoutSuccessType.md)
 - [GETAttachmentsResponseType](docs/Model/GETAttachmentsResponseType.md)
 - [GETCalloutHistoryVOType](docs/Model/GETCalloutHistoryVOType.md)
 - [GETCalloutHistoryVOsType](docs/Model/GETCalloutHistoryVOsType.md)
 - [GETCatalogType](docs/Model/GETCatalogType.md)
 - [GETChargeRSDetailType](docs/Model/GETChargeRSDetailType.md)
 - [GETCreditMemoCollectionType](docs/Model/GETCreditMemoCollectionType.md)
 - [GETCreditMemoTypewithSuccess](docs/Model/GETCreditMemoTypewithSuccess.md)
 - [GETCustomExchangeRatesDataType](docs/Model/GETCustomExchangeRatesDataType.md)
 - [GETCustomExchangeRatesType](docs/Model/GETCustomExchangeRatesType.md)
 - [GETDiscountApplyDetailsType](docs/Model/GETDiscountApplyDetailsType.md)
 - [GETEmailHistoryVOType](docs/Model/GETEmailHistoryVOType.md)
 - [GETEmailHistoryVOsType](docs/Model/GETEmailHistoryVOsType.md)
 - [GETEntitiesResponseType](docs/Model/GETEntitiesResponseType.md)
 - [GETEntitiesResponseTypeWithId](docs/Model/GETEntitiesResponseTypeWithId.md)
 - [GETEntitiesType](docs/Model/GETEntitiesType.md)
 - [GETEntitiesUserAccessibleResponseType](docs/Model/GETEntitiesUserAccessibleResponseType.md)
 - [GETEntityConnectionsArrayItemsType](docs/Model/GETEntityConnectionsArrayItemsType.md)
 - [GETEntityConnectionsResponseType](docs/Model/GETEntityConnectionsResponseType.md)
 - [GETInvoiceFileType](docs/Model/GETInvoiceFileType.md)
 - [GETInvoiceFileWrapper](docs/Model/GETInvoiceFileWrapper.md)
 - [GETInvoiceType](docs/Model/GETInvoiceType.md)
 - [GETInvoicesInvoiceItemType](docs/Model/GETInvoicesInvoiceItemType.md)
 - [GETJournalEntriesInJournalRunType](docs/Model/GETJournalEntriesInJournalRunType.md)
 - [GETJournalEntryDetailType](docs/Model/GETJournalEntryDetailType.md)
 - [GETJournalEntryDetailTypeWithoutSuccess](docs/Model/GETJournalEntryDetailTypeWithoutSuccess.md)
 - [GETJournalEntryItemType](docs/Model/GETJournalEntryItemType.md)
 - [GETJournalEntrySegmentType](docs/Model/GETJournalEntrySegmentType.md)
 - [GETJournalRunTransactionType](docs/Model/GETJournalRunTransactionType.md)
 - [GETJournalRunType](docs/Model/GETJournalRunType.md)
 - [GETMassUpdateType](docs/Model/GETMassUpdateType.md)
 - [GETPaidInvoicesType](docs/Model/GETPaidInvoicesType.md)
 - [GETPaymentMethodType](docs/Model/GETPaymentMethodType.md)
 - [GETPaymentMethodTypeCardHolderInfo](docs/Model/GETPaymentMethodTypeCardHolderInfo.md)
 - [GETPaymentMethodsType](docs/Model/GETPaymentMethodsType.md)
 - [GETPaymentType](docs/Model/GETPaymentType.md)
 - [GETPaymentsType](docs/Model/GETPaymentsType.md)
 - [GETProductDiscountApplyDetailsType](docs/Model/GETProductDiscountApplyDetailsType.md)
 - [GETProductRatePlanChargePricingTierType](docs/Model/GETProductRatePlanChargePricingTierType.md)
 - [GETProductRatePlanChargePricingType](docs/Model/GETProductRatePlanChargePricingType.md)
 - [GETProductRatePlanChargeType](docs/Model/GETProductRatePlanChargeType.md)
 - [GETProductRatePlanType](docs/Model/GETProductRatePlanType.md)
 - [GETProductType](docs/Model/GETProductType.md)
 - [GETRSDetailType](docs/Model/GETRSDetailType.md)
 - [GETRSDetailWithoutSuccessType](docs/Model/GETRSDetailWithoutSuccessType.md)
 - [GETRSDetailsByChargeType](docs/Model/GETRSDetailsByChargeType.md)
 - [GETRevenueEventDetailType](docs/Model/GETRevenueEventDetailType.md)
 - [GETRevenueEventDetailWithoutSuccessType](docs/Model/GETRevenueEventDetailWithoutSuccessType.md)
 - [GETRevenueEventDetailsType](docs/Model/GETRevenueEventDetailsType.md)
 - [GETRevenueItemType](docs/Model/GETRevenueItemType.md)
 - [GETRevenueItemsType](docs/Model/GETRevenueItemsType.md)
 - [GETRevenueRecognitionRuleAssociationType](docs/Model/GETRevenueRecognitionRuleAssociationType.md)
 - [GETRevenueStartDateSettingType](docs/Model/GETRevenueStartDateSettingType.md)
 - [GETRsRevenueItemType](docs/Model/GETRsRevenueItemType.md)
 - [GETRsRevenueItemsType](docs/Model/GETRsRevenueItemsType.md)
 - [GETSubscriptionProductFeatureType](docs/Model/GETSubscriptionProductFeatureType.md)
 - [GETSubscriptionRatePlanChargesType](docs/Model/GETSubscriptionRatePlanChargesType.md)
 - [GETSubscriptionRatePlanType](docs/Model/GETSubscriptionRatePlanType.md)
 - [GETSubscriptionType](docs/Model/GETSubscriptionType.md)
 - [GETSubscriptionTypeWithSuccess](docs/Model/GETSubscriptionTypeWithSuccess.md)
 - [GETSubscriptionWrapper](docs/Model/GETSubscriptionWrapper.md)
 - [GETTierType](docs/Model/GETTierType.md)
 - [GETUsageType](docs/Model/GETUsageType.md)
 - [GETUsageWrapper](docs/Model/GETUsageWrapper.md)
 - [GatewayOption](docs/Model/GatewayOption.md)
 - [GetBillingPreviewRunResponse](docs/Model/GetBillingPreviewRunResponse.md)
 - [GetDataQueryJobResponse](docs/Model/GetDataQueryJobResponse.md)
 - [GetDataQueryJobsResponse](docs/Model/GetDataQueryJobsResponse.md)
 - [GetHostedPageType](docs/Model/GetHostedPageType.md)
 - [GetHostedPagesType](docs/Model/GetHostedPagesType.md)
 - [GetProductFeatureType](docs/Model/GetProductFeatureType.md)
 - [InlineResponse200](docs/Model/InlineResponse200.md)
 - [Invoice](docs/Model/Invoice.md)
 - [InvoiceData](docs/Model/InvoiceData.md)
 - [InvoiceDataInvoice](docs/Model/InvoiceDataInvoice.md)
 - [InvoiceItem](docs/Model/InvoiceItem.md)
 - [InvoiceProcessingOptions](docs/Model/InvoiceProcessingOptions.md)
 - [ListOfExchangeRates](docs/Model/ListOfExchangeRates.md)
 - [NewChargeMetrics](docs/Model/NewChargeMetrics.md)
 - [POSTAccountResponseType](docs/Model/POSTAccountResponseType.md)
 - [POSTAccountType](docs/Model/POSTAccountType.md)
 - [POSTAccountTypeBillToContact](docs/Model/POSTAccountTypeBillToContact.md)
 - [POSTAccountTypeCreditCard](docs/Model/POSTAccountTypeCreditCard.md)
 - [POSTAccountTypeCreditCardCardHolderInfo](docs/Model/POSTAccountTypeCreditCardCardHolderInfo.md)
 - [POSTAccountTypeSoldToContact](docs/Model/POSTAccountTypeSoldToContact.md)
 - [POSTAccountTypeSubscription](docs/Model/POSTAccountTypeSubscription.md)
 - [POSTAccountTypeTaxInfo](docs/Model/POSTAccountTypeTaxInfo.md)
 - [POSTAccountingCodeResponseType](docs/Model/POSTAccountingCodeResponseType.md)
 - [POSTAccountingCodeType](docs/Model/POSTAccountingCodeType.md)
 - [POSTAccountingPeriodResponseType](docs/Model/POSTAccountingPeriodResponseType.md)
 - [POSTAccountingPeriodType](docs/Model/POSTAccountingPeriodType.md)
 - [POSTAttachmentResponseType](docs/Model/POSTAttachmentResponseType.md)
 - [POSTAttachmentType](docs/Model/POSTAttachmentType.md)
 - [POSTBillingPreviewInvoiceItem](docs/Model/POSTBillingPreviewInvoiceItem.md)
 - [POSTDecryptResponseType](docs/Model/POSTDecryptResponseType.md)
 - [POSTDecryptionType](docs/Model/POSTDecryptionType.md)
 - [POSTDistributionItemType](docs/Model/POSTDistributionItemType.md)
 - [POSTEntityConnectionsResponseType](docs/Model/POSTEntityConnectionsResponseType.md)
 - [POSTEntityConnectionsType](docs/Model/POSTEntityConnectionsType.md)
 - [POSTHMACSignatureResponseType](docs/Model/POSTHMACSignatureResponseType.md)
 - [POSTHMACSignatureType](docs/Model/POSTHMACSignatureType.md)
 - [POSTInvoiceCollectInvoicesType](docs/Model/POSTInvoiceCollectInvoicesType.md)
 - [POSTInvoiceCollectResponseType](docs/Model/POSTInvoiceCollectResponseType.md)
 - [POSTInvoiceCollectType](docs/Model/POSTInvoiceCollectType.md)
 - [POSTJournalEntryItemType](docs/Model/POSTJournalEntryItemType.md)
 - [POSTJournalEntryResponseType](docs/Model/POSTJournalEntryResponseType.md)
 - [POSTJournalEntrySegmentType](docs/Model/POSTJournalEntrySegmentType.md)
 - [POSTJournalEntryType](docs/Model/POSTJournalEntryType.md)
 - [POSTJournalRunResponseType](docs/Model/POSTJournalRunResponseType.md)
 - [POSTJournalRunTransactionType](docs/Model/POSTJournalRunTransactionType.md)
 - [POSTJournalRunType](docs/Model/POSTJournalRunType.md)
 - [POSTMassUpdateResponseType](docs/Model/POSTMassUpdateResponseType.md)
 - [POSTMassUpdateType](docs/Model/POSTMassUpdateType.md)
 - [POSTMassUpdateTypeParams](docs/Model/POSTMassUpdateTypeParams.md)
 - [POSTMemoPdfResponse](docs/Model/POSTMemoPdfResponse.md)
 - [POSTPaymentMethodDecryption](docs/Model/POSTPaymentMethodDecryption.md)
 - [POSTPaymentMethodDecryptionCardHolderInfo](docs/Model/POSTPaymentMethodDecryptionCardHolderInfo.md)
 - [POSTPaymentMethodResponseDecryption](docs/Model/POSTPaymentMethodResponseDecryption.md)
 - [POSTPaymentMethodResponseType](docs/Model/POSTPaymentMethodResponseType.md)
 - [POSTPaymentMethodType](docs/Model/POSTPaymentMethodType.md)
 - [POSTPaymentMethodTypeCardHolderInfo](docs/Model/POSTPaymentMethodTypeCardHolderInfo.md)
 - [POSTQuoteDocResponseType](docs/Model/POSTQuoteDocResponseType.md)
 - [POSTQuoteDocType](docs/Model/POSTQuoteDocType.md)
 - [POSTRSASignatureResponseType](docs/Model/POSTRSASignatureResponseType.md)
 - [POSTRSASignatureType](docs/Model/POSTRSASignatureType.md)
 - [POSTRevenueScheduleByChargeResponseType](docs/Model/POSTRevenueScheduleByChargeResponseType.md)
 - [POSTRevenueScheduleByChargeType](docs/Model/POSTRevenueScheduleByChargeType.md)
 - [POSTRevenueScheduleByChargeTypeRevenueEvent](docs/Model/POSTRevenueScheduleByChargeTypeRevenueEvent.md)
 - [POSTRevenueScheduleByDateRangeType](docs/Model/POSTRevenueScheduleByDateRangeType.md)
 - [POSTRevenueScheduleByDateRangeTypeRevenueEvent](docs/Model/POSTRevenueScheduleByDateRangeTypeRevenueEvent.md)
 - [POSTRevenueScheduleByTransactionResponseType](docs/Model/POSTRevenueScheduleByTransactionResponseType.md)
 - [POSTRevenueScheduleByTransactionType](docs/Model/POSTRevenueScheduleByTransactionType.md)
 - [POSTRevenueScheduleByTransactionTypeRevenueEvent](docs/Model/POSTRevenueScheduleByTransactionTypeRevenueEvent.md)
 - [POSTScCreateType](docs/Model/POSTScCreateType.md)
 - [POSTSrpCreateType](docs/Model/POSTSrpCreateType.md)
 - [POSTSubscriptionCancellationResponseType](docs/Model/POSTSubscriptionCancellationResponseType.md)
 - [POSTSubscriptionCancellationType](docs/Model/POSTSubscriptionCancellationType.md)
 - [POSTSubscriptionPreviewInvoiceItemsType](docs/Model/POSTSubscriptionPreviewInvoiceItemsType.md)
 - [POSTSubscriptionPreviewResponseType](docs/Model/POSTSubscriptionPreviewResponseType.md)
 - [POSTSubscriptionPreviewResponseTypeChargeMetrics](docs/Model/POSTSubscriptionPreviewResponseTypeChargeMetrics.md)
 - [POSTSubscriptionPreviewType](docs/Model/POSTSubscriptionPreviewType.md)
 - [POSTSubscriptionPreviewTypePreviewAccountInfo](docs/Model/POSTSubscriptionPreviewTypePreviewAccountInfo.md)
 - [POSTSubscriptionPreviewTypePreviewAccountInfoBillToContact](docs/Model/POSTSubscriptionPreviewTypePreviewAccountInfoBillToContact.md)
 - [POSTSubscriptionResponseType](docs/Model/POSTSubscriptionResponseType.md)
 - [POSTSubscriptionType](docs/Model/POSTSubscriptionType.md)
 - [POSTTierType](docs/Model/POSTTierType.md)
 - [POSTUsageResponseType](docs/Model/POSTUsageResponseType.md)
 - [PUTAcceptUserAccessResponseType](docs/Model/PUTAcceptUserAccessResponseType.md)
 - [PUTAccountType](docs/Model/PUTAccountType.md)
 - [PUTAccountTypeBillToContact](docs/Model/PUTAccountTypeBillToContact.md)
 - [PUTAccountTypeSoldToContact](docs/Model/PUTAccountTypeSoldToContact.md)
 - [PUTAccountingCodeType](docs/Model/PUTAccountingCodeType.md)
 - [PUTAccountingPeriodType](docs/Model/PUTAccountingPeriodType.md)
 - [PUTAllocateManuallyType](docs/Model/PUTAllocateManuallyType.md)
 - [PUTAttachmentType](docs/Model/PUTAttachmentType.md)
 - [PUTBasicSummaryJournalEntryType](docs/Model/PUTBasicSummaryJournalEntryType.md)
 - [PUTCatalogType](docs/Model/PUTCatalogType.md)
 - [PUTDenyUserAccessResponseType](docs/Model/PUTDenyUserAccessResponseType.md)
 - [PUTEntityConnectionsAcceptResponseType](docs/Model/PUTEntityConnectionsAcceptResponseType.md)
 - [PUTEntityConnectionsDenyResponseType](docs/Model/PUTEntityConnectionsDenyResponseType.md)
 - [PUTEntityConnectionsDisconnectResponseType](docs/Model/PUTEntityConnectionsDisconnectResponseType.md)
 - [PUTEventRIDetailType](docs/Model/PUTEventRIDetailType.md)
 - [PUTJournalEntryItemType](docs/Model/PUTJournalEntryItemType.md)
 - [PUTPaymentMethodResponseType](docs/Model/PUTPaymentMethodResponseType.md)
 - [PUTPaymentMethodType](docs/Model/PUTPaymentMethodType.md)
 - [PUTRSBasicInfoType](docs/Model/PUTRSBasicInfoType.md)
 - [PUTRSTermType](docs/Model/PUTRSTermType.md)
 - [PUTRenewSubscriptionResponseType](docs/Model/PUTRenewSubscriptionResponseType.md)
 - [PUTRenewSubscriptionType](docs/Model/PUTRenewSubscriptionType.md)
 - [PUTRevenueScheduleResponseType](docs/Model/PUTRevenueScheduleResponseType.md)
 - [PUTScAddType](docs/Model/PUTScAddType.md)
 - [PUTScUpdateType](docs/Model/PUTScUpdateType.md)
 - [PUTScheduleRIDetailType](docs/Model/PUTScheduleRIDetailType.md)
 - [PUTSendUserAccessRequestResponseType](docs/Model/PUTSendUserAccessRequestResponseType.md)
 - [PUTSendUserAccessRequestType](docs/Model/PUTSendUserAccessRequestType.md)
 - [PUTSpecificDateAllocationType](docs/Model/PUTSpecificDateAllocationType.md)
 - [PUTSrpAddType](docs/Model/PUTSrpAddType.md)
 - [PUTSrpRemoveType](docs/Model/PUTSrpRemoveType.md)
 - [PUTSrpUpdateType](docs/Model/PUTSrpUpdateType.md)
 - [PUTSubscriptionPreviewInvoiceItemsType](docs/Model/PUTSubscriptionPreviewInvoiceItemsType.md)
 - [PUTSubscriptionResponseType](docs/Model/PUTSubscriptionResponseType.md)
 - [PUTSubscriptionResponseTypeChargeMetrics](docs/Model/PUTSubscriptionResponseTypeChargeMetrics.md)
 - [PUTSubscriptionResumeResponseType](docs/Model/PUTSubscriptionResumeResponseType.md)
 - [PUTSubscriptionResumeType](docs/Model/PUTSubscriptionResumeType.md)
 - [PUTSubscriptionSuspendResponseType](docs/Model/PUTSubscriptionSuspendResponseType.md)
 - [PUTSubscriptionSuspendType](docs/Model/PUTSubscriptionSuspendType.md)
 - [PUTSubscriptionType](docs/Model/PUTSubscriptionType.md)
 - [PostBillingPreviewParam](docs/Model/PostBillingPreviewParam.md)
 - [PostBillingPreviewRunParam](docs/Model/PostBillingPreviewRunParam.md)
 - [ProvisionEntityResponseType](docs/Model/ProvisionEntityResponseType.md)
 - [ProxyActionamendRequest](docs/Model/ProxyActionamendRequest.md)
 - [ProxyActionamendResponse](docs/Model/ProxyActionamendResponse.md)
 - [ProxyActioncreateRequest](docs/Model/ProxyActioncreateRequest.md)
 - [ProxyActioncreateResponse](docs/Model/ProxyActioncreateResponse.md)
 - [ProxyActiondeleteRequest](docs/Model/ProxyActiondeleteRequest.md)
 - [ProxyActiondeleteResponse](docs/Model/ProxyActiondeleteResponse.md)
 - [ProxyActionexecuteRequest](docs/Model/ProxyActionexecuteRequest.md)
 - [ProxyActionexecuteResponse](docs/Model/ProxyActionexecuteResponse.md)
 - [ProxyActiongenerateRequest](docs/Model/ProxyActiongenerateRequest.md)
 - [ProxyActiongenerateResponse](docs/Model/ProxyActiongenerateResponse.md)
 - [ProxyActionqueryMoreRequest](docs/Model/ProxyActionqueryMoreRequest.md)
 - [ProxyActionqueryMoreResponse](docs/Model/ProxyActionqueryMoreResponse.md)
 - [ProxyActionqueryRequest](docs/Model/ProxyActionqueryRequest.md)
 - [ProxyActionqueryResponse](docs/Model/ProxyActionqueryResponse.md)
 - [ProxyActionsubscribeRequest](docs/Model/ProxyActionsubscribeRequest.md)
 - [ProxyActionsubscribeResponse](docs/Model/ProxyActionsubscribeResponse.md)
 - [ProxyActionupdateRequest](docs/Model/ProxyActionupdateRequest.md)
 - [ProxyActionupdateResponse](docs/Model/ProxyActionupdateResponse.md)
 - [ProxyBadRequestResponse](docs/Model/ProxyBadRequestResponse.md)
 - [ProxyBadRequestResponseErrors](docs/Model/ProxyBadRequestResponseErrors.md)
 - [ProxyCreateAccount](docs/Model/ProxyCreateAccount.md)
 - [ProxyCreateAmendment](docs/Model/ProxyCreateAmendment.md)
 - [ProxyCreateBillRun](docs/Model/ProxyCreateBillRun.md)
 - [ProxyCreateContact](docs/Model/ProxyCreateContact.md)
 - [ProxyCreateExport](docs/Model/ProxyCreateExport.md)
 - [ProxyCreateInvoice](docs/Model/ProxyCreateInvoice.md)
 - [ProxyCreateInvoiceAdjustment](docs/Model/ProxyCreateInvoiceAdjustment.md)
 - [ProxyCreateInvoicePayment](docs/Model/ProxyCreateInvoicePayment.md)
 - [ProxyCreateOrModifyResponse](docs/Model/ProxyCreateOrModifyResponse.md)
 - [ProxyCreatePayment](docs/Model/ProxyCreatePayment.md)
 - [ProxyCreatePaymentMethod](docs/Model/ProxyCreatePaymentMethod.md)
 - [ProxyCreateProduct](docs/Model/ProxyCreateProduct.md)
 - [ProxyCreateProductRatePlan](docs/Model/ProxyCreateProductRatePlan.md)
 - [ProxyCreateRefund](docs/Model/ProxyCreateRefund.md)
 - [ProxyCreateTaxationItem](docs/Model/ProxyCreateTaxationItem.md)
 - [ProxyCreateUnitOfMeasure](docs/Model/ProxyCreateUnitOfMeasure.md)
 - [ProxyCreateUsage](docs/Model/ProxyCreateUsage.md)
 - [ProxyDeleteResponse](docs/Model/ProxyDeleteResponse.md)
 - [ProxyGetAccount](docs/Model/ProxyGetAccount.md)
 - [ProxyGetAmendment](docs/Model/ProxyGetAmendment.md)
 - [ProxyGetBillRun](docs/Model/ProxyGetBillRun.md)
 - [ProxyGetCommunicationProfile](docs/Model/ProxyGetCommunicationProfile.md)
 - [ProxyGetContact](docs/Model/ProxyGetContact.md)
 - [ProxyGetCreditBalanceAdjustment](docs/Model/ProxyGetCreditBalanceAdjustment.md)
 - [ProxyGetExport](docs/Model/ProxyGetExport.md)
 - [ProxyGetFeature](docs/Model/ProxyGetFeature.md)
 - [ProxyGetImport](docs/Model/ProxyGetImport.md)
 - [ProxyGetInvoice](docs/Model/ProxyGetInvoice.md)
 - [ProxyGetInvoiceAdjustment](docs/Model/ProxyGetInvoiceAdjustment.md)
 - [ProxyGetInvoiceItem](docs/Model/ProxyGetInvoiceItem.md)
 - [ProxyGetInvoiceItemAdjustment](docs/Model/ProxyGetInvoiceItemAdjustment.md)
 - [ProxyGetInvoicePayment](docs/Model/ProxyGetInvoicePayment.md)
 - [ProxyGetInvoiceSplit](docs/Model/ProxyGetInvoiceSplit.md)
 - [ProxyGetInvoiceSplitItem](docs/Model/ProxyGetInvoiceSplitItem.md)
 - [ProxyGetPayment](docs/Model/ProxyGetPayment.md)
 - [ProxyGetPaymentMethod](docs/Model/ProxyGetPaymentMethod.md)
 - [ProxyGetPaymentMethodSnapshot](docs/Model/ProxyGetPaymentMethodSnapshot.md)
 - [ProxyGetPaymentMethodTransactionLog](docs/Model/ProxyGetPaymentMethodTransactionLog.md)
 - [ProxyGetPaymentTransactionLog](docs/Model/ProxyGetPaymentTransactionLog.md)
 - [ProxyGetProduct](docs/Model/ProxyGetProduct.md)
 - [ProxyGetProductFeature](docs/Model/ProxyGetProductFeature.md)
 - [ProxyGetProductRatePlan](docs/Model/ProxyGetProductRatePlan.md)
 - [ProxyGetProductRatePlanCharge](docs/Model/ProxyGetProductRatePlanCharge.md)
 - [ProxyGetProductRatePlanChargeTier](docs/Model/ProxyGetProductRatePlanChargeTier.md)
 - [ProxyGetRatePlan](docs/Model/ProxyGetRatePlan.md)
 - [ProxyGetRatePlanCharge](docs/Model/ProxyGetRatePlanCharge.md)
 - [ProxyGetRatePlanChargeTier](docs/Model/ProxyGetRatePlanChargeTier.md)
 - [ProxyGetRefund](docs/Model/ProxyGetRefund.md)
 - [ProxyGetRefundInvoicePayment](docs/Model/ProxyGetRefundInvoicePayment.md)
 - [ProxyGetRefundTransactionLog](docs/Model/ProxyGetRefundTransactionLog.md)
 - [ProxyGetSubscription](docs/Model/ProxyGetSubscription.md)
 - [ProxyGetSubscriptionProductFeature](docs/Model/ProxyGetSubscriptionProductFeature.md)
 - [ProxyGetTaxationItem](docs/Model/ProxyGetTaxationItem.md)
 - [ProxyGetUnitOfMeasure](docs/Model/ProxyGetUnitOfMeasure.md)
 - [ProxyGetUsage](docs/Model/ProxyGetUsage.md)
 - [ProxyModifyAccount](docs/Model/ProxyModifyAccount.md)
 - [ProxyModifyAmendment](docs/Model/ProxyModifyAmendment.md)
 - [ProxyModifyBillRun](docs/Model/ProxyModifyBillRun.md)
 - [ProxyModifyContact](docs/Model/ProxyModifyContact.md)
 - [ProxyModifyInvoice](docs/Model/ProxyModifyInvoice.md)
 - [ProxyModifyInvoiceAdjustment](docs/Model/ProxyModifyInvoiceAdjustment.md)
 - [ProxyModifyInvoicePayment](docs/Model/ProxyModifyInvoicePayment.md)
 - [ProxyModifyPayment](docs/Model/ProxyModifyPayment.md)
 - [ProxyModifyPaymentMethod](docs/Model/ProxyModifyPaymentMethod.md)
 - [ProxyModifyProduct](docs/Model/ProxyModifyProduct.md)
 - [ProxyModifyProductRatePlan](docs/Model/ProxyModifyProductRatePlan.md)
 - [ProxyModifyRefund](docs/Model/ProxyModifyRefund.md)
 - [ProxyModifySubscription](docs/Model/ProxyModifySubscription.md)
 - [ProxyModifyTaxationItem](docs/Model/ProxyModifyTaxationItem.md)
 - [ProxyModifyUnitOfMeasure](docs/Model/ProxyModifyUnitOfMeasure.md)
 - [ProxyModifyUsage](docs/Model/ProxyModifyUsage.md)
 - [ProxyNoDataResponse](docs/Model/ProxyNoDataResponse.md)
 - [QueryResult](docs/Model/QueryResult.md)
 - [RatePlan](docs/Model/RatePlan.md)
 - [RatePlanChargeData](docs/Model/RatePlanChargeData.md)
 - [RatePlanChargeDataRatePlanCharge](docs/Model/RatePlanChargeDataRatePlanCharge.md)
 - [RatePlanChargeTier](docs/Model/RatePlanChargeTier.md)
 - [RatePlanData](docs/Model/RatePlanData.md)
 - [RatePlanDataRatePlan](docs/Model/RatePlanDataRatePlan.md)
 - [RatePlanDataSubscriptionProductFeatureList](docs/Model/RatePlanDataSubscriptionProductFeatureList.md)
 - [RevenueScheduleItemType](docs/Model/RevenueScheduleItemType.md)
 - [SaveResult](docs/Model/SaveResult.md)
 - [SubmitDataQueryRequest](docs/Model/SubmitDataQueryRequest.md)
 - [SubmitDataQueryRequestOutput](docs/Model/SubmitDataQueryRequestOutput.md)
 - [SubmitDataQueryResponse](docs/Model/SubmitDataQueryResponse.md)
 - [SubscribeRequest](docs/Model/SubscribeRequest.md)
 - [SubscribeRequestAccount](docs/Model/SubscribeRequestAccount.md)
 - [SubscribeRequestBillToContact](docs/Model/SubscribeRequestBillToContact.md)
 - [SubscribeRequestPaymentMethod](docs/Model/SubscribeRequestPaymentMethod.md)
 - [SubscribeRequestPaymentMethodGatewayOptionData](docs/Model/SubscribeRequestPaymentMethodGatewayOptionData.md)
 - [SubscribeRequestPreviewOptions](docs/Model/SubscribeRequestPreviewOptions.md)
 - [SubscribeRequestSoldToContact](docs/Model/SubscribeRequestSoldToContact.md)
 - [SubscribeRequestSubscribeOptions](docs/Model/SubscribeRequestSubscribeOptions.md)
 - [SubscribeRequestSubscribeOptionsElectronicPaymentOptions](docs/Model/SubscribeRequestSubscribeOptionsElectronicPaymentOptions.md)
 - [SubscribeRequestSubscribeOptionsExternalPaymentOptions](docs/Model/SubscribeRequestSubscribeOptionsExternalPaymentOptions.md)
 - [SubscribeRequestSubscribeOptionsSubscribeInvoiceProcessingOptions](docs/Model/SubscribeRequestSubscribeOptionsSubscribeInvoiceProcessingOptions.md)
 - [SubscribeRequestSubscriptionData](docs/Model/SubscribeRequestSubscriptionData.md)
 - [SubscribeRequestSubscriptionDataSubscription](docs/Model/SubscribeRequestSubscriptionDataSubscription.md)
 - [SubscribeResult](docs/Model/SubscribeResult.md)
 - [SubscribeResultChargeMetricsData](docs/Model/SubscribeResultChargeMetricsData.md)
 - [SubscribeResultInvoiceResult](docs/Model/SubscribeResultInvoiceResult.md)
 - [SubscriptionProductFeature](docs/Model/SubscriptionProductFeature.md)
 - [SubscriptionProductFeatureList](docs/Model/SubscriptionProductFeatureList.md)
 - [TokenResponse](docs/Model/TokenResponse.md)
 - [UpdateEntityResponseType](docs/Model/UpdateEntityResponseType.md)
 - [UpdateEntityType](docs/Model/UpdateEntityType.md)
 - [ZObject](docs/Model/ZObject.md)
 - [ZObjectUpdate](docs/Model/ZObjectUpdate.md)
 - [ZObjectUsageCreate](docs/Model/ZObjectUsageCreate.md)


## Documentation For Authorization

 All endpoints do not require authorization.


## Author

docs@zuora.com


