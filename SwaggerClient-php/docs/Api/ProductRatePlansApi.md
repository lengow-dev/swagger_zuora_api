# Swagger\Client\ProductRatePlansApi

All URIs are relative to *https://rest.zuora.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**objectDELETEProductRatePlan**](ProductRatePlansApi.md#objectDELETEProductRatePlan) | **DELETE** /v1/object/product-rate-plan/{id} | CRUD: Delete ProductRatePlan
[**objectGETProductRatePlan**](ProductRatePlansApi.md#objectGETProductRatePlan) | **GET** /v1/object/product-rate-plan/{id} | CRUD: Retrieve ProductRatePlan
[**objectPOSTProductRatePlan**](ProductRatePlansApi.md#objectPOSTProductRatePlan) | **POST** /v1/object/product-rate-plan | CRUD: Create ProductRatePlan
[**objectPUTProductRatePlan**](ProductRatePlansApi.md#objectPUTProductRatePlan) | **PUT** /v1/object/product-rate-plan/{id} | CRUD: Update ProductRatePlan


# **objectDELETEProductRatePlan**
> \Swagger\Client\Model\ProxyDeleteResponse objectDELETEProductRatePlan($id, $entity_id, $entity_name)

CRUD: Delete ProductRatePlan



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\ProductRatePlansApi();
$id = "id_example"; // string | Object id
$entity_id = "entity_id_example"; // string | The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name).
$entity_name = "entity_name_example"; // string | The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name).

try {
    $result = $api_instance->objectDELETEProductRatePlan($id, $entity_id, $entity_name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductRatePlansApi->objectDELETEProductRatePlan: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Object id |
 **entity_id** | **string**| The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). | [optional]
 **entity_name** | **string**| The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). | [optional]

### Return type

[**\Swagger\Client\Model\ProxyDeleteResponse**](../Model/ProxyDeleteResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json; charset=utf-8
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **objectGETProductRatePlan**
> \Swagger\Client\Model\ProxyGetProductRatePlan objectGETProductRatePlan($id, $entity_id, $entity_name, $fields)

CRUD: Retrieve ProductRatePlan



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\ProductRatePlansApi();
$id = "id_example"; // string | Object id
$entity_id = "entity_id_example"; // string | The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name).
$entity_name = "entity_name_example"; // string | The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name).
$fields = "fields_example"; // string | Object fields to return

try {
    $result = $api_instance->objectGETProductRatePlan($id, $entity_id, $entity_name, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductRatePlansApi->objectGETProductRatePlan: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Object id |
 **entity_id** | **string**| The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). | [optional]
 **entity_name** | **string**| The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). | [optional]
 **fields** | **string**| Object fields to return | [optional]

### Return type

[**\Swagger\Client\Model\ProxyGetProductRatePlan**](../Model/ProxyGetProductRatePlan.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json; charset=utf-8
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **objectPOSTProductRatePlan**
> \Swagger\Client\Model\ProxyCreateOrModifyResponse objectPOSTProductRatePlan($create_request, $entity_id, $entity_name)

CRUD: Create ProductRatePlan



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\ProductRatePlansApi();
$create_request = new \Swagger\Client\Model\ProxyCreateProductRatePlan(); // \Swagger\Client\Model\ProxyCreateProductRatePlan | 
$entity_id = "entity_id_example"; // string | The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name).
$entity_name = "entity_name_example"; // string | The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name).

try {
    $result = $api_instance->objectPOSTProductRatePlan($create_request, $entity_id, $entity_name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductRatePlansApi->objectPOSTProductRatePlan: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_request** | [**\Swagger\Client\Model\ProxyCreateProductRatePlan**](../Model/\Swagger\Client\Model\ProxyCreateProductRatePlan.md)|  |
 **entity_id** | **string**| The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). | [optional]
 **entity_name** | **string**| The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). | [optional]

### Return type

[**\Swagger\Client\Model\ProxyCreateOrModifyResponse**](../Model/ProxyCreateOrModifyResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json; charset=utf-8
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **objectPUTProductRatePlan**
> \Swagger\Client\Model\ProxyCreateOrModifyResponse objectPUTProductRatePlan($id, $modify_request, $entity_id, $entity_name)

CRUD: Update ProductRatePlan



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\ProductRatePlansApi();
$id = "id_example"; // string | Object id
$modify_request = new \Swagger\Client\Model\ProxyModifyProductRatePlan(); // \Swagger\Client\Model\ProxyModifyProductRatePlan | 
$entity_id = "entity_id_example"; // string | The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name).
$entity_name = "entity_name_example"; // string | The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name).

try {
    $result = $api_instance->objectPUTProductRatePlan($id, $modify_request, $entity_id, $entity_name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductRatePlansApi->objectPUTProductRatePlan: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Object id |
 **modify_request** | [**\Swagger\Client\Model\ProxyModifyProductRatePlan**](../Model/\Swagger\Client\Model\ProxyModifyProductRatePlan.md)|  |
 **entity_id** | **string**| The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). | [optional]
 **entity_name** | **string**| The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). | [optional]

### Return type

[**\Swagger\Client\Model\ProxyCreateOrModifyResponse**](../Model/ProxyCreateOrModifyResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json; charset=utf-8
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

