# Swagger\Client\CreditMemosApi

All URIs are relative to *https://rest.zuora.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**gETCreditMemos**](CreditMemosApi.md#gETCreditMemos) | **GET** /v1/creditmemos | List credit memos
[**pOSTCreditMemoPDF**](CreditMemosApi.md#pOSTCreditMemoPDF) | **POST** /v1/creditmemos/{creditMemoKey}/pdfs | Generate a credit memo PDF file


# **gETCreditMemos**
> \Swagger\Client\Model\GETCreditMemoCollectionType gETCreditMemos($accept_encoding, $content_encoding, $authorization, $zuora_track_id, $zuora_entity_ids, $page, $page_size, $account_id, $account_number, $amount, $applied_amount, $auto_apply_upon_posting, $created_by_id, $created_date, $credit_memo_date, $currency, $exclude_from_auto_apply_rules, $number, $referred_invoice_id, $refund_amount, $status, $target_date, $tax_amount, $total_tax_exempt_amount, $transferred_to_accounting, $unapplied_amount, $updated_by_id, $updated_date, $sort)

List credit memos

**Note:** This operation is only available if you have [Invoice Settlement](https://knowledgecenter.zuora.com/Billing/Billing_and_Payments/Invoice_Settlement) enabled. The Invoice Settlement feature is generally available as of Zuora Billing Release 296 (March 2021). This feature includes Unapplied Payments, Credit and Debit Memo, and Invoice Item Settlement. If you want to enable Invoice Settlement, see [Invoice Settlement Enablement and Checklist Guide](https://knowledgecenter.zuora.com/Billing/Billing_and_Payments/Invoice_Settlement/Invoice_Settlement_Migration_Checklist_and_Guide) for more information.   Retrieves the information about all credit memos.   For a use case of this operation, see [Get credit memo](https://www.zuora.com/developer/rest-api/general-concepts/authentication//#Get-credit-memo).   ### Filtering  You can use query parameters to restrict the data returned in the response. Each query parameter corresponds to one field in the response body.  If the value of a filterable field is string, you can set the corresponding query parameter to `null` when filtering. Then, you can get the response data with this field value being `null`.     Examples:  - /v1/creditmemos?status=Posted  - /v1/creditmemos?referredInvoiceId=null&status=Draft  - /v1/creditmemos?status=Posted&type=External&sort=+number

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\CreditMemosApi();
$accept_encoding = "accept_encoding_example"; // string | Include the `Accept-Encoding: gzip` header to compress responses as a gzipped file. It can significantly reduce the bandwidth required for a response.   If specified, Zuora automatically compresses responses that contain over 1000 bytes of data, and the response contains a `Content-Encoding` header with the compression algorithm so that your client can decompress it.
$content_encoding = "content_encoding_example"; // string | Include the `Content-Encoding: gzip` header to compress a request. With this header specified, you should upload a gzipped file for the request payload instead of sending the JSON payload.
$authorization = "authorization_example"; // string | The value is in the `Bearer {token}` format where {token} is a valid OAuth token generated by calling [Create an OAuth token](https://www.zuora.com/developer/api-references/api/operation/createToken).
$zuora_track_id = "zuora_track_id_example"; // string | A custom identifier for tracing the API call. If you set a value for this header, Zuora returns the same value in the response headers. This header enables you to associate your system process identifiers with Zuora API calls, to assist with troubleshooting in the event of an issue.  The value of this field must use the US-ASCII character set and must not include any of the following characters: colon (`:`), semicolon (`;`), double quote (`\"`), and quote (`'`).
$zuora_entity_ids = "zuora_entity_ids_example"; // string | An entity ID. If you have [Zuora Multi-entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity) enabled and the OAuth token is valid for more than one entity, you must use this header to specify which entity to perform the operation in. If the OAuth token is only valid for a single entity, or you do not have Zuora Multi-entity enabled, you do not need to set this header.
$page = 1; // int | The index number of the page that you want to retrieve. This parameter is dependent on `pageSize`. You must set `pageSize` before specifying `page`. For example, if you set `pageSize` to `20` and `page` to `2`, the 21st to 40th records are returned in the response.
$page_size = 20; // int | The number of records returned per page in the response.
$account_id = "account_id_example"; // string | This parameter filters the response based on the `accountId` field.
$account_number = "account_number_example"; // string | This parameter filters the response based on the `accountNumber` field.
$amount = 1.2; // double | This parameter filters the response based on the `amount` field.
$applied_amount = 1.2; // double | This parameter filters the response based on the `appliedAmount` field.
$auto_apply_upon_posting = true; // bool | This parameter filters the response based on the `autoApplyUponPosting` field.
$created_by_id = "created_by_id_example"; // string | This parameter filters the response based on the `createdById` field.
$created_date = new \DateTime(); // \DateTime | This parameter filters the response based on the `createdDate` field.
$credit_memo_date = new \DateTime(); // \DateTime | This parameter filters the response based on the `creditMemoDate` field.
$currency = "currency_example"; // string | This parameter filters the response based on the `currency` field.
$exclude_from_auto_apply_rules = true; // bool | This parameter filters the response based on the `excludeFromAutoApplyRules` field.
$number = "number_example"; // string | This parameter filters the response based on the `number` field.
$referred_invoice_id = "referred_invoice_id_example"; // string | This parameter filters the response based on the `referredInvoiceId` field.
$refund_amount = 1.2; // double | This parameter filters the response based on the `refundAmount` field.
$status = "status_example"; // string | This parameter filters the response based on the `status` field.
$target_date = new \DateTime(); // \DateTime | This parameter filters the response based on the `targetDate` field.
$tax_amount = 1.2; // double | This parameter filters the response based on the `taxAmount` field.
$total_tax_exempt_amount = 1.2; // double | This parameter filters the response based on the `totalTaxExemptAmount` field.
$transferred_to_accounting = "transferred_to_accounting_example"; // string | This parameter filters the response based on the `transferredToAccounting` field.
$unapplied_amount = 1.2; // double | This parameter filters the response based on the `unappliedAmount` field.
$updated_by_id = "updated_by_id_example"; // string | This parameter filters the response based on the `updatedById` field.
$updated_date = new \DateTime(); // \DateTime | This parameter filters the response based on the `updatedDate` field.
$sort = "sort_example"; // string | This parameter restricts the order of the data returned in the response. You can use this parameter to supply a dimension you want to sort on.  A sortable field uses the following form:   *operator* *field_name*  You can use at most two sortable fields in one URL path. Use a comma to separate sortable fields. For example:  *operator* *field_name*, *operator* *field_name*    *operator* is used to mark the order of sequencing. The operator is optional. If you only specify the sortable field without any operator, the response data is sorted in descending order by this field.    - The `-` operator indicates an ascending order.   - The `+` operator indicates a descending order.  By default, the response data is displayed in descending order by credit memo number.  *field_name* indicates the name of a sortable field. The supported sortable fields of this operation are as below:    - accountId   - amount   - appliedAmount   - createdById   - createdDate   - creditMemoDate   - number   - referredInvoiceId   - refundAmount   - status   - targetDate   - taxAmount   - totalTaxExemptAmount   - transferredToAccounting   - unappliedAmount   - updatedDate     Examples:  - /v1/creditmemos?sort=+number  - /v1/creditmemos?status=Processed&sort=-number,+amount

try {
    $result = $api_instance->gETCreditMemos($accept_encoding, $content_encoding, $authorization, $zuora_track_id, $zuora_entity_ids, $page, $page_size, $account_id, $account_number, $amount, $applied_amount, $auto_apply_upon_posting, $created_by_id, $created_date, $credit_memo_date, $currency, $exclude_from_auto_apply_rules, $number, $referred_invoice_id, $refund_amount, $status, $target_date, $tax_amount, $total_tax_exempt_amount, $transferred_to_accounting, $unapplied_amount, $updated_by_id, $updated_date, $sort);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CreditMemosApi->gETCreditMemos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept_encoding** | **string**| Include the &#x60;Accept-Encoding: gzip&#x60; header to compress responses as a gzipped file. It can significantly reduce the bandwidth required for a response.   If specified, Zuora automatically compresses responses that contain over 1000 bytes of data, and the response contains a &#x60;Content-Encoding&#x60; header with the compression algorithm so that your client can decompress it. | [optional]
 **content_encoding** | **string**| Include the &#x60;Content-Encoding: gzip&#x60; header to compress a request. With this header specified, you should upload a gzipped file for the request payload instead of sending the JSON payload. | [optional]
 **authorization** | **string**| The value is in the &#x60;Bearer {token}&#x60; format where {token} is a valid OAuth token generated by calling [Create an OAuth token](https://www.zuora.com/developer/api-references/api/operation/createToken). | [optional]
 **zuora_track_id** | **string**| A custom identifier for tracing the API call. If you set a value for this header, Zuora returns the same value in the response headers. This header enables you to associate your system process identifiers with Zuora API calls, to assist with troubleshooting in the event of an issue.  The value of this field must use the US-ASCII character set and must not include any of the following characters: colon (&#x60;:&#x60;), semicolon (&#x60;;&#x60;), double quote (&#x60;\&quot;&#x60;), and quote (&#x60;&#39;&#x60;). | [optional]
 **zuora_entity_ids** | **string**| An entity ID. If you have [Zuora Multi-entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity) enabled and the OAuth token is valid for more than one entity, you must use this header to specify which entity to perform the operation in. If the OAuth token is only valid for a single entity, or you do not have Zuora Multi-entity enabled, you do not need to set this header. | [optional]
 **page** | **int**| The index number of the page that you want to retrieve. This parameter is dependent on &#x60;pageSize&#x60;. You must set &#x60;pageSize&#x60; before specifying &#x60;page&#x60;. For example, if you set &#x60;pageSize&#x60; to &#x60;20&#x60; and &#x60;page&#x60; to &#x60;2&#x60;, the 21st to 40th records are returned in the response. | [optional] [default to 1]
 **page_size** | **int**| The number of records returned per page in the response. | [optional] [default to 20]
 **account_id** | **string**| This parameter filters the response based on the &#x60;accountId&#x60; field. | [optional]
 **account_number** | **string**| This parameter filters the response based on the &#x60;accountNumber&#x60; field. | [optional]
 **amount** | **double**| This parameter filters the response based on the &#x60;amount&#x60; field. | [optional]
 **applied_amount** | **double**| This parameter filters the response based on the &#x60;appliedAmount&#x60; field. | [optional]
 **auto_apply_upon_posting** | **bool**| This parameter filters the response based on the &#x60;autoApplyUponPosting&#x60; field. | [optional]
 **created_by_id** | **string**| This parameter filters the response based on the &#x60;createdById&#x60; field. | [optional]
 **created_date** | **\DateTime**| This parameter filters the response based on the &#x60;createdDate&#x60; field. | [optional]
 **credit_memo_date** | **\DateTime**| This parameter filters the response based on the &#x60;creditMemoDate&#x60; field. | [optional]
 **currency** | **string**| This parameter filters the response based on the &#x60;currency&#x60; field. | [optional]
 **exclude_from_auto_apply_rules** | **bool**| This parameter filters the response based on the &#x60;excludeFromAutoApplyRules&#x60; field. | [optional]
 **number** | **string**| This parameter filters the response based on the &#x60;number&#x60; field. | [optional]
 **referred_invoice_id** | **string**| This parameter filters the response based on the &#x60;referredInvoiceId&#x60; field. | [optional]
 **refund_amount** | **double**| This parameter filters the response based on the &#x60;refundAmount&#x60; field. | [optional]
 **status** | **string**| This parameter filters the response based on the &#x60;status&#x60; field. | [optional]
 **target_date** | **\DateTime**| This parameter filters the response based on the &#x60;targetDate&#x60; field. | [optional]
 **tax_amount** | **double**| This parameter filters the response based on the &#x60;taxAmount&#x60; field. | [optional]
 **total_tax_exempt_amount** | **double**| This parameter filters the response based on the &#x60;totalTaxExemptAmount&#x60; field. | [optional]
 **transferred_to_accounting** | **string**| This parameter filters the response based on the &#x60;transferredToAccounting&#x60; field. | [optional]
 **unapplied_amount** | **double**| This parameter filters the response based on the &#x60;unappliedAmount&#x60; field. | [optional]
 **updated_by_id** | **string**| This parameter filters the response based on the &#x60;updatedById&#x60; field. | [optional]
 **updated_date** | **\DateTime**| This parameter filters the response based on the &#x60;updatedDate&#x60; field. | [optional]
 **sort** | **string**| This parameter restricts the order of the data returned in the response. You can use this parameter to supply a dimension you want to sort on.  A sortable field uses the following form:   *operator* *field_name*  You can use at most two sortable fields in one URL path. Use a comma to separate sortable fields. For example:  *operator* *field_name*, *operator* *field_name*    *operator* is used to mark the order of sequencing. The operator is optional. If you only specify the sortable field without any operator, the response data is sorted in descending order by this field.    - The &#x60;-&#x60; operator indicates an ascending order.   - The &#x60;+&#x60; operator indicates a descending order.  By default, the response data is displayed in descending order by credit memo number.  *field_name* indicates the name of a sortable field. The supported sortable fields of this operation are as below:    - accountId   - amount   - appliedAmount   - createdById   - createdDate   - creditMemoDate   - number   - referredInvoiceId   - refundAmount   - status   - targetDate   - taxAmount   - totalTaxExemptAmount   - transferredToAccounting   - unappliedAmount   - updatedDate     Examples:  - /v1/creditmemos?sort&#x3D;+number  - /v1/creditmemos?status&#x3D;Processed&amp;sort&#x3D;-number,+amount | [optional]

### Return type

[**\Swagger\Client\Model\GETCreditMemoCollectionType**](../Model/GETCreditMemoCollectionType.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json; charset=utf-8
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **pOSTCreditMemoPDF**
> \Swagger\Client\Model\POSTMemoPdfResponse pOSTCreditMemoPDF($credit_memo_key, $idempotency_key, $accept_encoding, $content_encoding, $authorization, $zuora_track_id, $zuora_entity_ids)

Generate a credit memo PDF file

**Note:** This operation is only available if you have [Invoice Settlement](https://knowledgecenter.zuora.com/Billing/Billing_and_Payments/Invoice_Settlement) enabled. The Invoice Settlement feature is generally available as of Zuora Billing Release 296 (March 2021). This feature includes Unapplied Payments, Credit and Debit Memo, and Invoice Item Settlement. If you want to enable Invoice Settlement, see [Invoice Settlement Enablement and Checklist Guide](https://knowledgecenter.zuora.com/Billing/Billing_and_Payments/Invoice_Settlement/Invoice_Settlement_Migration_Checklist_and_Guide) for more information.   Creates a PDF file for a specified credit memo. To access the generated PDF file, you can download it by clicking **View PDF** on the detailed credit memo page through the Zuora UI.  This REST API operation can be used only if you have the billing document file generation feature and the Billing user permission \"Regenerate PDF\" enabled.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\CreditMemosApi();
$credit_memo_key = "credit_memo_key_example"; // string | The unique ID or number of the credit memo that you want to create a PDF file for. For example, 8a8082e65b27f6c3015ba45ff82c7172 or CM00000001.
$idempotency_key = "idempotency_key_example"; // string | Specify a unique idempotency key if you want to perform an idempotent POST or PATCH request. Do not use this header in other request types.   With this header specified, the Zuora server can identify subsequent retries of the same request using this value, which prevents the same operation from being performed multiple times by accident.
$accept_encoding = "accept_encoding_example"; // string | Include the `Accept-Encoding: gzip` header to compress responses as a gzipped file. It can significantly reduce the bandwidth required for a response.   If specified, Zuora automatically compresses responses that contain over 1000 bytes of data, and the response contains a `Content-Encoding` header with the compression algorithm so that your client can decompress it.
$content_encoding = "content_encoding_example"; // string | Include the `Content-Encoding: gzip` header to compress a request. With this header specified, you should upload a gzipped file for the request payload instead of sending the JSON payload.
$authorization = "authorization_example"; // string | The value is in the `Bearer {token}` format where {token} is a valid OAuth token generated by calling [Create an OAuth token](https://www.zuora.com/developer/api-references/api/operation/createToken).
$zuora_track_id = "zuora_track_id_example"; // string | A custom identifier for tracing the API call. If you set a value for this header, Zuora returns the same value in the response headers. This header enables you to associate your system process identifiers with Zuora API calls, to assist with troubleshooting in the event of an issue.  The value of this field must use the US-ASCII character set and must not include any of the following characters: colon (`:`), semicolon (`;`), double quote (`\"`), and quote (`'`).
$zuora_entity_ids = "zuora_entity_ids_example"; // string | An entity ID. If you have [Zuora Multi-entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity) enabled and the OAuth token is valid for more than one entity, you must use this header to specify which entity to perform the operation in. If the OAuth token is only valid for a single entity, or you do not have Zuora Multi-entity enabled, you do not need to set this header.

try {
    $result = $api_instance->pOSTCreditMemoPDF($credit_memo_key, $idempotency_key, $accept_encoding, $content_encoding, $authorization, $zuora_track_id, $zuora_entity_ids);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CreditMemosApi->pOSTCreditMemoPDF: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **credit_memo_key** | **string**| The unique ID or number of the credit memo that you want to create a PDF file for. For example, 8a8082e65b27f6c3015ba45ff82c7172 or CM00000001. |
 **idempotency_key** | **string**| Specify a unique idempotency key if you want to perform an idempotent POST or PATCH request. Do not use this header in other request types.   With this header specified, the Zuora server can identify subsequent retries of the same request using this value, which prevents the same operation from being performed multiple times by accident. | [optional]
 **accept_encoding** | **string**| Include the &#x60;Accept-Encoding: gzip&#x60; header to compress responses as a gzipped file. It can significantly reduce the bandwidth required for a response.   If specified, Zuora automatically compresses responses that contain over 1000 bytes of data, and the response contains a &#x60;Content-Encoding&#x60; header with the compression algorithm so that your client can decompress it. | [optional]
 **content_encoding** | **string**| Include the &#x60;Content-Encoding: gzip&#x60; header to compress a request. With this header specified, you should upload a gzipped file for the request payload instead of sending the JSON payload. | [optional]
 **authorization** | **string**| The value is in the &#x60;Bearer {token}&#x60; format where {token} is a valid OAuth token generated by calling [Create an OAuth token](https://www.zuora.com/developer/api-references/api/operation/createToken). | [optional]
 **zuora_track_id** | **string**| A custom identifier for tracing the API call. If you set a value for this header, Zuora returns the same value in the response headers. This header enables you to associate your system process identifiers with Zuora API calls, to assist with troubleshooting in the event of an issue.  The value of this field must use the US-ASCII character set and must not include any of the following characters: colon (&#x60;:&#x60;), semicolon (&#x60;;&#x60;), double quote (&#x60;\&quot;&#x60;), and quote (&#x60;&#39;&#x60;). | [optional]
 **zuora_entity_ids** | **string**| An entity ID. If you have [Zuora Multi-entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity) enabled and the OAuth token is valid for more than one entity, you must use this header to specify which entity to perform the operation in. If the OAuth token is only valid for a single entity, or you do not have Zuora Multi-entity enabled, you do not need to set this header. | [optional]

### Return type

[**\Swagger\Client\Model\POSTMemoPdfResponse**](../Model/POSTMemoPdfResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json; charset=utf-8
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

