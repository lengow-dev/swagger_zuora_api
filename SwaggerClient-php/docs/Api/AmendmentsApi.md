# Swagger\Client\AmendmentsApi

All URIs are relative to *https://rest.zuora.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**gETAmendmentsByKey**](AmendmentsApi.md#gETAmendmentsByKey) | **GET** /v1/amendments/{amendment-key} | Get amendments by key
[**gETAmendmentsBySubscriptionID**](AmendmentsApi.md#gETAmendmentsBySubscriptionID) | **GET** /v1/amendments/subscriptions/{subscription-id} | Get amendments by subscription ID
[**objectDELETEAmendment**](AmendmentsApi.md#objectDELETEAmendment) | **DELETE** /v1/object/amendment/{id} | CRUD: Delete Amendment
[**objectGETAmendment**](AmendmentsApi.md#objectGETAmendment) | **GET** /v1/object/amendment/{id} | CRUD: Retrieve Amendment
[**objectPOSTAmendment**](AmendmentsApi.md#objectPOSTAmendment) | **POST** /v1/object/amendment | CRUD: Create Amendment
[**objectPUTAmendment**](AmendmentsApi.md#objectPUTAmendment) | **PUT** /v1/object/amendment/{id} | CRUD: Update Amendment


# **gETAmendmentsByKey**
> \Swagger\Client\Model\GETAmendmentType gETAmendmentsByKey($amendment_key, $entity_id, $entity_name)

Get amendments by key

Retrieves detailed information about the specified subscription amendment.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AmendmentsApi();
$amendment_key = "amendment_key_example"; // string | Can be the amendment ID or the amendment code.
$entity_id = "entity_id_example"; // string | The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name).
$entity_name = "entity_name_example"; // string | The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name).

try {
    $result = $api_instance->gETAmendmentsByKey($amendment_key, $entity_id, $entity_name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AmendmentsApi->gETAmendmentsByKey: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amendment_key** | **string**| Can be the amendment ID or the amendment code. |
 **entity_id** | **string**| The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). | [optional]
 **entity_name** | **string**| The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). | [optional]

### Return type

[**\Swagger\Client\Model\GETAmendmentType**](../Model/GETAmendmentType.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json; charset=utf-8
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **gETAmendmentsBySubscriptionID**
> \Swagger\Client\Model\GETAmendmentType gETAmendmentsBySubscriptionID($subscription_id, $entity_id, $entity_name)

Get amendments by subscription ID

Retrieves detailed information about the amendment with the specified subscription.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AmendmentsApi();
$subscription_id = "subscription_id_example"; // string | The ID of the subscription whose amendment changes you want to retrieve.
$entity_id = "entity_id_example"; // string | The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name).
$entity_name = "entity_name_example"; // string | The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name).

try {
    $result = $api_instance->gETAmendmentsBySubscriptionID($subscription_id, $entity_id, $entity_name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AmendmentsApi->gETAmendmentsBySubscriptionID: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subscription_id** | **string**| The ID of the subscription whose amendment changes you want to retrieve. |
 **entity_id** | **string**| The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). | [optional]
 **entity_name** | **string**| The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). | [optional]

### Return type

[**\Swagger\Client\Model\GETAmendmentType**](../Model/GETAmendmentType.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json; charset=utf-8
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **objectDELETEAmendment**
> \Swagger\Client\Model\ProxyDeleteResponse objectDELETEAmendment($id, $entity_id, $entity_name)

CRUD: Delete Amendment



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AmendmentsApi();
$id = "id_example"; // string | Object id
$entity_id = "entity_id_example"; // string | The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name).
$entity_name = "entity_name_example"; // string | The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name).

try {
    $result = $api_instance->objectDELETEAmendment($id, $entity_id, $entity_name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AmendmentsApi->objectDELETEAmendment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Object id |
 **entity_id** | **string**| The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). | [optional]
 **entity_name** | **string**| The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). | [optional]

### Return type

[**\Swagger\Client\Model\ProxyDeleteResponse**](../Model/ProxyDeleteResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json; charset=utf-8
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **objectGETAmendment**
> \Swagger\Client\Model\ProxyGetAmendment objectGETAmendment($id, $entity_id, $entity_name, $fields)

CRUD: Retrieve Amendment



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AmendmentsApi();
$id = "id_example"; // string | Object id
$entity_id = "entity_id_example"; // string | The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name).
$entity_name = "entity_name_example"; // string | The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name).
$fields = "fields_example"; // string | Object fields to return

try {
    $result = $api_instance->objectGETAmendment($id, $entity_id, $entity_name, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AmendmentsApi->objectGETAmendment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Object id |
 **entity_id** | **string**| The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). | [optional]
 **entity_name** | **string**| The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). | [optional]
 **fields** | **string**| Object fields to return | [optional]

### Return type

[**\Swagger\Client\Model\ProxyGetAmendment**](../Model/ProxyGetAmendment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json; charset=utf-8
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **objectPOSTAmendment**
> \Swagger\Client\Model\ProxyCreateOrModifyResponse objectPOSTAmendment($create_request, $entity_id, $entity_name)

CRUD: Create Amendment



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AmendmentsApi();
$create_request = new \Swagger\Client\Model\ProxyCreateAmendment(); // \Swagger\Client\Model\ProxyCreateAmendment | 
$entity_id = "entity_id_example"; // string | The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name).
$entity_name = "entity_name_example"; // string | The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name).

try {
    $result = $api_instance->objectPOSTAmendment($create_request, $entity_id, $entity_name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AmendmentsApi->objectPOSTAmendment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_request** | [**\Swagger\Client\Model\ProxyCreateAmendment**](../Model/\Swagger\Client\Model\ProxyCreateAmendment.md)|  |
 **entity_id** | **string**| The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). | [optional]
 **entity_name** | **string**| The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). | [optional]

### Return type

[**\Swagger\Client\Model\ProxyCreateOrModifyResponse**](../Model/ProxyCreateOrModifyResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json; charset=utf-8
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **objectPUTAmendment**
> \Swagger\Client\Model\ProxyCreateOrModifyResponse objectPUTAmendment($id, $modify_request, $entity_id, $entity_name)

CRUD: Update Amendment



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AmendmentsApi();
$id = "id_example"; // string | Object id
$modify_request = new \Swagger\Client\Model\ProxyModifyAmendment(); // \Swagger\Client\Model\ProxyModifyAmendment | 
$entity_id = "entity_id_example"; // string | The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name).
$entity_name = "entity_name_example"; // string | The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name).

try {
    $result = $api_instance->objectPUTAmendment($id, $modify_request, $entity_id, $entity_name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AmendmentsApi->objectPUTAmendment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Object id |
 **modify_request** | [**\Swagger\Client\Model\ProxyModifyAmendment**](../Model/\Swagger\Client\Model\ProxyModifyAmendment.md)|  |
 **entity_id** | **string**| The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). | [optional]
 **entity_name** | **string**| The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). | [optional]

### Return type

[**\Swagger\Client\Model\ProxyCreateOrModifyResponse**](../Model/ProxyCreateOrModifyResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json; charset=utf-8
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

