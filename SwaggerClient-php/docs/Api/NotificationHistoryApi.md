# Swagger\Client\NotificationHistoryApi

All URIs are relative to *https://rest.zuora.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**gETCalloutHistory**](NotificationHistoryApi.md#gETCalloutHistory) | **GET** /v1/notification-history/callout | Get callout notification histories
[**gETEmailHistory**](NotificationHistoryApi.md#gETEmailHistory) | **GET** /v1/notification-history/email | Get email notification histories


# **gETCalloutHistory**
> \Swagger\Client\Model\GETCalloutHistoryVOsType gETCalloutHistory($entity_id, $entity_name, $end_time, $start_time, $object_id, $failed_only, $event_category, $include_response_content)

Get callout notification histories

This REST API reference describes how to get a notification history for callouts.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\NotificationHistoryApi();
$entity_id = "entity_id_example"; // string | The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name).
$entity_name = "entity_name_example"; // string | The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name).
$end_time = new \DateTime(); // \DateTime | The final date and time of records to be returned. Defaults to now. Use format yyyy-MM-ddTHH:mm:ss.
$start_time = new \DateTime(); // \DateTime | The initial date and time of records to be returned. Defaults to (end time - 1 day). Use format yyyy-MM-ddTHH:mm:ss.
$object_id = "object_id_example"; // string | The ID of an object that triggered a callout notification.
$failed_only = true; // bool | If `true`, only return failed records. If `false`, return all records in the given date range. The default value is `true`.
$event_category = "event_category_example"; // string | Category of records to be returned by event category.
$include_response_content = true; // bool | 

try {
    $result = $api_instance->gETCalloutHistory($entity_id, $entity_name, $end_time, $start_time, $object_id, $failed_only, $event_category, $include_response_content);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NotificationHistoryApi->gETCalloutHistory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **entity_id** | **string**| The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). | [optional]
 **entity_name** | **string**| The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). | [optional]
 **end_time** | **\DateTime**| The final date and time of records to be returned. Defaults to now. Use format yyyy-MM-ddTHH:mm:ss. | [optional]
 **start_time** | **\DateTime**| The initial date and time of records to be returned. Defaults to (end time - 1 day). Use format yyyy-MM-ddTHH:mm:ss. | [optional]
 **object_id** | **string**| The ID of an object that triggered a callout notification. | [optional]
 **failed_only** | **bool**| If &#x60;true&#x60;, only return failed records. If &#x60;false&#x60;, return all records in the given date range. The default value is &#x60;true&#x60;. | [optional]
 **event_category** | **string**| Category of records to be returned by event category. | [optional]
 **include_response_content** | **bool**|  | [optional]

### Return type

[**\Swagger\Client\Model\GETCalloutHistoryVOsType**](../Model/GETCalloutHistoryVOsType.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json; charset=utf-8
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **gETEmailHistory**
> \Swagger\Client\Model\GETEmailHistoryVOsType gETEmailHistory($entity_id, $entity_name, $end_time, $start_time, $object_id, $failed_only, $event_category)

Get email notification histories

This REST API reference describes how to get a notification history for notification emails.   ## Notes Request parameters and their values may be appended with a \"?\" following the HTTPS GET request.  Additional request parameter are separated by \"&\".   For example:  `GET https://api.zuora.com/rest/v1/notification-history/email?startTime=2015-01-12T00:00:00&endTime=2015-01-15T00:00:00&failedOnly=false&eventCategory=1000&pageSize=1`

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\NotificationHistoryApi();
$entity_id = "entity_id_example"; // string | The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name).
$entity_name = "entity_name_example"; // string | The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name).
$end_time = new \DateTime(); // \DateTime | The end date and time of records to be returned. Defaults to now. Use format yyyy-MM-ddTHH:mm:ss. The maximum date range (endTime - startTime) is three days.
$start_time = new \DateTime(); // \DateTime | The initial date and time of records to be returned. Defaults to (end time - 1 day). Use format yyyy-MM-ddTHH:mm:ss. The maximum date range (endTime - startTime) is three days.
$object_id = "object_id_example"; // string | The Id of an object that triggered an email notification.
$failed_only = true; // bool | If `true`, only returns failed records. When `false`, returns all records in the given date range. Defaults to `true` when not specified.
$event_category = "event_category_example"; // string | Category of records to be returned by event category.

try {
    $result = $api_instance->gETEmailHistory($entity_id, $entity_name, $end_time, $start_time, $object_id, $failed_only, $event_category);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NotificationHistoryApi->gETEmailHistory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **entity_id** | **string**| The Id of the entity that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). | [optional]
 **entity_name** | **string**| The [name of the entity](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Multi-entity/B_Introduction_to_Entity_and_Entity_Hierarchy#Name_and_Display_Name) that you want to access. Note that you must have permission to access the entity. For more information, see [REST Authentication](https://www.zuora.com/developer/api-reference/#section/Authentication/Entity-Id-and-Entity-Name). | [optional]
 **end_time** | **\DateTime**| The end date and time of records to be returned. Defaults to now. Use format yyyy-MM-ddTHH:mm:ss. The maximum date range (endTime - startTime) is three days. | [optional]
 **start_time** | **\DateTime**| The initial date and time of records to be returned. Defaults to (end time - 1 day). Use format yyyy-MM-ddTHH:mm:ss. The maximum date range (endTime - startTime) is three days. | [optional]
 **object_id** | **string**| The Id of an object that triggered an email notification. | [optional]
 **failed_only** | **bool**| If &#x60;true&#x60;, only returns failed records. When &#x60;false&#x60;, returns all records in the given date range. Defaults to &#x60;true&#x60; when not specified. | [optional]
 **event_category** | **string**| Category of records to be returned by event category. | [optional]

### Return type

[**\Swagger\Client\Model\GETEmailHistoryVOsType**](../Model/GETEmailHistoryVOsType.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json; charset=utf-8
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

