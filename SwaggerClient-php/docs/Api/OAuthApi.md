# Swagger\Client\OAuthApi

All URIs are relative to *https://rest.zuora.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createToken**](OAuthApi.md#createToken) | **POST** /oauth/token | Create an OAuth token


# **createToken**
> \Swagger\Client\Model\TokenResponse createToken($client_id, $client_secret, $grant_type, $zuora_track_id)

Create an OAuth token

Creates a bearer token that enables an OAuth client to authenticate with the Zuora REST API. The OAuth client must have been created using the Zuora UI. See [Authentication](https://www.zuora.com/developer/rest-api/general-concepts/authentication/) for more information.  **Note:** When using this operation, do not set any authentication headers such as `Authorization`, `apiAccessKeyId`, or `apiSecretAccessKey`.  You should not use this operation to generate a large number of bearer tokens in a short period of time; each token should be used until it expires. If you receive a 429 Too Many Requests response when using this operation, reduce the frequency of requests. This endpoint is rate limited by IP address.  For the rate limit information of authentication, see [Rate and concurrent request limits](https://www.zuora.com/developer/rest-api/general-concepts/rate-concurrency-limits/).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\OAuthApi();
$client_id = "client_id_example"; // string | The Client ID of the OAuth client.
$client_secret = "client_secret_example"; // string | The Client Secret that was displayed when the OAuth client was created.
$grant_type = "grant_type_example"; // string | The OAuth grant type that will be used to generate the token. The value of this parameter must be `client_credentials`.
$zuora_track_id = "zuora_track_id_example"; // string | A custom identifier for tracing the API call. If you set a value for this header, Zuora returns the same value in the response headers. This header enables you to associate your system process identifiers with Zuora API calls, to assist with troubleshooting in the event of an issue.  The value of this field must use the US-ASCII character set and must not include any of the following characters: colon (`:`), semicolon (`;`), double quote (`\"`), and quote (`'`).

try {
    $result = $api_instance->createToken($client_id, $client_secret, $grant_type, $zuora_track_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OAuthApi->createToken: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client_id** | **string**| The Client ID of the OAuth client. |
 **client_secret** | **string**| The Client Secret that was displayed when the OAuth client was created. |
 **grant_type** | **string**| The OAuth grant type that will be used to generate the token. The value of this parameter must be &#x60;client_credentials&#x60;. |
 **zuora_track_id** | **string**| A custom identifier for tracing the API call. If you set a value for this header, Zuora returns the same value in the response headers. This header enables you to associate your system process identifiers with Zuora API calls, to assist with troubleshooting in the event of an issue.  The value of this field must use the US-ASCII character set and must not include any of the following characters: colon (&#x60;:&#x60;), semicolon (&#x60;;&#x60;), double quote (&#x60;\&quot;&#x60;), and quote (&#x60;&#39;&#x60;). | [optional]

### Return type

[**\Swagger\Client\Model\TokenResponse**](../Model/TokenResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

