# GETSubscriptionProductFeatureType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **string** | Feature description. | [optional] 
**feature_code** | **string** | Feature code, up to 255 characters long. | [optional] 
**id** | **string** | SubscriptionProductFeature ID. | [optional] 
**name** | **string** | Feature name, up to 255 characters long. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


