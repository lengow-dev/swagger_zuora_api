# DeleteResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errors** | [**\Swagger\Client\Model\Error[]**](Error.md) | If the delete failed, this contains an array of Error objects. | [optional] 
**id** | **string** | ID of the deleted object. | [optional] 
**success** | **bool** | A boolean field indicating the success of the delete operation. If the delete was successful, it is &#x60;true&#x60;. Otherwise, &#x60;false&#x60;. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


