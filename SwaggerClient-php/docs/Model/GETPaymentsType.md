# GETPaymentsType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payments** | [**\Swagger\Client\Model\GETPaymentType[]**](GETPaymentType.md) | Information about one or more payments: | [optional] 
**success** | **bool** | Returns &#x60;true&#x60; if the request was processed successfully. | [optional] 
**next_page** | **string** | Return URI of the next page | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


