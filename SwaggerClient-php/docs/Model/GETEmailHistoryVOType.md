# GETEmailHistoryVOType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bcc** | **string** | Blind carbon copy recipients of the email. | [optional] 
**cc** | **string** | Carbon Copy recipients of the email. | [optional] 
**error_message** | **string** | null if the content of result is \&quot;OK\&quot;. A description of the error if the content of result is not \&quot;OK\&quot;. | [optional] 
**event_category** | **string** | The event category of the email. | [optional] 
**from_email** | **string** | The sender of the email. | [optional] 
**notification** | **string** | The name of the notification. | [optional] 
**reply_to** | **string** | The reply-to address as configured in the email template. | [optional] 
**result** | **string** | The result from the mail server of sending the email. | [optional] 
**send_time** | **string** | The date and time the email was sent. | [optional] 
**subject** | **string** | The subject of the email. | [optional] 
**to_email** | **string** | The intended recipient of the email. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


