# ProxyModifyInvoice

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **string** | The status of the invoice in the system. This status is not the status of the payment of the invoice, just the status of the invoice itself. **Character limit**: 8 **Values**: one of the following:  -  Draft (default, automatically set upon invoice creation)  -  Posted  -  Canceled | [optional] 
**transferred_to_accounting** | **string** | Specifies whether or not the invoice was transferred to an external accounting system, such as NetSuite. **Character limit**: 10 **Values**: Processing, Yes, Error, Ignore | [optional] 
**external_payment_reference__c** | **string** | Value of merchantReference field, returned by Adyen events.   **Character limit**: 100 | [optional] 
**external_payment_status__c** | **string** | Status of the payment determined by the Adyen events.   **Values**: Processing, Processed, Canceled | [optional] 
**external_payment_date_time__c** | [**\DateTime**](\DateTime.md) | Value of eventDate field, returned by Adyen events. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


