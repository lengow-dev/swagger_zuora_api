# GETAccountSummaryTypeTaxInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vat_id** | **string** | EU Value Added Tax ID. | [optional] 
**company_code** | **string** | Unique code that identifies a company account in Avalara. | [optional] 
**exempt_certificate_id** | **string** | ID of the customer tax exemption certificate. | [optional] 
**exempt_certificate_type** | **string** | Type of tax exemption certificate that the customer holds. | [optional] 
**exempt_description** | **string** | Description of the tax exemption certificate that the customer holds. | [optional] 
**exempt_effective_date** | [**\DateTime**](Date.md) | Date when the customer tax exemption starts. | [optional] 
**exempt_expiration_date** | [**\DateTime**](Date.md) | Date when the customer tax exemption expires. | [optional] 
**exempt_issuing_jurisdiction** | **string** | Jurisdiction in which the customer tax exemption certificate was issued. | [optional] 
**exempt_status** | **string** | Status of the account tax exemption. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


