# ProxyActionsubscribeRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**subscribes** | [**\Swagger\Client\Model\SubscribeRequest[]**](SubscribeRequest.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


