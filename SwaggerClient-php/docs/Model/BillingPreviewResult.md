# BillingPreviewResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_id** | **string** | ID of the customer account to which the billing preview applies. | [optional] 
**invoice_items** | [**\Swagger\Client\Model\POSTBillingPreviewInvoiceItem[]**](POSTBillingPreviewInvoiceItem.md) | An array of invoice items returned as the result of the billing preview request. | [optional] 
**success** | **bool** | Returns &#x60;true&#x60; if the request was processed successfully. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


