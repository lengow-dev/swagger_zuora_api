# SubscriptionProductFeature

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_by_id** | **string** |  | [optional] 
**created_date** | [**\DateTime**](\DateTime.md) | Date and time when the product feature was added to the subscription.   **Character limit**: 29   **Values**: | [optional] 
**description** | **string** | Description of the subscription product feature.   **Character limit**: 500   **Values**: | [optional] 
**feature_code** | **string** | Unique code of the feature.   **Character limit**: 255   **Values**: | [optional] 
**feature_id** | **string** | Internal Zuora ID of the feature.   **Character limit**: 32   **Values**: | [optional] 
**name** | **string** | Name of the feature.   **Character limit**: 255   **Values**: | [optional] 
**rate_plan_id** | **string** | Id of the product rate plan to which the feature belongs.   **Character limit**: 32   **Values**: | [optional] 
**updated_by_id** | **string** | Internal Zuora ID of the user who last updated the subscription product feature.   **Character limit**: 32   **Values**: | [optional] 
**updated_date** | [**\DateTime**](\DateTime.md) | Date and time when the subscription product feature was last updated.   **Character limit**: 29   **Values**: | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


