# POSTMassUpdateTypeParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**action_type** | **string** | Type of mass action you want to perform. The following mass actions are supported: - UpdateAccountingCode - CreateRevenueSchedule - UpdateRevenueSchedule - DeleteRevenueSchedule - ImportFXRate | 
**checksum** | **string** | An MD5 checksum that is used to validate the integrity of the uploaded file. The checksum is a 32-character string. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


