# ProxyGetProductRatePlan

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_by_id** | **string** | The ID of the Zuora user who created the &#x60;ProductRatePlan&#x60; object. **Character limit**: 32 **Values**: automatically generated | [optional] 
**created_date** | [**\DateTime**](\DateTime.md) | The date when the &#x60;ProductRatePlan&#x60; object was created. **Character limit**: 29 **Values**: automatically generated | [optional] 
**description** | **string** | A description of the product rate plan. **Character limit**: 500 **Values**: a string of 500 characters or fewer | [optional] 
**effective_end_date** | [**\DateTime**](Date.md) | The date when the product rate plan expires and can&#39;t be subscribed to, in &#x60;yyyy-mm-dd&#x60; format. **Character limit**: 29 | [optional] 
**effective_start_date** | [**\DateTime**](Date.md) | The date when the product rate plan becomes available and can be subscribed to, in &#x60;yyyy-mm-dd&#x60; format. **Character limit**: 29 | [optional] 
**id** | **string** | Object identifier. | [optional] 
**name** | **string** | The name of the product rate plan. The name doesn&#39;t have to be unique in a Product Catalog, but the name has to be unique within a product. **Character limit**: 100 **Values**: a string of 100 characters or fewer | [optional] 
**product_id** | **string** | The ID of the product that contains the product rate plan. **Character limit**: 32 **Values**: a string of 32 characters or fewer | [optional] 
**updated_by_id** | **string** | The ID of the last user to update the object. **Character limit**: 32 **Values**: automatically generated | [optional] 
**updated_date** | [**\DateTime**](\DateTime.md) | The date when the object was last updated. **Character limit**: 29 **Values**: automatically generated | [optional] 
**region__c** | **string** | Used to set an Area to rate plan object | [optional] 
**country__c** | **string** | Used to set a Country to rate plan object | [optional] 
**product_bounds__c** | **string** | Used to set an Product bounds limit to rate plan object | [optional] 
**user_bounds__c** | **string** | Used to set an User bounds limit to rate plan object | [optional] 
**catalog_bounds__c** | **string** | Used to set an catalog bounds limit to rate plan object | [optional] 
**marketplace_bounds__c** | **string** | Used to set an marketplace bounds limit to rate plan object | [optional] 
**lia_bounds__c** | **string** | Used to set an local inventory bounds limit to rate plan object | [optional] 
**app_bounds__c** | **string** | Used to set an app bounds limit to rate plan object | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


