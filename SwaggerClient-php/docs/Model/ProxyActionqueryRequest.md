# ProxyActionqueryRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**query_string** | **string** | ZOQL expression that specifies the object to query, the fields to retrieve, and any filters. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


