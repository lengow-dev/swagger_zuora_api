# GETRevenueStartDateSettingType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start_date** | [**\DateTime**](Date.md) | The date on which revenue automation starts. This is the first day of an accounting period. | [optional] 
**success** | **bool** | Returns &#x60;true&#x60; if the request was processed successfully. | [optional] 
**updated_by** | **string** | The user who made the change. | [optional] 
**updated_on** | [**\DateTime**](\DateTime.md) | The date when the revenue automation start date was set. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


