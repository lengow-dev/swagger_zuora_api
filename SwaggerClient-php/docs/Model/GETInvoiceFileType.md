# GETInvoiceFileType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | ID of the invoice PDF file. This is the ID for the file object and different from the file handle id in the pdfFileUrl field. To open a file, you need to use the file handle ID. | [optional] 
**pdf_file_url** | **string** | REST URL for the invoice PDF file. Click the URL to open the invoice PDF file. | [optional] 
**version_number** | **int** | Version number of the invoice PDF file | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


