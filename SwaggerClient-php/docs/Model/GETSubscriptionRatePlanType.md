# GETSubscriptionRatePlanType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**custom_field__c** | **string** | Any custom fields defined for this object. The custom field name is case-sensitive. | [optional] 
**product_bounds_override__c** | **string** | Used to set an Product bounds override to Subscription rate plan object | [optional] 
**user_bounds_override__c** | **string** | Used to set an User bounds override to Subscription rate plan object | [optional] 
**catalog_bounds_override__c** | **string** | Used to set an catalog bounds override to Subscription rate plan object | [optional] 
**marketplace_bounds_override__c** | **string** | Used to set an marketplace bounds override to Subscription rate plan object | [optional] 
**lia_bounds_override__c** | **string** | Used to set an local inventory ads bounds override to Subscription rate plan object | [optional] 
**app_bounds_override__c** | **string** | Used to set an app bounds override to Subscription rate plan object | [optional] 
**project__c** | **string** | Used to set a project type | [optional] 
**id** | **string** | Rate plan ID. | [optional] 
**last_change_type** | **string** | The last amendment on the rate plan.  Possible Values:  * &#x60;Add&#x60; * &#x60;Update&#x60; * &#x60;Remove&#x60; | [optional] 
**product_id** | **string** |  | [optional] 
**product_name** | **string** |  | [optional] 
**product_rate_plan_id** | **string** |  | [optional] 
**product_sku** | **string** | The unique SKU for the product. | [optional] 
**subscription_rate_plan_number** | **string** | Consistent identifier for a rateplan on a subscription (does not change over versions). | [optional] 
**rate_plan_charges** | [**\Swagger\Client\Model\GETSubscriptionRatePlanChargesType[]**](GETSubscriptionRatePlanChargesType.md) | Container for one or more charges. | [optional] 
**rate_plan_name** | **string** | Name of the rate plan. | [optional] 
**subscription_product_features** | [**\Swagger\Client\Model\GETSubscriptionProductFeatureType[]**](GETSubscriptionProductFeatureType.md) | Container for one or more features.   Only available when the following settings are enabled:  * The Entitlements feature in your tenant.  * The Enable Feature Specification in Product and Subscriptions setting in Zuora Billing Settings | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


