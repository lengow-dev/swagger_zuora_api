# POSTSrpCreateType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**charge_overrides** | [**\Swagger\Client\Model\POSTScCreateType[]**](POSTScCreateType.md) | This optional container is used to override the quantity of one or more product rate plan charges for this subscription. | [optional] 
**custom_field__c** | **string** | Any custom fields defined for this object. The custom field name is case-sensitive. | [optional] 
**product_rate_plan_id** | **string** | ID of a product rate plan for this subscription. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


