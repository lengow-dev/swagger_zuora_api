# AmendRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amend_options** | [**\Swagger\Client\Model\AmendRequestAmendOptions**](AmendRequestAmendOptions.md) |  | [optional] 
**amendments** | [**\Swagger\Client\Model\Amendment[]**](Amendment.md) | The &#x60;Amendment&#x60; object holds the changes that you want to make to the Subscription specified by the &#x60;SubscriptionId&#x60;,  including its &#x60;RatePlan&#x60;, &#x60;RatePlanCharge&#x60;, and &#x60;RatePlanChargeTier&#x60;. | [optional] 
**preview_options** | [**\Swagger\Client\Model\AmendRequestPreviewOptions**](AmendRequestPreviewOptions.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


