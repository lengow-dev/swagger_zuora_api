# POSTInvoiceCollectResponseType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount_collected** | **string** | Payment amount applied. | [optional] 
**invoices** | [**\Swagger\Client\Model\POSTInvoiceCollectInvoicesType[]**](POSTInvoiceCollectInvoicesType.md) | Information on one or more invoices associated with this operation: | [optional] 
**payment_id** | **string** | Payment ID. | [optional] 
**success** | **bool** | Returns &#x60;true&#x60; if the request was processed successfully. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


