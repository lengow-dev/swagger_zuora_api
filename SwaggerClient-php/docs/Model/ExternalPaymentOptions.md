# ExternalPaymentOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **double** |  | [optional] 
**effective_date** | [**\DateTime**](Date.md) |  | [optional] 
**gateway_order_id** | **string** |  | [optional] 
**payment_method_id** | **string** |  | [optional] 
**reference_id** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


