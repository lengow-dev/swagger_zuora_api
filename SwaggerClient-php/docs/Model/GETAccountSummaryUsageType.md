# GETAccountSummaryUsageType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**quantity** | **string** | Number of units used. | [optional] 
**start_date** | [**\DateTime**](Date.md) | The start date of a usage period as &#x60;yyyy-mm&#x60;. Zuora uses this field value to determine the usage date. | [optional] 
**unit_of_measure** | **string** | Unit by which consumption is measured, as configured in the Billing Settings section of the web-based UI. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


