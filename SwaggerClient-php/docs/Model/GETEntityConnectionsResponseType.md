# GETEntityConnectionsResponseType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**entity_connections** | [**\Swagger\Client\Model\GETEntityConnectionsArrayItemsType[]**](GETEntityConnectionsArrayItemsType.md) | Container for one or more connections that are related to the entity. | [optional] 
**success** | **bool** | Returns &#x60;true&#x60; if the request was processed successfully. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


