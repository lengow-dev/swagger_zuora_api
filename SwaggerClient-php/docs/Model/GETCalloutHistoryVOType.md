# GETCalloutHistoryVOType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attempted_num** | **string** | The number of times the callout was retried. | [optional] 
**create_time** | **string** | The time that the calloutHistory record was made. | [optional] 
**event_category** | **string** | The event category for the callout. | [optional] 
**event_context** | **string** | The context of the callout event. | [optional] 
**notification** | **string** | The name of the notification. | [optional] 
**request_method** | **string** | The request method set in notifications settings. | [optional] 
**request_url** | **string** | The base url set in notifications settings. | [optional] 
**response_code** | **string** | The responseCode of the request. | [optional] 
**response_content** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


