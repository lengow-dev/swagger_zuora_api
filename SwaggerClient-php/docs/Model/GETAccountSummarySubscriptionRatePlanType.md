# GETAccountSummarySubscriptionRatePlanType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_id** | **string** | Product ID. | [optional] 
**product_name** | **string** | Product name. | [optional] 
**product_rate_plan_id** | **string** | Product Rate Plan ID. | [optional] 
**product_sku** | **string** |  | [optional] 
**rate_plan_name** | **string** | Rate plan name. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


