# ProxyActioncreateRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**objects** | [**\Swagger\Client\Model\ZObjectUsageCreate[]**](ZObjectUsageCreate.md) |  | [optional] 
**type** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


