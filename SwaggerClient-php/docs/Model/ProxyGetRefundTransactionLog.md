# ProxyGetRefundTransactionLog

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**batch_id** | **string** |  | [optional] 
**gateway** | **string** |  | [optional] 
**gateway_reason_code** | **string** |  | [optional] 
**gateway_reason_code_description** | **string** |  | [optional] 
**gateway_state** | **string** |  | [optional] 
**gateway_transaction_type** | **string** |  | [optional] 
**id** | **string** | Object identifier. | [optional] 
**refund_id** | **string** |  | [optional] 
**request_string** | **string** |  | [optional] 
**response_string** | **string** |  | [optional] 
**transaction_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**transaction_id** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


