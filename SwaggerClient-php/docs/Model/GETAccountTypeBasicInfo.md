# GETAccountTypeBasicInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_number** | **string** | Account number. | [optional] 
**batch** | **string** | The alias name given to a batch. A string of 50 characters or less. | [optional] 
**communication_profile_id** | **string** | ID of a communication profile. | [optional] 
**crm_id** | **string** | CRM account ID for the account, 100 characters or less. | [optional] 
**i_dclient__c** | **string** | ID of the v2 account. | [optional] 
**sire_nfacturation__c** | **string** | Registration number. | [optional] 
**codesage__c** | **string** | SAGE code, use by the accounting team. | [optional] 
**engagement__c** | [**\DateTime**](Date.md) | Commitment date. | [optional] 
**legacyid__c** | **string** | Legacy identifier. | [optional] 
**typeversion__c** | **string** | Accoun type, V2 or V3. | [optional] 
**analytique__c** | **string** | Analytics code. | [optional] 
**compteclient__c** | **string** | Accounting code. | [optional] 
**id** | **string** | Account ID. | [optional] 
**invoice_template_id** | **string** | Invoice template ID, configured in Billing Settings in the Zuora UI. | [optional] 
**name** | **string** | Account name. | [optional] 
**notes** | **string** | Notes associated with the account, up to 65,535 characters. | [optional] 
**status** | **string** | Account status; possible values are: &#x60;Active&#x60;, &#x60;Draft&#x60;, &#x60;Canceled&#x60;. | [optional] 
**tags** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


