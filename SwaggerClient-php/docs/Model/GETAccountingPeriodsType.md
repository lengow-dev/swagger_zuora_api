# GETAccountingPeriodsType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accounting_periods** | [**\Swagger\Client\Model\GETAccountingPeriodWithoutSuccessType[]**](GETAccountingPeriodWithoutSuccessType.md) | An array of all accounting periods on your tenant. The accounting periods are returned in ascending order of start date; that is, the latest period is returned first. | [optional] 
**success** | **bool** | Returns &#x60;true&#x60; if the request was processed successfully. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


