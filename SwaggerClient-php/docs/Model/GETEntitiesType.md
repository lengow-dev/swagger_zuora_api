# GETEntitiesType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**display_name** | **string** | The display name of the entity that is shown in the Zuora UI and APIs. | [optional] 
**id** | **string** | The entity Id. | [optional] 
**locale** | **string** | The locale that is used in this entity. | [optional] 
**name** | **string** | The name of the entity. | [optional] 
**parent_id** | **string** | The Id of the parent entity. | [optional] 
**status** | **string** | The status of the entity. | [optional] 
**tenant_id** | **string** | The Id of the tenant that the entity belongs to. | [optional] 
**timezone** | **string** | The time zone that is used in this entity. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


