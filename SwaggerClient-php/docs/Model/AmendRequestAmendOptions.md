# AmendRequestAmendOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**apply_credit_balance** | **bool** |  | [optional] 
**electronic_payment_options** | [**\Swagger\Client\Model\ElectronicPaymentOptions**](ElectronicPaymentOptions.md) |  | [optional] 
**external_payment_options** | [**\Swagger\Client\Model\ExternalPaymentOptions**](ExternalPaymentOptions.md) |  | [optional] 
**generate_invoice** | **bool** |  | [optional] 
**invoice_processing_options** | [**\Swagger\Client\Model\InvoiceProcessingOptions**](InvoiceProcessingOptions.md) |  | [optional] 
**process_payments** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


