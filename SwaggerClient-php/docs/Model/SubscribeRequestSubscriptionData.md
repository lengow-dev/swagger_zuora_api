# SubscribeRequestSubscriptionData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rate_plan_data** | [**\Swagger\Client\Model\RatePlanData[]**](RatePlanData.md) |  | [optional] 
**subscription** | [**\Swagger\Client\Model\SubscribeRequestSubscriptionDataSubscription**](SubscribeRequestSubscriptionDataSubscription.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


