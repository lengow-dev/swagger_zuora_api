# POSTPaymentMethodDecryption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_id** | **string** | ID of the billing account on which the payment method will be created. | 
**card_holder_info** | [**\Swagger\Client\Model\POSTPaymentMethodDecryptionCardHolderInfo**](POSTPaymentMethodDecryptionCardHolderInfo.md) |  | [optional] 
**integration_type** | **string** | Field to identify the token decryption type.  **Note:** The only value at this time is ‘ApplePay’. | 
**invoice_id** | **string** | The id of invoice this payment will apply to.  **Note:** When processPayment is true, this field is required. Only one invoice can be paid; for scenarios where you want to pay for multiple invoices, set processPayment to false and call payment API separately. | [optional] 
**merchant_id** | **string** | The Merchant ID that was configured for use with Apple Pay in the Apple iOS Developer Center. | 
**payment_gateway** | **string** | The label name of the gateway instance configured in Zuora that should process the payment. When creating a Payment, this must be a valid gateway instance ID and this gateway must support the specific payment method. If not specified, the default gateway on the Account will be used.  **Note:** When processPayment is true, this field is required. | [optional] 
**payment_token** | **object** | The complete JSON Object representing the encrypted payment token payload returned in the response from the Apple Pay session. | 
**process_payment** | **bool** | A boolean flag to control whether a payment should be processed after creating payment method. The payment amount will be equivalent to the amount the merchant supplied in the ApplePay session. Default is false. When processPayment is false, it must be followed by a separate subscribe or payment API call to transaction. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


