# POSTJournalRunTransactionType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** | Transaction type.   The transaction types are: * Invoice Item * Taxation Item * Invoice Item Adjustment (Invoice) * Invoice Item Adjustment (Tax) * Invoice Adjustment (Invoice Adjustment is deprecated on Production. Zuora recommends that you use the Invoice Item Adjustment instead.) * Electronic Payment * External Payment * Electronic Refund * External Refund * Electronic Credit Balance Payment * External Credit Balance Payment * Electronic Credit Balance Refund * External Credit Balance Refund * Credit Balance Adjustment (Applied from Credit Balance) * Credit Balance Adjustment (Transferred to Credit Balance) * Revenue Event Item  To include all transaction types, pass in \&quot;All\&quot;. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


