# PUTPaymentMethodType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address_line1** | **string** | First address line, 255 characters or less. | [optional] 
**address_line2** | **string** | Second address line, 255 characters or less. | [optional] 
**card_holder_name** | **string** | The full name as it appears on the card, e.g., \&quot;John J Smith\&quot;, 50 characters or less. | [optional] 
**city** | **string** | City, 40 characters or less. | [optional] 
**country** | **string** | Country; must be a valid country name or abbreviation. | [optional] 
**default_payment_method** | **bool** | Specify \&quot;true\&quot; to make this card the default payment method; otherwise, omit this parameter to keep the current default payment method. | [optional] 
**email** | **string** | Card holder&#39;s email address, 80 characters or less. | [optional] 
**expiration_month** | **string** | Two-digit expiration month (01-12). | [optional] 
**expiration_year** | **string** | Four-digit expiration year. | [optional] 
**phone** | **string** | Phone number, 40 characters or less. | [optional] 
**security_code** | **string** | The CVV or CVV2 security code for the credit card or debit card. Only required if changing expirationMonth, expirationYear, or cardHolderName. To ensure PCI compliance, this value isn&#39;t stored and can&#39;t be queried. | [optional] 
**state** | **string** | State; must be a valid state name or 2-character abbreviation. | [optional] 
**zip_code** | **string** | Zip code, 20 characters or less. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


