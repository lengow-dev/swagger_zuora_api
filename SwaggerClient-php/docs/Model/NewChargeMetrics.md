# NewChargeMetrics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**charge_number** | **string** |  | [optional] 
**dmrr** | **double** |  | [optional] 
**dtcv** | **double** |  | [optional] 
**mrr** | **double** |  | [optional] 
**original_id** | **string** |  | [optional] 
**original_rate_plan_id** | **string** |  | [optional] 
**product_rate_plan_charge_id** | **string** |  | [optional] 
**product_rate_plan_id** | **string** |  | [optional] 
**tcv** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


