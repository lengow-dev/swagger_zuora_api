# PUTAccountingPeriodType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**custom_field__c** | **string** | Any custom fields defined for this object. The custom field name is case-sensitive. | [optional] 
**end_date** | [**\DateTime**](Date.md) | The end date of the accounting period in yyyy-mm-dd format, for example, \&quot;2016-02-19\&quot;. | [optional] 
**fiscal_year** | **string** | Fiscal year of the accounting period in yyyy format, for example, \&quot;2016\&quot;. | [optional] 
**fiscal_quarter** | **int** |  | [optional] 
**name** | **string** | Name of the accounting period.  Accounting period name must be unique. Maximum of 100 characters. | [optional] 
**notes** | **string** | Notes about the accounting period.  Maximum of 255 characters. | [optional] 
**start_date** | [**\DateTime**](Date.md) | The start date of the accounting period in yyyy-mm-dd format, for example, \&quot;2016-02-19\&quot;. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


