# PostBillingPreviewParam

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_id** | **string** | ID of the customer account to which the billing preview applies. | 
**assume_renewal** | **string** | Indicates whether to generate a preview of future invoice items with the assumption that the subscriptions are renewed.  Set one of the following values in this field to decide how the assumption is applied in the billing preview.    * **All:** The assumption is applied to all the subscriptions. Zuora generates preview invoice item data from the first day of the customer&#39;s next billing period to the target date.      * **None:** (Default) The assumption is not applied to the subscriptions. Zuora generates preview invoice item data based on the current term end date and the target date.        * If the target date is later than the current term end date, Zuora generates preview invoice item data from the first day of the customer&#39;s next billing period to the current term end date.      * If the target date is earlier than the current term end date, Zuora generates preview invoice item data from the first day of the customer&#39;s next billing period to the target date.    * **Autorenew:** The assumption is applied to the subscriptions that have auto-renew enabled. Zuora generates preview invoice item data from the first day of the customer&#39;s next billing period to the target date.  **Note:** This field can only be used if the subscription renewal term is not set to 0. | [optional] 
**charge_type_to_exclude** | **string** | The charge types to exclude from the billing preview.  **Possible values:** OneTime, Recurring, Usage, and any combination of these values. | [optional] 
**including_evergreen_subscription** | **bool** | Indicates if evergreen subscriptions are included in the billingPreview call. | [optional] 
**target_date** | [**\DateTime**](Date.md) | The target date for the billingPreview call. The billingPreview call generates preview invoice item data from the first day of the customer&#39;s next billing period to the TargetDate.   If the TargetDate is later than the subscription current term end date, the preview invoice item data is generated from the first day of the customer&#39;s next billing period to the current term end date. If you want to generate preview invoice item data past the end of the subscription current term, specify the &#x60;AssumeRenewal&#x60; field in the request. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


