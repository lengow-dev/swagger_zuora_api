# GETAttachmentResponseType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_by** | **string** | Zuora user id who added this attachment to the object. | [optional] 
**created_on** | [**\DateTime**](\DateTime.md) | Date and time when the attachment was added to the object. | [optional] 
**description** | **string** | Description of the attachment. | [optional] 
**file_content_type** | **string** | File type. | [optional] 
**file_id** | **string** | File id of the attached file. | [optional] 
**file_name** | **string** | Attachment file name. | [optional] 
**id** | **string** | Id of this attachment. | [optional] 
**success** | **bool** | Returns &#x60;true&#x60; if the request was processed successfully. | [optional] 
**updated_by** | **string** | Zuora user id who last updated the attachment. | [optional] 
**updated_on** | [**\DateTime**](\DateTime.md) | Date and time when the attachment was last updated. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


