# POSTAccountResponseType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_id** | **string** | Auto-generated account ID. | [optional] 
**account_number** | **string** | Account number. | [optional] 
**contracted_mrr** | **string** | Contracted monthly recurring revenue of the subscription. | [optional] 
**invoice_id** | **string** | ID of the invoice generated at account creation, if applicable. | [optional] 
**paid_amount** | **string** | Amount collected on the invoice generated at account creation, if applicable. | [optional] 
**payment_id** | **string** | ID of the payment collected on the invoice generated at account creation, if applicable. | [optional] 
**payment_method_id** | **string** | ID of the payment method that was set up at account creation, which automatically becomes the default payment method for this account. | [optional] 
**subscription_id** | **string** | ID of the subscription that was set up at account creation, if applicable. | [optional] 
**subscription_number** | **string** | Number of the subscription that was set up at account creation, if applicable. | [optional] 
**success** | **bool** | Returns &#x60;true&#x60; if the request was processed successfully. | [optional] 
**total_contracted_value** | **string** | Total contracted value of the subscription. | [optional] 
**reasons** | [**\Swagger\Client\Model\CommonResponseTypeReasons[]**](CommonResponseTypeReasons.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


