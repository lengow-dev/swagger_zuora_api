# POSTBillingPreviewInvoiceItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**charge_amount** | **string** | The amount of the charge. This amount doesn&#39;t include taxes regardless if the charge&#39;s tax mode is inclusive or exclusive. | [optional] 
**charge_date** | [**\DateTime**](\DateTime.md) | The date when the invoice item was created. | [optional] 
**charge_description** | **string** | Description of the charge. | [optional] 
**charge_id** | **string** | Id of the charge. | [optional] 
**charge_name** | **string** | Name of the charge. | [optional] 
**charge_number** | **string** | Number of the charge. | [optional] 
**charge_type** | **string** | The type of charge.   Possible values are &#x60;OneTime&#x60;, &#x60;Recurring&#x60;, and &#x60;Usage&#x60;. | [optional] 
**id** | **string** | Invoice item ID. | [optional] 
**processing_type** | **double** | Identifies the kind of charge.   Possible values: * 0: charge * 1: discount * 2: prepayment * 3: tax | [optional] 
**product_name** | **string** | Name of the product associated with this item. | [optional] 
**quantity** | **string** | Quantity of this item, in the configured unit of measure for the charge. | [optional] 
**service_end_date** | [**\DateTime**](Date.md) | End date of the service period for this item, i.e., the last day of the service period, in &#x60;yyyy-mm-dd&#x60; format. | [optional] 
**service_start_date** | [**\DateTime**](Date.md) | Start date of the service period for this item, in &#x60;yyyy-mm-dd&#x60; format. If the charge is a one-time fee, this is the date of that charge. | [optional] 
**subscription_id** | **string** | ID of the subscription associated with this item. | [optional] 
**subscription_name** | **string** | Name of the subscription associated with this item. | [optional] 
**subscription_number** | **string** | Number of the subscription associated with this item. | [optional] 
**tax_amount** | **string** | Tax applied to the charge. | [optional] 
**unit_of_measure** | **string** | Unit used to measure consumption. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


