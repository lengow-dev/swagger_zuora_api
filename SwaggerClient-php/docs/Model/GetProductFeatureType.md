# GetProductFeatureType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **string** | Feature code, up to 255 characters long. | [optional] 
**custom_field__c** | **string** | Any custom fields defined for this object. The custom field name is case-sensitive. | [optional] 
**description** | **string** | Feature description. | [optional] 
**id** | **string** | Feature ID. | [optional] 
**name** | **string** | Feature name, up to 255 characters long. | [optional] 
**status** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


