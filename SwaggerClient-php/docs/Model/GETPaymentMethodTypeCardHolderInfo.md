# GETPaymentMethodTypeCardHolderInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address_line1** | **string** | First address line, 255 characters or less. | [optional] 
**address_line2** | **string** | Second address line, 255 characters or less. | [optional] 
**card_holder_name** | **string** | The full name as it appears on the card, e.g., \&quot;John J Smith\&quot;, 50 characters or less. | [optional] 
**city** | **string** | City, 40 characters or less. | [optional] 
**country** | **string** | Country, must be a valid country name or abbreviation. | [optional] 
**email** | **string** | Card holder&#39;s email address, 80 characters or less. | [optional] 
**phone** | **string** | Phone number, 40 characters or less. | [optional] 
**state** | **string** | State, must be a valid state name or 2-character abbreviation. | [optional] 
**zip_code** | **string** | Zip code, 20 characters or less. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


