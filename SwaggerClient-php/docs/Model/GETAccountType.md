# GETAccountType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**basic_info** | [**\Swagger\Client\Model\GETAccountTypeBasicInfo**](GETAccountTypeBasicInfo.md) |  | [optional] 
**bill_to_contact** | [**\Swagger\Client\Model\GETAccountTypeBillToContact**](GETAccountTypeBillToContact.md) |  | [optional] 
**billing_and_payment** | [**\Swagger\Client\Model\GETAccountTypeBillingAndPayment**](GETAccountTypeBillingAndPayment.md) |  | [optional] 
**metrics** | [**\Swagger\Client\Model\GETAccountTypeMetrics**](GETAccountTypeMetrics.md) |  | [optional] 
**sold_to_contact** | [**\Swagger\Client\Model\GETAccountTypeSoldToContact**](GETAccountTypeSoldToContact.md) |  | [optional] 
**success** | **bool** | Returns &#x60;true&#x60; if the request was processed successfully. | [optional] 
**tax_info** | [**\Swagger\Client\Model\GETAccountSummaryTypeTaxInfo**](GETAccountSummaryTypeTaxInfo.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


