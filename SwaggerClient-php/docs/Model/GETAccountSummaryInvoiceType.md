# GETAccountSummaryInvoiceType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **string** | Invoice amount before adjustments, discounts, and similar items. | [optional] 
**balance** | **string** | Balance due on the invoice. | [optional] 
**due_date** | [**\DateTime**](Date.md) | Due date as &#x60;yyyy-mm-dd&#x60;. | [optional] 
**id** | **string** | Invoice ID. | [optional] 
**invoice_date** | [**\DateTime**](Date.md) | Invoice date as &#x60;yyyy-mm-dd&#x60;. | [optional] 
**invoice_number** | **string** | Invoice number. | [optional] 
**status** | **string** | Invoice status - not the payment status of the invoice, just the status of the invoice itself. Possible values are: &#x60;Posted&#x60;, &#x60;Draft&#x60;, &#x60;Canceled&#x60;, &#x60;Error&#x60;. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


