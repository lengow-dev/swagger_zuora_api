# POSTSubscriptionPreviewTypePreviewAccountInfoBillToContact

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**city** | **string** | The city of the bill-to address. The value should be 40 characters or less. | [optional] 
**country** | **string** | The country of the bill-to address. The value must be a valid country name or abbreviation. | [optional] 
**county** | **string** | The county of the bill-to address. The value should be 32 characters or less. | [optional] 
**state** | **string** | The state of the bill-to address. The value must be a valid state or province name or 2-character abbreviation. | [optional] 
**tax_region** | **string** | If using Zuora Tax, a region string as optionally defined in your tax rules. | [optional] 
**zip_code** | **string** | The zip code of the bill-to address. The value should be 20 characters or less. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


