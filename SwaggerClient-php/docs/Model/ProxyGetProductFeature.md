# ProxyGetProductFeature

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_by_id** | **string** | The ID of the Zuora user who created the Account object. **Character limit**: 32 **Values**: automatically generated | [optional] 
**created_date** | [**\DateTime**](\DateTime.md) | The date when the Account object was created. **Character limit**: 29 **Values**: automatically generated | [optional] 
**feature_id** | **string** | Internal Zuora ID of the product feature. This field is not editable. **Character limit**: 32 **Values**: a string of 32 characters or fewer | [optional] 
**id** | **string** | Object identifier. | [optional] 
**product_id** | **string** | Id of the product to which the feature belongs. This field is not editable. **Character limit**: 32 **Values**: a string of 32 characters or fewer | [optional] 
**updated_by_id** | **string** | The ID of the user who last updated the account. **Character limit**: 32 **Values**: automatically generated | [optional] 
**updated_date** | [**\DateTime**](\DateTime.md) | The date when the account was last updated. **Character limit**: 29 **Values**: automatically generated | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


