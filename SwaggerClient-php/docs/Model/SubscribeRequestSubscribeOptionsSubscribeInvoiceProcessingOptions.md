# SubscribeRequestSubscribeOptionsSubscribeInvoiceProcessingOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**invoice_date** | [**\DateTime**](Date.md) |  | [optional] 
**invoice_processing_scope** | **string** |  | [optional] 
**invoice_target_date** | [**\DateTime**](Date.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


