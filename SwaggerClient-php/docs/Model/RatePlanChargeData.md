# RatePlanChargeData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rate_plan_charge** | [**\Swagger\Client\Model\RatePlanChargeDataRatePlanCharge**](RatePlanChargeDataRatePlanCharge.md) |  | [optional] 
**rate_plan_charge_tier** | [**\Swagger\Client\Model\RatePlanChargeTier[]**](RatePlanChargeTier.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


