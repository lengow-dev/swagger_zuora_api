# SubscriptionProductFeatureList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**subscription_product_feature** | [**\Swagger\Client\Model\SubscriptionProductFeature[]**](SubscriptionProductFeature.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


