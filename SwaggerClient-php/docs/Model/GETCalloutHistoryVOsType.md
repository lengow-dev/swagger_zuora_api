# GETCalloutHistoryVOsType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**callout_histories** | [**\Swagger\Client\Model\GETCalloutHistoryVOType[]**](GETCalloutHistoryVOType.md) | A container for callout histories. | [optional] 
**success** | **bool** | Returns &#x60;true&#x60; if the request was processed successfully. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


