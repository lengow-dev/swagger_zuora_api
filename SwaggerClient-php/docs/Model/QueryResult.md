# QueryResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**done** | **bool** |  | [optional] 
**query_locator** | **string** |  | [optional] 
**records** | [**\Swagger\Client\Model\ZObject[]**](ZObject.md) |  | [optional] 
**size** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


