# POSTSubscriptionResponseType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contracted_mrr** | **string** | Monthly recurring revenue of the subscription. | [optional] 
**invoice_id** | **string** | Invoice ID, if an invoice is generated during the subscription process. | [optional] 
**paid_amount** | **string** | Payment amount, if a payment is collected. | [optional] 
**payment_id** | **string** | Payment ID, if a payment is collected. | [optional] 
**subscription_id** | **string** |  | [optional] 
**subscription_number** | **string** |  | [optional] 
**success** | **bool** | Returns &#x60;true&#x60; if the request was processed successfully. | [optional] 
**total_contracted_value** | **string** | Total contracted value of the subscription. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


