# PUTAccountType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**additional_email_addresses** | **string[]** | A list of additional email addresses to receive emailed invoices. Use a comma to separate each email address.  **Note:** Invoices are emailed to the email addresses specified in this field only when the &#x60;invoiceDeliveryPrefsEmail&#x60; field is &#x60;true&#x60;. | [optional] 
**auto_pay** | **bool** | Specifies whether future payments are to be automatically billed when they are due. Possible values are: &#x60;true&#x60;, &#x60;false&#x60;. | [optional] 
**batch** | **string** | The alias name given to a batch. A string of 50 characters or less. | [optional] 
**bill_to_contact** | [**\Swagger\Client\Model\PUTAccountTypeBillToContact**](PUTAccountTypeBillToContact.md) |  | [optional] 
**communication_profile_id** | **string** | The ID of a communication profile. | [optional] 
**crm_id** | **string** | CRM account ID for the account, up to 100 characters. | [optional] 
**custom_field__c** | **string** | Any custom fields defined for this object. The custom field name is case-sensitive. | [optional] 
**invoice_delivery_prefs_email** | **bool** | Whether the customer wants to receive invoices through email.   The default value is &#x60;false&#x60;. | [optional] 
**invoice_delivery_prefs_print** | **bool** | Whether the customer wants to receive printed invoices, such as through postal mail.  The default value is &#x60;false&#x60;. | [optional] 
**invoice_template_id** | **string** | Invoice template ID, configured in Billing Settings in the Zuora UI. | [optional] 
**i_dclient__c** | **string** | ID of the v2 account. | [optional] 
**sire_nfacturation__c** | **string** | Registration number. | [optional] 
**codesage__c** | **string** | SAGE code, use by the accounting team. | [optional] 
**engagement__c** | [**\DateTime**](Date.md) | Commitment date. | [optional] 
**legacyid__c** | **string** | Legacy identifier. | [optional] 
**typeversion__c** | **string** | Accoun type, V2 or V3. | [optional] 
**analytique__c** | **string** | Analytics code. | [optional] 
**compteclient__c** | **string** | Accounting code. | [optional] 
**name** | **string** | Account name, up to 255 characters. | [optional] 
**notes** | **string** | A string of up to 65,535 characters. | [optional] 
**payment_gateway** | **string** | The name of the payment gateway instance. If null or left unassigned, the Account will use the Default Gateway. | [optional] 
**sold_to_contact** | [**\Swagger\Client\Model\PUTAccountTypeSoldToContact**](PUTAccountTypeSoldToContact.md) |  | [optional] 
**tagging** | **string** |  | [optional] 
**tax_info** | [**\Swagger\Client\Model\POSTAccountTypeTaxInfo**](POSTAccountTypeTaxInfo.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


