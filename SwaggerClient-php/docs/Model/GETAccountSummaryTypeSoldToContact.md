# GETAccountSummaryTypeSoldToContact

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address1** | **string** | First address line, 255 characters or less. | [optional] 
**address2** | **string** | Second address line, 255 characters or less. | [optional] 
**city** | **string** | City, 40 characters or less. | [optional] 
**country** | **string** | A valid country name or abbreviation | [optional] 
**county** | **string** | County; 32 characters or less. Zuora Tax uses this information to calculate county taxation. | [optional] 
**custom_field__c** | **string** | Any custom fields defined for this object. The custom field name is case-sensitive. | [optional] 
**fax** | **string** | Fax phone number, 40 characters or less. | [optional] 
**first_name** | **string** | First name, 100 characters or less. | [optional] 
**id** | **string** | Contact ID. | [optional] 
**last_name** | **string** | Last name, 100 characters or less. | [optional] 
**state** | **string** | State name or 2-character abbreviation. | [optional] 
**tax_region** | **string** | A region string, defined in your Zuora tax rules. | [optional] 
**work_email** | **string** | Work email address, 80 characters or less. | [optional] 
**work_phone** | **string** | Work phone number, 40 characters or less. | [optional] 
**zip_code** | **string** | Zip code, 20 characters or less. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


