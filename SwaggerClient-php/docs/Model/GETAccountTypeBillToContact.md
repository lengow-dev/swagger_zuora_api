# GETAccountTypeBillToContact

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address1** | **string** | First address line, 255 characters or less. | [optional] 
**address2** | **string** | Second address line, 255 characters or less. | [optional] 
**city** | **string** | City, 40 characters or less. | [optional] 
**country** | **string** | Country name or abbreviation. | [optional] 
**county** | **string** | County; 32 characters or less. Zuora Tax uses this information to calculate county taxation. | [optional] 
**custom_field__c** | **string** | Any custom fields defined for this object. The custom field name is case-sensitive. | [optional] 
**fax** | **string** | Fax phone number, 40 characters or less. | [optional] 
**first_name** | **string** | First name, 100 characters or less. | [optional] 
**home_phone** | **string** | Home phone number, 40 characters or less. | [optional] 
**last_name** | **string** | Last name, 100 characters or less. | [optional] 
**mobile_phone** | **string** | Mobile phone number, 40 characters or less. | [optional] 
**nickname** | **string** | Nickname for this contact. | [optional] 
**other_phone** | **string** | Other phone number, 40 characters or less. | [optional] 
**other_phone_type** | **string** | Possible values are: &#x60;Work&#x60;, &#x60;Mobile&#x60;, &#x60;Home&#x60;, &#x60;Other&#x60;. | [optional] 
**personal_email** | **string** | Personal email address, 80 characters or less. | [optional] 
**state** | **string** | State name or 2-character abbreviation. | [optional] 
**tax_region** | **string** | A region string, defined in your Zuora tax rules. | [optional] 
**work_email** | **string** | Work email address, 80 characters or less. | [optional] 
**work_phone** | **string** | Work phone number, 40 characters or less. | [optional] 
**zip_code** | **string** | Zip code, 20 characters or less. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


