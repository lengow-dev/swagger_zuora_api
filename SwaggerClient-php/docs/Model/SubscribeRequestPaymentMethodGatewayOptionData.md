# SubscribeRequestPaymentMethodGatewayOptionData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**gateway_option** | [**\Swagger\Client\Model\GatewayOption[]**](GatewayOption.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


