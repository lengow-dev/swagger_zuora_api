# GETPaidInvoicesType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**applied_payment_amount** | **string** | Amount of the payment applied to this invoice. | [optional] 
**invoice_id** | **string** | Invoice ID. | [optional] 
**invoice_number** | **string** | Invoice number. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


