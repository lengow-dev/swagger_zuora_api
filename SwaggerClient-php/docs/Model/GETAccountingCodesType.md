# GETAccountingCodesType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accounting_codes** | [**\Swagger\Client\Model\GETAccountingCodeItemWithoutSuccessType[]**](GETAccountingCodeItemWithoutSuccessType.md) | An array of all the accounting codes in your chart of accounts. Each accounting code has the following fields. | [optional] 
**success** | **bool** | Returns &#x60;true&#x60; if the request was processed successfully. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


