# AmendmentRatePlanData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rate_plan** | [**\Swagger\Client\Model\RatePlan**](RatePlan.md) |  | [optional] 
**rate_plan_charge_data** | [**\Swagger\Client\Model\RatePlanChargeData[]**](RatePlanChargeData.md) |  | [optional] 
**subscription_product_feature_list** | [**\Swagger\Client\Model\SubscriptionProductFeatureList**](SubscriptionProductFeatureList.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


