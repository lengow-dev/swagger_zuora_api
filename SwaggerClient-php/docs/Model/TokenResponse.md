# TokenResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **string** | The generated token. | [optional] 
**expires_in** | **float** | The number of seconds until the token expires. | [optional] 
**jti** | **string** | A globally unique identifier for the token. | [optional] 
**scope** | **string** | A space-delimited list of scopes that the token can be used to access. | [optional] 
**token_type** | **string** | The type of token that was generated, i.e., &#x60;bearer&#x60;. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


