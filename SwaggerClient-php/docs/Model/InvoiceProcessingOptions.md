# InvoiceProcessingOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**invoice_date** | [**\DateTime**](Date.md) | The invoice date. | [optional] 
**invoice_target_date** | [**\DateTime**](Date.md) | The date that determines which charges to bill. Charges prior to this date or on this date are billed on the resulting invoices. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


