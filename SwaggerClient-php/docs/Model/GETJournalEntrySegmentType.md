# GETJournalEntrySegmentType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**segment_name** | **string** | Name of segment. | [optional] 
**segment_value** | **string** | Value of segment in this summary journal entry. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


