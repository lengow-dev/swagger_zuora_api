# POSTInvoiceCollectInvoicesType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**invoice_amount** | **string** | Invoice amount. | [optional] 
**invoice_id** | **string** | Invoice ID. | [optional] 
**invoice_number** | **string** | Invoice number. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


