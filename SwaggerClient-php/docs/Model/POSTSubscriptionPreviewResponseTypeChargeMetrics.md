# POSTSubscriptionPreviewResponseTypeChargeMetrics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dmrr** | **string** | Change in monthly recurring revenue. | [optional] 
**dtcv** | **string** | Change in total contract value. | [optional] 
**mrr** | **string** | Monthly recurring revenue. | [optional] 
**number** | **string** | The charge number of the subscription. Only available for update subscription. | [optional] 
**origin_rate_plan_id** | **string** | The origin rate plan ID. Only available for update subscription. | [optional] 
**original_id** | **string** | The original rate plan charge ID. Only available for update subscription. | [optional] 
**product_rate_plan_charge_id** | **string** | The product rate plan charge ID. | [optional] 
**product_rate_plan_id** | **string** | The product rate plan ID. | [optional] 
**tcv** | **string** | Total contract value. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


