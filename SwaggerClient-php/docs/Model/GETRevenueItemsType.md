# GETRevenueItemsType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**revenue_items** | [**\Swagger\Client\Model\GETRevenueItemType[]**](GETRevenueItemType.md) | Revenue items are listed in ascending order by the accounting period start date. | [optional] 
**success** | **bool** | Returns &#x60;true&#x60; if the request was processed successfully. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


