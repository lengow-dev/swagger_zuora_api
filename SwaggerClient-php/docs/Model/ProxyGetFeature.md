# ProxyGetFeature

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_by_id** | **string** | Internal Zuora ID of the user who created the feature **Character limit**: 32 | [optional] 
**created_date** | [**\DateTime**](\DateTime.md) | Date and time when the feature was created **Character limit**: 29 | [optional] 
**description** | **string** | Description of the feature **Character limit**: 1000 | [optional] 
**feature_code** | **string** | Unique code of the feature **Character limit**: 255 | [optional] 
**id** | **string** | Object identifier. | [optional] 
**name** | **string** | Name of the feature **Character limit**: 255 | [optional] 
**status** | **string** | Status of the feature, Active or Inactive **Character limit**: 8 | [optional] 
**updated_by_id** | **string** | Internal Zuora ID of the user who last updated the feature **Character limit**: 32 | [optional] 
**updated_date** | [**\DateTime**](\DateTime.md) | Date and time when the feature was last updated **Character limit**: 29 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


