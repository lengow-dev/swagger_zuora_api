# SubscribeResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_id** | **string** |  | [optional] 
**account_number** | **string** |  | [optional] 
**charge_metrics_data** | [**\Swagger\Client\Model\SubscribeResultChargeMetricsData**](SubscribeResultChargeMetricsData.md) |  | [optional] 
**errors** | [**\Swagger\Client\Model\Error[]**](Error.md) |  | [optional] 
**gateway_response** | **string** |  | [optional] 
**gateway_response_code** | **string** |  | [optional] 
**invoice_data** | [**\Swagger\Client\Model\InvoiceData[]**](InvoiceData.md) |  | [optional] 
**invoice_id** | **string** |  | [optional] 
**invoice_number** | **string** |  | [optional] 
**invoice_result** | [**\Swagger\Client\Model\SubscribeResultInvoiceResult**](SubscribeResultInvoiceResult.md) |  | [optional] 
**payment_id** | **string** |  | [optional] 
**payment_transaction_number** | **string** |  | [optional] 
**subscription_id** | **string** |  | [optional] 
**subscription_number** | **string** |  | [optional] 
**success** | **bool** |  | [optional] 
**total_mrr** | **double** |  | [optional] 
**total_tcv** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


