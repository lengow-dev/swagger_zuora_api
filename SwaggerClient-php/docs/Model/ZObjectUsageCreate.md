# ZObjectUsageCreate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_id** | **string** |  | [optional] 
**charge_number** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**quantity** | **int** |  | [optional] 
**start_date_time** | **string** |  | [optional] 
**subscription_number** | **string** |  | [optional] 
**uom** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


