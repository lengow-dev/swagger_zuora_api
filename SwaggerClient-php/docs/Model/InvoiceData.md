# InvoiceData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**invoice** | [**\Swagger\Client\Model\InvoiceDataInvoice**](InvoiceDataInvoice.md) |  | [optional] 
**invoice_item** | [**\Swagger\Client\Model\InvoiceItem[]**](InvoiceItem.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


