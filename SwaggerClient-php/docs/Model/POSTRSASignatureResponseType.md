# POSTRSASignatureResponseType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **string** | Public key generated for this Payment Page. | [optional] 
**signature** | **string** | Digital signature generated for this Payment Page. | [optional] 
**success** | **bool** | Returns &#x60;true&#x60; if the request was processed successfully. | [optional] 
**tenant_id** | **string** | ID of the Zuora tenant. | [optional] 
**token** | **string** | Token generated for this Payment Page. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


