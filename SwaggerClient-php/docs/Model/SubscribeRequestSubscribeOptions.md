# SubscribeRequestSubscribeOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**apply_credit_balance** | **bool** |  | [optional] 
**electronic_payment_options** | [**\Swagger\Client\Model\SubscribeRequestSubscribeOptionsElectronicPaymentOptions**](SubscribeRequestSubscribeOptionsElectronicPaymentOptions.md) |  | [optional] 
**external_payment_options** | [**\Swagger\Client\Model\SubscribeRequestSubscribeOptionsExternalPaymentOptions**](SubscribeRequestSubscribeOptionsExternalPaymentOptions.md) |  | [optional] 
**generate_invoice** | **bool** |  | [optional] 
**process_payments** | **bool** |  | [optional] 
**subscribe_invoice_processing_options** | [**\Swagger\Client\Model\SubscribeRequestSubscribeOptionsSubscribeInvoiceProcessingOptions**](SubscribeRequestSubscribeOptionsSubscribeInvoiceProcessingOptions.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


