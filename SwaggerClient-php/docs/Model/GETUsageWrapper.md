# GETUsageWrapper

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | **bool** | Returns &#x60;true&#x60; if the request was processed successfully. | [optional] 
**usage** | [**\Swagger\Client\Model\GETUsageType[]**](GETUsageType.md) | Contains one or more usage items. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


