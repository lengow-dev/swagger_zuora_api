# ProxyGetCommunicationProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_by_id** | **string** |  | [optional] 
**created_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**description** | **string** |  | [optional] 
**id** | **string** | Object identifier. | [optional] 
**profile_name** | **string** |  | [optional] 
**updated_by_id** | **string** |  | [optional] 
**updated_date** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


