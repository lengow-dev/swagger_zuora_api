# GETEmailHistoryVOsType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email_histories** | [**\Swagger\Client\Model\GETEmailHistoryVOType[]**](GETEmailHistoryVOType.md) | A container for email histories. | [optional] 
**success** | **bool** | Returns &#x60;true&#x60; if the request was processed successfully. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


