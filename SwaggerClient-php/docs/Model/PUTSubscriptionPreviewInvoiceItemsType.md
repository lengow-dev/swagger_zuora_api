# PUTSubscriptionPreviewInvoiceItemsType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**charge_amount** | **string** | The amount of the charge. This amount doesn&#39;t include taxes unless the charge&#39;s tax mode is inclusive. | [optional] 
**charge_description** | **string** | Description of the charge. | [optional] 
**charge_name** | **string** | Name of the charge | [optional] 
**product_name** | **string** | Name of the product associated with this item. | [optional] 
**product_rate_plan_charge_id** | **string** |  | [optional] 
**quantity** | **string** | Quantity of this item. | [optional] 
**service_end_date** | [**\DateTime**](Date.md) | End date of the service period for this item, i.e., the last day of the period, as yyyy-mm-dd. | [optional] 
**service_start_date** | [**\DateTime**](Date.md) | Service start date as yyyy-mm-dd. If the charge is a one-time fee, this is the date of that charge. | [optional] 
**unit_of_measure** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


