# POSTMassUpdateType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file** | **string** | File containing data about the mass action you want to perform. The file requirements are the same as when uploading a file through the Mass Updater on the Zuora UI.  The file must be a .csv file or a zipped .csv file.   The maximum file size is 4 MB.  The data in the file must be formatted according to the mass action type you want to perform. Refer to the articles listed in the &#x60;actionType&#x60; field below for the requirements of each mass action type. | 
**params** | [**\Swagger\Client\Model\POSTMassUpdateTypeParams**](POSTMassUpdateTypeParams.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


