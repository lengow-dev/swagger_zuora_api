# SubscribeRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account** | [**\Swagger\Client\Model\SubscribeRequestAccount**](SubscribeRequestAccount.md) |  | [optional] 
**bill_to_contact** | [**\Swagger\Client\Model\SubscribeRequestBillToContact**](SubscribeRequestBillToContact.md) |  | [optional] 
**payment_method** | [**\Swagger\Client\Model\SubscribeRequestPaymentMethod**](SubscribeRequestPaymentMethod.md) |  | [optional] 
**preview_options** | [**\Swagger\Client\Model\SubscribeRequestPreviewOptions**](SubscribeRequestPreviewOptions.md) |  | [optional] 
**sold_to_contact** | [**\Swagger\Client\Model\SubscribeRequestSoldToContact**](SubscribeRequestSoldToContact.md) |  | [optional] 
**subscribe_options** | [**\Swagger\Client\Model\SubscribeRequestSubscribeOptions**](SubscribeRequestSubscribeOptions.md) |  | [optional] 
**subscription_data** | [**\Swagger\Client\Model\SubscribeRequestSubscriptionData**](SubscribeRequestSubscriptionData.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


