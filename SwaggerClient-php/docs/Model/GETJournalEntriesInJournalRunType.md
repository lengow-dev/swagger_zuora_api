# GETJournalEntriesInJournalRunType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**journal_entries** | [**\Swagger\Client\Model\GETJournalEntryDetailTypeWithoutSuccess[]**](GETJournalEntryDetailTypeWithoutSuccess.md) | Key name that represents the list of journal entries. | [optional] 
**success** | **bool** | Returns &#x60;true&#x60; if the request was processed successfully. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


