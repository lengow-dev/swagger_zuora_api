# AmendResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amendment_ids** | **string** | The IDs of the associated Amendment object. There can be as many as three AmendmentId values. | [optional] 
**charge_metrics_data** | [**\Swagger\Client\Model\ChargeMetricsData**](ChargeMetricsData.md) |  | [optional] 
**errors** | [**\Swagger\Client\Model\Error[]**](Error.md) |  | [optional] 
**gateway_response** | **string** |  | [optional] 
**gateway_response_code** | **string** |  | [optional] 
**invoice_datas** | [**\Swagger\Client\Model\InvoiceData[]**](InvoiceData.md) |  | [optional] 
**invoice_id** | **string** |  | [optional] 
**payment_id** | **string** |  | [optional] 
**payment_transaction_number** | **string** |  | [optional] 
**subscription_id** | **string** |  | [optional] 
**success** | **bool** |  | [optional] 
**total_delta_mrr** | **double** |  | [optional] 
**total_delta_tcv** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


